/*
Navicat MySQL Data Transfer

Source Server         : 1
Source Server Version : 50560
Source Host           : localhost:3306
Source Database       : rmt_zx

Target Server Type    : MYSQL
Target Server Version : 50560
File Encoding         : 65001

Date: 2020-04-18 16:54:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for advise
-- ----------------------------
DROP TABLE IF EXISTS `advise`;
CREATE TABLE `advise` (
  `id` int(11) NOT NULL,
  `advisors` varchar(255) DEFAULT NULL COMMENT '建议人',
  `picture` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advise
-- ----------------------------

-- ----------------------------
-- Table structure for advisory
-- ----------------------------
DROP TABLE IF EXISTS `advisory`;
CREATE TABLE `advisory` (
  `id` int(11) NOT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of advisory
-- ----------------------------

-- ----------------------------
-- Table structure for announcement
-- ----------------------------
DROP TABLE IF EXISTS `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext,
  `editor` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` int(10) DEFAULT '0',
  `dz_count` int(10) DEFAULT '0',
  `zf_count` int(10) DEFAULT '0',
  `pl_count` int(10) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of announcement
-- ----------------------------
INSERT INTO `announcement` VALUES ('1', '1', '1', '1', '2020-01-02 01:26:37', '1', '1', '11', '1', '1', '1');

-- ----------------------------
-- Table structure for announcement_fb
-- ----------------------------
DROP TABLE IF EXISTS `announcement_fb`;
CREATE TABLE `announcement_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '正文内容',
  `editor` varchar(50) DEFAULT NULL COMMENT '编辑人',
  `picture` varchar(255) DEFAULT NULL COMMENT '预览图',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `code` int(10) DEFAULT '0' COMMENT '状态1审核0未审核',
  `dz_count` int(10) DEFAULT '0' COMMENT '点赞数',
  `zf_count` int(10) DEFAULT '0' COMMENT '转发数',
  `pl_count` int(10) DEFAULT '0' COMMENT '评论数',
  `uid` int(10) DEFAULT NULL COMMENT '修改人',
  `category` varchar(255) DEFAULT '公告' COMMENT '分类',
  `status` int(10) DEFAULT '0' COMMENT '状态',
  `pre_url` varchar(255) DEFAULT NULL COMMENT '图片前缀',
  `abs_url` varchar(255) DEFAULT NULL COMMENT '网站图片绝对路径',
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of announcement_fb
-- ----------------------------
INSERT INTO `announcement_fb` VALUES ('1', '1', '1', '1', '2020-02-07 12:27:03', null, '1', '1', '1', '1', '1', '公告', '1', '1', '1', '1', '11');

-- ----------------------------
-- Table structure for circular_economy
-- ----------------------------
DROP TABLE IF EXISTS `circular_economy`;
CREATE TABLE `circular_economy` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of circular_economy
-- ----------------------------

-- ----------------------------
-- Table structure for cj_paper
-- ----------------------------
DROP TABLE IF EXISTS `cj_paper`;
CREATE TABLE `cj_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(32) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `author` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `article_sources` text COMMENT '文章内容',
  `news_column` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `picture` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cj_paper
-- ----------------------------

-- ----------------------------
-- Table structure for class_table
-- ----------------------------
DROP TABLE IF EXISTS `class_table`;
CREATE TABLE `class_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `large_category` varchar(255) DEFAULT NULL COMMENT '大类别',
  `small_category` varchar(255) DEFAULT NULL COMMENT '预览图',
  `y_l1` varchar(255) DEFAULT NULL,
  `y_l2` varchar(255) DEFAULT NULL,
  `y_l3` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_table
-- ----------------------------

-- ----------------------------
-- Table structure for company_content
-- ----------------------------
DROP TABLE IF EXISTS `company_content`;
CREATE TABLE `company_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` longtext COMMENT '内容',
  `time` varchar(255) DEFAULT NULL COMMENT '编辑时间',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `code` int(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `admin_id` varchar(255) DEFAULT NULL COMMENT '管理员账号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_content
-- ----------------------------
INSERT INTO `company_content` VALUES ('1', '超级管理员', null, '2020-02-05', '20200205/1f180474da4d487bbd8fae2eecd53f1a.jpg', '1', '0', 'wangjn');
INSERT INTO `company_content` VALUES ('2', '管理员', null, '2020-02-05', '20200205/eaad8de11b4549118495af6979b33ded.jpg', '1', '0', 'zdgwh');
INSERT INTO `company_content` VALUES ('3', '超级管理员', '<p>123213</p>', '2020-02-05', 'http://36.189.242.73:8084/uploads/', '1', '0', 'wangjn');
INSERT INTO `company_content` VALUES ('4', '管理员', '<p>123213</p>', '2020-02-05', 'http://36.189.242.73:8084/uploads/', '1', '1', 'zdgwh');

-- ----------------------------
-- Table structure for company_description
-- ----------------------------
DROP TABLE IF EXISTS `company_description`;
CREATE TABLE `company_description` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) DEFAULT NULL COMMENT '职位',
  `place` varchar(255) DEFAULT NULL COMMENT '地点',
  `experience` varchar(255) DEFAULT NULL COMMENT '经验',
  `education` varchar(255) DEFAULT NULL COMMENT '学历',
  `part_time` varchar(255) DEFAULT NULL COMMENT '全职、兼职',
  `expectation` varchar(255) DEFAULT NULL COMMENT '期望薪资',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `release_id` varchar(255) DEFAULT NULL COMMENT '发布公司',
  `bright` varchar(255) DEFAULT NULL COMMENT '职位亮点',
  `description` varchar(255) DEFAULT NULL COMMENT '职位描述',
  `cid` int(11) DEFAULT NULL COMMENT '关联公司id',
  `code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_description
-- ----------------------------
INSERT INTO `company_description` VALUES ('1', '', '', '', '', '', '', '2020-02-05', '超级管理员', '', '', null, '1');

-- ----------------------------
-- Table structure for company_id
-- ----------------------------
DROP TABLE IF EXISTS `company_id`;
CREATE TABLE `company_id` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gongsi_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `zhiwei_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of company_id
-- ----------------------------

-- ----------------------------
-- Table structure for complaint
-- ----------------------------
DROP TABLE IF EXISTS `complaint`;
CREATE TABLE `complaint` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `complainant` varchar(255) DEFAULT NULL COMMENT '投诉人',
  `department` varchar(255) DEFAULT NULL COMMENT '投诉部门',
  `user_mail` varchar(255) DEFAULT NULL COMMENT '用户邮箱',
  `contents` varchar(255) DEFAULT NULL COMMENT '投诉内容',
  `complaint_time` varchar(255) DEFAULT NULL COMMENT '投诉时间',
  `feedback_contents` varchar(255) DEFAULT NULL COMMENT '回馈内容',
  `feedback_time` varchar(255) DEFAULT NULL COMMENT '回馈时间',
  `feedback_department` varchar(255) DEFAULT NULL COMMENT '回馈部门',
  `code` int(255) DEFAULT NULL COMMENT '处理状态',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id唯一',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of complaint
-- ----------------------------
INSERT INTO `complaint` VALUES ('1', '15193869953', '财务', '1111', '测试投诉', '2019.12.26', '1', '1', '1', '1', '44', '1', '投诉');
INSERT INTO `complaint` VALUES ('2', '15193869953', '部门', '111', '测试投诉', '2019.12.22', '', '', '', null, '44', '', '投诉');
INSERT INTO `complaint` VALUES ('3', '15193869953', '财务部', '投诉xxxx', '测试内容', '2020.1.4', '', '', '', null, '1', '825644945@qq.com', '投诉');
INSERT INTO `complaint` VALUES ('4', '15193869953', '财务部', '825644945@qq.com', '测试内容', '2020.1.4', '', '', '', null, '1', '投诉xxxx', '投诉');
INSERT INTO `complaint` VALUES ('5', '18388888888', '党政办', '1868555554555@136.com', '今天天气不错，哈哈哈哈哈哈', '2020-01-04', '', '', '', null, '44', '投诉测试', '投诉');
INSERT INTO `complaint` VALUES ('6', '18282828282', '党政办', 'Kaja', 'Kaja', '2020-01-07', '', '', '', null, '45', 'Kaja', '咨询');
INSERT INTO `complaint` VALUES ('7', '15191455644', '水务局', '595530057@qq.com', '水质问题', '2020-01-13', '', '', '', null, '47', '水质安全', '咨询');
INSERT INTO `complaint` VALUES ('8', '15594192506', '国土规划局', '1215687603@qq.com', '准东经济开发区的发展前景怎么样', '2020-02-06', '', '', '', null, '48', '准东经济开发区的发展前景', '咨询');
INSERT INTO `complaint` VALUES ('9', '18388888888', '党工委', '18888888888@.163.com', '你好', '2020-03-30', '', '', '', null, '44', '你好', '咨询');
INSERT INTO `complaint` VALUES ('10', '18389898899', '党工委', '18389898899', '测试', '2020-03-31', '', '', '', null, '44', '测试', '咨询');
INSERT INTO `complaint` VALUES ('11', '18080808800', '党工委', '123456', '456', '2020-03-31', '', '', '', null, '44', '123', '投诉');
INSERT INTO `complaint` VALUES ('12', '18282828822', '党工委', '45858582', '8686868', '2020-03-31', '', '', '', null, '44', '858582', '建议');

-- ----------------------------
-- Table structure for complaints
-- ----------------------------
DROP TABLE IF EXISTS `complaints`;
CREATE TABLE `complaints` (
  `id` int(11) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of complaints
-- ----------------------------

-- ----------------------------
-- Table structure for cycj_detail_fb
-- ----------------------------
DROP TABLE IF EXISTS `cycj_detail_fb`;
CREATE TABLE `cycj_detail_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `article` longtext,
  `cj_time` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `changeid` int(11) DEFAULT NULL,
  `code` int(3) DEFAULT NULL,
  `shid` int(3) DEFAULT NULL,
  `yl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cycj_detail_fb
-- ----------------------------

-- ----------------------------
-- Table structure for economic_information
-- ----------------------------
DROP TABLE IF EXISTS `economic_information`;
CREATE TABLE `economic_information` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of economic_information
-- ----------------------------

-- ----------------------------
-- Table structure for energy_tj
-- ----------------------------
DROP TABLE IF EXISTS `energy_tj`;
CREATE TABLE `energy_tj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `mt_energy` int(255) DEFAULT NULL,
  `ny_energy` int(11) DEFAULT NULL,
  `sy_energy` int(11) DEFAULT NULL,
  `dl_energy` int(11) DEFAULT NULL,
  `trq_energy` int(11) DEFAULT NULL,
  `zong_energy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of energy_tj
-- ----------------------------
INSERT INTO `energy_tj` VALUES ('1', '星期一', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('2', '星期二', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('3', '星期三', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('4', '星期四', '12', '565', '6', '464', '46', '0');
INSERT INTO `energy_tj` VALUES ('5', '星期五', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('6', '星期六', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('7', '星期天', '0', '0', '0', '0', '0', '0');
INSERT INTO `energy_tj` VALUES ('8', '本周总数', '21', '789', '457', '18', '53', '0');
INSERT INTO `energy_tj` VALUES ('9', '总数据', '7757', '11843', '39054', '1010', '16126', '75790');

-- ----------------------------
-- Table structure for enterprise_emission_management
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_emission_management`;
CREATE TABLE `enterprise_emission_management` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_emission_management
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_profile
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_profile`;
CREATE TABLE `enterprise_profile` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_profile
-- ----------------------------

-- ----------------------------
-- Table structure for enterprise_recruitment
-- ----------------------------
DROP TABLE IF EXISTS `enterprise_recruitment`;
CREATE TABLE `enterprise_recruitment` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of enterprise_recruitment
-- ----------------------------

-- ----------------------------
-- Table structure for environmental_protection
-- ----------------------------
DROP TABLE IF EXISTS `environmental_protection`;
CREATE TABLE `environmental_protection` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of environmental_protection
-- ----------------------------

-- ----------------------------
-- Table structure for excel_form
-- ----------------------------
DROP TABLE IF EXISTS `excel_form`;
CREATE TABLE `excel_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_department` varchar(255) DEFAULT NULL COMMENT '部门',
  `full_name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `full_post` varchar(255) DEFAULT NULL COMMENT '岗位',
  `full_identity` varchar(255) DEFAULT NULL COMMENT '身份',
  `full_leave_time` varchar(255) DEFAULT NULL COMMENT '请假时间',
  `full_ taking_leave` varchar(255) DEFAULT NULL COMMENT '请假原因',
  `
working_hours` varchar(255) DEFAULT NULL COMMENT '工作时间',
  `in_work` varchar(255) DEFAULT NULL COMMENT '入职时间',
  `leave_time` varchar(255) DEFAULT NULL COMMENT '年假时间',
  `accumulated_time` varchar(255) DEFAULT NULL COMMENT '累计请假',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of excel_form
-- ----------------------------

-- ----------------------------
-- Table structure for expert_library
-- ----------------------------
DROP TABLE IF EXISTS `expert_library`;
CREATE TABLE `expert_library` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `experts_category` varchar(255) DEFAULT NULL COMMENT '专家类别',
  `categories` varchar(255) DEFAULT NULL COMMENT '大专家类别',
  `pid` int(11) DEFAULT NULL COMMENT '分类id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of expert_library
-- ----------------------------

-- ----------------------------
-- Table structure for expert_library_name
-- ----------------------------
DROP TABLE IF EXISTS `expert_library_name`;
CREATE TABLE `expert_library_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `expert_name` varchar(255) DEFAULT NULL COMMENT '专家姓名',
  `expert_unit` varchar(255) DEFAULT NULL COMMENT '专家单位',
  `expert_post` varchar(255) DEFAULT NULL COMMENT '职务',
  `expert_title` varchar(255) DEFAULT NULL COMMENT '职称',
  `expert_field` varchar(255) DEFAULT NULL COMMENT '领域',
  `expert_brief` varchar(255) DEFAULT NULL COMMENT '简介',
  `cid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of expert_library_name
-- ----------------------------

-- ----------------------------
-- Table structure for government_affairs
-- ----------------------------
DROP TABLE IF EXISTS `government_affairs`;
CREATE TABLE `government_affairs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext,
  `editor` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` int(3) DEFAULT '0',
  `dz_count` int(10) DEFAULT '0',
  `zf_count` int(10) DEFAULT '0',
  `pl_count` int(10) DEFAULT '0',
  `uid` int(10) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_affairs
-- ----------------------------

-- ----------------------------
-- Table structure for government_affairs_fb
-- ----------------------------
DROP TABLE IF EXISTS `government_affairs_fb`;
CREATE TABLE `government_affairs_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '正文内容',
  `editor` varchar(50) DEFAULT NULL COMMENT '编辑人',
  `picture` varchar(255) DEFAULT NULL COMMENT '预览图片',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `code` int(3) DEFAULT '0' COMMENT '状态1审核0未审核',
  `dz_count` int(10) DEFAULT '0' COMMENT '点赞数',
  `zf_count` int(10) DEFAULT '0' COMMENT '转发数',
  `pl_count` int(10) DEFAULT '0' COMMENT '评论数',
  `uid` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT '政务' COMMENT '分类',
  `status` int(10) DEFAULT '0',
  `pre_url` varchar(255) DEFAULT NULL COMMENT '图片前缀',
  `abs_url` varchar(255) DEFAULT NULL COMMENT '网站图片绝对路径',
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of government_affairs_fb
-- ----------------------------
INSERT INTO `government_affairs_fb` VALUES ('1', '1', '1', '1', '2020-03-05 12:27:33', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', null);

-- ----------------------------
-- Table structure for head_line
-- ----------------------------
DROP TABLE IF EXISTS `head_line`;
CREATE TABLE `head_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article` longtext COMMENT '文章内容',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `code` int(255) unsigned zerofill DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL COMMENT '来源',
  `differentiate` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of head_line
-- ----------------------------
INSERT INTO `head_line` VALUES ('1', '1', '1', '1', '1', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '1', '1', '1');

-- ----------------------------
-- Table structure for host_tweets
-- ----------------------------
DROP TABLE IF EXISTS `host_tweets`;
CREATE TABLE `host_tweets` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `created_at` varchar(255) DEFAULT NULL COMMENT '微博发表时间',
  `like_num` int(11) DEFAULT NULL COMMENT '点赞数',
  `repost_num` int(11) DEFAULT NULL COMMENT '转发数',
  `comment_num` int(11) DEFAULT NULL COMMENT '评论数',
  `content` varchar(255) DEFAULT NULL COMMENT '微博内容',
  `user_id` varchar(255) DEFAULT NULL COMMENT '发表该微博用户的id',
  `tool` varchar(255) DEFAULT NULL COMMENT '发布微博的工具',
  `image_url` varchar(255) DEFAULT NULL COMMENT '图片',
  `video_url` varchar(255) DEFAULT NULL COMMENT '视频',
  `location` varchar(255) DEFAULT NULL COMMENT '定位信息',
  `origin_weibo` varchar(255) DEFAULT NULL COMMENT '原始微博',
  `crawled_at` varchar(255) DEFAULT NULL COMMENT '抓取时间戳',
  `weibo_url` varchar(255) DEFAULT NULL COMMENT '微博地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of host_tweets
-- ----------------------------

-- ----------------------------
-- Table structure for institutional_training
-- ----------------------------
DROP TABLE IF EXISTS `institutional_training`;
CREATE TABLE `institutional_training` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT NULL COMMENT '班名',
  `class_begins` varchar(255) DEFAULT NULL COMMENT '开课时间',
  `organization_name` varchar(255) DEFAULT NULL,
  `registration_time` varchar(255) DEFAULT NULL COMMENT '报名时间',
  `class_cost` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `entry_requirements` varchar(255) DEFAULT NULL COMMENT '报名条件',
  `contact_information` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `company_introduction` varchar(255) DEFAULT NULL COMMENT '公司介绍',
  `code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of institutional_training
-- ----------------------------

-- ----------------------------
-- Table structure for investment_information
-- ----------------------------
DROP TABLE IF EXISTS `investment_information`;
CREATE TABLE `investment_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `investment_user` varchar(255) DEFAULT NULL,
  `investment_tel` varchar(255) DEFAULT NULL,
  `investment_mail` varchar(255) DEFAULT NULL,
  `investment_wx` varchar(255) DEFAULT NULL,
  `investment_bz` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of investment_information
-- ----------------------------

-- ----------------------------
-- Table structure for investment_invitation
-- ----------------------------
DROP TABLE IF EXISTS `investment_invitation`;
CREATE TABLE `investment_invitation` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of investment_invitation
-- ----------------------------

-- ----------------------------
-- Table structure for investment_policy
-- ----------------------------
DROP TABLE IF EXISTS `investment_policy`;
CREATE TABLE `investment_policy` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `policy_column` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of investment_policy
-- ----------------------------

-- ----------------------------
-- Table structure for key_industrial
-- ----------------------------
DROP TABLE IF EXISTS `key_industrial`;
CREATE TABLE `key_industrial` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `policy_column` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of key_industrial
-- ----------------------------

-- ----------------------------
-- Table structure for ljzd_detail_fb
-- ----------------------------
DROP TABLE IF EXISTS `ljzd_detail_fb`;
CREATE TABLE `ljzd_detail_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article` longtext COMMENT '内容',
  `cj_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `picture` varchar(255) DEFAULT NULL COMMENT '预览图',
  `end_time` varchar(255) DEFAULT NULL,
  `changeid` int(11) DEFAULT NULL,
  `code` int(3) DEFAULT '0',
  `shid` int(3) DEFAULT NULL,
  `yl` varchar(255) DEFAULT NULL,
  `bzcode` int(10) DEFAULT '0',
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ljzd_detail_fb
-- ----------------------------

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of log
-- ----------------------------

-- ----------------------------
-- Table structure for m3u8
-- ----------------------------
DROP TABLE IF EXISTS `m3u8`;
CREATE TABLE `m3u8` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
  `name` varchar(255) DEFAULT NULL COMMENT '电视频道名',
  `m3u8` varchar(255) DEFAULT NULL COMMENT 'm3u8连接地址',
  `re_time` varchar(255) DEFAULT NULL COMMENT '修改时间',
  `uid` int(10) DEFAULT NULL COMMENT '修改人id',
  `comment` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of m3u8
-- ----------------------------
INSERT INTO `m3u8` VALUES ('1', '中央1套', '请31212312', '2020-02-09', null, '大撒大撒');

-- ----------------------------
-- Table structure for market_dynamics
-- ----------------------------
DROP TABLE IF EXISTS `market_dynamics`;
CREATE TABLE `market_dynamics` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `policy_column` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of market_dynamics
-- ----------------------------

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `icon` varchar(32) DEFAULT NULL,
  `remarks` int(11) DEFAULT NULL,
  `p_id` varchar(128) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menu
-- ----------------------------

-- ----------------------------
-- Table structure for micro_video
-- ----------------------------
DROP TABLE IF EXISTS `micro_video`;
CREATE TABLE `micro_video` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of micro_video
-- ----------------------------

-- ----------------------------
-- Table structure for mp3_table
-- ----------------------------
DROP TABLE IF EXISTS `mp3_table`;
CREATE TABLE `mp3_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT '准东之声',
  `up_time` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `mp3_url` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mp3_table
-- ----------------------------

-- ----------------------------
-- Table structure for mylibrary
-- ----------------------------
DROP TABLE IF EXISTS `mylibrary`;
CREATE TABLE `mylibrary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT NULL,
  `d_z` int(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mylibrary
-- ----------------------------
INSERT INTO `mylibrary` VALUES ('1', '准东新闻', null, '', null, '2020-03-23', null, '', '', null, 'http://36.189.242.73:8084/uploads/', '1', null, '中央新闻', '0');
INSERT INTO `mylibrary` VALUES ('2', '准东新闻', null, '好', null, '2020-03-23', null, '', '好', null, 'http://36.189.242.73:8084/uploads/20200323/d87e9c1fe8a34b8ab0d754cc56f645c2.jpg', '1', null, '中央新闻', '0');
INSERT INTO `mylibrary` VALUES ('3', '准东新闻', null, ' 阿萨', null, '2020-03-23', null, '<p>阿萨达叔是</p>', '的发', null, 'http://36.189.242.73:8084/uploads/20200323/191f955055124a63a31b6fcc0255de49.jpg', '1', null, '准东新闻', '0');

-- ----------------------------
-- Table structure for nengyuan
-- ----------------------------
DROP TABLE IF EXISTS `nengyuan`;
CREATE TABLE `nengyuan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) DEFAULT NULL COMMENT '标题',
  `release_time` varchar(200) DEFAULT NULL COMMENT '发布时间',
  `source` varchar(200) DEFAULT NULL COMMENT '来源',
  `news_column` varchar(200) DEFAULT NULL COMMENT '专栏',
  `article_source` varchar(200) DEFAULT NULL COMMENT '文章来源',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(200) DEFAULT NULL COMMENT '编辑',
  `abs_url` varchar(500) DEFAULT NULL COMMENT '拼接图片相对路径的url',
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of nengyuan
-- ----------------------------

-- ----------------------------
-- Table structure for newspaper
-- ----------------------------
DROP TABLE IF EXISTS `newspaper`;
CREATE TABLE `newspaper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_h3` varchar(500) DEFAULT NULL COMMENT '标题_h3',
  `title_h1` varchar(500) DEFAULT NULL COMMENT '标题_h1',
  `title_h2` varchar(500) DEFAULT NULL COMMENT '标题_h2',
  `title_h4` varchar(500) DEFAULT NULL COMMENT '标题_h4',
  `source` varchar(255) DEFAULT NULL COMMENT '报纸来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `news_article` longtext COMMENT '报纸内容',
  `picture_url` varchar(500) DEFAULT NULL COMMENT '每版报纸首页图片地址',
  `d_z` int(11) DEFAULT '0' COMMENT '点赞量',
  `code` int(10) DEFAULT '0',
  `abs_url` varchar(255) DEFAULT NULL,
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  `status` int(10) DEFAULT '0',
  `differentiate` int(255) DEFAULT NULL,
  `local_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newspaper
-- ----------------------------
INSERT INTO `newspaper` VALUES ('1', '1', '1', '1', '1', '1', '1', '	1	1', '1', '1', '0', '1', '2020-03-19 17:35:59', '1', '1', '1');
INSERT INTO `newspaper` VALUES ('2', '2', '2', '2', '2', '2', '2', null, '2', '2', '0', '2', '2020-03-19 17:47:28', '1', '1', '1');
INSERT INTO `newspaper` VALUES ('3', '1', '1', '1', '1', '1', '1', '11', '1', '1', '0', '1', '2020-03-19 17:47:46', '1', '1', '1');

-- ----------------------------
-- Table structure for newspaper_fb
-- ----------------------------
DROP TABLE IF EXISTS `newspaper_fb`;
CREATE TABLE `newspaper_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_h3` varchar(500) DEFAULT NULL COMMENT '标题_h3',
  `title_h1` varchar(500) DEFAULT NULL COMMENT '标题_h1',
  `title_h2` varchar(500) DEFAULT NULL COMMENT '标题_h2',
  `title_h4` varchar(500) DEFAULT NULL COMMENT '标题_h4',
  `source` varchar(255) DEFAULT NULL COMMENT '报纸来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `news_article` longtext COMMENT '报纸内容',
  `picture_url` varchar(500) DEFAULT NULL COMMENT '每版报纸首页图片地址',
  `d_z` int(11) DEFAULT '0' COMMENT '点赞量',
  `code` int(10) DEFAULT '0',
  `abs_url` varchar(255) DEFAULT NULL,
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  `status` int(10) DEFAULT '0',
  `differentiate` int(255) DEFAULT NULL,
  `local_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of newspaper_fb
-- ----------------------------
INSERT INTO `newspaper_fb` VALUES ('1', null, '1', null, null, '1', '2020-03-19', '<p>1	1</p>', '1', '0', '1', null, null, '0', null, null);

-- ----------------------------
-- Table structure for order_comment
-- ----------------------------
DROP TABLE IF EXISTS `order_comment`;
CREATE TABLE `order_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(255) DEFAULT NULL,
  `order_star` varchar(255) DEFAULT NULL,
  `order_picture` varchar(255) DEFAULT NULL,
  `order_comment` varchar(255) DEFAULT NULL,
  `order_comment_time` varchar(255) DEFAULT NULL,
  `order_reply` varchar(255) DEFAULT NULL,
  `order_reply_time` varchar(255) DEFAULT NULL,
  `order_reply_name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `reserve_one` varchar(255) DEFAULT NULL,
  `reserve_two` varchar(255) DEFAULT NULL,
  `reserve_three` varchar(255) DEFAULT NULL,
  `reserve_four` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of order_comment
-- ----------------------------

-- ----------------------------
-- Table structure for others_news
-- ----------------------------
DROP TABLE IF EXISTS `others_news`;
CREATE TABLE `others_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(765) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(765) DEFAULT NULL COMMENT '专栏',
  `title` varchar(765) DEFAULT NULL COMMENT '文章标题',
  `release_time` varchar(1000) DEFAULT NULL COMMENT '发布时间',
  `article` text COMMENT '文章内容',
  `picture_url` varchar(765) DEFAULT NULL COMMENT '预览图图片',
  `article_source` varchar(765) DEFAULT NULL COMMENT '文章来源',
  `editor` varchar(765) DEFAULT NULL COMMENT '编辑',
  `abs_url` varchar(4000) DEFAULT NULL COMMENT '用于拼接相对路径图片的绝对路径地址',
  `local_url` varchar(4000) DEFAULT NULL COMMENT '本地url',
  `code` int(11) DEFAULT '0' COMMENT '是否发布，默认0',
  `d_z` int(11) DEFAULT '0' COMMENT '是否点赞，默认为空',
  `zzqCode` int(1) DEFAULT '0' COMMENT '是否发布到自治区，默认为0',
  `zyCode` int(1) DEFAULT '0' COMMENT '是否发布到中央新闻，默认为0',
  `zdCode` int(1) DEFAULT '0' COMMENT '是否发布到准东新闻，默认为0',
  `zCode` varchar(765) DEFAULT NULL COMMENT '是否发布到州新闻，默认为0',
  `category` varchar(765) DEFAULT NULL COMMENT '新闻类别，默认为空',
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  `status` int(10) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of others_news
-- ----------------------------

-- ----------------------------
-- Table structure for party_work
-- ----------------------------
DROP TABLE IF EXISTS `party_work`;
CREATE TABLE `party_work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext,
  `editor` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` int(2) NOT NULL DEFAULT '0',
  `dz_count` int(10) DEFAULT '0',
  `zf_count` int(10) DEFAULT '0',
  `pl_count` int(10) DEFAULT '0',
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of party_work
-- ----------------------------

-- ----------------------------
-- Table structure for party_work_fb
-- ----------------------------
DROP TABLE IF EXISTS `party_work_fb`;
CREATE TABLE `party_work_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '正文内容',
  `editor` varchar(50) DEFAULT NULL COMMENT '编辑人',
  `picture` varchar(255) DEFAULT NULL COMMENT '预览图',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `code` int(2) NOT NULL DEFAULT '0' COMMENT '状态1审核0未审核',
  `dz_count` int(10) DEFAULT '0' COMMENT '点赞数',
  `zf_count` int(10) DEFAULT '0' COMMENT '转发数',
  `pl_count` int(10) DEFAULT '0' COMMENT '评论数',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `category` varchar(255) DEFAULT '党务' COMMENT '分类',
  `pre_url` varchar(255) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of party_work_fb
-- ----------------------------
INSERT INTO `party_work_fb` VALUES ('1', '1', '1', '1', '2020-01-10 12:28:09', '1', '1', '1', '1', '1', '11', '0', null, null, null, '0', null);

-- ----------------------------
-- Table structure for people_paper
-- ----------------------------
DROP TABLE IF EXISTS `people_paper`;
CREATE TABLE `people_paper` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article` text COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of people_paper
-- ----------------------------

-- ----------------------------
-- Table structure for picture_zd
-- ----------------------------
DROP TABLE IF EXISTS `picture_zd`;
CREATE TABLE `picture_zd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `author` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `descripe` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `news_column` text COMMENT '文章内容',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '编辑人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of picture_zd
-- ----------------------------

-- ----------------------------
-- Table structure for policy
-- ----------------------------
DROP TABLE IF EXISTS `policy`;
CREATE TABLE `policy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext,
  `editor` varchar(50) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` int(10) DEFAULT '0',
  `dz_count` int(10) DEFAULT '0',
  `zf_count` int(10) DEFAULT '0',
  `pl_count` int(10) DEFAULT '0',
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of policy
-- ----------------------------

-- ----------------------------
-- Table structure for policy_fb
-- ----------------------------
DROP TABLE IF EXISTS `policy_fb`;
CREATE TABLE `policy_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '正文内容',
  `editor` varchar(50) DEFAULT NULL COMMENT '编辑人',
  `picture` varchar(255) DEFAULT NULL COMMENT '预览图',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `code` int(10) DEFAULT '0' COMMENT '状态1审核0未审核',
  `dz_count` int(10) DEFAULT '0' COMMENT '点赞数',
  `zf_count` int(10) DEFAULT '0' COMMENT '转发数',
  `pl_count` int(10) DEFAULT '0' COMMENT '评论数',
  `uid` int(10) DEFAULT '0',
  `status` int(10) DEFAULT '0',
  `category` varchar(255) DEFAULT '政策' COMMENT '分类',
  `pre_url` varchar(255) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of policy_fb
-- ----------------------------
INSERT INTO `policy_fb` VALUES ('1', '1', '1', '1', '2020-02-01 12:28:38', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for praise
-- ----------------------------
DROP TABLE IF EXISTS `praise`;
CREATE TABLE `praise` (
  `id` int(11) NOT NULL,
  `journalism_id` int(11) DEFAULT NULL COMMENT '评论新闻id',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '新闻评论',
  `commentator` varchar(255) DEFAULT NULL COMMENT '评论人',
  `commentary_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `praise_people` varchar(255) DEFAULT NULL COMMENT '评论点赞人',
  `reserved_fields` varchar(255) DEFAULT NULL COMMENT '预留字段'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of praise
-- ----------------------------

-- ----------------------------
-- Table structure for prize_peport
-- ----------------------------
DROP TABLE IF EXISTS `prize_peport`;
CREATE TABLE `prize_peport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `report_content` varchar(255) DEFAULT NULL COMMENT '内容',
  `report_picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `report_tel` varchar(255) DEFAULT NULL COMMENT '手机号',
  `report_wx` varchar(255) DEFAULT NULL,
  `report_time` varchar(255) DEFAULT NULL,
  `report_examine` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of prize_peport
-- ----------------------------
INSERT INTO `prize_peport` VALUES ('1', '投诉内容1', '照片', '手机号', '微信', '时间', '2');
INSERT INTO `prize_peport` VALUES ('2', '投诉内容2', '照片', '手机号', '微信', '时间', '2');
INSERT INTO `prize_peport` VALUES ('3', '投诉内容3', '照片', '手机号', '微信', '时间', '2');
INSERT INTO `prize_peport` VALUES ('4', '投诉内容4', '照片', '手机号', '微信', '时间', '1');
INSERT INTO `prize_peport` VALUES ('5', '投诉内容5', '照片', '手机号', '微信', '时间', '1');
INSERT INTO `prize_peport` VALUES ('6', '投诉内容6', '照片', '手机号', '微信', '时间', '0');
INSERT INTO `prize_peport` VALUES ('7', '1', '照片', '手机号', '微信', '时间', '1');

-- ----------------------------
-- Table structure for production_logistics
-- ----------------------------
DROP TABLE IF EXISTS `production_logistics`;
CREATE TABLE `production_logistics` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of production_logistics
-- ----------------------------

-- ----------------------------
-- Table structure for production_service
-- ----------------------------
DROP TABLE IF EXISTS `production_service`;
CREATE TABLE `production_service` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of production_service
-- ----------------------------

-- ----------------------------
-- Table structure for project_book
-- ----------------------------
DROP TABLE IF EXISTS `project_book`;
CREATE TABLE `project_book` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_book
-- ----------------------------

-- ----------------------------
-- Table structure for result_show
-- ----------------------------
DROP TABLE IF EXISTS `result_show`;
CREATE TABLE `result_show` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of result_show
-- ----------------------------

-- ----------------------------
-- Table structure for re_phone
-- ----------------------------
DROP TABLE IF EXISTS `re_phone`;
CREATE TABLE `re_phone` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键自增id',
  `bmName` varchar(255) DEFAULT NULL COMMENT '单位名',
  `phone` varchar(255) DEFAULT NULL COMMENT '电话',
  `code` int(3) DEFAULT NULL COMMENT '状态0未启用1启用',
  `comment` varchar(255) DEFAULT NULL COMMENT '备注',
  `txTime` varchar(255) DEFAULT NULL COMMENT '服务时间',
  `jzrTime` varchar(255) DEFAULT NULL COMMENT '节假日服务时间',
  `pid` int(10) DEFAULT NULL COMMENT '父级分类id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of re_phone
-- ----------------------------
INSERT INTO `re_phone` VALUES ('4', '准东管委会', '18700388330', '1', '上午9:00--12:00，\r\n下午13：00--18：00', '上午9:00--12:00，\r\n下午13：00--18：00', '上午9:00--12:00，\r\n下午13：00--18：00', '0');
INSERT INTO `re_phone` VALUES ('5', '准东管委会', '18700388330；0913-3213213', '1', '9:00--12:00，下午13：00--18：00', '9:00--12:00，下午13：00--18：00', '9:00--12:00，下午13：00--18：00', '0');
INSERT INTO `re_phone` VALUES ('6', '准东管委会', '18700388330', '1', '午9:00--12:00，下午13：00--18：', '午9:00--12:00，下午13：00--18：', '午9:00--12:00，下午13：00--18：', '2');
INSERT INTO `re_phone` VALUES ('7', '医院', null, null, null, null, null, '0');
INSERT INTO `re_phone` VALUES ('8', '准东管委会', '18700388330', '1', 'a 的', '范德萨发', ' 的a', '0');
INSERT INTO `re_phone` VALUES ('9', '准东管委会', '18700388330', '1', '1231', '213', '12312', '7');
INSERT INTO `re_phone` VALUES ('10', '准东管委会', '18700388330', '1', 'ad', 'dsad', 'dasd', '7');
INSERT INTO `re_phone` VALUES ('11', '213', '123', '1', '3123', '123', '123', '4');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------

-- ----------------------------
-- Table structure for role_menu
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for service_industry
-- ----------------------------
DROP TABLE IF EXISTS `service_industry`;
CREATE TABLE `service_industry` (
  `id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service_industry
-- ----------------------------

-- ----------------------------
-- Table structure for statistics
-- ----------------------------
DROP TABLE IF EXISTS `statistics`;
CREATE TABLE `statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wx_number` int(10) unsigned zerofill DEFAULT NULL,
  `ny_number` int(10) unsigned zerofill DEFAULT NULL,
  `qt_number` int(10) unsigned zerofill DEFAULT NULL,
  `bz_number` int(10) unsigned zerofill DEFAULT NULL,
  `z_number` int(10) unsigned zerofill DEFAULT NULL,
  `zy_number` int(10) unsigned zerofill DEFAULT NULL,
  `zzq_number` int(10) unsigned zerofill DEFAULT NULL,
  `zong_number` int(10) unsigned zerofill DEFAULT NULL COMMENT '历史总量',
  `dang_number` int(11) DEFAULT NULL,
  `order_number` int(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `wb_number` int(10) unsigned DEFAULT '1',
  `zd_number` int(11) DEFAULT NULL,
  `wz_number` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of statistics
-- ----------------------------

-- ----------------------------
-- Table structure for store_comment
-- ----------------------------
DROP TABLE IF EXISTS `store_comment`;
CREATE TABLE `store_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment_category` varchar(255) DEFAULT NULL COMMENT '评论类别',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `comment_picture` varchar(255) DEFAULT NULL COMMENT '评论图片',
  `comment_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `comment_star` int(255) DEFAULT NULL COMMENT '评论星级',
  `user_id` int(11) DEFAULT NULL COMMENT '评论人id',
  `comment_reply` varchar(255) DEFAULT NULL COMMENT '回复内容',
  `comment_time_reply` varchar(255) DEFAULT NULL COMMENT '回复时间',
  `comment_store` varchar(255) DEFAULT NULL COMMENT '回复店家',
  `pid` int(11) DEFAULT NULL COMMENT '订单id',
  `reserve_one` varchar(255) DEFAULT NULL,
  `reserve_two` varchar(255) DEFAULT NULL,
  `reserve_three` varchar(255) DEFAULT NULL,
  `reserve_four` varchar(255) DEFAULT NULL,
  `store_id` int(11) DEFAULT NULL COMMENT '店家id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store_comment
-- ----------------------------
INSERT INTO `store_comment` VALUES ('1', '类别', '内容', '图片', '时间', '5', '1', '回复内容', '回复时间', '回复店家', '2', null, null, null, null, '1');
INSERT INTO `store_comment` VALUES ('2', 'DJ', '评论内容测试', '无', '2020-1-23', '4', '1', null, null, null, '2', null, null, null, null, '1');
INSERT INTO `store_comment` VALUES ('3', 'DJ', '评论内容测试', 'http://192.168.10.246:8080/image/下载.jpg', '2020-1-23', '4', '1', null, null, null, '2', null, null, null, null, '1');

-- ----------------------------
-- Table structure for store_dish
-- ----------------------------
DROP TABLE IF EXISTS `store_dish`;
CREATE TABLE `store_dish` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dish_name` varchar(255) DEFAULT NULL COMMENT '菜名',
  `dish_price` int(11) DEFAULT NULL COMMENT '价格',
  `dish_picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `dish_grade` varchar(255) DEFAULT NULL COMMENT '评级',
  `dish_comment` int(255) DEFAULT NULL COMMENT '评论次数',
  `dish_times` int(11) DEFAULT NULL COMMENT '购买次数',
  `pid` int(11) DEFAULT NULL COMMENT '商家id',
  `reserve_one` varchar(255) DEFAULT NULL,
  `reserve_two` varchar(255) DEFAULT NULL,
  `reserve_three` varchar(255) DEFAULT NULL,
  `reserve_four` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store_dish
-- ----------------------------
INSERT INTO `store_dish` VALUES ('1', '菜名', '100', '图片', '介绍', '12', '5', '1', null, null, null, null);

-- ----------------------------
-- Table structure for store_order
-- ----------------------------
DROP TABLE IF EXISTS `store_order`;
CREATE TABLE `store_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `user_address` varchar(255) DEFAULT NULL COMMENT '地址',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名称',
  `user_tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `spot_name` varchar(255) DEFAULT NULL COMMENT '店名',
  `dish_id` int(11) DEFAULT NULL COMMENT '菜品id',
  `dish_name` varchar(255) DEFAULT NULL COMMENT '菜名',
  `dish_number` int(11) DEFAULT NULL COMMENT '个数',
  `dish_money` int(255) DEFAULT NULL COMMENT '金额',
  `order_number` varchar(11) DEFAULT NULL COMMENT '订单号',
  `order_state` varchar(255) DEFAULT NULL COMMENT '订单状态',
  `order_time` varchar(255) DEFAULT NULL COMMENT '下单时间',
  `user_remarks` varchar(255) DEFAULT NULL COMMENT '用户备注',
  `order_star` int(255) DEFAULT NULL COMMENT '评论星级',
  `order_picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `order_comment` varchar(255) DEFAULT NULL COMMENT '订单评论',
  `order_comment_time` varchar(255) DEFAULT NULL COMMENT '订单评论时间',
  `order_reply` varchar(255) DEFAULT NULL COMMENT '订单回复',
  `order_reply_time` varchar(255) DEFAULT NULL COMMENT '订单回复时间',
  `order_reply_name` varchar(255) DEFAULT NULL COMMENT '订单回复人',
  `pid` int(11) DEFAULT NULL,
  `reserve_one` varchar(255) DEFAULT NULL,
  `reserve_two` varchar(255) DEFAULT NULL,
  `reserve_three` varchar(255) DEFAULT NULL,
  `reserve_four` varchar(255) DEFAULT NULL,
  `order_total` int(255) DEFAULT NULL COMMENT '合计',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store_order
-- ----------------------------
INSERT INTO `store_order` VALUES ('1', '1', '地址', '姓名', '手机号', '店名1', '1', '菜名3', '10', '12', '订单号', '取消', '2020-1', '备注', '5', '回复图片', '评论内容', '2020-1', '回复内容', '回复时间', '回复名称', '1', null, null, null, null, null);
INSERT INTO `store_order` VALUES ('2', '1', '1', '1', '1', '1', null, '1', '1', '1', '1', '取消', '2020-3', null, null, null, null, '2020-3', null, null, null, '1', null, null, null, null, null);
INSERT INTO `store_order` VALUES ('3', '1', '地址', '姓名', '手机号', '店名1', '1', '菜名1', '10', '12', '订单号', '取消', '2020-2', '备注', '5', '评论图片', '评论内容1', '2020-1-10', '回复内容', '回复时间', '回复名称', '1', '', '', '', '', null);
INSERT INTO `store_order` VALUES ('4', '1', '地址', '姓名', '手机号', '店名1', '1', '菜名2', '10', '12', '订单号', '取消', '2020-4', '备注', '5', '回复图片', '评论内容', '2020-1', '回复内容', '回复时间', '回复名称', '1', '', '', '', '', null);
INSERT INTO `store_order` VALUES ('5', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', null, '1', null, null, null, null, null, null, null, '1', null, null, null, null, '1');
INSERT INTO `store_order` VALUES ('6', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '', '1', null, '', '', '', '', '', '', '1', '', '', '', '', '1');
INSERT INTO `store_order` VALUES ('7', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', null, '1', null, null, null, null, null, null, null, '1', null, null, null, null, '1');
INSERT INTO `store_order` VALUES ('8', '44', '用户地址', '用户名称', '18888888888', '店名', '1', '菜名', '10', '100', 'WM0000008', '下单', null, '不加饭', null, null, null, null, null, null, null, '8', null, null, null, null, '1000');

-- ----------------------------
-- Table structure for store_store
-- ----------------------------
DROP TABLE IF EXISTS `store_store`;
CREATE TABLE `store_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shop_owner` varchar(255) DEFAULT NULL COMMENT '店名（窗口）',
  `main_camp` varchar(255) DEFAULT NULL COMMENT '主营',
  `store_introduce` varchar(255) DEFAULT NULL COMMENT '店家介绍',
  `store_notice` varchar(255) DEFAULT NULL COMMENT '公告',
  `store_tel` varchar(255) DEFAULT NULL,
  `store_hours` varchar(255) DEFAULT NULL COMMENT '营业时间',
  `store_picture` varchar(255) DEFAULT NULL COMMENT '图片介绍',
  `store_increase_time` varchar(255) DEFAULT NULL,
  `store_location` varchar(255) DEFAULT NULL COMMENT '定位',
  `store_priority` int(255) DEFAULT NULL COMMENT '优先级',
  `store_area` varchar(255) DEFAULT NULL COMMENT '片区',
  `pid` int(11) DEFAULT NULL,
  `reserve_one` varchar(255) DEFAULT NULL COMMENT '预留',
  `reserve_two` varchar(255) DEFAULT NULL,
  `reserve_three` varchar(255) DEFAULT NULL,
  `reserve_four` varchar(255) DEFAULT NULL,
  `store_star` int(255) DEFAULT NULL COMMENT '星级',
  `store_address` varchar(255) DEFAULT NULL COMMENT '地址',
  `store_code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of store_store
-- ----------------------------
INSERT INTO `store_store` VALUES ('1', '店名', '主营', '店家介绍', null, '电话', '营业时间', '图片', '创建时间', '定位', '1', '片区', '0', '预留', '预留', '预留', '预留', '5', '地址', '1');

-- ----------------------------
-- Table structure for system_archives
-- ----------------------------
DROP TABLE IF EXISTS `system_archives`;
CREATE TABLE `system_archives` (
  `id` varchar(32) NOT NULL,
  `title` varchar(128) NOT NULL COMMENT '文章标题',
  `properties` varchar(32) DEFAULT 'n' COMMENT '自定义属性',
  `image_path` varchar(128) DEFAULT NULL COMMENT '缩略图',
  `tag` varchar(128) DEFAULT NULL COMMENT '标签',
  `description` varchar(256) DEFAULT NULL COMMENT '内容摘要',
  `category_id` varchar(32) NOT NULL COMMENT '上级栏目',
  `comment` int(11) DEFAULT '1' COMMENT '允许评论',
  `subscribe` int(11) DEFAULT '1' COMMENT '允许订阅',
  `clicks` int(11) DEFAULT '0' COMMENT '浏览量',
  `weight` int(11) DEFAULT '0' COMMENT '权重',
  `status` int(11) DEFAULT '1' COMMENT '状态1:已发布0未发布',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_archives
-- ----------------------------
INSERT INTO `system_archives` VALUES ('1', '213213213', 'n', '3213', '12321', '3213213', '231312', '1', '1', '0', '0', '1', '3213213', '2018-01-01 00:00:00', '321321', '2019-12-31 11:10:12');
INSERT INTO `system_archives` VALUES ('182306d8eab44225a5ce123ad6a01ffd', '温馨家居搭配设计css3网站模板', 'p', '20190815\\2dbcc0552eae41578b03f34235c71db0.jpg', null, '', 'e6cdbf119da14a30a86a0f9f2189b8e5', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 17:35:39', null, '2019-08-15 17:35:39');
INSERT INTO `system_archives` VALUES ('18f228df7b8443dfa1039fe3c3e53503', 'Dreamer CMS是企业在创立初期很好的技术基础框架', 'p', '20190816\\ecb040a2c77348958e6fd39b1afebe18.jpg', null, '', '4b1968cc854745418c4c0d305fbe7e15', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:21:14', null, '2019-08-16 09:21:14');
INSERT INTO `system_archives` VALUES ('31ca333eb9b04c8f8cc6f0f1c1447615', '微软', 'p', '20190816\\b39c908830744191b47830ac5e7d1b72.png', null, '', 'b0bb4daa71694c0ebdb59a986d11684f', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:02:00', null, '2019-08-16 10:02:00');
INSERT INTO `system_archives` VALUES ('4f451c4797c54536869f5cf748944f9e', 'Dreamer CMS 梦想家内容发布系统v1.0.0', 'n', '', null, '', '592ef65f97264ecda6caeb284d9f6355', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:20:53', '9f4b807db2e94670bb02cdc212ea7389', '2019-12-31 08:39:40');
INSERT INTO `system_archives` VALUES ('4f884e12b4be456a89156a51316c3f04', 'iop', 'p', '20190816\\ed8897c2c2ac4e61be5081584cf236ad.png', null, '', 'b0bb4daa71694c0ebdb59a986d11684f', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:07:06', null, '2019-08-16 10:07:06');
INSERT INTO `system_archives` VALUES ('779bc296ea4a4149bd3c97cdb8cd8593', '个人开发者也可以使用Dreamer CMS承接外包项目', 'p', '20190816\\3f3f65a75df74027b3755d155b0f29b2.png', null, '', '4b1968cc854745418c4c0d305fbe7e15', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:47:51', null, '2019-08-16 09:47:51');
INSERT INTO `system_archives` VALUES ('93b0ac1621024e4682442acd646da397', 'kid儿童用品商城shop网站模板', 'p', '20190815\\bbcb488fd42b42b4bd5af53f8c6ebcfe.jpg', null, '', 'e6cdbf119da14a30a86a0f9f2189b8e5', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 17:37:07', null, '2019-08-15 17:37:07');
INSERT INTO `system_archives` VALUES ('ae451258a33642d2b6e57729c83f3eb7', 'Yale', 'p', '20190816\\a04e19bd311c45bebfc7a3dd2695a88c.png', null, '', 'b0bb4daa71694c0ebdb59a986d11684f', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:11:22', null, '2019-08-16 10:11:22');
INSERT INTO `system_archives` VALUES ('cc1b4cc70af54410a9486e7d4babd5a7', '大气css3动画BUSINESS商务展会企业模板', 'p', '20190815\\6fa657689394413d9b724cfa522301e6.jpg', '商务', '', 'e6cdbf119da14a30a86a0f9f2189b8e5', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 17:38:19', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-21 16:47:41');
INSERT INTO `system_archives` VALUES ('d64d167032c84521ad21708cccc1f963', '大气orion渐变绿色css3响应式商业模板', 'p', '20190815\\18c8a53e1b63493b821c9ab865c4b395.jpg', 'css3,html5,bootstrap', '大气orion渐变绿色css3响应式商业模板', 'e6cdbf119da14a30a86a0f9f2189b8e5', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 17:28:10', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 11:32:05');
INSERT INTO `system_archives` VALUES ('e738f12d050849fd9998ac01fd0f5ad7', '初学JAVA的同学可以下载源代码来进行学习交流', 'p', '20190816\\da8377d716dc473a8f35d7a1b1a078c4.jpg', null, '', '4b1968cc854745418c4c0d305fbe7e15', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:49:55', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:58:21');
INSERT INTO `system_archives` VALUES ('fb7c4cf30dd1456e8b21af621479a2e4', 'Dreamer CMS 梦想家内容发布系统v2.0.0', 'n', '', '', '', '592ef65f97264ecda6caeb284d9f6355', '1', '1', '0', '0', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:30:57', null, '2019-12-31 11:09:57');

-- ----------------------------
-- Table structure for system_article
-- ----------------------------
DROP TABLE IF EXISTS `system_article`;
CREATE TABLE `system_article` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `aid` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '����ID',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ext01` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext02` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext03` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext04` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext05` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `html_content` text COLLATE utf8mb4_bin,
  `content` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_article
-- ----------------------------

-- ----------------------------
-- Table structure for system_attachment
-- ----------------------------
DROP TABLE IF EXISTS `system_attachment`;
CREATE TABLE `system_attachment` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `code` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '标识码',
  `filename` varchar(64) COLLATE utf8mb4_bin NOT NULL COMMENT '文件名',
  `filesize` int(11) DEFAULT NULL COMMENT '文件大小',
  `filetype` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '文件类型',
  `filepath` varchar(256) COLLATE utf8mb4_bin NOT NULL COMMENT '文件路径',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  `update_time` timestamp NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_attachment
-- ----------------------------
INSERT INTO `system_attachment` VALUES ('abd987209da941349001c85ffece6eb1', 'ez159g51', 'java1.8新特性.pptx', '113690', 'application/vnd.openxmlformats-officedocument.presentationml.presentation', '20191225\\acb604753b4c4d9ea281ae977d712f31.pptx', '9f4b807db2e94670bb02cdc212ea7389', '2019-12-25 05:47:34', '9f4b807db2e94670bb02cdc212ea7389', '2019-12-25 05:47:34');

-- ----------------------------
-- Table structure for system_category
-- ----------------------------
DROP TABLE IF EXISTS `system_category`;
CREATE TABLE `system_category` (
  `id` varchar(32) NOT NULL,
  `cnname` varchar(128) NOT NULL COMMENT '栏目名称',
  `enname` varchar(64) DEFAULT NULL COMMENT '栏目英文名称',
  `code` varchar(32) DEFAULT NULL COMMENT '编码',
  `cat_seq` varchar(128) DEFAULT NULL COMMENT '层级关系',
  `form_id` varchar(32) NOT NULL COMMENT '表单ID',
  `image_path` varchar(128) DEFAULT NULL COMMENT '栏目图片',
  `description` varchar(128) DEFAULT NULL COMMENT '栏目描述',
  `link_target` varchar(8) DEFAULT NULL COMMENT '链接目标',
  `page_size` int(11) DEFAULT '20' COMMENT '分页数量',
  `cat_model` int(11) DEFAULT '20' COMMENT '栏目模型（1封面，2列表，3外部链接）',
  `visit_url` varchar(128) DEFAULT NULL COMMENT '访问路径',
  `cover_temp` varchar(128) DEFAULT NULL COMMENT '封面模版',
  `list_temp` varchar(128) DEFAULT NULL COMMENT '列表模版',
  `article_temp` varchar(32) DEFAULT NULL COMMENT '内容页模版',
  `link_url` varchar(128) DEFAULT NULL COMMENT '链接路径',
  `default_editor` varchar(10) NOT NULL COMMENT '默认编辑器',
  `md_content` text COMMENT 'md内容',
  `html_content` text COMMENT 'html内容',
  `parent_id` varchar(128) NOT NULL COMMENT '上级栏目',
  `is_show` int(11) DEFAULT '1' COMMENT '是否显示1显示0隐藏',
  `level` varchar(10) DEFAULT NULL COMMENT '栏目级别',
  `sort` int(11) DEFAULT '50' COMMENT '排序',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ext01` varchar(32) DEFAULT NULL,
  `ext02` varchar(32) DEFAULT NULL,
  `ext03` varchar(32) DEFAULT NULL,
  `ext04` varchar(32) DEFAULT NULL,
  `ext05` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_category
-- ----------------------------
INSERT INTO `system_category` VALUES ('0ca9667691d045a098e138fddb644ee0', '分页栏目标签', 'Page Category Label', '2y1e2puq', '.822n58d3.u8xo56nx.2y1e2puq', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 分页栏目标签dreamer-cms:category\r\n### 1.标签说明\r\n这个标签是用于在分页（封面页面和列表页面和文章页面）调用当前栏目信息。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\nfield|typenamecn|字段名\r\n\r\n### 2.返回字段说明\r\n属性名|字段类型|描述\r\n:--:|:--:|:--:\r\ntypeid|String|栏目ID\r\ntypenamecn|String|栏目中文名称\r\ntypenameen|String|栏目英文名称\r\ntypecode|String|栏目编码（8位唯一）\r\ntypeseq|String|栏目的层级\r\ntypeimg|String|栏目图片\r\ndescription|String|栏目描述\r\nlinktarget|String|链接目标\r\npagesize|Integer|分页大小（该属性控制列表页面默认的分页大小）\r\nvisiturl|String|访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）\r\nlinkurl|String|链接URL\r\neditor|String|编辑器（md或ue）\r\nmdcontent|String|markdown内容\r\nhtmlcontent|String|生成的html内容\r\nparentname|String|上级栏目名称\r\nisshow|Integer|是否显示\r\nlevel|Integer|栏目层级\r\nsort|Integer|排序（默认为50）\r\ncreateby|String|创建人\r\ncreatetime|Date|创建时间\r\nupdateby|String|更新人\r\nupdatetime|Date|更新时间\r\ntypeurl|String|栏目URL\r\next01|String|扩展字段1\r\next02|String|扩展字段2\r\next03|String|扩展字段3\r\next04|String|扩展字段4\r\next05|String|扩展字段5\r\nhaschildren|Boolean|是否有下级栏目（该属性在dreamer-cms:type标签下不可用）\r\n\r\n### 3.示例代码\r\n```html\r\n<section class=\"page-header d-flex align-items-center\">\r\n<div class=\"container-fluid\">\r\n	<div class=\"page-header-content text-center\">\r\n		<div class=\"page-header-tag\">{dreamer-cms:global name=\"title\" /}</div>\r\n		<div class=\"page-header-title\">{dreamer-cms:category field=\"typenamecn\" /}</div>\r\n	</div>\r\n</div>\r\n</section>\r\n```\r\n### 4.效果\r\n![category标签](http://img.075400.cn/20190820100255.png \"category标签\")', '<h1 id=\"h1--dreamer-cms-category\"><a name=\"分页栏目标签dreamer-cms:category\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>分页栏目标签dreamer-cms:category</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>这个标签是用于在分页（封面页面和列表页面和文章页面）调用当前栏目信息。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">field</td>\r\n<td style=\"text-align:center\">typenamecn</td>\r\n<td style=\"text-align:center\">字段名</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">字段类型</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenamecn</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目中文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenameen</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目英文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typecode</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目编码（8位唯一）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeseq</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目的层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeimg</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目图片</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">description</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linktarget</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接目标</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">pagesize</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">分页大小（该属性控制列表页面默认的分页大小）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">visiturl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linkurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">editor</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">编辑器（md或ue）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">mdcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">markdown内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">htmlcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">生成的html内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">parentname</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">上级栏目名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">isshow</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否显示</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">level</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">栏目层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sort</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">排序（默认为50）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">创建人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createtime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">创建时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updateby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">更新人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updatetime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">更新时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext01</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段1</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext02</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段2</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext03</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段3</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext04</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段4</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext05</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段5</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">haschildren</td>\r\n<td style=\"text-align:center\">Boolean</td>\r\n<td style=\"text-align:center\">是否有下级栏目（该属性在dreamer-cms:type标签下不可用）</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;section class=\"page-header d-flex align-items-center\"&gt;\r\n&lt;div class=\"container-fluid\"&gt;\r\n    &lt;div class=\"page-header-content text-center\"&gt;\r\n        &lt;div class=\"page-header-tag\"&gt;{dreamer-cms:global name=\"title\" /}&lt;/div&gt;\r\n        &lt;div class=\"page-header-title\"&gt;{dreamer-cms:category field=\"typenamecn\" /}&lt;/div&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;\r\n&lt;/section&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://img.075400.cn/20190820100255.png\" alt=\"category标签\" title=\"category标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '9', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:04:05', null, '2019-08-20 10:04:25', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('2512e4593b514db2a10841cbc8a63a23', '开发者', 'developer', 'mvpeun1s', '.4ol61bbh.mvpeun1s', 'c17ddcad9fb149a6bf90f7bba0e0696b', '20190820\\8ab5d68c3f9e4687816d1cbb083d9f71.jpg', '', '1', '20', '1', 'developer', '/developer.html', '', '', '', 'ue', '', '<p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; font-family: &quot;Microsoft YaHei&quot;; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255); line-height: 2em;\"><span style=\"font-size: 14px;\"><span style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">王俊南(QQ:153095904)；</span><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">个人博客：</span></span><a href=\"http://blog.java1234.com/\" target=\"_blank\" style=\"color: rgb(0, 136, 204); text-decoration: underline; font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255); font-size: 14px;\"><span style=\"font-size: 14px;\">http://www.itechyou.cn/</span></a><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-size: 14px;\"><span style=\"font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">DreamerCMS创始人；草根站长；</span><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">有10年的JAVA学习工作研究经验，先后参与过国家电网登高项目，企业OA项目；</span><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">多个政府服务平台项目，具有丰富的J2EE开发经验；</span><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">5年多的Java培训指导教育经验，原创教程通俗易懂，深受喜爱；</span><br style=\"color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\"/><span style=\"font-size: 14px; color: rgb(51, 51, 51); font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; background-color: rgb(255, 255, 255);\">从事多年IT，拥有较广的人脉；&nbsp;</span></span></p><p style=\"padding: 0px; margin-top: 0px; margin-bottom: 0px; font-family: &quot;Microsoft YaHei&quot;; font-size: 12px; white-space: normal; background-color: rgb(255, 255, 255); line-height: 2em;\"><span style=\"font-size: 14px;\">个人名言：我喜欢程序员，他们单纯、固执、容易体会到成就感；面对压力，能够挑灯夜战不眠不休；面对困难，能够迎难而上挑战自我。他们也会感到困惑与傍徨，但每个程序员的心中都有一个比尔盖茨或是乔布斯的梦想“用智慧开创属于自己的事业”。我想说的是，其实我只是一个程序员！</span></p><p style=\"text-align: center; line-height: 2em;\"><img src=\"/uploads/20190816/1565949537281001415.jpg\" style=\"width: 228px; height: 228px;\" title=\"1565949537281001415.jpg\" width=\"228\" height=\"228\" border=\"0\" vspace=\"0\" alt=\"1565949537281001415.jpg\"/><span style=\"font-size: 14px;\">&nbsp;</span><img src=\"/uploads/20190816/1565949537316028864.jpg\" title=\"1565949537316028864.jpg\" width=\"228\" height=\"228\" border=\"0\" vspace=\"0\" alt=\"1565949537316028864.jpg\" style=\"width: 228px; height: 228px;\"/></p><p><br/></p>', '4e1c58e65fa9423482cf1a29a6ef629b', '1', '2', '2', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:33:33', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:50:05', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('25b7d2b36ea642d5ab9f39f90e10ae88', '全局标签', 'Global label', 'p78xb262', '.822n58d3.u8xo56nx.p78xb262', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 全局标签dreamer-cms:global\r\n### 1.标签说明\r\n该标签作用是获取后台系统配置中的配置项。该标签属于单标签，也可以写成{dreamer-cms:global name=\"xxx\"}{/dreamer-cms:global}。该标签只有一个属性 name，并适用于所有模版。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\nname|website|获取网站地址\r\nname|title|获取网站标题\r\nname|keywords|获取网站关键字\r\nname|describe|获取网站描述\r\nname|icp|获取网站备案号\r\nname|copyright|获取网站版权\r\nname|uploaddir|获取网站上传目录\r\nname|appid|暂无作用\r\nname|appkey|暂无作用\r\n\r\n### 2.示例代码\r\n```html\r\n<!--=== footer ===-->\r\n<footer class=\"footer\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-lg-5 col-md-6 col-sm-5 align-self-center\">\r\n            	<div>\r\n            		<p>\r\n            			{dreamer-cms:global name=\"copyright\" /}\r\n            			备案号：{dreamer-cms:global name=\"icp\" /}\r\n            		</p>\r\n            	</div>\r\n\r\n            </div>\r\n            <div class=\"col-lg-7 col-md-6 col-sm-7 align-self-center\">\r\n                <ul class=\"list-inline footer-menu\">\r\n                	{dreamer-cms:channel type=\'top\' currentstyle=\"\"}\r\n                    <li class=\"list-inline-item\"><a href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\">[field:typenamecn /]</a></li>\r\n                    {/dreamer-cms:channel}\r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</footer>\r\n```\r\n### 3.效果\r\n![global标签](http://www-x-itechyou-x-cn.img.abc188.com/uploads/allimg/190819/1-1ZQ9160P00-L.png \"global标签\")', '<h1 id=\"h1--dreamer-cms-global\"><a name=\"全局标签dreamer-cms:global\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>全局标签dreamer-cms:global</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签作用是获取后台系统配置中的配置项。该标签属于单标签，也可以写成{dreamer-cms:global name=”xxx”}{/dreamer-cms:global}。该标签只有一个属性 name，并适用于所有模版。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">website</td>\r\n<td style=\"text-align:center\">获取网站地址</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">title</td>\r\n<td style=\"text-align:center\">获取网站标题</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">keywords</td>\r\n<td style=\"text-align:center\">获取网站关键字</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">describe</td>\r\n<td style=\"text-align:center\">获取网站描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">icp</td>\r\n<td style=\"text-align:center\">获取网站备案号</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">copyright</td>\r\n<td style=\"text-align:center\">获取网站版权</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">uploaddir</td>\r\n<td style=\"text-align:center\">获取网站上传目录</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">appid</td>\r\n<td style=\"text-align:center\">暂无作用</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">appkey</td>\r\n<td style=\"text-align:center\">暂无作用</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.示例代码</h3><pre><code class=\"lang-html\">&lt;!--=== footer ===--&gt;\r\n&lt;footer class=\"footer\"&gt;\r\n    &lt;div class=\"container-fluid\"&gt;\r\n        &lt;div class=\"row\"&gt;\r\n            &lt;div class=\"col-lg-5 col-md-6 col-sm-5 align-self-center\"&gt;\r\n                &lt;div&gt;\r\n                    &lt;p&gt;\r\n                        {dreamer-cms:global name=\"copyright\" /}\r\n                        备案号：{dreamer-cms:global name=\"icp\" /}\r\n                    &lt;/p&gt;\r\n                &lt;/div&gt;\r\n\r\n            &lt;/div&gt;\r\n            &lt;div class=\"col-lg-7 col-md-6 col-sm-7 align-self-center\"&gt;\r\n                &lt;ul class=\"list-inline footer-menu\"&gt;\r\n                    {dreamer-cms:channel type=\'top\' currentstyle=\"\"}\r\n                    &lt;li class=\"list-inline-item\"&gt;&lt;a href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\"&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n                    {/dreamer-cms:channel}\r\n                &lt;/ul&gt;\r\n            &lt;/div&gt;\r\n        &lt;/div&gt;\r\n    &lt;/div&gt;\r\n&lt;/footer&gt;\r\n</code></pre>\r\n<h3 id=\"h3-3-\"><a name=\"3.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.效果</h3><p><img src=\"http://www-x-itechyou-x-cn.img.abc188.com/uploads/allimg/190819/1-1ZQ9160P00-L.png\" alt=\"global标签\" title=\"global标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 16:15:10', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 17:45:40', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('25e7a8290c8a468b90b7453dab8c2b19', '文章列表标签', 'List Label', 'sp7hj754', '.822n58d3.u8xo56nx.sp7hj754', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 文章列表标签dreamer-cms:list\r\n### 1.标签说明\r\n该标签是常用标记，主要用于获取栏目下的文章并适用于所有模版，通常用于网站内容列表等信息。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\ntypeid|4ol61bbh|后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目\r\nstart|0|从第几个开始获取\r\nlength|5|获取个数\r\nflag|p|文章属性p图片\r\naddfields|evaluator,content,source|附加字段，多个字段间用英文逗号分隔\r\nformkey|2EFPN7JY|表单模型的编码（可以在后台表单管理列表中获取）\r\nsortWay|asc|排序方式（asc或desc）\r\nsortBy||排序字段\r\n\r\n### 2.返回字段说明\r\n\r\n属性名|字段类型|描述\r\n:--:|:--:|:--:\r\nid|String|文章ID\r\ntitle|String|标题\r\nproperties|String|文章属性（头条h加粗b幻灯f图片b）\r\nlitpic|String|缩略图\r\ntag|String|文章标签\r\nremark|String|文章描述\r\ncategoryid|String|栏目ID\r\ncomment|Integer|是否允许评论\r\nsubscribe|Integer|是否允许订阅\r\nclicks|Integer|点击数\r\nweight|Integer|权重（可以利用该字段来排序）\r\nstatus|Integer|状态\r\ncreateby|String|发布人\r\ncreatetime|Date|发布时间\r\nupdateby|String|更新人\r\nupdatetime|Date|更新时间\r\narcurl|String|文章访问URL\r\n\r\n### 3.示例代码\r\n```html\r\n<div class=\"row\">\r\n	<div class=\"col-lg-12 text-center\">\r\n		<div class=\"section-tag\">{dreamer-cms:global name=\"title\" /}</div>\r\n		<div class=\"section-title mb-5\"><h2>模版中心</h2></div>\r\n	</div>\r\n	{dreamer-cms:list typeid=\"A54547W2\" start=\"0\" length=\"8\" flag=\"p\"}\r\n	<div class=\"col-lg-3 col-md-4 col-sm-6\">\r\n		<div class=\"feature-wrap\">\r\n			<div class=\"feature-image\"><img src=\"[field:litpic /]\" class=\"ion ion-ios-cloud-outline\"></i></div>\r\n			<a href=\"[field:arcurl/]\">[field:title /]</a>\r\n			<p></p>\r\n		</div>\r\n	</div>\r\n	{/dreamer-cms:list}\r\n</div>\r\n```\r\n### 4.效果\r\n![list标签](http://img.075400.cn/20190820094908.png \"list标签\")', '<h1 id=\"h1--dreamer-cms-list\"><a name=\"文章列表标签dreamer-cms:list\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>文章列表标签dreamer-cms:list</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签是常用标记，主要用于获取栏目下的文章并适用于所有模版，通常用于网站内容列表等信息。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">4ol61bbh</td>\r\n<td style=\"text-align:center\">后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">start</td>\r\n<td style=\"text-align:center\">0</td>\r\n<td style=\"text-align:center\">从第几个开始获取</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">length</td>\r\n<td style=\"text-align:center\">5</td>\r\n<td style=\"text-align:center\">获取个数</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">flag</td>\r\n<td style=\"text-align:center\">p</td>\r\n<td style=\"text-align:center\">文章属性p图片</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">addfields</td>\r\n<td style=\"text-align:center\">evaluator,content,source</td>\r\n<td style=\"text-align:center\">附加字段，多个字段间用英文逗号分隔</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">formkey</td>\r\n<td style=\"text-align:center\">2EFPN7JY</td>\r\n<td style=\"text-align:center\">表单模型的编码（可以在后台表单管理列表中获取）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sortWay</td>\r\n<td style=\"text-align:center\">asc</td>\r\n<td style=\"text-align:center\">排序方式（asc或desc）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sortBy</td>\r\n<td style=\"text-align:center\"></td>\r\n<td style=\"text-align:center\">排序字段</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">字段类型</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">id</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">title</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">标题</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">properties</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章属性（头条h加粗b幻灯f图片b）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">litpic</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">缩略图</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">tag</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章标签</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">remark</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">categoryid</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">comment</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否允许评论</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">subscribe</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否允许订阅</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">clicks</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">点击数</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">weight</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">权重（可以利用该字段来排序）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">status</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">状态</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">发布人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createtime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">发布时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updateby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">更新人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updatetime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">更新时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">arcurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章访问URL</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;div class=\"row\"&gt;\r\n    &lt;div class=\"col-lg-12 text-center\"&gt;\r\n        &lt;div class=\"section-tag\"&gt;{dreamer-cms:global name=\"title\" /}&lt;/div&gt;\r\n        &lt;div class=\"section-title mb-5\"&gt;&lt;h2&gt;模版中心&lt;/h2&gt;&lt;/div&gt;\r\n    &lt;/div&gt;\r\n    {dreamer-cms:list typeid=\"A54547W2\" start=\"0\" length=\"8\" flag=\"p\"}\r\n    &lt;div class=\"col-lg-3 col-md-4 col-sm-6\"&gt;\r\n        &lt;div class=\"feature-wrap\"&gt;\r\n            &lt;div class=\"feature-image\"&gt;&lt;img src=\"[field:litpic /]\" class=\"ion ion-ios-cloud-outline\"&gt;&lt;/i&gt;&lt;/div&gt;\r\n            &lt;a href=\"[field:arcurl/]\"&gt;[field:title /]&lt;/a&gt;\r\n            &lt;p&gt;&lt;/p&gt;\r\n        &lt;/div&gt;\r\n    &lt;/div&gt;\r\n    {/dreamer-cms:list}\r\n&lt;/div&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://img.075400.cn/20190820094908.png\" alt=\"list标签\" title=\"list标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '7', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 09:51:26', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:24:48', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('44e864cda6a049349e1516ce40eaa58e', '框架说明', 'Framework', '6rztc072', '.4ol61bbh.6rztc072', 'c17ddcad9fb149a6bf90f7bba0e0696b', '20190820\\aad2aaef17ca4cb49b3ffac27c29bb02.png', '', '1', '10', '1', 'framework', '/framework.html', '', '', '', 'md', '# 梦想家CMS内容管理系统开源版J2EE代码\r\n\r\n当前版本：1.0.1\r\n\r\n梦想家CMS官网：[http://www.itechyou.cn](www.itechyou.cn)\r\n\r\nQQ群交流：\r\n- ①597652651\r\n\r\nDreamer CMS 梦想家内容发布系统是国内首款java开发的内容发布系统，采用最流行的springboot+thymeleaf框架搭建，灵活小巧，配置简单。\r\n\r\n# 特点\r\n* 免费完整开源：基于MIT协议，源代码完全开源；\r\n* 标签化建站：不需要专业的后台开发技能，只要使用系统提供的标签，就能轻松建设网站；\r\n* 模版开发方便：支持在线上传模版包开发方便快捷；\r\n* 每月更新：每月进行系统升级，分享更多好用的模版与插件；\r\n\r\n# 面向对象\r\n* Dreamer CMS是企业在创立初期很好的技术基础框架，加快公司项目开发进度，当然也可以对现有的系统进行升级；\r\n* 个人开发者也可以使用Dreamer CMS承接外包项目；\r\n* 初学JAVA的同学可以下载源代码来进行学习交流；\r\n\r\n# 技术框架\r\n* 核心框架：Spring Boot 2\r\n* 安全框架：Apache Shiro 1.5\r\n* 视图框架：Spring MVC 4\r\n* 持久层框架：MyBatis 3\r\n* 日志管理：Log4j2\r\n* 模版框架：Thymeleaf\r\n* JS框架：jQuery，Bootstrap\r\n* CSS框架：Bootstrap\r\n* 富文本：Ueditor、editor.md\r\n\r\n# 系统结构\r\n![系统结构](http://itechyou.cn/uploads/allimg/180930/1-1P9301149510-L.jpg \"系统结构\")\r\n\r\n# 开发环境\r\n建议开发者使用以下环境，这样避免版本带来的问题\r\n* IDE：Spring Tool Suite（STS）\r\n* DB：Mysql 5.7\r\n* JDK：jdk8\r\n\r\n# 快速入门\r\n* 克隆项目到本地\r\n* 导入Eclipse或Sts等开发工具\r\n* 项目需要Redis，请自行修改application.yml中Redis配置\r\n* 将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录\r\n* 运行项目DreamerBlogApplication.java\r\n* 项目首页：http://localhost:8888；项目管理后台：http://localhost:8888/admin\r\n* 管理后台用户名：wangjn；密码：123456\r\n\r\n# 系统美图\r\n![后台登录](http://itechyou.cn/uploads/allimg/180930/1-1P9301135560-L.png)\r\n\r\n![系统管理](http://itechyou.cn/uploads/allimg/180930/1-1P9301136210-L.png)\r\n\r\n![发布文章](http://itechyou.cn/uploads/allimg/180930/1-1P9301136400-L.png)\r\n\r\n![前台首页](http://itechyou.cn/uploads/allimg/180930/1-1P930113A10-L.png)', '<h1 id=\"h1--cms-j2ee-\"><a name=\"梦想家CMS内容管理系统开源版J2EE代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>梦想家CMS内容管理系统开源版J2EE代码</h1><p>当前版本：1.0.1</p>\r\n<p>梦想家CMS官网：<a href=\"www.itechyou.cn\">http://www.itechyou.cn</a></p>\r\n<p>QQ群交流：</p>\r\n<ul>\r\n<li>①597652651</li></ul>\r\n<p>Dreamer CMS 梦想家内容发布系统是国内首款java开发的内容发布系统，采用最流行的springboot+thymeleaf框架搭建，灵活小巧，配置简单。</p>\r\n<h1 id=\"h1-u7279u70B9\"><a name=\"特点\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>特点</h1><ul>\r\n<li>免费完整开源：基于MIT协议，源代码完全开源；</li><li>标签化建站：不需要专业的后台开发技能，只要使用系统提供的标签，就能轻松建设网站；</li><li>模版开发方便：支持在线上传模版包开发方便快捷；</li><li>每月更新：每月进行系统升级，分享更多好用的模版与插件；</li></ul>\r\n<h1 id=\"h1-u9762u5411u5BF9u8C61\"><a name=\"面向对象\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>面向对象</h1><ul>\r\n<li>Dreamer CMS是企业在创立初期很好的技术基础框架，加快公司项目开发进度，当然也可以对现有的系统进行升级；</li><li>个人开发者也可以使用Dreamer CMS承接外包项目；</li><li>初学JAVA的同学可以下载源代码来进行学习交流；</li></ul>\r\n<h1 id=\"h1-u6280u672Fu6846u67B6\"><a name=\"技术框架\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>技术框架</h1><ul>\r\n<li>核心框架：Spring Boot 2</li><li>安全框架：Apache Shiro 1.5</li><li>视图框架：Spring MVC 4</li><li>持久层框架：MyBatis 3</li><li>日志管理：Log4j2</li><li>模版框架：Thymeleaf</li><li>JS框架：jQuery，Bootstrap</li><li>CSS框架：Bootstrap</li><li>富文本：Ueditor、editor.md</li></ul>\r\n<h1 id=\"h1-u7CFBu7EDFu7ED3u6784\"><a name=\"系统结构\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>系统结构</h1><p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301149510-L.jpg\" alt=\"系统结构\" title=\"系统结构\">\r\n<h1 id=\"h1-u5F00u53D1u73AFu5883\"><a name=\"开发环境\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>开发环境</h1><p>建议开发者使用以下环境，这样避免版本带来的问题</p>\r\n<ul>\r\n<li>IDE：Spring Tool Suite（STS）</li><li>DB：Mysql 5.7</li><li>JDK：jdk8</li></ul>\r\n<h1 id=\"h1-u5FEBu901Fu5165u95E8\"><a name=\"快速入门\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>快速入门</h1><ul>\r\n<li>克隆项目到本地</li><li>导入Eclipse或Sts等开发工具</li><li>项目需要Redis，请自行修改application.yml中Redis配置</li><li>将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录</li><li>运行项目DreamerBlogApplication.java</li><li>项目首页：<a href=\"http://localhost:8888；项目管理后台：http://localhost:8888/admin\">http://localhost:8888；项目管理后台：http://localhost:8888/admin</a></li><li>管理后台用户名：wangjn；密码：123456</li></ul>\r\n<h1 id=\"h1-u7CFBu7EDFu7F8Eu56FE\"><a name=\"系统美图\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>系统美图</h1><p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301135560-L.png\" alt=\"后台登录\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301136210-L.png\" alt=\"系统管理\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301136400-L.png\" alt=\"发布文章\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P930113A10-L.png\" alt=\"前台首页\">', '4e1c58e65fa9423482cf1a29a6ef629b', '1', '2', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:44:38', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:45:18', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('47505ff560a3413488ad71f097a04172', '后台使用', 'Background use', '1f5uz4zy', '.822n58d3.1f5uz4zy', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '建设中...', '<p>建设中…</p>', 'ce1c25b233c0422bb136c27aca4b38f4', '1', '2', '52', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:49:05', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:52:14', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('4b1968cc854745418c4c0d305fbe7e15', '客户评价', 'evaluate', '1hq8501w', '.1hq8501w', '06c540abf3a64d9699762711439c82dd', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', '-1', '0', '1', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:06:45', null, '2019-08-19 08:58:47', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('4e1c58e65fa9423482cf1a29a6ef629b', '框架简介', 'framework', '4ol61bbh', '.4ol61bbh', 'c17ddcad9fb149a6bf90f7bba0e0696b', '20191231\\44db62f106eb4c69ad4615d063b8bafb.jpg', 'Dreamer CMS 梦想家内容发布系统是国内首款java开发的内容发布系统，采用最流行的springboot+thymeleaf框架搭建，灵活小巧，配置简单。', '1', '20', '1', 'framework', '/framework.html', '', '', '', 'md', '![](http://localhost:8888/\\uploads\\\\20191231\\542736fa18f04c24acd6c7a6a5a9045d.jpg)', '<p><img src=\"http://localhost:8888/\\uploads\\\\20191231\\542736fa18f04c24acd6c7a6a5a9045d.jpg\" alt=\"\">', '-1', '1', '1', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:51:16', '9f4b807db2e94670bb02cdc212ea7389', '2019-12-31 05:59:50', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('592ef65f97264ecda6caeb284d9f6355', '更新记录', 'versions', 'he9g4d0j', '.he9g4d0j', '440dc002ad4846018d7a1b9433021b22', '', '', '1', '10', '2', 'versionnotes', '', '/list_version.html', '/article_version.html', '', 'md', '', '', '-1', '1', '1', '2', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:58:41', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 11:21:28', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('6d1b5b4467cf456693181b697e553d69', '常见问题', 'Common problem', '6o3y4wsk', '.822n58d3.6o3y4wsk', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '', '', 'ce1c25b233c0422bb136c27aca4b38f4', '1', '2', '54', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:49:51', null, '2019-08-19 15:50:07', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('7518e8daebfe4a2b9589b88a00f73ea4', '文章分页标签', 'Page Article Label', '4gs4256i', '.822n58d3.u8xo56nx.4gs4256i', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 文章分页标签dreamer-cms:pagelist\r\n### 1.标签说明\r\n这个标签是用于在列表页面调用文章列表并显示分页信息。一般该标签配合{dreamer-cms:pagination /}共同使用。见下方例子。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\nformkey|4ol61bbh|表单模型的编码\r\naddfields|versionnum,notes|附加字段（该字段须设置了formkey后生效）\r\nsortWay|asc或desc|排序方式\r\nsortBy||排序字段\r\n\r\n### 2.返回字段说明\r\n属性名|字段类型|描述\r\n:--:|:--:|:--:\r\nid|String|文章ID\r\ntitle|String|标题\r\nproperties|String|文章属性（头条h加粗b幻灯f图片b）\r\nlitpic|String|缩略图\r\ntag|String|文章标签\r\nremark|String|文章描述\r\ncategoryid|String|栏目ID\r\ncomment|Integer|是否允许评论\r\nsubscribe|Integer|是否允许订阅\r\nclicks|Integer|点击数\r\nweight|Integer|权重（可以利用该字段来排序）\r\nstatus|Integer|状态\r\ncreateby|String|发布人\r\ncreatetime|Date|发布时间\r\nupdateby|String|更新人\r\nupdatetime|Date|更新时间\r\narcurl|String|文章访问URL\r\n\r\n### 3.示例代码\r\n```html\r\n<div class=\"row\">\r\n	{dreamer-cms:pagelist pagesize=\"10\" formkey=\"LUVC0AW2\" addfields=\"versionnum,notes\"}\r\n	<div class=\"col-lg-4 col-md-6 col-sm-6\">\r\n		<div class=\"blog-post\">\r\n			<div class=\"blog-details\">\r\n				<ul class=\"list-inline blog-item-links\">\r\n					<li class=\"list-inline-item\"><a href=\"#\"><i class=\"ion ion-calendar\"></i>版本号：[field:versionnum/]</a></li>\r\n					<li class=\"list-inline-item\"><a href=\"#\"><i class=\"ion ion-chatbubbles\"></i>点击：[field:clicks/]</a></li>\r\n				</ul>\r\n				<a href=\"[field:arcurl/]\"><h4 class=\"media-heading\">[field:title/]</h4></a>\r\n				<p class=\"product-para\">版本号：[field:versionnum/]</p>\r\n				<p class=\"product-para\">发布日期：[field:createtime/]</p>\r\n				<a href=\"[field:arcurl/]\" class=\"blog-post-link\">查看详情 <i class=\"fa fa-angle-double-right\"></i></a>\r\n			</div>\r\n		</div>\r\n	</div>\r\n	{/dreamer-cms:pagelist}\r\n</div>\r\n<div class=\"row\">\r\n	<div class=\"col-lg-12 col-md-12 col-sm-12\">\r\n	{dreamer-cms:pagination /}\r\n	</div>\r\n</div>\r\n```\r\n### 4.效果\r\n![pagelist标签](http://img.075400.cn/20190820101533.png \"pagelist标签\")', '<h1 id=\"h1--dreamer-cms-pagelist\"><a name=\"文章分页标签dreamer-cms:pagelist\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>文章分页标签dreamer-cms:pagelist</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>这个标签是用于在列表页面调用文章列表并显示分页信息。一般该标签配合{dreamer-cms:pagination /}共同使用。见下方例子。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">formkey</td>\r\n<td style=\"text-align:center\">4ol61bbh</td>\r\n<td style=\"text-align:center\">表单模型的编码</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">addfields</td>\r\n<td style=\"text-align:center\">versionnum,notes</td>\r\n<td style=\"text-align:center\">附加字段（该字段须设置了formkey后生效）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sortWay</td>\r\n<td style=\"text-align:center\">asc或desc</td>\r\n<td style=\"text-align:center\">排序方式</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sortBy</td>\r\n<td style=\"text-align:center\"></td>\r\n<td style=\"text-align:center\">排序字段</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">字段类型</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">id</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">title</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">标题</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">properties</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章属性（头条h加粗b幻灯f图片b）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">litpic</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">缩略图</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">tag</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章标签</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">remark</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">categoryid</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">comment</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否允许评论</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">subscribe</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否允许订阅</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">clicks</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">点击数</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">weight</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">权重（可以利用该字段来排序）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">status</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">状态</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">发布人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createtime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">发布时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updateby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">更新人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updatetime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">更新时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">arcurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">文章访问URL</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;div class=\"row\"&gt;\r\n    {dreamer-cms:pagelist pagesize=\"10\" formkey=\"LUVC0AW2\" addfields=\"versionnum,notes\"}\r\n    &lt;div class=\"col-lg-4 col-md-6 col-sm-6\"&gt;\r\n        &lt;div class=\"blog-post\"&gt;\r\n            &lt;div class=\"blog-details\"&gt;\r\n                &lt;ul class=\"list-inline blog-item-links\"&gt;\r\n                    &lt;li class=\"list-inline-item\"&gt;&lt;a href=\"#\"&gt;&lt;i class=\"ion ion-calendar\"&gt;&lt;/i&gt;版本号：[field:versionnum/]&lt;/a&gt;&lt;/li&gt;\r\n                    &lt;li class=\"list-inline-item\"&gt;&lt;a href=\"#\"&gt;&lt;i class=\"ion ion-chatbubbles\"&gt;&lt;/i&gt;点击：[field:clicks/]&lt;/a&gt;&lt;/li&gt;\r\n                &lt;/ul&gt;\r\n                &lt;a href=\"[field:arcurl/]\"&gt;&lt;h4 class=\"media-heading\"&gt;[field:title/]&lt;/h4&gt;&lt;/a&gt;\r\n                &lt;p class=\"product-para\"&gt;版本号：[field:versionnum/]&lt;/p&gt;\r\n                &lt;p class=\"product-para\"&gt;发布日期：[field:createtime/]&lt;/p&gt;\r\n                &lt;a href=\"[field:arcurl/]\" class=\"blog-post-link\"&gt;查看详情 &lt;i class=\"fa fa-angle-double-right\"&gt;&lt;/i&gt;&lt;/a&gt;\r\n            &lt;/div&gt;\r\n        &lt;/div&gt;\r\n    &lt;/div&gt;\r\n    {/dreamer-cms:pagelist}\r\n&lt;/div&gt;\r\n&lt;div class=\"row\"&gt;\r\n    &lt;div class=\"col-lg-12 col-md-12 col-sm-12\"&gt;\r\n    {dreamer-cms:pagination /}\r\n    &lt;/div&gt;\r\n&lt;/div&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://img.075400.cn/20190820101533.png\" alt=\"pagelist标签\" title=\"pagelist标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '10', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:26:12', null, '2019-08-20 10:26:29', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('7b773cb86e574302a8878573de87b32a', '文档丰富', 'richdoc', '1ggrj6n9', '.4ol61bbh.tnleskz4.1ggrj6n9', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', 'd9fea2574384477199d038ac66ebd095', '0', '3', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 16:49:37', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 08:58:47', 'fa fa-file-word-o', '', '', '', '');
INSERT INTO `system_category` VALUES ('7ddb8cde57554702976f01593488d807', '模版化', 'templateing', 'e96c4wkr', '.4ol61bbh.tnleskz4.e96c4wkr', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', 'd9fea2574384477199d038ac66ebd095', '0', '3', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 16:48:26', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 08:58:47', 'fa fa-object-group', '', '', '', '');
INSERT INTO `system_category` VALUES ('8923a09d403f49a289f3af780dcca0c2', '标签', 'label', 'u8xo56nx', '.822n58d3.u8xo56nx', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 标签\r\n* 标签统一dreamer-cms:打头，花括号包裹，dreamer-cms标签只能用于html模版页面中；\r\n* 标签格式：{ms:标签 属性1=值1 属性2=值2 属性n=值n/}属性值需要用双引号包裹，相邻的两个属性之间也要用空格隔开；\r\n* 模版也是普通的html文件格式，文件后缀为.html；\r\n* 只支持两种默认首页，分别是引导页（default.html）与主页（index.html）；\r\n	* default.html一般是做网站的欢迎引导界面；\r\n	* index.html为网站的主页；\r\n* 模版需要与后台的栏目绑定，通过生成器生成才有效；\r\n* 栏目有两个非常重要的概念，列表与封面；\r\n* 列表：例如产品列表、文章列表，列表会绑定两个模版地址一是列表模版、二是内容模版；\r\n* 封面：例如关于我们、联系方式这些都属于单篇模版；\r\n* 文章是必须发布在最终子栏目；\r\n* 自定义模型，如果文章现有的字段无法满足可以通过自定义模型来满足，通过[field:*/]获取自定义属性。', '<h1 id=\"h1-u6807u7B7E\"><a name=\"标签\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>标签</h1><ul>\r\n<li>标签统一dreamer-cms:打头，花括号包裹，dreamer-cms标签只能用于html模版页面中；</li><li>标签格式：{ms:标签 属性1=值1 属性2=值2 属性n=值n/}属性值需要用双引号包裹，相邻的两个属性之间也要用空格隔开；</li><li>模版也是普通的html文件格式，文件后缀为.html；</li><li>只支持两种默认首页，分别是引导页（default.html）与主页（index.html）；<ul>\r\n<li>default.html一般是做网站的欢迎引导界面；</li><li>index.html为网站的主页；</li></ul>\r\n</li><li>模版需要与后台的栏目绑定，通过生成器生成才有效；</li><li>栏目有两个非常重要的概念，列表与封面；</li><li>列表：例如产品列表、文章列表，列表会绑定两个模版地址一是列表模版、二是内容模版；</li><li>封面：例如关于我们、联系方式这些都属于单篇模版；</li><li>文章是必须发布在最终子栏目；</li><li>自定义模型，如果文章现有的字段无法满足可以通过自定义模型来满足，通过[field:*/]获取自定义属性。</li></ul>', 'ce1c25b233c0422bb136c27aca4b38f4', '1', '2', '51', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:45:45', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 18:12:09', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('a74b9beee883448b8f59cad4a8396695', '变量标签', 'Variable label', '40350iuz', '.822n58d3.u8xo56nx.40350iuz', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 变量标签dreamer-cms:variable\r\n### 1.标签说明\r\n该标签作用是获取后台变量管理中的配置项。该标签属于单标签，也可以写成{dreamer-cms:variable name=\"xxx\"}{/dreamer-cms:variable}。该标签只有一个属性 name，并适用于所有模版。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\nname|cfg_github_addr|后台变量管理中的变量名\r\n\r\n### 2.示例代码\r\n```html\r\n<section id=\"action\" class=\"call-to-action-2\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row\">\r\n            <div class=\"col-12\">\r\n                <div class=\"call-to-action-wrap\">\r\n                    <div class=\"call-to-action-title\">\r\n                        <div class=\"text-center\"><a href=\"{dreamer-cms:variable name=\'cfg_gitee_addr\'/}\" class=\"btn btn-primary main-btn\">源码下载</a></div>\r\n                        <h3>梦想家CMS内容管理系统支持码云、Github等多种渠道下载。</h3>\r\n                    </div>\r\n                    <div class=\"list-wrap\">\r\n                        <ul class=\"list-inline applications\">\r\n                            <li class=\"list-inline-item\"><a href=\"{dreamer-cms:variable name=\'cfg_gitee_addr\'/}\"><img src=\"{dreamer-cms:template /}images/app1.png\" alt=\"Application\"></a></li>\r\n                            <li class=\"list-inline-item\"><a href=\"{dreamer-cms:variable name=\'cfg_github_addr\'/}\"><img src=\"{dreamer-cms:template /}images/app2.png\" alt=\"Application\"></a></li>\r\n                        </ul>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>\r\n```\r\n### 3.效果\r\n![variable标签](http://www.itechyou.cn/uploads/allimg/190819/1-1ZQ91JT40-L.png \"variable标签\")', '<h1 id=\"h1--dreamer-cms-variable\"><a name=\"变量标签dreamer-cms:variable\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>变量标签dreamer-cms:variable</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签作用是获取后台变量管理中的配置项。该标签属于单标签，也可以写成{dreamer-cms:variable name=”xxx”}{/dreamer-cms:variable}。该标签只有一个属性 name，并适用于所有模版。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">name</td>\r\n<td style=\"text-align:center\">cfg_github_addr</td>\r\n<td style=\"text-align:center\">后台变量管理中的变量名</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.示例代码</h3><pre><code class=\"lang-html\">&lt;section id=\"action\" class=\"call-to-action-2\"&gt;\r\n    &lt;div class=\"container-fluid\"&gt;\r\n        &lt;div class=\"row\"&gt;\r\n            &lt;div class=\"col-12\"&gt;\r\n                &lt;div class=\"call-to-action-wrap\"&gt;\r\n                    &lt;div class=\"call-to-action-title\"&gt;\r\n                        &lt;div class=\"text-center\"&gt;&lt;a href=\"{dreamer-cms:variable name=\'cfg_gitee_addr\'/}\" class=\"btn btn-primary main-btn\"&gt;源码下载&lt;/a&gt;&lt;/div&gt;\r\n                        &lt;h3&gt;梦想家CMS内容管理系统支持码云、Github等多种渠道下载。&lt;/h3&gt;\r\n                    &lt;/div&gt;\r\n                    &lt;div class=\"list-wrap\"&gt;\r\n                        &lt;ul class=\"list-inline applications\"&gt;\r\n                            &lt;li class=\"list-inline-item\"&gt;&lt;a href=\"{dreamer-cms:variable name=\'cfg_gitee_addr\'/}\"&gt;&lt;img src=\"{dreamer-cms:template /}images/app1.png\" alt=\"Application\"&gt;&lt;/a&gt;&lt;/li&gt;\r\n                            &lt;li class=\"list-inline-item\"&gt;&lt;a href=\"{dreamer-cms:variable name=\'cfg_github_addr\'/}\"&gt;&lt;img src=\"{dreamer-cms:template /}images/app2.png\" alt=\"Application\"&gt;&lt;/a&gt;&lt;/li&gt;\r\n                        &lt;/ul&gt;\r\n                    &lt;/div&gt;\r\n                &lt;/div&gt;\r\n            &lt;/div&gt;\r\n        &lt;/div&gt;\r\n    &lt;/div&gt;\r\n&lt;/section&gt;\r\n</code></pre>\r\n<h3 id=\"h3-3-\"><a name=\"3.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.效果</h3><p><img src=\"http://www.itechyou.cn/uploads/allimg/190819/1-1ZQ91JT40-L.png\" alt=\"variable标签\" title=\"variable标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '2', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 17:21:18', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 17:49:41', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('b0bb4daa71694c0ebdb59a986d11684f', '合作伙伴', 'partner', '2jk6313y', '.2jk6313y', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', '-1', '0', '1', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:59:32', null, '2019-08-19 08:58:47', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('b2cca489d9e14f4e8e291c59308d1ae0', '栏目标签', 'Type Label', '1765m053', '.822n58d3.u8xo56nx.1765m053', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 变量标签dreamer-cms:type\r\n### 1.标签说明\r\n该标签作用是获取指定栏目的内容。该标签只有一个属性typeid，并适用于所有模版。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\ntypeid|4ol61bbh|后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目\r\n\r\n### 2.返回字段说明\r\n\r\n属性名|字段类型|描述\r\n:--:|:--:|:--:\r\ntypeid|String|栏目ID\r\ntypenamecn|String|栏目中文名称\r\ntypenameen|String|栏目英文名称\r\ntypecode|String|栏目编码（8位唯一）\r\ntypeseq|String|栏目的层级\r\ntypeimg|String|栏目图片\r\ndescription|String|栏目描述\r\nlinktarget|String|链接目标\r\npagesize|Integer|分页大小（该属性控制列表页面默认的分页大小）\r\nvisiturl|String|访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）\r\nlinkurl|String|链接URL\r\neditor|String|编辑器（md或ue）\r\nmdcontent|String|markdown内容\r\nhtmlcontent|String|生成的html内容\r\nparentname|String|上级栏目名称\r\nisshow|Integer|是否显示\r\nlevel|Integer|栏目层级\r\nsort|Integer|排序（默认为50）\r\ncreateby|String|创建人\r\ncreatetime|Date|创建时间\r\nupdateby|String|更新人\r\nupdatetime|Date|更新时间\r\ntypeurl|String|栏目URL\r\next01|String|扩展字段1\r\next02|String|扩展字段2\r\next03|String|扩展字段3\r\next04|String|扩展字段4\r\next05|String|扩展字段5\r\nhaschildren|Boolean|是否有下级栏目（该属性在dreamer-cms:type标签下不可用）\r\n\r\n### 3.示例代码\r\n```html\r\n<div class=\"row\">\r\n	<div class=\"col-12 mt-5\">\r\n		{dreamer-cms:type typeid=\"A54547W2\"}\r\n		<div class=\"text-center\"><a href=\"[field:typeurl/]\" class=\"btn btn-primary main-btn btn-reload\"><i class=\"fa fa-spinner fa-spin\"></i> 查看更多</a></div>\r\n		{/dreamer-cms:type}\r\n	</div>\r\n</div>\r\n```\r\n### 4.效果\r\n![type标签](http://www.itechyou.cn/uploads/allimg/190819/1-1ZQ91P6090-L.png \"type标签\")', '<h1 id=\"h1--dreamer-cms-type\"><a name=\"变量标签dreamer-cms:type\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>变量标签dreamer-cms:type</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签作用是获取指定栏目的内容。该标签只有一个属性typeid，并适用于所有模版。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">4ol61bbh</td>\r\n<td style=\"text-align:center\">后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">字段类型</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenamecn</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目中文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenameen</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目英文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typecode</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目编码（8位唯一）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeseq</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目的层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeimg</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目图片</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">description</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linktarget</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接目标</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">pagesize</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">分页大小（该属性控制列表页面默认的分页大小）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">visiturl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linkurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">editor</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">编辑器（md或ue）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">mdcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">markdown内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">htmlcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">生成的html内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">parentname</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">上级栏目名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">isshow</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否显示</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">level</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">栏目层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sort</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">排序（默认为50）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">创建人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createtime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">创建时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updateby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">更新人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updatetime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">更新时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext01</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段1</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext02</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段2</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext03</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段3</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext04</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段4</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext05</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段5</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">haschildren</td>\r\n<td style=\"text-align:center\">Boolean</td>\r\n<td style=\"text-align:center\">是否有下级栏目（该属性在dreamer-cms:type标签下不可用）</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;div class=\"row\"&gt;\r\n    &lt;div class=\"col-12 mt-5\"&gt;\r\n        {dreamer-cms:type typeid=\"A54547W2\"}\r\n        &lt;div class=\"text-center\"&gt;&lt;a href=\"[field:typeurl/]\" class=\"btn btn-primary main-btn btn-reload\"&gt;&lt;i class=\"fa fa-spinner fa-spin\"&gt;&lt;/i&gt; 查看更多&lt;/a&gt;&lt;/div&gt;\r\n        {/dreamer-cms:type}\r\n    &lt;/div&gt;\r\n&lt;/div&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://www.itechyou.cn/uploads/allimg/190819/1-1ZQ91P6090-L.png\" alt=\"type标签\" title=\"type标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '5', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 18:08:54', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 09:51:47', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('b86a0c79aec447d78afb7d25111eae1a', '模版制作', 'Template making', '24is7n55', '.822n58d3.24is7n55', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '', '', 'ce1c25b233c0422bb136c27aca4b38f4', '1', '2', '53', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:48:31', null, '2019-08-19 15:50:07', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('bd2ff1d7e4174078ab573457507f7a08', '栏目内容列表', 'Category Article List', 'h56yw2e9', '.822n58d3.u8xo56nx.h56yw2e9', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 栏目内容列表标签dreamer-cms:categoryartlist\r\n### 1.标签说明\r\n这个标签是系统中唯一的一个支持嵌套的标签，这个标签通常使用在首页（含封面首页），用于输出一组栏目内容列表，主要用于获取下级栏目的内容列表标签并适用于所有模版，通常用于网站内容列表等信息。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\ntypeid|4ol61bbh|后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目\r\nlength|5|获取个数\r\n\r\n### 2.返回字段说明\r\n参照channel和list标签返回字段说明\r\n\r\n### 3.示例代码\r\n```html\r\n<ul class=\"nav navbar-nav mr-auto\">\r\n	<li class=\"nav-item active\"><a class=\"nav-link scroll\" href=\"/\">首页</a></li>\r\n	{dreamer-cms:categoryartlist length=\"1\"}\r\n		{dreamer-cms:if test=\"(\'true\' eq [field:haschildren/])\"}\r\n		<li class=\"dropdown nav-item\"><a href=\"[field:typeurl/]\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\">[field:typenamecn /]</a>\r\n			<ul class=\"dropdown-menu\">\r\n				{dreamer-cms:channel}\r\n				<li><a class=\"dropdown-item\" href=\"[field:typeurl/]\"><i class=\"ion ion-chevron-right\"></i>[field:typenamecn /]</a></li>\r\n				{/dreamer-cms:channel}\r\n			</ul>\r\n		</li>\r\n		{/dreamer-cms:if}\r\n		{dreamer-cms:if test=\"(\'false\' eq [field:haschildren/])\"}\r\n		<li class=\"nav-item\"><a class=\"nav-link scroll\" href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\">[field:typenamecn /]</a></li>\r\n		{/dreamer-cms:if}\r\n	{/dreamer-cms:categoryartlist}\r\n</ul>\r\n```\r\n### 4.效果\r\n![categoryartlist标签](http://img.075400.cn/20190820095818.png \"categoryartlist标签\")', '<h1 id=\"h1--dreamer-cms-categoryartlist\"><a name=\"栏目内容列表标签dreamer-cms:categoryartlist\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>栏目内容列表标签dreamer-cms:categoryartlist</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>这个标签是系统中唯一的一个支持嵌套的标签，这个标签通常使用在首页（含封面首页），用于输出一组栏目内容列表，主要用于获取下级栏目的内容列表标签并适用于所有模版，通常用于网站内容列表等信息。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">4ol61bbh</td>\r\n<td style=\"text-align:center\">后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">length</td>\r\n<td style=\"text-align:center\">5</td>\r\n<td style=\"text-align:center\">获取个数</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><p>参照channel和list标签返回字段说明</p>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;ul class=\"nav navbar-nav mr-auto\"&gt;\r\n    &lt;li class=\"nav-item active\"&gt;&lt;a class=\"nav-link scroll\" href=\"/\"&gt;首页&lt;/a&gt;&lt;/li&gt;\r\n    {dreamer-cms:categoryartlist length=\"1\"}\r\n        {dreamer-cms:if test=\"(\'true\' eq [field:haschildren/])\"}\r\n        &lt;li class=\"dropdown nav-item\"&gt;&lt;a href=\"[field:typeurl/]\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\"&gt;[field:typenamecn /]&lt;/a&gt;\r\n            &lt;ul class=\"dropdown-menu\"&gt;\r\n                {dreamer-cms:channel}\r\n                &lt;li&gt;&lt;a class=\"dropdown-item\" href=\"[field:typeurl/]\"&gt;&lt;i class=\"ion ion-chevron-right\"&gt;&lt;/i&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n                {/dreamer-cms:channel}\r\n            &lt;/ul&gt;\r\n        &lt;/li&gt;\r\n        {/dreamer-cms:if}\r\n        {dreamer-cms:if test=\"(\'false\' eq [field:haschildren/])\"}\r\n        &lt;li class=\"nav-item\"&gt;&lt;a class=\"nav-link scroll\" href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\"&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n        {/dreamer-cms:if}\r\n    {/dreamer-cms:categoryartlist}\r\n&lt;/ul&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://img.075400.cn/20190820095818.png\" alt=\"categoryartlist标签\" title=\"categoryartlist标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '8', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:00:03', null, '2019-08-20 10:04:25', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('c057161b83274a708265da8808cb8f87', '概要', 'outline', 'xk315i87', '.822n58d3.xk315i87', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 梦想家CMS内容管理系统开源版J2EE代码\r\n\r\n当前版本：1.0.1\r\n\r\n梦想家CMS官网：[http://www.itechyou.cn](www.itechyou.cn)\r\n\r\nQQ群交流：\r\n- ①597652651\r\n\r\nDreamer CMS 梦想家内容发布系统是国内首款java开发的内容发布系统，采用最流行的springboot+thymeleaf框架搭建，灵活小巧，配置简单。\r\n\r\n# 特点\r\n* 免费完整开源：基于MIT协议，源代码完全开源；\r\n* 标签化建站：不需要专业的后台开发技能，只要使用系统提供的标签，就能轻松建设网站；\r\n* 模版开发方便：支持在线上传模版包开发方便快捷；\r\n* 每月更新：每月进行系统升级，分享更多好用的模版与插件；\r\n\r\n# 面向对象\r\n* Dreamer CMS是企业在创立初期很好的技术基础框架，加快公司项目开发进度，当然也可以对现有的系统进行升级；\r\n* 个人开发者也可以使用Dreamer CMS承接外包项目；\r\n* 初学JAVA的同学可以下载源代码来进行学习交流；\r\n\r\n# 技术框架\r\n* 核心框架：Spring Boot 2\r\n* 安全框架：Apache Shiro 1.5\r\n* 视图框架：Spring MVC 4\r\n* 持久层框架：MyBatis 3\r\n* 日志管理：Log4j2\r\n* 模版框架：Thymeleaf\r\n* JS框架：jQuery，Bootstrap\r\n* CSS框架：Bootstrap\r\n* 富文本：Ueditor、editor.md\r\n\r\n# 系统结构\r\n![系统结构](http://itechyou.cn/uploads/allimg/180930/1-1P9301149510-L.jpg \"系统结构\")\r\n\r\n# 开发环境\r\n建议开发者使用以下环境，这样避免版本带来的问题\r\n* IDE：Spring Tool Suite（STS）\r\n* DB：Mysql 5.7\r\n* JDK：jdk8\r\n\r\n# 快速入门\r\n* 克隆项目到本地\r\n* 导入Eclipse或Sts等开发工具\r\n* 项目需要Redis，请自行修改application.yml中Redis配置\r\n* 将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录\r\n* 运行项目DreamerBlogApplication.java\r\n* 项目首页：http://localhost:8888；项目管理后台：http://localhost:8888/admin\r\n* 管理后台用户名：wangjn；密码：123456\r\n\r\n# 系统美图\r\n![后台登录](http://itechyou.cn/uploads/allimg/180930/1-1P9301135560-L.png)\r\n\r\n![系统管理](http://itechyou.cn/uploads/allimg/180930/1-1P9301136210-L.png)\r\n\r\n![发布文章](http://itechyou.cn/uploads/allimg/180930/1-1P9301136400-L.png)\r\n\r\n![前台首页](http://itechyou.cn/uploads/allimg/180930/1-1P930113A10-L.png)', '<h1 id=\"h1--cms-j2ee-\"><a name=\"梦想家CMS内容管理系统开源版J2EE代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>梦想家CMS内容管理系统开源版J2EE代码</h1><p>当前版本：1.0.1</p>\r\n<p>梦想家CMS官网：<a href=\"www.itechyou.cn\">http://www.itechyou.cn</a></p>\r\n<p>QQ群交流：</p>\r\n<ul>\r\n<li>①597652651</li></ul>\r\n<p>Dreamer CMS 梦想家内容发布系统是国内首款java开发的内容发布系统，采用最流行的springboot+thymeleaf框架搭建，灵活小巧，配置简单。</p>\r\n<h1 id=\"h1-u7279u70B9\"><a name=\"特点\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>特点</h1><ul>\r\n<li>免费完整开源：基于MIT协议，源代码完全开源；</li><li>标签化建站：不需要专业的后台开发技能，只要使用系统提供的标签，就能轻松建设网站；</li><li>模版开发方便：支持在线上传模版包开发方便快捷；</li><li>每月更新：每月进行系统升级，分享更多好用的模版与插件；</li></ul>\r\n<h1 id=\"h1-u9762u5411u5BF9u8C61\"><a name=\"面向对象\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>面向对象</h1><ul>\r\n<li>Dreamer CMS是企业在创立初期很好的技术基础框架，加快公司项目开发进度，当然也可以对现有的系统进行升级；</li><li>个人开发者也可以使用Dreamer CMS承接外包项目；</li><li>初学JAVA的同学可以下载源代码来进行学习交流；</li></ul>\r\n<h1 id=\"h1-u6280u672Fu6846u67B6\"><a name=\"技术框架\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>技术框架</h1><ul>\r\n<li>核心框架：Spring Boot 2</li><li>安全框架：Apache Shiro 1.5</li><li>视图框架：Spring MVC 4</li><li>持久层框架：MyBatis 3</li><li>日志管理：Log4j2</li><li>模版框架：Thymeleaf</li><li>JS框架：jQuery，Bootstrap</li><li>CSS框架：Bootstrap</li><li>富文本：Ueditor、editor.md</li></ul>\r\n<h1 id=\"h1-u7CFBu7EDFu7ED3u6784\"><a name=\"系统结构\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>系统结构</h1><p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301149510-L.jpg\" alt=\"系统结构\" title=\"系统结构\">\r\n<h1 id=\"h1-u5F00u53D1u73AFu5883\"><a name=\"开发环境\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>开发环境</h1><p>建议开发者使用以下环境，这样避免版本带来的问题</p>\r\n<ul>\r\n<li>IDE：Spring Tool Suite（STS）</li><li>DB：Mysql 5.7</li><li>JDK：jdk8</li></ul>\r\n<h1 id=\"h1-u5FEBu901Fu5165u95E8\"><a name=\"快速入门\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>快速入门</h1><ul>\r\n<li>克隆项目到本地</li><li>导入Eclipse或Sts等开发工具</li><li>项目需要Redis，请自行修改application.yml中Redis配置</li><li>将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录</li><li>运行项目DreamerBlogApplication.java</li><li>项目首页：<a href=\"http://localhost:8888；项目管理后台：http://localhost:8888/admin\">http://localhost:8888；项目管理后台：http://localhost:8888/admin</a></li><li>管理后台用户名：wangjn；密码：123456</li></ul>\r\n<h1 id=\"h1-u7CFBu7EDFu7F8Eu56FE\"><a name=\"系统美图\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>系统美图</h1><p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301135560-L.png\" alt=\"后台登录\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301136210-L.png\" alt=\"系统管理\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P9301136400-L.png\" alt=\"发布文章\">\r\n<p><img src=\"http://itechyou.cn/uploads/allimg/180930/1-1P930113A10-L.png\" alt=\"前台首页\">', 'ce1c25b233c0422bb136c27aca4b38f4', '1', '2', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:28:41', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:44:54', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('ce1c25b233c0422bb136c27aca4b38f4', '开发文档', 'document', '822n58d3', '.822n58d3', 'c17ddcad9fb149a6bf90f7bba0e0696b', '20190819\\bbe58a0cbf1a473d81d0479aad753e2a.png', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '', '', '-1', '1', '1', '3', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:53:38', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:27:26', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('cea6987366f54fe7b9f0d331aa5e226e', '标签化建站', 'tagging', 'cyp0c2ed', '.4ol61bbh.tnleskz4.cyp0c2ed', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', 'd9fea2574384477199d038ac66ebd095', '0', '3', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 16:47:35', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 08:58:47', 'fa fa-code', '', '', '', '');
INSERT INTO `system_category` VALUES ('d63c35e50d1f42f49904f263b5571a87', '使用说明', 'quickstart', '76crpo3s', '.76crpo3s', 'c17ddcad9fb149a6bf90f7bba0e0696b', '20190819\\7600b31a769b470d87a013254f85dccf.png', '', '1', '10', '1', 'quickstart', '/quickstart.html', '', '', '', 'md', '####1.克隆项目到本地\r\n####2.导入Eclipse或Sts等开发工具\r\n####3.项目需要Redis，请自行修改application.yml中Redis配置\r\n####4.将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录\r\n####5.运行项目DreamerBlogApplication.java\r\n####6.项目首页：http://localhost:8888\r\n####7.项目管理后台：http://localhost:8888/admin\r\n####8.管理后台用户名：wangjn；密码：123456', '<h4 id=\"h4-1-\"><a name=\"1.克隆项目到本地\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.克隆项目到本地</h4><h4 id=\"h4-2-eclipse-sts-\"><a name=\"2.导入Eclipse或Sts等开发工具\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.导入Eclipse或Sts等开发工具</h4><h4 id=\"h4-3-redis-application-yml-redis-\"><a name=\"3.项目需要Redis，请自行修改application.yml中Redis配置\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.项目需要Redis，请自行修改application.yml中Redis配置</h4><h4 id=\"h4-4-dreamer-blog-zip-d-\"><a name=\"4.将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.将项目主页附件标签页里的dreamer-blog.zip附件下载并解压到D盘根目录</h4><h4 id=\"h4-5-dreamerblogapplication-java\"><a name=\"5.运行项目DreamerBlogApplication.java\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>5.运行项目DreamerBlogApplication.java</h4><h4 id=\"h4-6-http-localhost-8888\"><a name=\"6.项目首页：  http://localhost:8888\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>6.项目首页：<a href=\"http://localhost:8888\">http://localhost:8888</a></h4><h4 id=\"h4-7-http-localhost-8888-admin\"><a name=\"7.项目管理后台：  http://localhost:8888/admin\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>7.项目管理后台：<a href=\"http://localhost:8888/admin\">http://localhost:8888/admin</a></h4><h4 id=\"h4-8-wangjn-123456\"><a name=\"8.管理后台用户名：wangjn；密码：123456\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>8.管理后台用户名：wangjn；密码：123456</h4>', '-1', '1', '1', '4', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:54:13', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 15:12:23', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('d90066ad28e04b2bb59ba446246b7191', '引入文件标签', 'Include Label', '8d119618', '.822n58d3.u8xo56nx.8d119618', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 变量标签dreamer-cms:include\r\n### 1.标签说明\r\n该标签作用是引入其它模板的内容，以方便拆分头部、尾部等公共部分。该标签属于单标签，也可以写成{dreamer-cms:include file=\"xxx.html\"}{/dreamer-cms:include}。该标签只有一个file属性，并适用于所有模版，建议使用单标签\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\nfile|header.html|引入当前模版同目录下的名为header.html的头部模版\r\n\r\n### 2.示例代码\r\n```html\r\n{dreamer-cms:include file=\'header.html\'/}\r\n```\r\n### 3.效果\r\n![include标签](http://www.itechyou.cn./uploads/allimg/190819/1-1ZQ91KI60-L.png \"include标签\")', '<h1 id=\"h1--dreamer-cms-include\"><a name=\"变量标签dreamer-cms:include\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>变量标签dreamer-cms:include</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签作用是引入其它模板的内容，以方便拆分头部、尾部等公共部分。该标签属于单标签，也可以写成{dreamer-cms:include file=”xxx.html”}{/dreamer-cms:include}。该标签只有一个file属性，并适用于所有模版，建议使用单标签</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">file</td>\r\n<td style=\"text-align:center\">header.html</td>\r\n<td style=\"text-align:center\">引入当前模版同目录下的名为header.html的头部模版</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.示例代码</h3><pre><code class=\"lang-html\">{dreamer-cms:include file=\'header.html\'/}\r\n</code></pre>\r\n<h3 id=\"h3-3-\"><a name=\"3.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.效果</h3><p><img src=\"http://www.itechyou.cn./uploads/allimg/190819/1-1ZQ91KI60-L.png\" alt=\"include标签\" title=\"include标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '4', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 17:59:27', null, '2019-08-19 18:00:00', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('d9c8397d7474485e879b4e5d58992609', '模版路径标签', 'Template Path Label', 'a44xd784', '.822n58d3.u8xo56nx.a44xd784', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 变量标签dreamer-cms:template\r\n### 1.标签说明\r\n该标签作用是获取当前模板的路径，以方便加载静态资源。该标签属于单标签，也可以写成{dreamer-cms:template}{/dreamer-cms:template}。该标签没有属性，并适用于所有模版，建议使用单标签\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\n无|无|无\r\n\r\n### 2.示例代码\r\n```html\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}bootstrap/css/bootstrap.min.css\">\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/font-awesome.min.css\">\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}plugins/slick/slick.css\">\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}plugins/slick/slick-theme.css\">\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/app.css\">\r\n<link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/responsive.css\">\r\n<script src=\"{dreamer-cms:template /}js/jquery-3.2.1.slim.min.js\"></script>\r\n<script src=\"{dreamer-cms:template /}bootstrap/js/bootstrap.min.js\"></script>\r\n<script src=\"{dreamer-cms:template /}js/popper.min.js\"></script>\r\n<script src=\"{dreamer-cms:template /}plugins/slick/slick.min.js\"></script>\r\n<script src=\"{dreamer-cms:template /}js/app.js\"></script>\r\n```\r\n### 3.效果\r\n无', '<h1 id=\"h1--dreamer-cms-template\"><a name=\"变量标签dreamer-cms:template\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>变量标签dreamer-cms:template</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签作用是获取当前模板的路径，以方便加载静态资源。该标签属于单标签，也可以写成{dreamer-cms:template}{/dreamer-cms:template}。该标签没有属性，并适用于所有模版，建议使用单标签</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">无</td>\r\n<td style=\"text-align:center\">无</td>\r\n<td style=\"text-align:center\">无</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.示例代码</h3><pre><code class=\"lang-html\">&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}bootstrap/css/bootstrap.min.css\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/font-awesome.min.css\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}plugins/slick/slick.css\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}plugins/slick/slick-theme.css\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/app.css\"&gt;\r\n&lt;link rel=\"stylesheet\" href=\"{dreamer-cms:template /}css/responsive.css\"&gt;\r\n&lt;script src=\"{dreamer-cms:template /}js/jquery-3.2.1.slim.min.js\"&gt;&lt;/script&gt;\r\n&lt;script src=\"{dreamer-cms:template /}bootstrap/js/bootstrap.min.js\"&gt;&lt;/script&gt;\r\n&lt;script src=\"{dreamer-cms:template /}js/popper.min.js\"&gt;&lt;/script&gt;\r\n&lt;script src=\"{dreamer-cms:template /}plugins/slick/slick.min.js\"&gt;&lt;/script&gt;\r\n&lt;script src=\"{dreamer-cms:template /}js/app.js\"&gt;&lt;/script&gt;\r\n</code></pre>\r\n<h3 id=\"h3-3-\"><a name=\"3.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.效果</h3><p>无</p>', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '3', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 17:53:42', null, '2019-08-19 18:00:00', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('d9fea2574384477199d038ac66ebd095', '框架特点', 'feature', 'tnleskz4', '.4ol61bbh.tnleskz4', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', '4e1c58e65fa9423482cf1a29a6ef629b', '0', '2', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 16:33:50', null, '2019-08-19 08:58:47', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('dc10045aed08446bb2d0e5c5d5bf1ae7', '频道标签', 'Channel Label', '6y146016', '.822n58d3.u8xo56nx.6y146016', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 频道标签dreamer-cms:channel\r\n### 1.标签说明\r\n该标签是常用标记，主要用于显示页面的栏目分类并适用于所有模版，通常用于网站顶部以获取站点栏目信息。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\ntypeid|4ol61bbh|后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目\r\nstart|0|从第几个开始获取\r\nlength|5|获取个数\r\ntype|top或son|显示类型（top为顶级栏目，son在设置typeid时起作用，获取指定栏目的下级栏目）\r\nshowall|true或false|是否显示隐藏栏目\r\n\r\n### 2.返回字段说明\r\n\r\n属性名|字段类型|描述\r\n:--:|:--:|:--:\r\ntypeid|String|栏目ID\r\ntypenamecn|String|栏目中文名称\r\ntypenameen|String|栏目英文名称\r\ntypecode|String|栏目编码（8位唯一）\r\ntypeseq|String|栏目的层级\r\ntypeimg|String|栏目图片\r\ndescription|String|栏目描述\r\nlinktarget|String|链接目标\r\npagesize|Integer|分页大小（该属性控制列表页面默认的分页大小）\r\nvisiturl|String|访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）\r\nlinkurl|String|链接URL\r\neditor|String|编辑器（md或ue）\r\nmdcontent|String|markdown内容\r\nhtmlcontent|String|生成的html内容\r\nparentname|String|上级栏目名称\r\nisshow|Integer|是否显示\r\nlevel|Integer|栏目层级\r\nsort|Integer|排序（默认为50）\r\ncreateby|String|创建人\r\ncreatetime|Date|创建时间\r\nupdateby|String|更新人\r\nupdatetime|Date|更新时间\r\ntypeurl|String|栏目URL\r\next01|String|扩展字段1\r\next02|String|扩展字段2\r\next03|String|扩展字段3\r\next04|String|扩展字段4\r\next05|String|扩展字段5\r\nhaschildren|Boolean|是否有下级栏目（该属性在dreamer-cms:type标签下不可用）\r\n\r\n### 3.示例代码\r\n```html\r\n<div class=\"popup\" id=\"popup\">\r\n    <div class=\"popup-content\"><div class=\"left-slide-sidebar \">\r\n        <div class=\"canvas-title\">DreamerCMS MENUS</div>\r\n        <ul class=\"list-unstyled slide-sidebar\">\r\n            <li><a href=\"#\">首页</a></li>\r\n            {dreamer-cms:channel type=\'top\' currentstyle=\"\"}\r\n            <li><a href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\">[field:typenamecn /]</a></li>\r\n            {/dreamer-cms:channel}\r\n        </ul>\r\n    </div>\r\n        <a href=\"#close\" class=\"popup-close\">&times;</a>\r\n    </div>\r\n</div>\r\n```\r\n### 4.效果\r\n![channel标签](http://img.075400.cn/20190820092154.png \"channel标签\")', '<h1 id=\"h1--dreamer-cms-channel\"><a name=\"频道标签dreamer-cms:channel\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>频道标签dreamer-cms:channel</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>该标签是常用标记，主要用于显示页面的栏目分类并适用于所有模版，通常用于网站顶部以获取站点栏目信息。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">4ol61bbh</td>\r\n<td style=\"text-align:center\">后台栏目管理列表页面中的编码，通过唯一编码，获取指定栏目</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">start</td>\r\n<td style=\"text-align:center\">0</td>\r\n<td style=\"text-align:center\">从第几个开始获取</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">length</td>\r\n<td style=\"text-align:center\">5</td>\r\n<td style=\"text-align:center\">获取个数</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">type</td>\r\n<td style=\"text-align:center\">top或son</td>\r\n<td style=\"text-align:center\">显示类型（top为顶级栏目，son在设置typeid时起作用，获取指定栏目的下级栏目）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">showall</td>\r\n<td style=\"text-align:center\">true或false</td>\r\n<td style=\"text-align:center\">是否显示隐藏栏目</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">字段类型</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">typeid</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目ID</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenamecn</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目中文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typenameen</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目英文名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typecode</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目编码（8位唯一）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeseq</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目的层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeimg</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目图片</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">description</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目描述</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linktarget</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接目标</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">pagesize</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">分页大小（该属性控制列表页面默认的分页大小）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">visiturl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">访问URL（浏览器地址栏中将以该url显示，如果为空，则为栏目中文名称的拼音）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">linkurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">链接URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">editor</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">编辑器（md或ue）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">mdcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">markdown内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">htmlcontent</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">生成的html内容</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">parentname</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">上级栏目名称</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">isshow</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">是否显示</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">level</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">栏目层级</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">sort</td>\r\n<td style=\"text-align:center\">Integer</td>\r\n<td style=\"text-align:center\">排序（默认为50）</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">创建人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">createtime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">创建时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updateby</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">更新人</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">updatetime</td>\r\n<td style=\"text-align:center\">Date</td>\r\n<td style=\"text-align:center\">更新时间</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">typeurl</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">栏目URL</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext01</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段1</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext02</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段2</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext03</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段3</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext04</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段4</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">ext05</td>\r\n<td style=\"text-align:center\">String</td>\r\n<td style=\"text-align:center\">扩展字段5</td>\r\n</tr>\r\n<tr>\r\n<td style=\"text-align:center\">haschildren</td>\r\n<td style=\"text-align:center\">Boolean</td>\r\n<td style=\"text-align:center\">是否有下级栏目（该属性在dreamer-cms:type标签下不可用）</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">&lt;div class=\"popup\" id=\"popup\"&gt;\r\n    &lt;div class=\"popup-content\"&gt;&lt;div class=\"left-slide-sidebar \"&gt;\r\n        &lt;div class=\"canvas-title\"&gt;DreamerCMS MENUS&lt;/div&gt;\r\n        &lt;ul class=\"list-unstyled slide-sidebar\"&gt;\r\n            &lt;li&gt;&lt;a href=\"#\"&gt;首页&lt;/a&gt;&lt;/li&gt;\r\n            {dreamer-cms:channel type=\'top\' currentstyle=\"\"}\r\n            &lt;li&gt;&lt;a href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\"&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n            {/dreamer-cms:channel}\r\n        &lt;/ul&gt;\r\n    &lt;/div&gt;\r\n        &lt;a href=\"#close\" class=\"popup-close\"&gt;&amp;times;&lt;/a&gt;\r\n    &lt;/div&gt;\r\n&lt;/div&gt;\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p><img src=\"http://img.075400.cn/20190820092154.png\" alt=\"channel标签\" title=\"channel标签\">', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '6', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 09:38:41', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 09:51:47', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('dfd4ca19e29841d99a33cc4829b7033a', '免费开源', 'opensource', '01k96rfd', '.4ol61bbh.tnleskz4.01k96rfd', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '20', '1', '', '', '', '', '', 'md', '', '', 'd9fea2574384477199d038ac66ebd095', '0', '3', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 16:46:53', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 08:58:47', 'fa fa-github', '', '', '', '');
INSERT INTO `system_category` VALUES ('e5297d56df9a4e8ead60c4347e2dd5c4', '开发社区', 'community', '71e2w1xx', '.71e2w1xx', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '3', '', '', '', '', 'http://www.itechyou.cn', 'md', '', '', '-1', '1', '1', '6', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:55:33', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 14:07:40', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('e65a0268deb14b9fa7cd6c057034e886', '判断标签', 'If Label', 'g4x40i76', '.822n58d3.u8xo56nx.g4x40i76', 'c17ddcad9fb149a6bf90f7bba0e0696b', '', '', '1', '10', '1', 'document', '/document.html', '', '', '', 'md', '# 判断标签dreamer-cms:if\r\n### 1.标签说明\r\n这个标签是用于在页面中作简单的判断，目前只支持eq和neq。\r\n\r\n属性名|取值范围|描述\r\n:--:|:--:|:--:\r\ntest|(\'true\' eq [field:haschildren/])|表达式（注：一定要用小括号包裹，目前只支持eq和neq。）\r\n\r\n### 2.返回字段说明\r\n无\r\n\r\n### 3.示例代码\r\n```html\r\n{dreamer-cms:if test=\"(\'true\' eq [field:haschildren/])\"}\r\n<li class=\"dropdown nav-item\"><a href=\"[field:typeurl/]\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\">[field:typenamecn /]</a>\r\n	<ul class=\"dropdown-menu\">\r\n		{dreamer-cms:channel}\r\n		<li><a class=\"dropdown-item\" href=\"[field:typeurl/]\"><i class=\"ion ion-chevron-right\"></i>[field:typenamecn /]</a></li>\r\n		{/dreamer-cms:channel}\r\n	</ul>\r\n</li>\r\n{/dreamer-cms:if}\r\n{dreamer-cms:if test=\"(\'false\' eq [field:haschildren/])\"}\r\n<li class=\"nav-item\"><a class=\"nav-link scroll\" href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\">[field:typenamecn /]</a></li>\r\n{/dreamer-cms:if}\r\n```\r\n### 4.效果\r\n无', '<h1 id=\"h1--dreamer-cms-if\"><a name=\"判断标签dreamer-cms:if\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>判断标签dreamer-cms:if</h1><h3 id=\"h3-1-\"><a name=\"1.标签说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>1.标签说明</h3><p>这个标签是用于在页面中作简单的判断，目前只支持eq和neq。</p>\r\n<table>\r\n<thead>\r\n<tr>\r\n<th style=\"text-align:center\">属性名</th>\r\n<th style=\"text-align:center\">取值范围</th>\r\n<th style=\"text-align:center\">描述</th>\r\n</tr>\r\n</thead>\r\n<tbody>\r\n<tr>\r\n<td style=\"text-align:center\">test</td>\r\n<td style=\"text-align:center\">(‘true’ eq [field:haschildren/])</td>\r\n<td style=\"text-align:center\">表达式（注：一定要用小括号包裹，目前只支持eq和neq。）</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3 id=\"h3-2-\"><a name=\"2.返回字段说明\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>2.返回字段说明</h3><p>无</p>\r\n<h3 id=\"h3-3-\"><a name=\"3.示例代码\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>3.示例代码</h3><pre><code class=\"lang-html\">{dreamer-cms:if test=\"(\'true\' eq [field:haschildren/])\"}\r\n&lt;li class=\"dropdown nav-item\"&gt;&lt;a href=\"[field:typeurl/]\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\"&gt;[field:typenamecn /]&lt;/a&gt;\r\n    &lt;ul class=\"dropdown-menu\"&gt;\r\n        {dreamer-cms:channel}\r\n        &lt;li&gt;&lt;a class=\"dropdown-item\" href=\"[field:typeurl/]\"&gt;&lt;i class=\"ion ion-chevron-right\"&gt;&lt;/i&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n        {/dreamer-cms:channel}\r\n    &lt;/ul&gt;\r\n&lt;/li&gt;\r\n{/dreamer-cms:if}\r\n{dreamer-cms:if test=\"(\'false\' eq [field:haschildren/])\"}\r\n&lt;li class=\"nav-item\"&gt;&lt;a class=\"nav-link scroll\" href=\"[field:typeurl /]\" title=\"[field:typenamecn /]\"&gt;[field:typenamecn /]&lt;/a&gt;&lt;/li&gt;\r\n{/dreamer-cms:if}\r\n</code></pre>\r\n<h3 id=\"h3-4-\"><a name=\"4.效果\" class=\"reference-link\"></a><span class=\"header-link octicon octicon-link\"></span>4.效果</h3><p>无</p>', '8923a09d403f49a289f3af780dcca0c2', '1', '3', '50', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 10:42:23', null, '2019-08-20 10:42:24', '', '', '', '', '');
INSERT INTO `system_category` VALUES ('e6cdbf119da14a30a86a0f9f2189b8e5', '模版中心', 'templates', 'a54547w2', '.a54547w2', '9b5112043c2240d8823bd15759c67604', '', '', '1', '10', '2', 'templates', '', '/list_templates.html', '/article_templates.html', '', 'md', '', '', '-1', '1', '1', '5', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 15:55:05', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-19 13:51:07', '', '', '', '', '');

-- ----------------------------
-- Table structure for system_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `system_evaluate`;
CREATE TABLE `system_evaluate` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `aid` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主表ID',
  `ext01` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext02` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext03` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext04` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext05` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `evaluator` varchar(64) COLLATE utf8mb4_bin DEFAULT '',
  `source` varchar(255) COLLATE utf8mb4_bin DEFAULT '',
  `content` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_evaluate
-- ----------------------------
INSERT INTO `system_evaluate` VALUES ('02238eaa19b44f7bbb2203f01a92ee8f', '18f228df7b8443dfa1039fe3c3e53503', null, null, null, null, null, '程雨', 'www.xinhudong.com.cn', 0x447265616D657220434D53E698AFE4BC81E4B89AE59CA8E5889BE7AB8BE5889DE69C9FE5BE88E5A5BDE79A84E68A80E69CAFE59FBAE7A180E6A186E69EB6EFBC8CE58AA0E5BFABE585ACE58FB8E9A1B9E79BAEE5BC80E58F91E8BF9BE5BAA6EFBC8CE5BD93E784B6E4B99FE58FAFE4BBA5E5AFB9E78EB0E69C89E79A84E7B3BBE7BB9FE8BF9BE8A18CE58D87E7BAA7);
INSERT INTO `system_evaluate` VALUES ('c70f70e7dd5a4cfa87e0d5c89cfb6ff7', '779bc296ea4a4149bd3c97cdb8cd8593', null, null, null, null, null, '王俊南', 'www.itechyou.cn', 0xE6A087E7ADBEE58C96E5BBBAE7AB99EFBC9AE4B88DE99C80E8A681E4B893E4B89AE79A84E5908EE58FB0E5BC80E58F91E68A80E883BDEFBC8CE58FAAE8A681E4BDBFE794A8E7B3BBE7BB9FE68F90E4BE9BE79A84E6A087E7ADBEEFBC8CE5B0B1E883BDE8BDBBE69DBEE5BBBAE8AEBEE7BD91E7AB99EFBC9BE6A8A1E78988E5BC80E58F91E696B9E4BEBFEFBC9AE694AFE68C81E59CA8E7BABFE4B88AE4BCA0E6A8A1E78988E58C85E5BC80E58F91E696B9E4BEBFE5BFABE68DB7);
INSERT INTO `system_evaluate` VALUES ('63aac7b1d6ca4f81ac051b2e8d79198f', 'e738f12d050849fd9998ac01fd0f5ad7', null, null, null, null, null, '王亚伟', 'www.itechyou.cn', 0xE5858DE8B4B9E5AE8CE695B4E5BC80E6BA90EFBC9AE59FBAE4BA8E4D4954E58D8FE8AEAEEFBC8CE6BA90E4BBA3E7A081E5AE8CE585A8E5BC80E6BA90EFBC9BE5889DE5ADA64A415641E79A84E5908CE5ADA6E58FAFE4BBA5E4B88BE8BDBDE6BA90E4BBA3E7A081E69DA5E8BF9BE8A18CE5ADA6E4B9A0E4BAA4E6B581);

-- ----------------------------
-- Table structure for system_fields
-- ----------------------------
DROP TABLE IF EXISTS `system_fields`;
CREATE TABLE `system_fields` (
  `id` varchar(32) NOT NULL,
  `form_id` varchar(32) NOT NULL,
  `field_text` varchar(32) DEFAULT NULL COMMENT '字段提示名',
  `field_name` varchar(128) NOT NULL COMMENT '字段名',
  `type` int(11) DEFAULT '1' COMMENT '类型：0系统自动生成表单字段',
  `data_type` varchar(32) DEFAULT NULL COMMENT '数据类型',
  `default_value` varchar(128) DEFAULT NULL COMMENT '默认值',
  `max_length` int(11) DEFAULT NULL COMMENT '最大长度',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ext01` varchar(32) DEFAULT NULL,
  `ext02` varchar(32) DEFAULT NULL,
  `ext03` varchar(32) DEFAULT NULL,
  `ext04` varchar(32) DEFAULT NULL,
  `ext05` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_fields
-- ----------------------------
INSERT INTO `system_fields` VALUES ('01a3d20d81f24d678d50238fcd5892ca', 'c17ddcad9fb149a6bf90f7bba0e0696b', '文章内容', 'content', '1', 'html', '', '200', '9f4b807db2e94670bb02cdc212ea7389', '2018-09-21 11:51:24', null, '2018-09-21 11:51:24', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('125fcd235ab5419ca50e513485f359cf', '9b5112043c2240d8823bd15759c67604', '内容', 'body', '1', 'html', '', '200', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-20 11:30:53', null, '2019-08-20 11:30:53', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('574d8ab867e948a4ab8bed0eb9162bf1', '440dc002ad4846018d7a1b9433021b22', '版本号', 'versionnum', '1', 'varchar', '', '128', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:17:06', null, '2019-08-16 10:17:06', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('74e71cc418494c18bd2b2026fa0b21db', '440dc002ad4846018d7a1b9433021b22', '升级说明', 'notes', '1', 'markdown', '', '1024', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:17:51', null, '2019-08-16 10:17:51', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('86601971f1bb47bd81e06f2167ba4398', '06c540abf3a64d9699762711439c82dd', '客户来源', 'source', '1', 'varchar', '', '255', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:05:41', null, '2019-08-16 09:05:41', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('a2b7dcd22d1042ef929e2d17c97e747c', '02c73a0271644e8c91cf2400d9325e93', '链接地址', 'link_url', '1', 'varchar', '', '256', '9f4b807db2e94670bb02cdc212ea7389', '2018-09-25 14:33:17', null, '2018-09-25 14:33:16', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('e7d3405934a148c19a7fc1327551a146', '06c540abf3a64d9699762711439c82dd', '评价内容', 'content', '1', 'textarea', '', '200', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:06:19', null, '2019-08-16 09:06:19', null, null, null, null, null);
INSERT INTO `system_fields` VALUES ('f312ae651c254b0b88bf090ae3db6c3f', '06c540abf3a64d9699762711439c82dd', '评价人', 'evaluator', '1', 'varchar', '', '64', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:05:18', null, '2019-08-16 09:05:18', null, null, null, null, null);

-- ----------------------------
-- Table structure for system_forms
-- ----------------------------
DROP TABLE IF EXISTS `system_forms`;
CREATE TABLE `system_forms` (
  `id` varchar(32) NOT NULL,
  `form_name` varchar(32) DEFAULT NULL COMMENT '表单名',
  `table_name` varchar(128) NOT NULL COMMENT '表名',
  `code` varchar(32) DEFAULT NULL COMMENT '编码系统自动生成',
  `type` int(11) DEFAULT '1' COMMENT '类型：0系统模型，1自定义模型',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ext01` varchar(32) DEFAULT NULL,
  `ext02` varchar(32) DEFAULT NULL,
  `ext03` varchar(32) DEFAULT NULL,
  `ext04` varchar(32) DEFAULT NULL,
  `ext05` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_forms
-- ----------------------------
INSERT INTO `system_forms` VALUES ('02c73a0271644e8c91cf2400d9325e93', '友情链接', 'links', '80AZ783M', '1', '9f4b807db2e94670bb02cdc212ea7389', '2018-09-25 14:30:02', null, '2018-09-25 14:30:01', null, null, null, null, null);
INSERT INTO `system_forms` VALUES ('06c540abf3a64d9699762711439c82dd', '客户评价', 'evaluate', '2EFPN7JY', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 09:04:22', null, '2019-08-16 09:04:22', null, null, null, null, null);
INSERT INTO `system_forms` VALUES ('440dc002ad4846018d7a1b9433021b22', '更新记录', 'versionnotes', 'LUVC0AW2', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 10:16:31', null, '2019-08-16 10:16:31', null, null, null, null, null);
INSERT INTO `system_forms` VALUES ('9b5112043c2240d8823bd15759c67604', '模版中心', 'templates', '3NF20KCW', '1', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-15 17:26:20', null, '2019-08-15 17:26:20', null, null, null, null, null);
INSERT INTO `system_forms` VALUES ('c17ddcad9fb149a6bf90f7bba0e0696b', '普通文章', 'article', 'V1WL66A0', '0', '9f4b807db2e94670bb02cdc212ea7389', '2018-09-12 09:59:38', null, '2018-09-12 10:26:00', 'selected', null, null, null, null);

-- ----------------------------
-- Table structure for system_labels
-- ----------------------------
DROP TABLE IF EXISTS `system_labels`;
CREATE TABLE `system_labels` (
  `id` varchar(32) NOT NULL,
  `tag_name` varchar(128) NOT NULL COMMENT '标签名称',
  `pinyin` varchar(256) DEFAULT NULL COMMENT '拼音',
  `first_char` varchar(2) DEFAULT NULL COMMENT '首字母',
  `tag_count` varchar(64) NOT NULL COMMENT '标签使用次数',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_labels
-- ----------------------------
INSERT INTO `system_labels` VALUES ('0ebca91a708a4fd8ba9548c3817e437d', '32', '32', '3', '2', null, '2019-12-18 02:09:02', null, '2019-12-18 10:09:02');
INSERT INTO `system_labels` VALUES ('241d2e14a9764795acc29ff6b4ef0a4a', 'hbase', 'hbase', 'h', '1', null, '2018-09-27 11:29:08', null, '2018-09-27 11:29:08');
INSERT INTO `system_labels` VALUES ('27ee21fd44ee4b3996d2546e700ca68a', 'css', 'css', 'c', '1', null, '2018-09-21 12:47:09', null, '2018-09-21 12:47:09');
INSERT INTO `system_labels` VALUES ('2df0b5c5dc474fc38969ce4fab5b1ee7', '花开', 'huakai', 'h', '1', null, '2018-09-21 13:40:16', null, '2018-09-21 13:40:15');
INSERT INTO `system_labels` VALUES ('30b42859b3b2400396b123947fb4f5b4', '单例', 'danli', 'd', '1', null, '2018-09-27 10:54:28', null, '2018-09-27 10:54:28');
INSERT INTO `system_labels` VALUES ('3459b5788ec04732b336c484db6c8806', '大数据', 'dashuju', 'd', '1', null, '2018-09-27 11:29:08', null, '2018-09-27 11:29:08');
INSERT INTO `system_labels` VALUES ('3808d4ebd78a4861bc49cf3d653000aa', '职业发展', 'zhiyefazhan', 'z', '1', null, '2018-09-21 13:44:36', null, '2018-09-21 13:44:35');
INSERT INTO `system_labels` VALUES ('4ccffe77671a4c3bbbf7854fdc44d0c8', 'css3', 'css3', 'c', '1', null, '2019-08-20 11:32:05', null, '2019-08-20 11:32:05');
INSERT INTO `system_labels` VALUES ('5b471f24146144899e5227a5dc4eaadd', '原型', 'yuanxing', 'y', '1', null, '2018-09-27 11:19:00', null, '2018-09-27 11:18:59');
INSERT INTO `system_labels` VALUES ('5e7586fcb4d44c15a3d94e93f6281c54', 'eee', 'eee', 'e', '1', null, '2019-12-18 02:18:59', null, '2019-12-18 10:18:59');
INSERT INTO `system_labels` VALUES ('77eae8930c30458aac733a4e778ba498', '商务', 'shangwu', 's', '1', null, '2019-08-21 16:47:41', null, '2019-08-21 16:47:41');
INSERT INTO `system_labels` VALUES ('8daeee953c764a33ba2c91ae0f3610b7', 'html5', 'html5', 'h', '1', null, '2019-08-20 11:32:05', null, '2019-08-20 11:32:05');
INSERT INTO `system_labels` VALUES ('96f315f72dfa498e8e65afe85a22775e', 'hive', 'hive', 'h', '1', null, '2018-09-27 11:29:08', null, '2018-09-27 11:29:08');
INSERT INTO `system_labels` VALUES ('a49146f9af0b43a592998f6072af08bf', 'html', 'html', 'h', '1', null, '2018-09-21 12:47:09', null, '2018-09-21 12:47:08');
INSERT INTO `system_labels` VALUES ('a8e12dfe133a4bd8808e029f8a8c4a05', 'eeee', 'eeee', 'e', '1', null, '2019-12-18 02:21:34', null, '2019-12-18 10:21:34');
INSERT INTO `system_labels` VALUES ('a96f5f9c526d447cbee797b2536af1b3', '要强', 'yaoqiang', 'y', '1', null, '2018-09-21 13:42:05', null, '2018-09-21 13:42:04');
INSERT INTO `system_labels` VALUES ('a9d47da353be4e10aac3e1e23338862a', 'js', 'js', 'j', '1', null, '2018-09-21 12:47:09', null, '2018-09-21 12:47:08');
INSERT INTO `system_labels` VALUES ('cd6cd19104ab4bdf86bad96c0c9ac0cf', 'factory', 'factory', 'f', '1', null, '2018-09-27 11:16:22', null, '2018-09-27 11:16:21');
INSERT INTO `system_labels` VALUES ('d51ddfca743b47f686120300691cecd0', '工厂模式', 'gongchangmoshi', 'g', '1', null, '2018-09-27 11:16:22', null, '2018-09-27 11:16:21');
INSERT INTO `system_labels` VALUES ('e281ef86dfbb4d75b8141f8abfb7291b', '建造者', 'jianzaozhe', 'j', '1', null, '2018-09-27 11:19:00', null, '2018-09-27 11:18:59');
INSERT INTO `system_labels` VALUES ('e39a57377c814132b9db8d7c1105a3db', 'qqq', 'qqq', 'q', '1', null, '2019-12-18 02:34:41', null, '2019-12-18 10:34:41');
INSERT INTO `system_labels` VALUES ('e4bf078774b840b5a13eafa48c03a3a7', 'singleton', 'singleton', 's', '1', null, '2018-09-27 10:54:28', null, '2018-09-27 10:54:27');
INSERT INTO `system_labels` VALUES ('e6d72c095f9c4532a298337e6a612c22', 'script', 'script', 's', '1', null, '2018-09-21 12:47:09', null, '2018-09-21 12:47:09');
INSERT INTO `system_labels` VALUES ('f0a3ce8ba0594bc6975b600eeb33015f', '3333', '3333', '3', '1', null, '2019-12-18 02:14:45', null, '2019-12-18 10:14:45');
INSERT INTO `system_labels` VALUES ('f4d45bfa3b284792b6411390831063d7', 'bootstrap', 'bootstrap', 'b', '1', null, '2019-08-20 11:32:05', null, '2019-08-20 11:32:05');
INSERT INTO `system_labels` VALUES ('f55bab7dc7234dfa9883c181e8d29825', 'hadoop', 'hadoop', 'h', '1', null, '2018-09-27 11:29:08', null, '2018-09-27 11:29:08');
INSERT INTO `system_labels` VALUES ('fdf1a43c32a7454dbfd42626ec4d4263', 'thymeleaf', 'thymeleaf', 't', '1', null, '2018-09-28 16:52:32', null, '2018-09-28 16:52:31');

-- ----------------------------
-- Table structure for system_links
-- ----------------------------
DROP TABLE IF EXISTS `system_links`;
CREATE TABLE `system_links` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `aid` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主表ID',
  `ext01` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext02` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext03` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext04` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext05` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `link_url` varchar(256) COLLATE utf8mb4_bin DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_links
-- ----------------------------
INSERT INTO `system_links` VALUES ('00de5fcc84f44d07ba2fd3b80098dd3a', '47864f1e06334cdc8c87b9419dc63abf', null, null, null, null, null, 'http://www.itechyou.cn');
INSERT INTO `system_links` VALUES ('60534518cfa04fb8bc864119035f6970', '598fb8d59fbf4bdca544d71cb64c1ce0', null, null, null, null, null, 'http://www.baidu.com');

-- ----------------------------
-- Table structure for system_logger
-- ----------------------------
DROP TABLE IF EXISTS `system_logger`;
CREATE TABLE `system_logger` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `level` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '级别',
  `oper_user` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作用户',
  `oper_type` int(11) DEFAULT NULL COMMENT '操作类型',
  `oper_source` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '操作源',
  `ip` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'IP',
  `module` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '模块',
  `browser` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '浏览器',
  `platform` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '平台',
  `content` text COLLATE utf8mb4_bin COMMENT '内容',
  `create_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '创建人',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '更新人',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新时间',
  `extend1` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `extend2` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `extend3` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_logger
-- ----------------------------
INSERT INTO `system_logger` VALUES ('f06074356fbb483eaad89cff6788a3ef', 'INFO', 'wangjn', '1', 'cn.itechyou.cms.controller.admin.UserLoginController.login()', '本地主机', '登录模块', 'Chrome/78.0.3904.70', 'Windows 10', 0xE794A8E688B7E799BBE5BD95E6938DE4BD9C, '9f4b807db2e94670bb02cdc212ea7389', '2020-01-02 13:50:37', null, '0000-00-00 00:00:00', null, null, null);
INSERT INTO `system_logger` VALUES ('9ba812ac58a54282910dedf2c658c339', 'INFO', 'wangjn', '1', 'cn.itechyou.cms.controller.admin.UserLoginController.login()', '本地主机', '登录模块', 'Chrome/78.0.3904.70', 'Windows 10', 0xE794A8E688B7E799BBE5BD95E6938DE4BD9C, '9f4b807db2e94670bb02cdc212ea7389', '2020-01-04 04:42:25', null, '0000-00-00 00:00:00', null, null, null);

-- ----------------------------
-- Table structure for system_pages
-- ----------------------------
DROP TABLE IF EXISTS `system_pages`;
CREATE TABLE `system_pages` (
  `id` varchar(32) NOT NULL,
  `page_name` varchar(128) NOT NULL COMMENT '页面名称',
  `page_url` varchar(32) DEFAULT NULL COMMENT '页面路径',
  `page_temp` varchar(128) NOT NULL COMMENT '缩略图',
  `code` varchar(32) DEFAULT NULL COMMENT '编码',
  `status` int(11) DEFAULT '1' COMMENT '状态1显示0隐藏',
  `default_eidtor` varchar(10) NOT NULL COMMENT '默认编辑器',
  `md_content` text COMMENT 'md内容',
  `html_content` text COMMENT 'html内容',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_pages
-- ----------------------------

-- ----------------------------
-- Table structure for system_permission
-- ----------------------------
DROP TABLE IF EXISTS `system_permission`;
CREATE TABLE `system_permission` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `pName` varchar(255) NOT NULL,
  `rid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_permission
-- ----------------------------

-- ----------------------------
-- Table structure for system_role
-- ----------------------------
DROP TABLE IF EXISTS `system_role`;
CREATE TABLE `system_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `roleName` varchar(255) NOT NULL,
  `roleNum` int(10) DEFAULT NULL,
  `roleUser` varchar(255) DEFAULT NULL,
  `roleCode` int(10) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_role
-- ----------------------------

-- ----------------------------
-- Table structure for system_search
-- ----------------------------
DROP TABLE IF EXISTS `system_search`;
CREATE TABLE `system_search` (
  `id` varchar(32) NOT NULL,
  `keywords` varchar(128) NOT NULL COMMENT '文章标题',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_search
-- ----------------------------
INSERT INTO `system_search` VALUES ('27b96c0b2575442cb34d62d1520865f0', '生命', '2018-09-25 16:55:49');
INSERT INTO `system_search` VALUES ('38f9dd4448864a77b50a2753a01e6566', '123', '2018-09-29 10:13:52');
INSERT INTO `system_search` VALUES ('3a009f62856e4b59b2bbbe8dc856ebe1', '生命', '2018-09-25 16:55:54');
INSERT INTO `system_search` VALUES ('6fd3f7e5383c43d3a80231e04b274366', '网页', '2018-09-29 13:59:37');
INSERT INTO `system_search` VALUES ('71378a798659412ba35c5bf4b049d993', '生涯', '2018-09-29 10:18:10');
INSERT INTO `system_search` VALUES ('78c3ef5c791c41bf98c8f653b424eb3a', '程序员', '2018-09-25 15:17:08');
INSERT INTO `system_search` VALUES ('9a022ea5908746c8be4b617dc843bad7', '程序员', '2018-09-25 15:13:43');
INSERT INTO `system_search` VALUES ('f2c798f6baba4963ae23883d2afe41ee', '程序员', '2018-09-25 15:17:53');

-- ----------------------------
-- Table structure for system_setting
-- ----------------------------
DROP TABLE IF EXISTS `system_setting`;
CREATE TABLE `system_setting` (
  `id` varchar(32) NOT NULL,
  `website` varchar(128) NOT NULL COMMENT '网站地址',
  `title` varchar(64) NOT NULL COMMENT '站点标题',
  `keywords` varchar(128) NOT NULL COMMENT '站点关键词',
  `describe` varchar(128) NOT NULL COMMENT '站点描述',
  `icp` varchar(32) NOT NULL COMMENT '备案号',
  `copyright` varchar(128) NOT NULL COMMENT '版权',
  `uploaddir` varchar(32) DEFAULT 'uploads' COMMENT '文档HTML默认保存路径',
  `appid` varchar(128) DEFAULT NULL COMMENT '畅言appid',
  `appkey` varchar(128) DEFAULT NULL COMMENT '畅言appkey',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_setting
-- ----------------------------
INSERT INTO `system_setting` VALUES ('1', 'http://127.0.0.1:8084/', 'Zhun Dong 融媒体管理系统', 'Zhun Dong 融媒体管理系统', 'Zhun Dong 融媒体管理系统是准东首款java开发的后台发布管理系统。', '疆ICP备19026223号', 'Copyright © 2018 Powered by I Zhun Dong,融媒体，Zhun Dong 融媒体管理系统', 'uploads', 'cytQcJUcb', '2dd87fd430f0509cf00e18de4328e100');

-- ----------------------------
-- Table structure for system_templates
-- ----------------------------
DROP TABLE IF EXISTS `system_templates`;
CREATE TABLE `system_templates` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `aid` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主表ID',
  `ext01` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext02` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext03` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext04` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext05` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `body` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_templates
-- ----------------------------
INSERT INTO `system_templates` VALUES ('989bac83b8004120845fb3f8791936e4', 'd64d167032c84521ad21708cccc1f963', null, null, null, null, null, 0x3C703E3C7374726F6E67207374796C653D2277686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323470783B20636F6C6F723A20726762283235352C203235352C20323535293B20666F6E742D66616D696C793A20417269616C3B20666F6E742D73697A653A20313570783B2070616464696E673A203070783B206D617267696E3A203070783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223EE6A8A1E69DBFE5908DE7A7B0EFBC9A3C2F7370616E3E3C2F7374726F6E673E3C6272207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B222F3E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B223E3C7374726F6E673EE8B4A2E7A88EE8AEB0E8B4A6E5B7A5E59586E6B3A8E5868CE8AEA4E8AF81E585ACE58FB8E7B1BB3C6120687265663D22687474703A2F2F7777772E6465646535382E636F6D2F22207461726765743D225F626C616E6B22207374796C653D22636F6C6F723A207267622837302C2038312C203936293B206C65747465722D73706163696E673A203170783B20746578742D6465636F726174696F6E2D6C696E653A206E6F6E653B223E3C7370616E207374796C653D22746578742D6465636F726174696F6E3A756E6465726C696E653B223EE7BB87E6A2A6E6A8A1E69DBF3C2F7370616E3E3C2F613E28E5B8A6E6898BE69CBAE7ABAF292B50432BE7A7BBE58AA8E7ABAF2BE588A9E4BA8E53454FE4BC98E58C963C2F7374726F6E673E3C2F7370616E3E3C6272207374796C653D22666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B20636F6C6F723A207267622835312C2035312C203531293B20666F6E742D66616D696C793A205461686F6D612C20E5AE8BE4BD932C2047656E6576612C2073616E732D73657269663B206C696E652D6865696768743A2032352E3270783B222F3E3C7374726F6E67207374796C653D2277686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323470783B20636F6C6F723A20726762283235352C203235352C20323535293B20666F6E742D66616D696C793A20417269616C3B20666F6E742D73697A653A20313570783B2070616464696E673A203070783B206D617267696E3A203070783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223EE6A8A1E69DBFE4BB8BE7BB8DEFBC9A3C2F7370616E3E3C2F7374726F6E673E3C6272207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B222F3E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B223EE7BB87E6A2A6E69C80E696B0E58685E6A0B8E5BC80E58F91E79A84E6A8A1E69DBFEFBC8CE8AFA5E6A8A1E69DBFE5B19EE4BA8EE8B4A2E7A88EE8AEB0E8B4A6E38081E5B7A5E59586E6B3A8E5868CE7B1BBE4BC81E4B89AE4BDBFE794A8EFBC8C3C2F7370616E3E3C6272207374796C653D22666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B20636F6C6F723A207267622835312C2035312C203531293B20666F6E742D66616D696C793A205461686F6D612C20E5AE8BE4BD932C2047656E6576612C2073616E732D73657269663B206C696E652D6865696768743A2032352E3270783B222F3E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B223E3C7370616E207374796C653D22636F6C6F723A20726762283235352C20302C2030293B223EE887AAE5B8A6E69C80E696B0E79A84E6898BE69CBAE7A7BBE58AA8E7ABAFEFBC8CE5908CE4B880E4B8AAE5908EE58FB0EFBC8CE695B0E68DAEE58DB3E697B6E5908CE6ADA5EFBC8CE7AE80E58D95E98082E794A8EFBC813C2F7370616E3E3C62722F3EE58E9FE5889BE8AEBEE8AEA1E38081E6898BE5B7A5E4B9A6E586994449562B435353EFBC8C3C62722F3EE5AE8CE7BE8EE585BCE5AEB94945372BE3808146697265666F78E380814368726F6D65E38081333630E6B58FE8A788E599A8E7AD89EFBC9BE4B8BBE6B581E6B58FE8A788E599A8EFBC9B3C62722F3EE9A1B5E99DA2E7AE80E6B481E7AE80E58D95EFBC8CE5AEB9E69893E7AEA1E79086EFBC8C44454445E58685E6A0B8E983BDE58FAFE4BBA5E4BDBFE794A8EFBC9BE99984E5B8A6E6B58BE8AF95E695B0E68DAEEFBC813C2F7370616E3E3C6272207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B222F3E3C2F703E3C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A203070783B2070616464696E673A203070783B20636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323470783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B2077696474683A2037303870783B223E3C7370616E207374796C653D22636F6C6F723A207267622835312C2035312C203531293B20666F6E742D66616D696C793A20417269616C3B20666F6E742D73697A653A20313570783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B20636F6C6F723A20726762283235352C203235352C20323535293B223E3C7374726F6E67207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223EE6A8A1E69DBFE789B9E782B9EFBC9A3C2F7370616E3E3C2F7374726F6E673E3C2F7370616E3E3C2F7370616E3E3C2F703E3C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A203070783B2070616464696E673A203070783B20636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B206C696E652D6865696768743A20323670783B223EE8AFA5E6A8A1E69DBFE5A49AE695B0E4B8BAE58D95E9A1B5E99DA2EFBC8CE4BFAEE694B9E5BE88E5A49AE99C80E8A681E59CA8E4BBA3E7A081E69687E4BBB6E4B88AE4BFAEE694B9EFBC8CE59BBEE78987E5BE88E5A49AE4B99FE99C80E8A681E9809AE8BF87465450E5B7A5E585B7E4BFAEE694B9EFBC8C3C62722F3EE59BA0E4B8BAE8B4A2E7A88EE7B1BBE79A84E9A1B5E99DA2E6AF94E8BE83E5A49AEFBC8CE68980E4BBA5E5BE88E5A49AE983BDE698AFE58699E59CA8E6A8A1E69DBFE69687E4BBB6E4B88AE79A84EFBC813C2F703E3C703E3C7374726F6E67207374796C653D22636F6C6F723A20726762283235352C203235352C20323535293B2070616464696E673A203070783B206D617267696E3A203070783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223E266E6273703BE4BDBFE794A8E7A88BE5BA8FEFBC9A3C2F7370616E3E3C2F7374726F6E673E3C2F703E3C703E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313570783B223EE7BB87E6A2A644454445434D53E78988E69CACE983BDE58FAFE4BBA5E4BDBFE794A8E380823C2F7370616E3E3C62722F3E3C7374726F6E67207374796C653D22636F6C6F723A2072676228302C20302C2030293B20666F6E742D66616D696C793A20417269616C3B20666F6E742D73697A653A20313570783B2070616464696E673A203070783B206D617267696E3A203070783B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B20636F6C6F723A20726762283235352C203235352C20323535293B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223E266E6273703B3C2F7370616E3E3C2F7370616E3E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B20636F6C6F723A20726762283234302C203235352C20323430293B223E3C7370616E207374796C653D2270616464696E673A203070783B206D617267696E3A203070783B207472616E736974696F6E3A20616C6C20302E3273206C696E6561722030733B206261636B67726F756E642D636F6C6F723A2072676228302C203132382C2030293B223EE6A8A1E69DBFE9A1B5E99DA2EFBC9A3C2F7370616E3E3C2F7370616E3E3C2F7374726F6E673E3C62722F3E3C7370616E207374796C653D22636F6C6F723A233436353136303B666F6E742D66616D696C793A4D6963726F736F66742059614865692C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B223E696E6465782E68746D20E9A696E9A1B5E6A8A1E69DBF3C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A233436353136303B666F6E742D66616D696C793A4D6963726F736F66742059614865692C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B223E686561642E68746D3C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A233436353136303B666F6E742D66616D696C793A4D6963726F736F66742059614865692C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B223E666F6F7465722E68746D266E6273703B3C2F7370616E3E3C62722F3E3C2F703E3C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A203070783B2070616464696E673A203070783B20636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B223E61727469636C655F61727469636C652E68746D20E69687E7ABA0E58685E5AEB93C2F703E3C70207374796C653D226D617267696E2D746F703A203070783B206D617267696E2D626F74746F6D3A203070783B2070616464696E673A203070783B223E3C7370616E207374796C653D22636F6C6F723A233436353136303B666F6E742D66616D696C793A4D6963726F736F66742059614865692C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B223EE8BF99E9878CE4B88DE4B880E4B880E58897E587BAEFBC813C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A20726762283235352C203235352C20323535293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059616865692671756F743B2C205461686F6D612C20535448656974693B206261636B67726F756E642D636F6C6F723A207267622835312C203135332C20313032293B223E266E6273703BE6B8A9E9A6A8E68F90E7A4BAEFBC9A3C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313570783B223EE68C89E785A7E6ADA3E5B8B8E79A84E7BB87E6A2A6E5AE89E8A385E6ADA5E9AAA4E69DA5E5AE89E8A385E8BF98E58E9FE5B0B1E58FAFE4BBA5E794A8E4BA86EFBC8CE4BB8EE5908EE58FB0E9878DE696B0E782B9E587BBE4BF9DE5AD98E4B88BE7B3BBE7BB9FE59FBAE69CACE58F82E695B0E3808220E7B3BBE7BB9F2667743BE7B3BBE7BB9FE59FBAE69CACE58F82E695B02667743B20E4BF9DE5AD98EFBC88E7A1AEE5AE9AEFBC89E380823C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A20726762283235352C203235352C20323535293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059616865692671756F743B2C205461686F6D612C20535448656974693B206261636B67726F756E642D636F6C6F723A207267622835312C203135332C20313032293B223E266E6273703BE5908EE69C9F627567E4BFAEE6ADA3EFBC9A3C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A233030303030303B666F6E742D66616D696C793A417269616C223E3C7370616E207374796C653D22666F6E742D73697A653A20313570783B223EE69A82E697A03C2F7370616E3E3C2F7370616E3E3C62722F3E3C7370616E207374796C653D22636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313570783B223E5043E688AAE59BBEEFBC9A3C2F7370616E3E3C2F703E3C703E3C696D6720616C743D2222207372633D222F75706C6F61642F32303139303832302F313536363237313932303233313036373136352E6A706722207374796C653D22626F726465723A206E6F6E653B20636F6C6F723A207267622837302C2038312C203936293B20666F6E742D66616D696C793A202671756F743B4D6963726F736F66742059614865692671756F743B2C205461686F6D612C2048656C7665746963612C20417269616C2C2073616E732D73657269663B20666F6E742D73697A653A20313470783B2077686974652D73706163653A206E6F726D616C3B206261636B67726F756E642D636F6C6F723A20726762283235352C203235352C20323535293B222F3E3C2F703E3C703E3C62722F3E3C2F703E);
INSERT INTO `system_templates` VALUES ('abdf55c143414c7e9c1f16789805c88e', '182306d8eab44225a5ce123ad6a01ffd', null, null, null, null, null, null);
INSERT INTO `system_templates` VALUES ('5ee77e9a26b449fe9ee300fe1b888c77', '93b0ac1621024e4682442acd646da397', null, null, null, null, null, null);
INSERT INTO `system_templates` VALUES ('964090d9b8ae429d9b22d8ba346f2348', 'cc1b4cc70af54410a9486e7d4babd5a7', null, null, null, null, null, '');

-- ----------------------------
-- Table structure for system_theme
-- ----------------------------
DROP TABLE IF EXISTS `system_theme`;
CREATE TABLE `system_theme` (
  `id` varchar(32) NOT NULL,
  `theme_name` varchar(128) NOT NULL COMMENT '风格名称',
  `theme_author` varchar(32) DEFAULT NULL COMMENT '作者',
  `theme_img` varchar(128) NOT NULL COMMENT '缩略图',
  `theme_path` varchar(128) DEFAULT NULL COMMENT '目录名',
  `status` int(11) DEFAULT '0' COMMENT '状态1:启用0:禁用',
  `create_by` varchar(32) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(32) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_theme
-- ----------------------------
INSERT INTO `system_theme` VALUES ('782c003ac9024eba94ae3b59dab17b5d', '新版主题', '王俊南', 'http://localhost:8888/uploads\\20190815\\18c8a53e1b63493b821c9ab865c4b395.jpg', 'default', '1', '9f4b807db2e94670bb02cdc212ea7389', '2018-01-01 00:00:00', '9f4b807db2e94670bb02cdc212ea7389', '2019-12-25 12:00:51');

-- ----------------------------
-- Table structure for system_user
-- ----------------------------
DROP TABLE IF EXISTS `system_user`;
CREATE TABLE `system_user` (
  `id` varchar(32) NOT NULL,
  `username` varchar(32) DEFAULT NULL COMMENT '用户名',
  `password` varchar(32) DEFAULT NULL COMMENT '密码',
  `mobile` varchar(32) DEFAULT NULL COMMENT '手机',
  `realname` varchar(32) DEFAULT NULL COMMENT '真实姓名',
  `status` int(11) DEFAULT '0' COMMENT '状态（1：启用0：禁用）',
  `page_style` varchar(32) DEFAULT NULL COMMENT '页面风格',
  `salt` varchar(64) DEFAULT NULL COMMENT '密码盐',
  `last_login_ip` varchar(32) DEFAULT NULL COMMENT '上次登录IP',
  `last_login_time` varchar(32) DEFAULT NULL COMMENT '上次登录时间',
  `portrait` varchar(32) DEFAULT NULL COMMENT '头像',
  `create_by` varchar(255) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00',
  `update_by` varchar(255) DEFAULT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `salt_byte` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of system_user
-- ----------------------------
INSERT INTO `system_user` VALUES ('9f4b807db2e94670bb02cdc212ea7388', 'zdgwh', '51c04b026c6cf5f785622796aba7e56f', null, '管理员', '1', null, 'd2FuZ2puMTIzNDU2', null, '2020-01-05 10:09:46.471', null, null, '2018-01-01 00:00:00', null, '2020-02-05 09:18:48', null);
INSERT INTO `system_user` VALUES ('9f4b807db2e94670bb02cdc212ea7389', 'wangjn', '51c04b026c6cf5f785622796aba7e56f', null, '超级管理员', '1', null, 'd2FuZ2puMTIzNDU2', null, '2020-03-24 01:33:07', null, null, '2018-01-01 00:00:00', null, '2019-10-11 11:21:13', null);

-- ----------------------------
-- Table structure for system_variable
-- ----------------------------
DROP TABLE IF EXISTS `system_variable`;
CREATE TABLE `system_variable` (
  `ID` varchar(32) NOT NULL COMMENT '主键ID',
  `ITEM_NAME` varchar(255) DEFAULT NULL COMMENT '变量名称',
  `INFO` varchar(255) DEFAULT NULL COMMENT '描述',
  `GROUP_ID` varchar(10) DEFAULT NULL COMMENT '组ID',
  `TYPE` varchar(255) DEFAULT NULL COMMENT '字段类型',
  `VALUE` varchar(255) DEFAULT NULL COMMENT '变量值',
  `CREATE_BY` varchar(32) DEFAULT NULL COMMENT '创建人',
  `CREATE_TIME` timestamp NOT NULL DEFAULT '2018-01-01 00:00:00' COMMENT '创建时间',
  `UPDATE_BY` varchar(32) DEFAULT NULL COMMENT '修改人',
  `UPDATE_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='变量管理';

-- ----------------------------
-- Records of system_variable
-- ----------------------------
INSERT INTO `system_variable` VALUES ('2d3add7ac60949cc9035ea0bfd50f113', 'cfg_gitee_addr', '码云下载地址', '', null, 'https://gitee.com/isoftforce/dreamer_cms', null, '2019-08-16 14:38:59', null, '2019-08-16 14:38:59');
INSERT INTO `system_variable` VALUES ('383e2a267d0045e5aad93ed20ca1b038', 'cfg_github_addr', 'Github下载地址', '', null, 'https://github.com/xinhudong/Dreamer_cms', '9f4b807db2e94670bb02cdc212ea7389', '2019-08-16 14:39:47', null, '2019-08-16 14:39:47');

-- ----------------------------
-- Table structure for system_versionnotes
-- ----------------------------
DROP TABLE IF EXISTS `system_versionnotes`;
CREATE TABLE `system_versionnotes` (
  `id` varchar(32) COLLATE utf8mb4_bin NOT NULL,
  `aid` varchar(32) COLLATE utf8mb4_bin NOT NULL COMMENT '主表ID',
  `ext01` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext02` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext03` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext04` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `ext05` varchar(32) COLLATE utf8mb4_bin DEFAULT NULL,
  `versionnum` varchar(128) COLLATE utf8mb4_bin DEFAULT '',
  `notes` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- ----------------------------
-- Records of system_versionnotes
-- ----------------------------
INSERT INTO `system_versionnotes` VALUES ('c508f9f8fb7a4a99ae09154156d07fb8', '4f451c4797c54536869f5cf748944f9e', null, null, null, null, null, 'Previous Releases 1.0.1', 0x2A20E5858DE8B4B9E5AE8CE695B4E5BC80E6BA90EFBC9AE59FBAE4BA8E4D4954E58D8FE8AEAEEFBC8CE6BA90E4BBA3E7A081E5AE8CE585A8E5BC80E6BA90EFBC9B0D0A2A20E6A087E7ADBEE58C96E5BBBAE7AB99EFBC9AE4B88DE99C80E8A681E4B893E4B89AE79A84E5908EE58FB0E5BC80E58F91E68A80E883BDEFBC8CE58FAAE8A681E4BDBFE794A8E7B3BBE7BB9FE68F90E4BE9BE79A84E6A087E7ADBEEFBC8CE5B0B1E883BDE8BDBBE69DBEE5BBBAE8AEBEE7BD91E7AB99EFBC9B0D0A2A20E6A8A1E78988E5BC80E58F91E696B9E4BEBFEFBC9AE694AFE68C81E59CA8E7BABFE4B88AE4BCA0E6A8A1E78988E58C85E5BC80E58F91E696B9E4BEBFE5BFABE68DB7EFBC9B0D0A2A20E6AF8FE69C88E69BB4E696B0EFBC9AE6AF8FE69C88E8BF9BE8A18CE7B3BBE7BB9FE58D87E7BAA7EFBC8CE58886E4BAABE69BB4E5A49AE5A5BDE794A8E79A84E6A8A1E78988E4B88EE68F92E4BBB6EFBC9B);
INSERT INTO `system_versionnotes` VALUES ('52dfa8ed6c974c9cb397b10efabac57b', 'fb7c4cf30dd1456e8b21af621479a2e4', null, null, null, null, null, 'Previous Releases 2.0.0', 0x2A20E5AFB9E68980E69C89E887AAE5AE9AE4B989E6A087E7ADBEE8BF9BE8A18CE8A7A3E69E900D0A2A20E5BC80E58F91E6A087E7ADBEE8A7A3E69E90E5BC95E6938E0D0A2A20E694AFE68C81E689A9E5B195E6A087E7ADBE);

-- ----------------------------
-- Table structure for thesaurus
-- ----------------------------
DROP TABLE IF EXISTS `thesaurus`;
CREATE TABLE `thesaurus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thesauru_one` varchar(255) DEFAULT NULL COMMENT '词',
  `adding_time` varchar(255) DEFAULT NULL COMMENT '添加时间',
  `adding_name` varchar(255) DEFAULT NULL COMMENT '添加人',
  `heat` int(255) unsigned zerofill DEFAULT NULL COMMENT '热度',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of thesaurus
-- ----------------------------
INSERT INTO `thesaurus` VALUES ('1', '准东管委会', '2020-02-06', null, '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');

-- ----------------------------
-- Table structure for three_services
-- ----------------------------
DROP TABLE IF EXISTS `three_services`;
CREATE TABLE `three_services` (
  `id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `article` varchar(255) DEFAULT NULL,
  `publisher` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `release_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of three_services
-- ----------------------------

-- ----------------------------
-- Table structure for tweets_fb
-- ----------------------------
DROP TABLE IF EXISTS `tweets_fb`;
CREATE TABLE `tweets_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` varchar(255) DEFAULT NULL,
  `like_num` int(11) DEFAULT NULL,
  `repost_num` int(11) DEFAULT NULL,
  `comment_num` int(11) DEFAULT NULL,
  `content` text,
  `user_id` varchar(255) DEFAULT NULL,
  `tool` varchar(255) DEFAULT NULL,
  `image_url` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `origin_weibo` varchar(255) DEFAULT NULL,
  `crawled_at` varchar(255) DEFAULT NULL,
  `weibo_url` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tweets_fb
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '序列',
  `roleId` int(11) DEFAULT NULL,
  `department` varchar(32) NOT NULL COMMENT '各部门',
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET armscii8 DEFAULT NULL COMMENT '头像',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_browsing_history
-- ----------------------------
DROP TABLE IF EXISTS `user_browsing_history`;
CREATE TABLE `user_browsing_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `historical_user` varchar(50) DEFAULT NULL COMMENT '访问用户',
  `historical_title` varchar(50) DEFAULT NULL COMMENT '访问标题',
  `historical_source` varchar(50) DEFAULT NULL COMMENT '来源',
  `historical_news_id` int(11) DEFAULT NULL COMMENT '新闻ID',
  `historical_category` varchar(50) DEFAULT NULL COMMENT '新闻类别',
  `historical_time` varchar(50) DEFAULT NULL COMMENT '浏览时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_browsing_history
-- ----------------------------

-- ----------------------------
-- Table structure for user_collection_record
-- ----------------------------
DROP TABLE IF EXISTS `user_collection_record`;
CREATE TABLE `user_collection_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `collection_user` varchar(50) DEFAULT NULL COMMENT '收藏人',
  `collection_article` varchar(50) DEFAULT NULL COMMENT '文章标题',
  `collection_source` varchar(50) DEFAULT NULL COMMENT '来源',
  `collection_time` varchar(50) DEFAULT NULL COMMENT '收藏时间',
  `collection_article_category` varchar(50) DEFAULT NULL COMMENT '收藏类别',
  `collection_article_id` int(11) DEFAULT NULL COMMENT '收藏id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_collection_record
-- ----------------------------
INSERT INTO `user_collection_record` VALUES ('1', '1', '1', '1', '1', '1', '1', null);
INSERT INTO `user_collection_record` VALUES ('2', '111', '11111', '11111', '11111', '11111', '11111', null);
INSERT INTO `user_collection_record` VALUES ('3', '11', '11', '11', '11', '11', '11', '11');
INSERT INTO `user_collection_record` VALUES ('4', '15193869953', '1', '1', '1', '1', '1', '44');
INSERT INTO `user_collection_record` VALUES ('5', '15193869953', '1', '1', '1', '1', '11', '44');
INSERT INTO `user_collection_record` VALUES ('6', '15193869953', '1', '1', '1', '1', '111', '44');
INSERT INTO `user_collection_record` VALUES ('7', '15193869953', '1', '1', '1', '1', '1111', '44');
INSERT INTO `user_collection_record` VALUES ('8', '15193869953', '1', '1', '1', '1', '11111', '44');
INSERT INTO `user_collection_record` VALUES ('9', '15193869953', '1', '1', '1', '1', '111111', '44');
INSERT INTO `user_collection_record` VALUES ('10', '15193869953', '1', '1', '1', '1', '1111111', '44');

-- ----------------------------
-- Table structure for user_dz
-- ----------------------------
DROP TABLE IF EXISTS `user_dz`;
CREATE TABLE `user_dz` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `news_title` varchar(255) DEFAULT NULL COMMENT '标题',
  `news_time` varchar(255) DEFAULT NULL,
  `news_category` varchar(255) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `reserve_one` varchar(255) DEFAULT NULL,
  `reserve_two` varchar(255) DEFAULT NULL,
  `news_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_dz
-- ----------------------------
INSERT INTO `user_dz` VALUES ('2', '标题', '时间', 'ZZQXW', '1', '2020-04-01 18:55:27', null, '1');
INSERT INTO `user_dz` VALUES ('3', '标题', '时间', 'ZXW', '1', '2020-04-01 19:14:52', null, '1');
INSERT INTO `user_dz` VALUES ('4', '标题', '时间', 'ZYXW', '1', '2020-04-01 19:18:30', null, '1');
INSERT INTO `user_dz` VALUES ('9', '标题', '时间', 'ZDXW', '1', '2020-04-01 19:26:08', null, '1');

-- ----------------------------
-- Table structure for user_express
-- ----------------------------
DROP TABLE IF EXISTS `user_express`;
CREATE TABLE `user_express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_user` varchar(255) DEFAULT NULL COMMENT '用户快递',
  `sender` varchar(255) DEFAULT NULL COMMENT '寄件人',
  `mailing_address` varchar(255) DEFAULT NULL COMMENT '寄件地址',
  `addressee` varchar(255) DEFAULT NULL COMMENT '收件人',
  `receiving_address` varchar(255) DEFAULT NULL COMMENT '收件地址',
  `waybill` varchar(255) DEFAULT NULL COMMENT '运单号',
  `user_order` varchar(255) DEFAULT NULL COMMENT '订单号',
  `courier_services_company` varchar(255) DEFAULT NULL COMMENT '快递公司',
  `express_time` varchar(255) DEFAULT NULL COMMENT '订单时间',
  `unique_identifier` varchar(255) DEFAULT NULL COMMENT '唯一标示',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_express
-- ----------------------------

-- ----------------------------
-- Table structure for user_grsl
-- ----------------------------
DROP TABLE IF EXISTS `user_grsl`;
CREATE TABLE `user_grsl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_user` varchar(255) DEFAULT NULL COMMENT '姓名',
  `user_id` varchar(255) DEFAULT NULL COMMENT '身份证',
  `title` varchar(255) DEFAULT NULL COMMENT '主题',
  `content` varchar(255) DEFAULT NULL COMMENT '详情',
  `code` int(11) DEFAULT '0' COMMENT '状态',
  `ysl_number` int(11) DEFAULT '0' COMMENT '已申领人数',
  `sl_number` int(11) DEFAULT '0' COMMENT '申领人数',
  `sl_remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `pid` int(11) DEFAULT '0',
  `cj_time` varchar(255) DEFAULT NULL COMMENT '创建时间',
  `lq_time` varchar(255) DEFAULT NULL COMMENT '领取时间',
  `yl1` int(255) DEFAULT NULL,
  `yl2` int(255) DEFAULT NULL,
  `yl3` varchar(255) DEFAULT NULL,
  `yl4` varchar(255) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_grsl
-- ----------------------------
INSERT INTO `user_grsl` VALUES ('2', null, null, '标题', '内容2', '1', '0', '9', '备注', '0', '创建时间2', '2020-04-17 18:27:55', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('30', null, null, null, null, '0', '0', '1', null, '0', null, null, null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('32', '1', '1', null, null, '1', '0', '0', '1', '30', '2020-04-17 18:12:19', null, null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('34', 'wefwfw', 'fwfwfwfewf', null, null, '2', '0', '0', 'ewfewqfewqfewfew', '2', '2020-04-17 18:17:33', '2020-04-17 18:37:16', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('35', 'wefrwrwf', 'ewfewfewfewfwfewf', null, null, '2', '0', '0', '5G24GERAG24ERAGWGWewfwEVA2', '2', '2020-04-17 18:19:03', '2020-04-17 18:38:03', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('36', '353435', '435435435', null, null, '1', '0', '0', '43543543543', '2', '2020-04-17 18:19:13', '2020-04-17 18:37:08', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('37', 'w4321 5431gvw', '56terhehtr5614``', null, null, '1', '0', '2', '56461203', '2', '2020-04-17 18:25:07', '2020-04-17 18:42:09', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('38', null, null, '申领申领申领', '内容内容内容内容', '0', '0', '0', '备注备注备注备注备注备注备注', '0', '2020-04-17 18:30:33', null, null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('39', '4654654654', '65426426542654275427', null, null, '1', '0', '0', '7425747427427427642', '2', '2020-04-17 18:37:26', '2020-04-17 18:37:50', null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('40', 'hx', '6140064631354', null, null, '1', '0', '0', '哦金凤电脑', '2', '2020-04-17 18:40:11', null, null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('41', '李杰', '610', '标题', '内容', '1', '0', '0', '测试', '2', '2020-04-18 11:05:49', null, null, null, null, null, null);
INSERT INTO `user_grsl` VALUES ('43', '姚鑫', '45164651', '标题', '内容2', '1', '0', '0', '11测试', '2', '2020-04-18 11:42:15', null, null, null, null, null, null);

-- ----------------------------
-- Table structure for user_resume
-- ----------------------------
DROP TABLE IF EXISTS `user_resume`;
CREATE TABLE `user_resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_age` varchar(255) DEFAULT NULL,
  `user_sex` varchar(255) DEFAULT NULL,
  `user_tel` varchar(255) DEFAULT NULL,
  `user_mail` varchar(255) DEFAULT NULL,
  `user_xl` varchar(255) DEFAULT NULL,
  `user_zwms` varchar(255) DEFAULT NULL,
  `user_qwxz` varchar(255) DEFAULT NULL,
  `user_bz` varchar(255) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_resume
-- ----------------------------
INSERT INTO `user_resume` VALUES ('14', '姓名1', '年龄1', '性别1', '电话1', '邮箱1', '学历1', '自我描述1', '期望薪资1', '备注1', '2');
INSERT INTO `user_resume` VALUES ('15', '姓名1', '年龄1', '性别1', '电话1', '邮箱1', '学历1', '自我描述1', '期望薪资1', '备注11', '1');
INSERT INTO `user_resume` VALUES ('16', '姓名1', '年龄1', '性别1', '电话1', '邮箱1', '学历1', '自我描述1', '期望薪资1', '备注1', '3');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int(11) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL,
  `roleId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_role
-- ----------------------------

-- ----------------------------
-- Table structure for user_user
-- ----------------------------
DROP TABLE IF EXISTS `user_user`;
CREATE TABLE `user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name_tel` varchar(50) DEFAULT NULL COMMENT '用户名.手机号',
  `pass_word` varchar(50) DEFAULT NULL COMMENT '密码',
  `user_id` varchar(50) DEFAULT NULL COMMENT '身份证',
  `user_nick_name` varchar(50) DEFAULT NULL COMMENT '昵称',
  `user_age` varchar(50) DEFAULT NULL COMMENT '用户年龄',
  `user_birthday` varchar(50) DEFAULT NULL COMMENT '用户生日',
  `user_city` varchar(50) DEFAULT NULL COMMENT '用户地区',
  `user_portrait` varchar(50) DEFAULT 'http://36.189.242.73:8083/image/mrtx.jpg' COMMENT '头像',
  `user_sex` varchar(50) DEFAULT NULL COMMENT '性别',
  `user_tel` varchar(50) DEFAULT NULL COMMENT '电话',
  `user_name` varchar(50) DEFAULT NULL COMMENT '用户姓名',
  `user_collection` int(11) DEFAULT '0' COMMENT '收藏',
  `user_dz` int(11) DEFAULT '0' COMMENT '点赞',
  `user_history` varchar(255) DEFAULT NULL COMMENT '历史',
  `resume_id` int(11) DEFAULT NULL COMMENT '简历id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_user
-- ----------------------------
INSERT INTO `user_user` VALUES ('1', null, null, null, null, null, null, null, 'http://36.189.242.73:8083/image/mrtx.jpg', null, null, null, '1', '7', null, null);
INSERT INTO `user_user` VALUES ('41', '17782680217', '123456', '', '', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '', '', '', null, null, '', '7');
INSERT INTO `user_user` VALUES ('42', '15193869953', '1234567', '', '23', '', '', '', '36.189.242.73:8083/image/下载.jpg', '', '', '阿萨德', null, null, '2020-03-26 13:49:41', null);
INSERT INTO `user_user` VALUES ('43', '15191455644', '123456', '', '', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '', '', '', null, null, '2020-03-30 17:43:21', null);
INSERT INTO `user_user` VALUES ('44', '18392129312', '123456', '', '桀1998', '11', '2018-1-13', '陕西-西安', '36.189.242.73:8083/image/1585130935525491.png', '男', '18392129312', '桀', '3', null, '2020-03-31 22:03:08', '17');
INSERT INTO `user_user` VALUES ('45', '18271626377', 'yh123456', '', '', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '', '', '', null, null, '', null);
INSERT INTO `user_user` VALUES ('46', '13709944470', '970616', '', '王新斌', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '男', '13709944470', '', null, null, '2020-01-13 13:04:59', null);
INSERT INTO `user_user` VALUES ('47', '18748077574', '123456qwe', '', '', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '', '', '', null, null, '2020-01-13 20:46:46', null);
INSERT INTO `user_user` VALUES ('48', '15594192506', '123456789', '', '', '', '', '', 'http://36.189.242.73:8083/image/mrtx.jpg', '', '', '', null, null, '2020-02-06 19:46:25', null);
INSERT INTO `user_user` VALUES ('49', '18699102809', '2121007', '', '哈哈', '', '2019-10-1', '新疆', 'http://36.189.242.73:8083/image/mrtx.jpg', '男', '18699102809', '李', null, null, '2020-03-27 11:46:47', null);

-- ----------------------------
-- Table structure for viewzd
-- ----------------------------
DROP TABLE IF EXISTS `viewzd`;
CREATE TABLE `viewzd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT NULL,
  `d_z` int(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of viewzd
-- ----------------------------

-- ----------------------------
-- Table structure for viewzd_fb
-- ----------------------------
DROP TABLE IF EXISTS `viewzd_fb`;
CREATE TABLE `viewzd_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT NULL,
  `d_z` int(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(10) DEFAULT NULL,
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of viewzd_fb
-- ----------------------------
INSERT INTO `viewzd_fb` VALUES ('1', '准东', null, '123', null, '2020-03-19', null, '<p>321</p>', '123', 'http://36.189.242.73:8084/uploads/', 'http://36.189.242.73:8084/uploads/', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('2', '准东', null, '测试微视频是否上传成功', null, '2020-03-23', null, '', '本人', 'http://36.189.242.73:8084/uploads/20200323/177ce753cd224e2f9eca2a1f325de64d.mp4', 'http://36.189.242.73:8084/uploads/20200323/177ce753cd224e2f9eca2a1f325de64d.mp4', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('3', '准东', null, '', null, '2020-03-23', null, '', '', 'http://36.189.242.73:8084/uploads/20200323/ce4d579ef04c4c4ab25784f73d460931.jpg', 'http://36.189.242.73:8084/uploads/20200323/ce4d579ef04c4c4ab25784f73d460931.jpg', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('4', '准东', null, '啊', null, '2020-03-23', null, '<p>&nbsp;啊啊</p>', 'sad官方', 'http://36.189.242.73:8084/uploads/20200323/a1b04a94b92b4068ae7133952cf90908.mp4', 'http://36.189.242.73:8084/uploads/20200323/a1b04a94b92b4068ae7133952cf90908.mp4', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('5', '准东', null, '', null, '2020-03-23', null, '', '阿萨', 'http://36.189.242.73:8084/uploads/20200323/c1135bbbb93b4ade8921ac1d7b380d9d.jpg', 'http://36.189.242.73:8084/uploads/20200323/c1135bbbb93b4ade8921ac1d7b380d9d.jpg', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('6', '准东', null, '阿萨德', null, '2020-03-23', null, '<p>阿三哥</p>', '啊', 'http://36.189.242.73:8084/uploads/', 'http://36.189.242.73:8084/uploads/', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('7', '准东', null, '', null, '2020-03-23', null, '', '', 'http://36.189.242.73:8084/uploads/20200323/9b54e9a8d0f04977ae8786d990921ce3.mp4', 'http://36.189.242.73:8084/uploads/20200323/9b54e9a8d0f04977ae8786d990921ce3.mp4', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('8', '准东', null, '我', null, '2020-03-24', null, '<p>我</p>', '我', 'http://36.189.242.73:8084/uploads/20200324/a27845c210f942a28f99fae5af3c6816.mp4', 'http://36.189.242.73:8084/uploads/20200324/a27845c210f942a28f99fae5af3c6816.mp4', '1', '0', '', '0', null);
INSERT INTO `viewzd_fb` VALUES ('9', '准东', null, '收到', null, '2020-03-24', null, '<p>啊</p>', '仨', 'http://36.189.242.73:8084/uploads/20200324/accfa78557334e49a6cbba67e1b576d0.mp4', 'http://36.189.242.73:8084/uploads/20200324/accfa78557334e49a6cbba67e1b576d0.mp4', '1', '0', '', '0', null);

-- ----------------------------
-- Table structure for vote_option
-- ----------------------------
DROP TABLE IF EXISTS `vote_option`;
CREATE TABLE `vote_option` (
  `id` int(10) DEFAULT NULL,
  `option_nr` varchar(765) DEFAULT NULL,
  `tp_count` int(10) DEFAULT NULL,
  `picture` varchar(300) DEFAULT NULL,
  `xx_commit` varchar(765) DEFAULT NULL,
  `sort_id` int(10) DEFAULT NULL,
  `yls` varchar(765) DEFAULT NULL,
  `yli` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vote_option
-- ----------------------------

-- ----------------------------
-- Table structure for vote_sort
-- ----------------------------
DROP TABLE IF EXISTS `vote_sort`;
CREATE TABLE `vote_sort` (
  `id` int(6) DEFAULT NULL,
  `sort` varchar(180) DEFAULT NULL,
  `tp_code` int(2) DEFAULT NULL,
  `s_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `e_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `picture` varchar(600) DEFAULT NULL,
  `tp_commit` varchar(600) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vote_sort
-- ----------------------------

-- ----------------------------
-- Table structure for voting_detailss
-- ----------------------------
DROP TABLE IF EXISTS `voting_detailss`;
CREATE TABLE `voting_detailss` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `originator` varchar(255) DEFAULT NULL COMMENT '发起人',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `picture` varchar(255) DEFAULT NULL COMMENT '投票图片',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `start_date` varchar(255) DEFAULT NULL COMMENT '开始日期',
  `stop_date` varchar(255) DEFAULT NULL COMMENT '结束日期',
  `option_title` varchar(255) DEFAULT NULL COMMENT '选项标题',
  `option_picture` varchar(255) DEFAULT NULL COMMENT '选项图片',
  `option_content` varchar(255) DEFAULT NULL COMMENT '选项人内容',
  `votes` int(255) DEFAULT NULL COMMENT '票数',
  `option_id` int(11) DEFAULT '0' COMMENT '选项id',
  `voting_category` int(255) DEFAULT NULL COMMENT '投票类别',
  `code` int(255) DEFAULT '0' COMMENT '是否启用0',
  `tp_remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  `code_picture` int(255) DEFAULT '0' COMMENT '投票图片开启状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voting_detailss
-- ----------------------------
INSERT INTO `voting_detailss` VALUES ('9', null, '标题2', null, null, null, null, null, null, null, null, '0', '1', '0', null, '0');
INSERT INTO `voting_detailss` VALUES ('10', null, '标题1', null, null, null, null, null, null, null, null, '0', '2', '0', null, '0');
INSERT INTO `voting_detailss` VALUES ('11', null, '标题3', null, null, null, null, null, null, null, null, '0', '1', '0', null, '0');
INSERT INTO `voting_detailss` VALUES ('12', null, '标题4', null, null, null, null, null, null, null, null, '0', '2', '0', null, '0');

-- ----------------------------
-- Table structure for voting_user
-- ----------------------------
DROP TABLE IF EXISTS `voting_user`;
CREATE TABLE `voting_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL COMMENT '投票人',
  `user_id` int(11) DEFAULT NULL COMMENT '投票id',
  `cid` int(11) DEFAULT NULL COMMENT '选项id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of voting_user
-- ----------------------------

-- ----------------------------
-- Table structure for web_announcement
-- ----------------------------
DROP TABLE IF EXISTS `web_announcement`;
CREATE TABLE `web_announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '详情',
  `editor` varchar(50) DEFAULT NULL COMMENT '图片链接',
  `picture` varchar(255) DEFAULT NULL COMMENT '照片',
  `release_time` varchar(25) DEFAULT NULL COMMENT '发布时间',
  `title` varchar(50) DEFAULT NULL COMMENT '标题',
  `code` int(10) DEFAULT '0' COMMENT '状态0未审核1审核',
  `dz_count` int(10) DEFAULT '0' COMMENT '点赞数',
  `zf_count` int(10) DEFAULT '0' COMMENT '转发数',
  `pl_count` int(10) DEFAULT '0' COMMENT '评论数',
  `category` varchar(255) DEFAULT NULL COMMENT '类别名',
  `cid` int(11) DEFAULT NULL,
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT NULL COMMENT '区分数组json',
  `local_url` text,
  `source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_announcement
-- ----------------------------
INSERT INTO `web_announcement` VALUES ('209', '<p>本报北京1月10日电（记者冯春梅、易舒冉）全国政协10日在京召开网络议政远程协商会，议题是“加强大数据时代个人信息保护”。中共中央政治局常委、全国政协主席汪洋主持会议并讲话。他强调，要深入学习贯彻习近平总书记关于网络安全和个人信息保护工作的重要指示精神，以发展的眼光和辩证的思维看待大数据时代个人信息保护问题，坚持以人民为中心，坚持政府监管、行业自律、社会参与统筹推进，坚持标本兼治，在提高信息资源利用水平的同时科学有效保护信息安全，让大数据更好服务社会、造福人民。</p><p>14位委员与专家在全国政协机关和辽宁、安徽、湖南、贵州5个会场以及通过手机连线方式发言，近120位委员通过移动履职平台发表意见。大家认为，党中央高度重视大数据时代个人信息保护，有关方面做了大量工作，取得了积极成效。但传统个人信息保护制度和方式跟不上互联网广泛普及和数字产业迅猛发展的新形势，相关法律法规不完善、多头监管与监管缺失并存、企业主体责任落实不到位、公民自我保护意识不强等问题较为突出，维护信息安全依然任重道远。</p><p>一些委员建议，要加快出台专门的个人信息保护法律，明确个人信息概念、适用对象和权属，明确采集、处理、使用个人信息的程序、规则和相关责任。要加大专项治理力度，重拳打击非法收集、交易、使用个人信息的违法犯罪活动，提高违法违规成本，最大限度挤压网络黑灰产业生存空间。要实施分级分类保护，建立个人信息利用清单，强化对人脸识别、数据爬取等技术应用和“人肉搜索”等行为的监管。要压实企业主体责任，引导企业建立健全内部管控机制，克服重发展轻安全的倾向。要广泛宣传个人信息安全知识、技术和理念，增强公民自我保护意识，不给违法犯罪行为以可乘之机。要加强部门间信息共享和统筹协调，建立统一的个人信息保护监管平台，避免多头执法和重复执法。</p><p>全国政协副主席张庆黎、夏宝龙、辜胜阻、邵鸿出席会议。政协委员张英、陈晓红、景亚萍、赖明勇、朱山、吴杰庄、王小川、童国华、王悦群、汪利民、谈剑锋、方来英、崔仑和专家杜跃进作了发言。中央网信办、工业和信息化部、公安部、卫生健康委、市场监管总局等部门负责同志现场作了互动交流。</p><p><br/></p>', '(责编：杨光宇、曹昆)', 'http://36.189.242.73:8084/uploads/None', '2020-01-11', '全国政协召开网络议政远程协商会', '1', '0', '0', '0', '头条新闻', '73', '\"http://36.189.242.73:8084/uploads/\"', null, '', '');
INSERT INTO `web_announcement` VALUES ('270', '<p><strong>今年我区全面深化改革工作要点“出炉”<br/></strong></p><p><strong>着力推进7个领域、32个方面、107项具体改革举措</strong></p><p>记者从自治区党委政策研究室（改革办）获悉：《自治区党委全面深化改革委员会2020年工作要点》（以下简称《工作要点》）“出炉”，全面部署推进社会治理、经济生态、宣传文化、民生保障、党的建设、纪检监察、兵团等7个重点领域的32个方面、107项具体改革举措。</p><p>据了解，今年我区坚持顶层设计和基层探索相结合、整体推进和重点突破相结合，在持续深入学习习近平总书记全面深化改革重要论述、全面承接落实中央改革部署的同时，着力推进重点领域改革创新。其中，经济和生态体制改革是今年全面深化改革的重中之重，涉及营商环境、农业农村、旅游、核心区建设、国资国企、民营经济、生态环境等10个方面的改革。</p><p>《工作要点》明确，今年我区将继续深化营商环境改革，在全区推开“基层一张表”“最多跑一次”改革，对已出台的优化营商环境政策措施的落实情况开展民主监督和评估问效，着力解决政策不落实、承诺不兑现等问题。同时，我区将深化供给侧结构性改革，制订关于促进人工智能与实体经济深度融合的实施方案和深化新一代信息技术与制造业融合发展的实施意见，推动制造业转型升级。</p><p>在深化农业农村改革中，明确今年要全面开展农村集体产权制度改革，探索研究新疆农村综合产权交易中心建设模式；积极推广高效节水试点经验，推动南疆土地适度规模经营。今年我区还将深入实施旅游服务质量提升计划，继续开展全域旅游示范区创建，提升旅游有效供给；加快优质资源市场化整合，支持新疆旅游投资集团发展壮大。在深化国资国企改革方面，我区将制订国企改革三年行动方案，推动国有资本布局优化调整；在能源、交通、旅游、水利等领域培育壮大一批国有投资集团，做优做强国有资本。</p><p>在推动重点领域改革的同时，我区还将持续深入推进教育、医疗卫生、脱贫攻坚等民生领域改革。制订新疆教育现代化2035规划等，推进各级各类教育高质量发展。健全完善医疗与公共卫生服务高效协调、联防联控工作机制；规范医联体医共体建设管理，深入推进“互联网+医疗”建设。建立持续巩固脱贫成果防止返贫长效机制，建立完善返贫监测预警和帮扶机制，深入实施重大传染病防治和健康扶贫攻坚行动，巩固脱贫攻坚成果。</p><p>《工作要点》还明确，加快推进丝绸之路经济带核心区建设，进一步完善乌鲁木齐国际陆港区管理架构，探索乌鲁木齐国际陆港区与临空经济示范区联动发展模式，加快推进中欧班列集结中心建设。我区还聚焦新疆稳定发展的突出问题和短板，谋划部署了经济高质量发展、乡村振兴、旅游、对外开放、营商环境等10项改革试点，力争通过试点为面上改革提供可复制、可推广的经验做法。</p><p><br/></p>', '信息中心', 'http://36.189.242.73:8084/uploads/', '2020-03-30', '涉及7个重点领域！2020年新疆全面深化改革工作要点“出炉”', '1', '0', '0', '0', '头条新闻', '73', '', null, '', '');
INSERT INTO `web_announcement` VALUES ('271', '<p>陈甬军 中国人民大学商学院教授、中国经济改革发展研究院副院长、中国人民大学重阳金融研究院高级研究员</p><p>2020年春天以来，新冠病毒在全球范围内广泛传播，并带来国际资本市场巨大的动荡。这既是对世界防疫体系和金融体系的重大挑战，更是对全球化体系的一个大考。一些学者认为，这次疫情将终结全球化，迫使各国重新审视全球产业链风险，实体产业回归本土。我认为，新冠肺炎疫情全球传播和最终治理的结果将会终结原来的全球化时代，并会以强大的力量推动国际社会进入一个新型的全球化进程。</p><p>这轮全球化从三十年前冷战结束发端，以发挥各国比较优势为出发点，以资金、商品、人员在全球范围内的自由流动为特征，在世界范围内形成了高效的产业体系和链条，大大推动了全球的经济发展，但也存在导致贫富差距越来越大、各种地缘政治和全球热点问题屡屡发生的缺陷。国际社会虽已设立了一些协调机制，如G20等，但对于这次新冠病毒的全球传播却反应迟钝，基本上是束手无策。所以通过这次病毒的全球传播和最终治理，国际社会将进一步加强协调，推动形成更高层次的对新型全球化的共同认识。这种新型全球化并不是再退回到过去各个国家各自为政的状况，而是要设计和建立一种既重视经济发展，又注重社会治理，既有平时协商制度，更有危机时刻紧急协调和执行机制的治理体系，注意禀赋充分利用和贫富差距遏制内在平衡机制的全球化体系。</p><p>实际上，中国领导人六年前提出的“一带一路”倡议和四年前提出的构建人类命运共同体理念，就是对这个新型全球化的一个基本的设计构思。这次疫情传播和资本市场动荡，将全球利益共同体的特征反映得淋漓尽致，它会倒逼人类责任共同体的设计和运行。而这两者的结合和联动，正是人类命运共同体的具体体现。因此，在旧的全球化经过如此一“疫”逐渐消失之际，又会催生新的全球化的诞生和发展。而这次疫情的全球蔓延和治理客观上又充当了新旧全球化转化的＂催化剂＂。它的基本方向就是加快推动构建人类命运共同体，基本途径就是继续发挥“一带一路”对构建人类命运共同体的基础性作用。</p><p>六年来，“一带一路”已成为中国与世界各国一起构建人类命运共同体的基础平台。开展“一带一路”国际合作就是实现这个过程的具体路径。这实际上反映“一带一路”与构建人类命运共同体两者之间的联系：通过“一带一路”国际合作的高质量发展引领新型全球化，促进世界和平发展。理解了这一点，就可从疫情的全球蔓延和治理这个新的角度来认识“一带一路”的意义与作用。这可用供求结合的模型来说明。在疫情的防控和治理方面，中国的现有的供给要素有两个：第一个是防疫产品和产能。中国是世界工业门类最齐全的国家，并在防疫阻击战取得了重大进展，因此中国现在有充足的抗疫产品产能，抗疫产品的生产能力充足。第二，中国具有抗疫的丰富经验和比较先进的技术，在全世界领先一步。现在世界各个国家还处在中国抗疫初期的水平，对新冠病毒防护和治疗的经验和技术还比较缺乏。所以现在可创造一个抗击新冠病毒的“一带一路”国际合作模式，以进一步丰富“一带一路”倡议的内容。当然世界各个国家也正在创造阻击新冠病毒的各种模式和经验，特别在病理的科学研究方面有许多重要的突破。可以为中国的抗击新冠病毒提供帮助和指导。中国当然欢迎这些好东西。中国也可与这些国家合作开展科学研究，在合作中学习和提高。最后获得的利益可以为全人类的发展服务。</p><p>“一带一路”的这个新的国际合作模式还将推动全球治理结构的改革。在这次疫情中，过去形成的全球治理体系的弱点暴露无遗。通过全球“一带一路”国际抗疫合作新模式的实施，如全球先后有上百个项目进行合作建设与交流，就会逐步改变由生产力决定的经济基础。而在经济基础调整之后，全球治理结构这个上层建筑必然要随之改变，从而为新型全球化的发展提供新的治理机制。</p><p>构建人类命运共同体，为世界提供中国智慧、中国方案，是由实质性的行动来支撑的。今年新出现的全球疫情治理需要“一带一路”新的合作模式。它的内在性质和发展机制又决定了它是构建人类命运共同体的基础。所以，推动发展新型全球化，人类命运共同体是基础；构建人类命运共同体，“一带一路”需要新行动。开展“一带一路”全球疫情治理合作，惟此时为然。</p><p>（本文是国家社会科学基金专项研究项目《共建“一带一路”高质量发展的实现路径》（19VDL004）前期研究成果节选。）</p>', '（编辑：戎睿）', 'http://36.189.242.73:8084/uploads/', '2020-03-30', '观点中国：全球疫情防控需要“一带一路”国际合作新模式', '1', '0', '0', '0', '头条新闻', '73', '', null, '', '');

-- ----------------------------
-- Table structure for web_user
-- ----------------------------
DROP TABLE IF EXISTS `web_user`;
CREATE TABLE `web_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `user_dq` varchar(255) DEFAULT NULL COMMENT '地区',
  `user_tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `user_tjtime` varchar(255) DEFAULT NULL COMMENT '提交时间',
  `user_nr` varchar(255) DEFAULT NULL COMMENT '内容',
  `user_zt` varchar(255) DEFAULT NULL COMMENT '主题',
  `user_lb` varchar(255) DEFAULT NULL COMMENT '内容类别',
  `user_xjlb` varchar(255) DEFAULT NULL COMMENT '信件类别',
  `admin_hfbm` varchar(255) DEFAULT NULL COMMENT '回复部门',
  `admin_hftime` varchar(255) DEFAULT NULL COMMENT '回复时间',
  `admin_hfnr` varchar(255) DEFAULT NULL COMMENT '回复内容',
  `xj_id` int(11) DEFAULT NULL COMMENT '编号',
  `xj_rd` int(11) DEFAULT NULL COMMENT '热度',
  `code` int(255) unsigned zerofill DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of web_user
-- ----------------------------
INSERT INTO `web_user` VALUES ('1', '黄翔', '陕西', '4', '5', '6', '主题', '类别', '信件类别', '回复部门', '回复时间', '回复内容', '1', '7', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '归档\r\n归档');
INSERT INTO `web_user` VALUES ('2', '黄翔', '山西', '1', '1', '1', '主题', '类别', '信件类别', '回复部门', '回复时间', '回复内容', '1', '18', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '归档');
INSERT INTO `web_user` VALUES ('3', '黄翔', '甘肃', '15193869953', '1', '准东的暖气改修了', '主题', '类别', '信件类别', '回复部门', '回复时间', '回复内容', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '归档');
INSERT INTO `web_user` VALUES ('4', '黄翔', '甘肃', '15193869953', '1', '准东的暖气改修了', '主题', '类别', '信件类别', '回复部门', '回复时间', '回复内容', '1', '4', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '归档');
INSERT INTO `web_user` VALUES ('5', '黄翔', '地区', '手机号', '提交时间', '内容', '主题', '类别', '信件类别', '回复部门', '回复时间', '回复内容', '1', '4', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '归档');
INSERT INTO `web_user` VALUES ('6', '23r234r23rt4f3ft', 'r2q34t3434t', '3151345', '2020-01-09 15:42:09', '3rtq4tq34tq34', 'ergdsrgergserg', '2', '1', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('7', 'wetegear', 'gweagaweawegg', 'gaweawegawegawe', '2020-01-09 16:08:19', 'gawegeawgawegegawgawge', 'awgawegawegeas', '1', '2', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('8', 'wEFWFWff', 'wFwefWFWF', 'fEWWEFwfW', '2020-01-09 16:09:10', 'fwaefwafwEwf', 'fwEFWafEwefWEw', '1', '2', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('9', 'wEFWFWff', 'wFwefWFWF', 'fEWWEFwfW', '2020-01-09 16:10:01', '				console.log(data)\n', 'fwaefwafwEwffwEFWafEwefWEw', '1', '2', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('10', 'aFfWEF', 'FwefWFwe', 'eWeWFwewE', '2020-01-09 20:44:54', 'EFeWFweWEEW', 'FWfWEwe', '1', '2', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('11', 'rmt', 'asd', 'asd', '2020-01-11 13:37:32', 'asd', 'asd', '1', '2', '', '', '', null, '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('12', 'rmt', 'asd', 'asd', '2020-01-11 13:37:58', 'asd', 'asd', '1', '1', '', '', '', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('13', '王新斌', '准东', '13709944470', '2020-03-14 12:52:16', '智', '坐车', '1', '1', '', '', '', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('14', 'HX', '132456789`', '18691005086', '2020-03-30 15:50:42', 'fiyg lkjbwriufgewjgfdkjgfAD  ', '213', '', '1', '', '', '', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('15', 'HX', '准东管委会', '18691005086', '2020-03-30 19:00:39', '怎么白天鹅也被征用了', '测试1', '', '进言献策', '', '', '', '1', '4', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('16', 'HX', '准东白天鹅', '18691005086', '2020-03-30 19:08:31', '白天鹅白天鹅白天鹅白天鹅白天鹅白天鹅', '测试2', '', '企业诉求', '', '', '', '1', '1', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');
INSERT INTO `web_user` VALUES ('17', '黄祥', '3546431564', '5646654+864', '2020-03-30 19:59:12', '3514364356136c\nsakvcfhglewSFLKJEWgfewfewf', '132564', '', '进言献策', '', '', '', '1', '2', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '');

-- ----------------------------
-- Table structure for weixin_gzh
-- ----------------------------
DROP TABLE IF EXISTS `weixin_gzh`;
CREATE TABLE `weixin_gzh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `source` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '来源',
  `article` longtext CHARACTER SET utf8 COMMENT '文章',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '作者',
  `article_link` varchar(800) CHARACTER SET utf8 DEFAULT NULL COMMENT '文章url',
  `ping_lun` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '评论量',
  `yue_du` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '阅读量',
  `dian_zan` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '点赞量',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `code` int(10) DEFAULT '0',
  `picture_url` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT '预览图地址',
  `category` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `local_url` text CHARACTER SET utf8 COMMENT '文章图片地址',
  `differentiate` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '区分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- ----------------------------
-- Records of weixin_gzh
-- ----------------------------

-- ----------------------------
-- Table structure for weixin_gzh_fb
-- ----------------------------
DROP TABLE IF EXISTS `weixin_gzh_fb`;
CREATE TABLE `weixin_gzh_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(500) CHARACTER SET utf8 DEFAULT NULL COMMENT '标题',
  `source` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '来源',
  `article` longtext CHARACTER SET utf8 COMMENT '文章',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '作者',
  `article_link` varchar(800) CHARACTER SET utf8 DEFAULT NULL COMMENT '文章url',
  `ping_lun` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '评论量',
  `yue_du` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '阅读量',
  `dian_zan` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '点赞量',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `code` int(10) DEFAULT '0',
  `picture_url` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT '预览图地址',
  `category` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL,
  `local_url` text CHARACTER SET utf8 COMMENT '文章图片地址',
  `differentiate` varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '区分',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci;

-- ----------------------------
-- Records of weixin_gzh_fb
-- ----------------------------

-- ----------------------------
-- Table structure for wheel_planting
-- ----------------------------
DROP TABLE IF EXISTS `wheel_planting`;
CREATE TABLE `wheel_planting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `text` text COMMENT '文章内容',
  `publisher` varchar(50) DEFAULT NULL COMMENT '发布人',
  `time` varchar(50) DEFAULT NULL COMMENT '时间',
  `code` int(255) DEFAULT NULL COMMENT '审核状态',
  `d_z` int(255) unsigned zerofill DEFAULT NULL COMMENT '点赞',
  `comment` varchar(255) DEFAULT NULL COMMENT '评论',
  `category` varchar(255) DEFAULT NULL COMMENT '轮播类型',
  `title_picture` varchar(255) DEFAULT NULL COMMENT '标题图片',
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wheel_planting
-- ----------------------------

-- ----------------------------
-- Table structure for wjdc_da
-- ----------------------------
DROP TABLE IF EXISTS `wjdc_da`;
CREATE TABLE `wjdc_da` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '问题/答案',
  `picture` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `count` int(11) unsigned zerofill DEFAULT '00000000000' COMMENT '答案次数',
  `wj_id` int(11) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wjdc_da
-- ----------------------------

-- ----------------------------
-- Table structure for wjdc_name
-- ----------------------------
DROP TABLE IF EXISTS `wjdc_name`;
CREATE TABLE `wjdc_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '说明',
  `picture` varchar(255) DEFAULT NULL COMMENT '图片',
  `start_time` varchar(255) DEFAULT NULL,
  `stop_time` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `code` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wjdc_name
-- ----------------------------
INSERT INTO `wjdc_name` VALUES ('1', '32', '13', 'http://36.189.242.73:8084/uploads/', '2020-02-13', '2020-02-21', '', null, '1');
INSERT INTO `wjdc_name` VALUES ('2', '312', '3213', 'http://36.189.242.73:8084/uploads/', '', '', '', null, '1');

-- ----------------------------
-- Table structure for wx_pay
-- ----------------------------
DROP TABLE IF EXISTS `wx_pay`;
CREATE TABLE `wx_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(255) DEFAULT NULL,
  `nonce_str` varchar(255) DEFAULT NULL,
  `bank_type` varchar(255) DEFAULT NULL,
  `openid` varchar(255) DEFAULT NULL,
  `sign` varchar(255) DEFAULT NULL,
  `fee_type` varchar(255) DEFAULT NULL,
  `mch_id` varchar(255) DEFAULT NULL,
  `cash_fee` int(255) DEFAULT NULL,
  `out_trade_no` varchar(255) DEFAULT NULL,
  `appid` varchar(255) DEFAULT NULL,
  `total_fee` int(255) DEFAULT NULL,
  `trade_type` varchar(255) DEFAULT NULL,
  `result_code` varchar(255) DEFAULT NULL,
  `time_end` varchar(255) DEFAULT NULL,
  `is_subscribe` varchar(255) DEFAULT NULL,
  `order_code` varchar(255) DEFAULT NULL,
  `user_tel` varchar(255) DEFAULT NULL,
  `user_money` int(255) DEFAULT NULL,
  `user_oder` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of wx_pay
-- ----------------------------
INSERT INTO `wx_pay` VALUES ('1', null, null, null, null, null, null, null, null, 'dasd', null, null, null, null, null, null, '支付成功', null, null, null);
INSERT INTO `wx_pay` VALUES ('2', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `wx_pay` VALUES ('3', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `wx_pay` VALUES ('4', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `wx_pay` VALUES ('5', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `wx_pay` VALUES ('6', null, null, null, null, null, null, null, null, 'WX0000006', null, null, null, null, null, null, '支付中', '15193869953', '1', null);
INSERT INTO `wx_pay` VALUES ('7', null, null, null, null, null, null, null, null, 'WX0000007', null, null, null, null, null, null, '支付中', null, null, null);
INSERT INTO `wx_pay` VALUES ('20', '', '', '', '', '', '', '', null, 'WX0000021', '', null, '', '', '', '', '支付中', '18392129312', '1', '');
INSERT INTO `wx_pay` VALUES ('21', null, null, null, null, null, null, null, null, 'WX0000022', null, null, null, null, null, null, '支付中', '18392129312', '1', null);
INSERT INTO `wx_pay` VALUES ('22', null, null, null, null, null, null, null, null, 'WX0000022', null, null, null, null, null, null, '支付中', '18392129312', '1', null);
INSERT INTO `wx_pay` VALUES ('23', null, null, null, null, null, null, null, null, 'WX00000011', null, null, null, null, null, null, '支付中', '15193869953', '1', null);
INSERT INTO `wx_pay` VALUES ('24', null, null, null, null, null, null, null, null, 'WX00000012', null, null, null, null, null, null, '支付中', '15193869953', '1', null);
INSERT INTO `wx_pay` VALUES ('25', null, null, null, null, null, null, null, null, 'WX0000023', null, null, null, null, null, null, '支付中', '18392129312', '1', null);
INSERT INTO `wx_pay` VALUES ('26', null, null, null, null, null, null, null, null, 'WX0000023', null, null, null, null, null, null, '支付成功', null, '1', null);
INSERT INTO `wx_pay` VALUES ('27', null, null, null, null, null, null, null, null, 'WX0000024', null, null, null, null, null, null, '支付中', '18392129312', '1', null);
INSERT INTO `wx_pay` VALUES ('28', null, null, null, null, null, null, null, null, 'WX0000025', null, null, null, null, null, null, '支付成功', '18392129312', '1', null);
INSERT INTO `wx_pay` VALUES ('29', null, null, null, null, null, null, null, null, 'WX0000025', null, null, null, null, null, null, '支付成功', null, null, null);

-- ----------------------------
-- Table structure for yq_news_fb
-- ----------------------------
DROP TABLE IF EXISTS `yq_news_fb`;
CREATE TABLE `yq_news_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` varchar(255) DEFAULT NULL,
  `article_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `news_column` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `release_time` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `news_click` int(11) DEFAULT NULL COMMENT '点击量',
  `local_url` varchar(255) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `differentiate` varchar(255) DEFAULT NULL,
  `news_comment` int(11) DEFAULT NULL,
  `news_reprinted` varchar(255) DEFAULT NULL COMMENT '转载',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yq_news_fb
-- ----------------------------
INSERT INTO `yq_news_fb` VALUES ('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for ys_zszc
-- ----------------------------
DROP TABLE IF EXISTS `ys_zszc`;
CREATE TABLE `ys_zszc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext,
  `article_source` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT NULL,
  `editor` varchar(255) DEFAULT NULL,
  `news_column` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `release_time` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `d_z` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL,
  `cid` int(11) DEFAULT NULL,
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ys_zszc
-- ----------------------------

-- ----------------------------
-- Table structure for zd_comment
-- ----------------------------
DROP TABLE IF EXISTS `zd_comment`;
CREATE TABLE `zd_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentator` varchar(50) DEFAULT NULL COMMENT '评论人',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `commentary_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `d_z` int(255) DEFAULT NULL COMMENT '评论点赞',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zd_comment
-- ----------------------------

-- ----------------------------
-- Table structure for zd_news
-- ----------------------------
DROP TABLE IF EXISTS `zd_news`;
CREATE TABLE `zd_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `article` text COMMENT '文章内容',
  `picture_url` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `code` int(11) DEFAULT '0',
  `d_z` int(11) DEFAULT NULL,
  `zzqCode` int(1) DEFAULT '0',
  `zyCode` int(1) DEFAULT '0',
  `zdCode` int(1) DEFAULT '0',
  `zCode` varchar(255) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `uid` int(10) DEFAULT '0',
  `status` int(10) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zd_news
-- ----------------------------

-- ----------------------------
-- Table structure for zd_news_fb
-- ----------------------------
DROP TABLE IF EXISTS `zd_news_fb`;
CREATE TABLE `zd_news_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `article` text COMMENT '文章内容',
  `picture_url` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `code` int(255) DEFAULT '0' COMMENT '审核状态',
  `d_z` int(255) DEFAULT '0' COMMENT '点赞',
  `uid` int(10) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL,
  `author` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `status` int(10) DEFAULT '0',
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  `clicks` int(255) unsigned zerofill DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  `reload` int(255) unsigned zerofill DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' COMMENT '转载量',
  `comment_quantity` int(255) unsigned zerofill DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32819 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zd_news_fb
-- ----------------------------
INSERT INTO `zd_news_fb` VALUES ('1', '准东新闻', '自编辑', '', '准东开发区财政局三举措强化预算绩效管理', '', '2020-01-08', '<p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">2019年，准东开发区财政局建机制、抓重点，多举措扎实推进预算绩效管理工作，不断提高财政资金使用效益，助力经济高质量发展。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">一是深化预算绩效目标管理。将绩效目标申报融入预算编制全过程，对没有编制预算绩效目标的项目支出，一律不纳入往后预算安排；加强绩效目标审核，确保绩效目标指向明确、细化量化、合理可行。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">二是落实绩效监控、绩效评价监督。定期采集绩效运行信息并汇总分析，对绩效目标运行情况进行跟踪管理和督促检查，以每年5月、8月、11月为监控节点，全面开展监控工作，发现问题立即纠偏，对连续监控结果不佳的项目，及时调整预算。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">三是加强部门绩效培训指导。进一步深化预算绩效管理理念，组织各预算绩效业务人员对预算绩效管理的相关文件、政策、梳理预算绩效各个环节开展集中培训学习，使绩效评价业务水平得以进一步提高。</p><section style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); color: rgb(62, 62, 62); font-size: 16px; font-family: 微软雅黑; overflow-wrap: break-word !important;\" data-role=\"outer\" label=\"Powered by 135editor.com\" data-find=\"_6\"><section style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; overflow-wrap: break-word !important;\" data-role=\"outer\" label=\"Powered by 135editor.com\" data-find=\"_3\"><p style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; overflow-wrap: break-word !important;\"><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; letter-spacing: 0.544px; color: rgb(63, 63, 63);\">&nbsp; 来&nbsp; &nbsp;源：财政局</span></p><p style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; overflow-wrap: break-word !important;\"><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; letter-spacing: 0.544px; color: rgb(63, 63, 63);\">&nbsp; 编&nbsp; &nbsp;辑：刘&nbsp;&nbsp; 歌</span></p></section></section><p><section style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); color: rgb(62, 62, 62); font-size: 16px; font-family: 微软雅黑; overflow-wrap: break-word !important;\" data-role=\"outer\" label=\"Powered by 135editor.com\" data-find=\"_6\"><section style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; overflow-wrap: break-word !important;\" data-role=\"outer\" label=\"Powered by 135editor.com\" data-find=\"_3\"><p style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; overflow-wrap: break-word !important;\"><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; color: rgb(63, 63, 63); letter-spacing: 0.544px;\">&nbsp; 责&nbsp; &nbsp;编：王新斌</span><br style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important;\"/></p><p style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; overflow-wrap: break-word !important;\"><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; color: rgb(63, 63, 63); letter-spacing: 0.544px;\">&nbsp; 主&nbsp; &nbsp;编：王永刚</span></p></section></section></p><p><br/></p>', '', '编辑：刘   歌 责编：王新斌', '1', '8', '1', '', '', '0', 'http://36.189.242.73:8084/uploads/', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000057', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000006');
INSERT INTO `zd_news_fb` VALUES ('32782', '准东新闻', '自编辑', '', '货车司机深夜突发疾病，准东交警暖心救助获赞', '', '2020-01-08', '<p>&nbsp; &nbsp; &nbsp; <span style=\"color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 34px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;\">2019年12月28日凌晨5时许，准东交警大队东方希望中队辅警杨龙、安东在Z917线28公里执行夜间勤务时，一名货车司机在同伴的搀扶下向警车走来求助。</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 34px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;\"><img title=\"正在上传...\" class=\"loadingclass\" id=\"loading_k54s26i3\" src=\"http://36.189.242.73:8084/resource/js/ueditor-1.4.3.3/themes/default/images/spacer.gif\"/></span></p><p><span style=\"color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 34px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;\"></span></p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“警察同志，能不能帮助我们去医院，他肚子痛得实在受不了了”。只见面前的男子搀扶着同伴，满脸焦急。旁边的同伴单手捂着腹部，身体弯曲，眉头紧蹙，表情痛苦。得知情况后，执勤辅警杨龙、安东迅速将男子扶上警车，火速前往吉木萨尔县人民医院五彩湾分院进行检查。经医生诊断，所幸只是因饮食不当造成的肠胃不适，经过简单处理后，疼痛明显缓解。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">凌晨7点，杨龙和安东将男子送回货车上，再次确定身体无碍后，随即返回工作岗位。<span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; text-indent: 2em;\">谢某在临行前说：</span><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; text-indent: 2em;\">“你们不仅送我就医，还关心我的冷暖、安全，作为一名长期在外的货车司机我非常感动。</span><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; text-indent: 2em;\">你们真正的诠释了作为人民卫士的职责！</span><span style=\"margin: 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; text-indent: 2em;\">”（文/图 刘姣）</span></p><p><span style=\"color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-indent: 34px; text-transform: none; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); display: inline !important; float: none;\"><br/></span><br/></p>', '', '编辑：刘   歌 责编：王新斌', '1', '0', '1', '', '', '0', 'http://36.189.242.73:8084/uploads/', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32784', '准东新闻', '准东新闻', '', '准东人•准东事 | 艾力，你好', '', '2020-01-09', '<p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">知道新疆的人很多，了解新疆的人很少。阳煤国泰新华人作为集团公司的一支化工劲旅，初识新疆就结下了不解情缘。在五彩戈壁植根阳煤文化，五湖四海的职工汇聚同心，唯美的少数民族情怀和独特的异域风情风俗不知不觉融入到阳煤人的文化环境，始于阳煤国泰新华的温情故事，感染横跨千里的阳煤儿女。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">艾力·阿卜杜克热木，来自南疆和田地区策勒县，黝黑的脸庞印满岁月沉淀的痕迹，敦厚的体态显得质朴踏实。他的话不多，可是喜欢笑，笑起来周围的人都会跟着笑，他普通话说的不好，他见每一个认识的人说的最多的就是“你好”。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">艾力第一次来到国泰新华，是在2017年12月15日，他跟随在队伍最后，步履蹒跚。那天有零下10度之多，雪花打在他的睫毛上，通红的脸颊微微皴裂，他双手插在兜里，弓着背，冻得瑟瑟发抖，他薄薄的外套内仅仅穿着一件，若不护着身型寒气就能瞬间侵入体内。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">来到国泰新华公司就坚定了艾力转变的决心，是救赎自己的生活，更是挽救家庭的窘迫。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">&nbsp;“你好，王澍亲戚，我想请你吃烤肉！”他将这段话发送到王澍的微信上。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">王澍是艾力在国泰新华公司的结对亲戚，生活上给予艾力许多帮扶。第一次见艾力只带着几件薄衣服，他二话没说就捐出了一套羽绒服，而且买了许多生活用品递交到艾力手上。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">因为这件事，艾力学“谢谢”的发音研究了好久。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">国泰新华公司成立工厂课堂，安排周密的学习计划，艾力没有缺席过一堂课，他爱学习的劲头被阿那尔古丽老师津津乐道。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“艾力，身子坐直才能写好字，不要着急，汉字的一笔一画都很有讲究。”阿那尔古丽是哈萨克族姑娘，她对艾力的学习十分上心，一年多的时间，她已经把小学5年的课本知识倾囊相授，艾力的基础比起别的维吾尔族职工要差很多，可是他进步得速度令人称赞。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“你好，王书记，谢谢你一直以来的关心，我们准备了一首红歌，今年的红歌大赛我们几个朋友可以唱给大家听么。”艾力向国泰新华公司党委书记王向阳申请红歌大赛的表演节目，艾力说他们练习红歌有一段时日了。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">他们唱的歌曲是《没有共产党就没有新中国》，瞬间青春朝气满满当当，异域风情萦绕礼堂。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">小小的礼堂掌声雷动。职工看到他们在台上紧张得握紧衣裙，索性去掉背景音乐，就大声的唱，他们嘹亮的声音一发出来，场下观众就不由得鼓起掌来，这首歌饱含他们内心的感恩之心，同时也触动了职工的心弦。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“艾力，好样的。你们的红歌节目公司要呈报到准东管委会，在他们的舞台上表演，以后出了名，咱们可以去昌吉唱，可以去乌鲁木齐唱。”王澍看望艾力时说。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“要是可以的话，我们练好了去集团公司唱起来，为集团公司建企70周年献歌。”艾力说到自己的愿望。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">艾力回到书桌上边画边说：“阳煤国泰新华公司对我们来讲是第二个故乡，是我们最美丽的相遇。”他勾勒的简笔画是一条笔直的马路，终点是齐整的楼房，而道路两侧的路灯上，他一点点描绘出国旗和阳煤集团旗的样子，他不太会表达，但是他指着两面国旗说到“一个是国家，一个是家”。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“坚决抵制三股势力，与两面人斗争到底！”艾力和自己的亲戚首次在升旗仪式上发生亮剑，表明坚决的立场和态度，随后与现场干部职工共同颂读阳煤文化理念，感受阳煤文化的温暖。来到国泰新华公司的第二年，艾力逐渐理解了阳煤文化，他在微信写下“同心做人，合力做事”的个性签名，开始用朋友圈记录在公司工作、生活的点点滴滴，他想用自己的行动感召周围的人，只有同心同德谋工作，谋生活，才能过上想要的日子。用旁人的话来讲，他领略到今日的荣华，也感受到明日的美好。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">现在艾力一个月的工资下来可以拿到4000多元，除了工作之外他还去饭店打工，餐厅的老板看他辛苦按工作时间给他结账，偶尔会多给一些，他总说“不要给的太多，我就赚个饭钱。”</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">在和田策勒县老家，艾力稳定的收入就村里来讲算高的了。他之前靠收棉花打临活为生，一年收入不到10000元，还有许多生活压力，满打满算剩下的钱都不够吃饭。如今政府和企业多方联合帮助他们，个人工作和家庭温饱同时得到解决，艾力的感激之心溢于言表，他在随堂的日记本上写到“我希望自己是一只雄鹰，盘旋在国泰新华的长空，我哪都不去，我只想守护爱我的人和我爱的土地。”</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">“在集团公司建企70周年之际，咱们排练一段舞蹈，去集团公司的舞台上表演，把弹不尔乐曲和《麦西来普》带到口里，将我们的祝福送去集团公司，感谢集团公司对我们的帮助和关爱！”艾力编排的少数民族舞蹈得到公司工会的大力支持，国泰新华公司党委副书记、工会主席顾立新动员职工一起学维吾尔族舞蹈，瞬间点燃职工文娱文化的滚滚热浪，艾力是舞蹈老师，职工纷纷加入到舞蹈训练学习中。</p><p style=\"margin: 5px 0px; padding: 0px; max-width: 100%; box-sizing: border-box !important; word-wrap: break-word !important; clear: both; min-height: 1em; color: rgb(51, 51, 51); font-family: -apple-system-font, BlinkMacSystemFont, &quot;Helvetica Neue&quot;, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei UI&quot;, &quot;Microsoft YaHei&quot;, Arial, sans-serif; font-size: 17px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: normal; letter-spacing: 0.544px; orphans: 2; text-align: justify; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; background-color: rgb(255, 255, 255); line-height: 2em; text-indent: 2em;\">艾力喜欢说“你好”，也喜欢笑。自那以后，艾力常常笑着说“你好”。“你好”这个问候的代名词成为了国泰新华公司的热词，说起“你好”，也不由自主的想到了艾力。</p><p><img title=\"1578546906565026746.png\" alt=\"企业微信截图_1578546840701.png\" src=\"http://36.189.242.73:8084/uploads/20200109/1578546906565026746.png\"/></p>', 'http://36.189.242.73:8084/uploads/20200109/444646b1d0c04ba4b779890e5a1389c9.png', '文/王越', '1', '0', '1', '', '', '0', 'http://36.189.242.73:8084/uploads/', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000023', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32793', '准东新闻', '准东新闻', '', '准东开发区人力资源和绩效管理研修班在四川大学望江校区开班', '', '2020-01-14', '<p><br/></p><p>1月11日，准东开发区人力资源和绩效管理研修班在四川大学望江校区开班。开发区党工委委员、</p><p>组织部（人社局）部（局）长</p><p>王作昌及开发区40名干部，参加了此次培训。</p><p>四川大学成人继续教育学院工会主席殷明教授，在开班仪式上对准东开发区干部一行的到来表示热烈欢迎，并向学员们介绍了四川大学的办学历史，同时也希望学员们能在短暂的学习时间里，学有所获。</p><p>王作昌在开班仪式上做了动员讲话，要求大家担起身上肩负的责任和使命，珍惜学习时间，端正学习态度，结合工作实际，认真学习、认真思考，带着问题来， 带着答案回，达到一人学习多人受益的效果。</p><p>1月11日至1月13日，学员们共聆听6场讲座。</p><p>三天里学员们聆</p><p>听的</p><p>是更具专业、更具权威的匠者心声，接受的是更具前瞻、更加务实的教育理论。</p><p>专</p><p>家们的讲座既推心置腹、客观实在，又新颖深邃、发人深思；</p><p>既有丰富理论的滋养，又有前沿实践的指引</p><p>，令人耳目一新，深受启发。</p><p>据了解，此次培训共6天，主要围绕“人才激励之道与绩效考核”“人力资源管理”“国学智慧与现代管理”“如何培养干部的情商与影响力”“企业目标管理”“国家级新区及人才体系建设研究”“人才培养的新思路、新方法”“人才培养的新思路、新方法”“商务礼仪”以及成都经济技术开发区（龙泉驿区）人才管理体制机制经验进行授课。（文/图 乔威）</p><p>1月13日准东开发区空气质量状况</p><p>空气质量指数AQI：43</p><p>空气质量指数级别：一级</p><p>空气质量指数类别：优</p><p>空气质量颜色预警：绿色</p><p>【昌吉州气象局】</p><p>准东气象台发布2020年1月13日夜间到14日白天天气预报：准东国家经济开发区多云，气温-23到-9度，风力3级。</p><p>来 源：</p><p>党政办</p><p>编 辑：</p><p>刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>联系电话：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[]', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000037', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32795', '准东新闻', '准东新闻', '', '​规划引领 打造准东开发区经济产业生态圈', '', '2020-01-14', '<p><img src=\"http://36.189.242.73:8084/uploads/20200114/1578933392616035129.jpg\" title=\"1578933392616035129.jpg\" alt=\"111111.jpg\"/></p><p>1月8日至9日，昌吉州林草局及北京林业大学相关领导一行莅临准东开发区，对接准东开发区“十四五”及今后15年林业生态规划编制工作，开发区水务局、规建局、环保局、国土局等部门负责人参加会议。</p><p>会上，开发区水务局负责人详细介绍准东开发区生态规划有关背景、总体思路及设想情况，提出规划必须坚持“经济可持续发展、生态可持续发展”理念，深入贯彻习近平总书记“坚定走生产发展、生活富裕、生态良好的文明发展之路，构建人与自然和谐发展现代化建设新格局，科学编制规划方案，争取列入区州、国家“十四五”林业规划范围，争取国家重点林业项目落地准东，为准东开发区生态建设提供资金支持、科技支撑、理论依据、实践保障，推动开发区在实现高质量发展上不断取得新进展。</p><p>会后，参会人员实地调研开发区湿地公园、无灌溉造林、企业绿化等生态建设情况，北京大学相关人员表示，将广泛听取意见，科学编制林业生态规划，为开发区经济产业生态圈建设明确方向，为准东开发区高质量发展奠定坚实基础。（文/图 汤金凤 ）</p><p>1月12日准东开发区空气质量状况</p><p>空气质量指数AQI：43</p><p>空气质量指数级别：一级</p><p>空气质量指数类别：优</p><p>空气质量颜色预警：绿色</p><p>【昌吉州气象局】</p><p>准东气象台发布2020年1月12日夜间到13日白天天气预报：</p><p>准东国家经济开发区多云，气温-23到-9度，风力3级。</p><p>来 源：水务局</p><p>编 辑：刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>联系电话：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200114/d011f016c6c242c89f87f5c8382c6cfb.jpg', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[]', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000011', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32796', '准东新闻', '准东新闻', '', '昌吉州党委宣布干部任命：吉木萨尔县副县长张艳珍兼任准东开发区党工委委员', '', '2020-01-16', '<p><br/></p><p>1月7日，准东开发区召开会议，昌吉州党委组织部副部长韩宏宇出席会议并宣布州党委干部任职决定：吉木萨尔县副县长张艳珍兼任准东开发区党工委委员。准东开发区党工委书记李绍海主持会议并讲话。开发区党工委班子成员任建品、史海生、周友仁、丁志春及开发区各部门主要负责人参加会议。</p><p><img title=\"1579169097174076355.png\" alt=\"1.png\" src=\"http://36.189.242.73:8084/uploads/20200116/1579169097174076355.png\"/></p><p>会上，韩宏宇宣读州党委干部任职文件并介绍了张艳珍同志的工作简历，张艳珍作了表态发言。</p><p><img title=\"1579169120460001567.png\" alt=\"2.png\" src=\"http://36.189.242.73:8084/uploads/20200116/1579169120460001567.png\"/></p><p>李绍海主持会议并讲话</p><p><img title=\"1579169163479080086.png\" alt=\"3.png\" src=\"http://36.189.242.73:8084/uploads/20200116/1579169163479080086.png\"/></p><p>张艳珍作表态发言</p><p>李绍海说，坚决拥护州党委的决定。州党委安排张艳珍同志兼任准东开发区党工委委员，这是州党委根据准东开发区党工委领导班子建设和实际工作需要作出的重大决定，体现了州党委对准东开发区领导班子建设和准东发展的高度重视。真诚欢迎张艳珍同志来准东工作，张艳珍同志思想政治素质好，经历了多个重要岗位的锻炼，取得了显著的工作业绩，积累了丰富的工作经验。兼任准东开发区党工委委员之后，希望进一步解放思想、开拓创新，敢于担当、勇于克难，充分发挥桥梁纽带作用，做好开发区与吉木萨尔县之间的协调工作，与开发区党工委班子一道推动准东各项事业再上新台阶，不负组织重托和群众期昐。</p><p>李绍海指出，当前，准东开发区正值建设“现代煤电煤化工创新产业开发区”的关键时期，处于三年奋斗规划开局之年，大家一定要心往一处想、劲往一处使，坚持一套班子抓稳定、一套班子抓发展，严格落实州党委各项决策部署，努力打造营商环境新高地，推进经济高质量发展，以实际行动和发展成效向州党委、向8万准东各族人民交上一份满意的答卷。（文/王前喜 王永刚 图/刘歌）</p><p><img title=\"1579169176690030493.png\" alt=\"4.png\" src=\"http://36.189.242.73:8084/uploads/20200116/1579169176690030493.png\"/></p><p>张艳珍同志简历</p><p>张艳珍，女，汉族，1976年1月生，新疆吉木萨尔人，1996年1月参加工作，1999年1月入党，在职大学学历（2004年8月新疆自治区党校经济管理专业毕业）。</p><p>1993.09—1995.07 昌吉财校财贸专业学习;</p><p>1996.03—2001.05 北庭镇小学任教师；</p><p>2001.05—2006.01 北庭镇政府先后任党委秘书、组织干事、信访干事、党政办主任；</p><p>2006.01—2009.01 北庭镇纪检委书记；</p><p>2009.01—2011.03 北庭镇党委副书记；</p><p>2011.03—2015.02 新地乡党委副书记、乡长；</p><p>2015.02—2016.07 新地乡党委书记；</p><p>2016.07—至今 吉木萨尔县人民政府副县长；</p><p>2019.12—至今 新疆准东经济技术开发区党工委委员（兼）</p><p><br/></p><p>来 源：党政办</p><p>编 辑：刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>联系电话：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200116/60587ccfc0414897959bde9d5cf1ab0d.png', '信息中心', '1', '7', '1', '', '', '0', '', '0', '[]', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000042', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32797', '准东新闻', '准东开发区零距离', '', '准东大讲堂第二讲：政务商务文明礼仪', '', '2020-01-17', '<p>1月14日，准东开发区举办第二期准东大讲堂——“政务商务文明礼仪”专题培训，200多名干部职工参加了培训。</p><p><img title=\"1579259788246042658.png\" alt=\"企业微信截图_15792597608763.png\" src=\"http://36.189.242.73:8084/uploads/20200117/1579259788246042658.png\"/></p><p>开发区党工委委员、党政办主任赵斌主持培训会。</p><p>培训邀请了昌吉州党校高级讲师、综合教研室主任、区州党校系统优秀教师杨可前来讲授。文明礼仪培训，不仅是提升个人内在修养、外在形象的需要，也是提高工作效率、维护准东开发区形象的内在要求。</p><p>讲座中，杨可重点从政务礼仪概述、政务礼仪着装、政务会晤礼仪、办会会议礼仪等方面作了详细讲解，并结合自身工作经验，通过举例分析，使学员们逐步加深对提升政务服务水平重要性的认识和理解。</p><p><img title=\"1579259851118034933.png\" alt=\"企业微信截图_157925984122.png\" src=\"http://36.189.242.73:8084/uploads/20200117/1579259851118034933.png\"/></p><p>学员们表示，今后将把学到的知识充分运用到实际工作中，服务过程中做到有礼、有仪，不断提升政务形象，以良好的政务礼仪姿态展示准东人的全新面貌，助力开发区经济高质量发展。</p><p>（文/刘歌 图/王前喜）</p><p><br/></p><p>来 源：党政办</p><p>编 辑：刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>联系电话：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200117/1251af53988a476fa1ed69c306f1fd62.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[]', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000047', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32798', '准东新闻', '准东开发区零距离', '', '准东开发区政务服务热线公示', '', '2020-02-04', '<p><br/></p><p>为进一步提高政务服务水平，优化营商环境，为办事群众、企业提供便利，现将准东开发区政务服务热线做如下公示：</p><p>投资服务中心总机/优化营商环境专线：0994-6738700</p><p>税务局专线：0994- 6928109</p><p>市场监督管理局专线：0994-6928577</p><p>社保业务专线：0994-6902556</p><p>热线接听时间：周一至周日 10:00-20:00</p><p>准东开发区政务服务热线目前主要受理事物咨询类、监督投诉类、建议意见类三类业务，欢迎办事群众来电，准东投资服务中心将竭诚为您解答。</p><p><br/></p><p>编 辑：刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/D:/wehchat_img/wechat_zw_img/475fce1439d0476ab8388684c47c66d0.jpg', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/475fce1439d0476ab8388684c47c66d0.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000009', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32799', '准东新闻', '准东开发区零距离', '', '15.5万元：开发区紧急划拨党费用于疫情防控', '', '2020-02-06', '<p><br/></p><p>近日，准东开发区组织部从代管党费中划拨15.5万元，用于支持各产业园、有关部门、有关企业开展疫情防控工作。</p><p>这笔党费主要用于慰问战斗在疫情防控斗争第一线的医务工作者和基层党员、干部职工；补助因患新型冠状病毒感染的肺炎而遇到生活困难的党员、群众；购买疫情防控药品及物资，包括口罩、防护服、防护镜、手套、消毒液、消毒酒精等。各产业园、有关部门、有关企业要做到专款专用，确保专项党费在疫情防控工作中发挥传递组织温暖、解决实际困难、凝聚党心民心的重要作用。</p><p>开发区组织部要求，各基层党组织和广大党员要坚定坚决贯彻落实习近平总书记重要指示精神和党中央决策部署，按照区、州党委和开发区党工委工作安排，把疫情防控工作作为当前最重要的工作来抓，充分发挥领导干部表率作用、基层党组织战斗堡垒作用和党员先锋模范作用，不忘初心、牢记使命，不畏艰险、英勇奋斗，科学防治、精准施策，让党旗在防控疫情第一线高高飘扬，坚决打赢疫情防控这场硬仗，确保开发区群众职工生命安全和身体健康。</p><p>开发区组织部将对下拨党费跟踪检查，各产业园、有关部门、企业要把党费切实用到防控疫病上，不能挪用党费，如发现挪用的将严肃追究相关责任人的责任。（文 季彦林）</p><p>来 源：组织部</p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200206/f70143fee6764df98f0a596b50a4b88d.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/1ac8b9d98e34492f85160032e227fb56.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000012', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32800', '准东新闻', '准东开发区零距离', '', '开发区产业园党委全力以赴打好疫情防控阻击战', '', '2020-02-06', '<p><br/></p><p>“大家出门前一定穿好防护服，戴好口罩、手套、护目镜。”每天外出行动前，准东开发区火烧山产业园党委书记云晓霆都要对工作人员再三叮嘱。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200206/1580980598402064029.png\" title=\"1580980598402064029.png\" alt=\"企业微信截图_1580980465842.png\"/></p><p>疫情就是命令，防控就是责任。面对突如其来的新型冠状病毒肺炎疫情，开发区各产业园党委快速响应、全面动员，采取多种措施打好疫情防控阻击战。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200206/1580980617467034219.png\" title=\"1580980617467034219.png\" alt=\"企业微信截图_15809804942537.png\"/></p><p>在疫情防控的特殊时期，每天来产业园办事的职工群众非常少。人员流动少，防护措施不能少。消毒、测体温、发放口罩，各产业园接待每一位办事群众都要做好的事前防护三步，保证产业园办公楼内部的防疫安全。同时，产业园还安排专门的工作人员每天对办公楼喷洒消毒液，做到消杀病毒无死角；每天对居家隔离的工作人员进行两次测体温并做好记录，同时做好被隔离人员的心理疏导工作。</p><p>各产业园还充分发挥辖区宣传阵地作用，不停地转发《新型冠状病毒感染的肺炎公众防护指南》《新型冠状病毒肺炎预防手册》等；在办事大厅设置大屏幕电视全天播放《防御新型冠状病毒公益广告》、《口罩如何正确佩戴》、《正确的洗手方法》等宣传视频；在大楼门口设置“大喇叭”循环播放《致湖北来返准东开发区人员的一封信》、《关于预防新型冠状病毒感染肺炎的健康指导》、《准东开发区新型冠状病毒感染肺炎疫情防控举报奖励办法》等音频。同时，各产业园还在在辖区的服务区、十字路口等显眼处悬挂、张贴防控疫情标语、宣传单，增强了战疫情必胜的信心，营造心往一处使、众志成城迎战困难的氛围。</p><p>（文/图 王新斌 楚亚飞）</p><p>来 源：</p><p>火烧山产业园 党政办</p><p><br/></p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200206/97a3d4b3f318435ea0d31bed81e22235.png', '', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/5fea57abfe8946e6977158d3d0f4f408.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/66a65b355afe47588fe56444fe7aa048.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/d2f7221d7e1c4574b6d733a7d024114a.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32801', '准东新闻', '准东开发区零距离', '', '准东公安民警坚守防控一线逆行最美', '', '2020-02-20', '<p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200220/1582205766351048521.png\" title=\"1582205766351048521.png\" alt=\"1.png\"/></p><p>疫情就是命令，防控就是责任。在这场疫情防控的人民战争、总体战、阻击战中，准东公安分局全体民辅警挺身而出，冲锋在前，守护着准东辖区人民群众的生命安全，他们是最美的逆行者。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200220/1582205778911072590.png\" title=\"1582205778911072590.png\" alt=\"2.png\"/></p><p>公安检查站和治安卡点全体警务人员严格执行查缉勤务要求，强化对车辆、人员、物品的双向查缉；多时段、高频次对重点场所进行巡查，全面配合相关部门加强人员进出管理，协助做好体温检测、筛查。<br/></p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200220/1582205802030056457.png\" title=\"1582205802030056457.png\" alt=\"3.png\"/></p><p>便民警务站的警务人员深入一线，战“疫”情，对辖区内所有餐饮店、宾馆、药店、超市等人员密集场所进行全面排查和告知，竭力保障辖区的平安环境。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200220/1582205813978014151.png\" title=\"1582205813978014151.png\" alt=\"4.png\"/></p><p>交警大队新增3个临时检查点，24小时值班值守，确保进入辖区道路无死角。</p><p>（文/图 石娅丽）</p><p>来 源：准东公安分局</p><p><br/></p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200220/8e7bee1843ea4a29abb132acdb05dc77.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/27c2b2ec4e4c4bbdb8832e8c6c3e7e26.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/0a2bc5e59e6f4bae888d3afbe32addf6.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/abdfeb1efd8a4ebb986b601a65e58942.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/787bf688c62f40e895093200317097af.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/8e36c152c22e490784cf134b066552e8.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32802', '准东新闻', '准东开发区零距离', '', '准东开发区安监局开展疫情防控期间安全生产检查和服务指导', '', '2020-02-24', '<p><br/></p><p>近日，准东开发区安监局指导检查组深入11家工贸企业一线开展现场检查与服务指导，督促各企业做好疫情防控工作的同时紧抓安全生产，切实做到“两不误，两促进”。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200224/1582555277260059035.png\" title=\"1582555277260059035.png\" alt=\"企业微信截图_15825552478763.png\"/></p><p>指导检查组有针对性地对企业主要负责人及安全管理人员履职情况，生产现场的安全管理，安全设施设备运行维护、危险作业安全防护、安全培训教育、风险分级防控与隐患排查治理双重机制的推行、应急救援演练等方面进行严格检查。对企业生产中存在的生产设备和维修配件采购、原材料供应、物流运输、重要岗位人员通行等相关问题，进行了登记并与相关部门协调解决，制定相关安全制度和预案，细化责任到人，确保疫情期间的安全生产工作持续稳定开展。同时，指导检查组要求各企业主要负责人持续开展“一封家书”与“反三违”活动，让每一位职工树立“安全第一”的思想，达到双向管控的目的。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200224/1582555335725026031.png\" title=\"1582555335725026031.png\" alt=\"企业微信截图_158255531722.png\"/></p><p>后期，安监局将按照“查大风险、除大隐患、防大事故”的要求，持续开展安全生产大检查、集中整治和安全风险研判，做好安全管控各项工作，为打赢疫情防控阻击战营造良好稳定的外部环境。</p><p>（文/图 李俊杰）</p><p>来 源：</p><p>安监局</p><p><br/></p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200224/ab54b1caa6da446eb605bebb82955e73.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/ae185f604962490c9a12bd3485116c2f.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/be920bc265fd49c5ba56f2191e3db463.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/a881a9dc73d94b4b9897153db676522a.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/04973869f4114b00b489322282ea35a3.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32803', '准东新闻', '准东开发区零距离', '', '准东开发区国企再显责任担当新疆宜化6675公斤多消毒原液捐赠战“疫”一线', '', '2020-02-25', '<p><br/></p><p>疫情当前，消毒液不可少。2月22日，准东开发区新疆宜化化工有限公司向火烧山产业园捐赠了3000公斤84消毒原液，为疫情防控工作提供了有力支持。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200225/1582634744901042114.png\" title=\"1582634744901042114.png\" alt=\"1.png\"/></p><p>当天下午，新疆宜化拉载着消毒原液的车辆将消毒原液送到火烧山产业园后，又被分送到各企业。火烧山产业园党委书记云晓霆说，企业尽社会义务为疫情防控工作提供有力支持，激励产业园干部职工将以更加努力工作状态，全力做好疫情防控工作。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200225/1582634763083052930.png\" title=\"1582634763083052930.png\" alt=\"2.png\"/></p><p>据了解，自2月14日以来，该企业已先后为10家单位捐赠了6675公斤消毒原液。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200225/1582634778640009402.png\" title=\"1582634778640009402.png\" alt=\"3.png\"/></p><p>该公司党委副书记冉琳介绍，在疫情防控工作中，公司结合自身是化工企业的条件，尽己所能为社会免费捐助消毒原液，同开发区全体齐心协力、万众一心，坚决打赢疫情防控阻击战。为确保供应充足，公司干部职工主动放弃休息，坚守生产一线，现场查看设备运行情况、关注工艺运行参数，确保次氯酸钠生产安全稳定。目前，公司陆续接收到社会各界的物资支援需求，公司将坚持履行社会责任，全方位组织生产，竭尽所能保证供应。</p><p>（文/图 王前喜 冯亮）</p><p>【小常识】</p><p>84消毒原液主要成分次氯酸钠，是广谱、高效、安全的消毒剂，按照1：</p><p>100配水制成84消毒液，用于消毒杀菌防疫。</p><p>来 源：党政办、新疆宜化化工有限公司</p><p><br/></p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200225/f0091188cad44e24a143342592c9581c.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/ed7fda27abfb48d2b83914bb7ed5d076.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/345f981170454eaeb109f405406ec127.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/589476c7342a45e7b37e4de8e3fec224.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/8b546cc11114474e8513b8f81e9c02ed.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/130bde65541840f28218f5301341017f.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32804', '准东新闻', '准东开发区零距离', '', '首批331名，准东开发区企业员工返企复工', '', '2020-02-26', '<p>2月24日，新疆天池能源有限责任公司将军戈壁二号露天煤矿98名员工返企复工。当天，随该企业一并返厂的还有新疆天池能源有限责任公司（南露天煤矿）的160名员工、新疆准东特变能源有限公司的73名员工。此次返企的331名员工是准东开发区企业自春节以来的首批返企人员。<br/></p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200226/1582726222820044362.png\" title=\"1582726222820044362.png\" alt=\"1.png\"/></p><p>杜龙是新疆天池能源有限责任公司将军戈壁二号露天煤矿机电科副科长，是当天返企复工的331名人员之一。他在电话里介绍，24日8时20分许，包括他在内98名返岗人员分别乘坐3辆客车出发，车上人与人之间坐位相距至少有1米多。14时30分许，他们抵达天池能源有限责任公司将军戈壁二号露天煤矿门口，随后防疫人员对他们进行了体温测试。在他们进入公司为他们准备的单人间宿舍后，便开始了复工前的准备。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200226/1582726232229037774.png\" title=\"1582726232229037774.png\" alt=\"2.png\"/></p><p>据了解，此次返岗复工人员为专业技术岗位人员和普通员工。在杜龙等98名员工返厂前，该公司将军戈壁二号露天煤矿在开发区疫情防控指挥部的指导下，制定了严密的返岗人员流程管理方案。方案采取点对点的流程进行人员转运，从接人车辆到达公司门口开始到返厂复工人员住宿、就餐、消毒杀菌、测温、上下班和岗位安排、岗前教育培训等各环节都有详细且严格的管理规定。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200226/1582726242140062505.png\" title=\"1582726242140062505.png\" alt=\"3.png\"/></p><p>该公司（南露天煤矿）为做好复工人员的防疫工作，在新建宿舍楼中腾出198间单人宿舍，用于160名返矿人员住宿，并严格落实好每天统一健康筛查、统一配餐、统一上下班接送、统一住宿、统一岗位管理的要求。</p><p>近期，开发区将有序迎来返厂复工人员。</p><p>（文/图 王前喜）</p><p>来 源：党政办</p><p><br/></p><p>编 辑：王绍琼</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>新闻热线：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200226/c719e01778b74a608acd7effa5807b93.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '[\'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/927144f9fee94ccb9e0845a8b555ce90.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/4af986595d56428b9b19da793dd807b8.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/f2991eb420ec4ef0b0a5cab007e67fec.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/cbc0dd726a304b299c50fa0c3bca3e39.jpg\', \'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/888eaa8e28b84e649aa15fe010732080.jpg\']', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32805', '准东新闻', '准东开发区零距离', '', '“元旦”期间出行安全注意事项', '', '2020-01-01', '<p>2020年元旦期间，短途出行增多。准东交警大队对辖区冬季道路安全情况进行分析研判，现向社会发布“两公布一提示”信息。准东交警提醒广大驾驶员朋友注意行车安全，减速慢行！在准东辖区行车途中，如遇困难，请及时拨打昌吉州公安局准东经济技术开发区分局交警大队救援电话：110；0994-5303817。</p><p>一、天气预判</p><p>据气象部门预判：元旦期间辖区整体天气状况良好，受近期降雪、降温天气影响，因温差早晚或会出现大雾天气，辖区部分路段会有结冰，尤其是路口处结冰较为明显，请各位群众驾车出行期间注意行车安全。</p><p>二、车流量预判</p><p>元旦期间返乡探亲、短途旅游、访友聚会等有所增加，预计辖区S11线、G216线、S239线客运车流量将有所增加，但辖区整体车流量较为平稳。遇到道路拥堵请您耐心排队等待，听从交警指挥，切莫随意加塞，避免引发不必要的交通事故。</p><p>三、交通安全隐患及危险路段提示</p><p>1、Z917线24公里（吉彩路交叉路口）至芨芨湖路段存在多处弯道，连续上下坡道，大型货运车辆流量较大，请驾驶员朋友们谨慎驾驶。</p><p>2、G216线599公里至618公里，路基较高，有多处弯道，请驾驶员朋友们减速慢行，谨慎驾驶。</p><p>3、S239线东方希望厂区与东方希望生活服务区附近交叉路口有行人及非机动车出入，请过往车辆减速慢行，注意非机动车和行人。</p><p>4、S228线芨芨湖镇296公里至306公里处时常有骆驼和马匹出没，请驾驶员减速慢行。</p><p>5、S327线60公里处路基较高，请驾驶员朋友们谨慎驾驶。</p><p>四、交通安全提示</p><p>1.保持良好车况出行。节日期间车辆使用率高，出行前，请提前对车辆进行安全检查和保养，确保车辆转向、制动、轮胎、灯光等关键部件安全状况良好，并随车携带警告标志牌、千斤顶、灭火器等工具，以备急需。</p><p>2.关注天气及路况信息。及时通过交通广播、微信公众号、电视、微博等媒介，了解出行路线沿途的天气、交通流量、交通管制、加油站和服务区分布情况等路况信息，提前选择出行路线，避免出现迷路、车辆燃油不足等情况。</p><p>3.杜绝各类交通违法陋习。节日期间驾车出行，请自觉遵守交通法规，拒绝超速行驶、疲劳驾驶、随意停车；亲朋好友聚会聚餐，要拒绝超员、酒后驾驶。</p><p>4.注意冰雪、大雾天气行车安全。冬季路面湿滑，雾天行车时请及时开启示廓灯和雾灯，适当增大跟车距离，保持安全车速，注意观察道路情况，遇情况提前采取制动、减速措施，避免紧急制动、紧急转向，以防车辆侧滑发生危险。</p><p>5.驶离路口时注意。行驶时及时查看出入口的交通标识和指路标牌，接近出口时，提前开启转向灯再减速并线，避免到路口才猛减速、急并线，甚至驶过路口后再倒车、逆行，以上做法都十分危险。</p><p>6.发生轻微事故快速撤离。车辆发生故障时，要及时将车辆移至路肩，开启双闪灯，并在车后100米外设置故障三角警示牌进行提醒。如发生轻微交通事故，应在确保安全的条件下拍照后立即撤离现场，驶至安全地带后自行协商处理或报警等候交警处理，以防造成交通拥堵或引发二次事故。（刘姣）</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '信息中心', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32806', '准东新闻', '准东开发区零距离', '', '准东开发区招商专题培训班在上海交通大学圆满结业', '', '2020-01-02', '<p style=\"text-align: left;\">12月30日，为期6天的准东开发区招商引资培训班在上海交通大学闵行校区圆满结业。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8085/wehchat_img/wechat_zw_img/fe5d493d69c74c5ebf2205b40c546121.jpg/\"/></p><p>此次培训根据开发区发展实际，从中国经济的新方位、产业聚集、产业链招商、招商谈判等多个角度，结合上海自由贸易试验区深化放管服改革、张江科技园区产业链招商、苏州工业园的构筑人才高地等具体案例，量身设计课程，系统讲授了当前的经济形势、面临机遇、存在问题和应对措施等学习内容。课堂教学形式多样，主要采取了现场教学、观摩考察等方式进行，得到了学员的一致好评。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8085/wehchat_img/wechat_zw_img/6d35da474257428592390687778cde2d.jpg/\"/></p><p>全体参训学员学风端正、严守纪律、认真听课、深入思考，纷纷表示此次培训思维受到启迪，视野得到了拓展，感受到了自身在开展招商引资工作中存在的差距，明确了今后的努力方向。下一步要把此次培训成果转化为工作实际效果，牢固树立新发展理念，进一步创新招商工作方式，为推进准东开发区产业转型升级和实现经济高质量发展做出贡献。（张调霞 尹鹏）<br/></p><p></p>', 'http://36.189.242.73:8084/uploads/20200307/74b2c507ac9a43a5af2434921b9b5d02.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/fe5d493d69c74c5ebf2205b40c546121.jpg, http://36.189.242.73:8085/wehchat_img/wechat_zw_img/6d35da474257428592390687778cde2d.jpg, http://36.189.242.73:8085/wehchat_img/wechat_zw_img/ca04b3f6cbb440faa9c612782758b144.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000003', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32807', '准东新闻', '准东开发区零距离', '', '准东开发区环保局着力解决企业废水处理难题', '', '2020-01-03', '<p></p><p>2019年12月30日，准东开发区环保局邀请武汉凯迪水务有限公司与辖区8家企业就电厂脱硫废水处理技术方面进行了座谈交流，助力企业提质增效，解决影响企业环保治理的“难点”“痛点”，推动开发区废水处理攻坚工作。<br/></p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583558319046069264.png\" title=\"1583558319046069264.png\" alt=\"企业微信截图_15835582863718.png\"/></p><p>据悉，武汉凯迪水务有限公司是一家专业从事水处理工作的综合服务企业，拥有国际先进技术和经验，研发了脱硫废水“零排放”技术，此技术中采用不同孔径的纳米膜去除废水中杂质，纯度可达99.99%，与传统脱硫废水技术相比，这项技术优势明显，不仅降低企业处理废水成本，而且可以提高处理效率，达到“零排放”的效果。</p><p>座谈中，武汉凯迪水务有限公司负责人围绕浓盐水超浓缩技术、结晶产物资源化技术、蒸发结晶等废水零排放的核心技术展开讲解，并针对各企业环保负责人提出的问题进行解惑答疑。</p><p>企业相关负责人纷纷表示，采用这种面对面的交流座谈，大家都直面问题，畅所欲言，让好的经验既“走出去”又“走进来”，切实为企业把准了关键问题和难点问题，做到了“对症下药、靶向治疗”，对提升企业水污染防治工作水平具有重要意义，对开发区营造良好营商环境也具有极大促进作用，会后将认真研究，统筹谋划，精心实施，坚决完成2020年水污染防治各项工作。（南丽君）</p>', 'http://36.189.242.73:8084/uploads/20200307/9a5555cd1a424b35bf4fc468823b6321.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/8a2aa2f877234981bf402aba047c8771.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32808', '准东新闻', '准东开发区零距离', '', '准东言｜大漠庆生更温暖（四十一）', '', '2020-01-04', '<p style=\"text-align: center;\">大漠庆生更温暖（四十一）<br/></p><p style=\"text-align: center;\">雷 锦</p><p>伴随过往的日日夜夜，承载各项事业的灿烂辉煌，2019年虽离我们渐渐远去，但对准东经济技术开发区的干部职工来说，岁末年初的12月却温馨暖心、情意悠远，烙印在记忆深处。因为开发区党工委、管委会给无法回家与亲人团聚的30多名同月出生的机关单位干部职工过了一个难忘的集体生日，这是开创准东经济开发区先河的一次意义特殊的纪念活动。几桌饭菜和几盒生日蛋糕看似简单，却在大漠寒冬中洋溢着浓浓的人性化关爱和满满的暖心热流。</p><p>国有节庆，家有记史，人有生日。不忘关心、牢记庆生，培养家国情怀、弘扬人文关怀，这是中华民族优良传统文化的题中应有之义，也是在时代飞速发展、世事异常繁重的当下，回应人们情感中渴望需求的基本功课，更是新形势下做好思想政治工作不可或缺的基本任务。</p><p>马斯诺原理将人的需求分为生理需求、安全需求、爱和归属需求、尊重需求、自我实现需求。其中，“尊重需求”属于较高层次的需求，表现为希望获得他人、社会对自己的认可与尊重。由此可见，给干部职工过生日看似些许小事，但对一个人来讲却是事关社会地位、人生尊重的礼遇和价值，这往往能触及人的情感软肋，最能激发内心深处的感动和感激。</p><p>在以发展为主要特征的当今社会，作为基层组织和单位，抓大事、理要务毋容置疑，但洞悉情感世界、细心做好群众的衣食住行和慰藉心理情感的“小事”也不可忽视。因为“一滴水中见太阳”，它体现的是组织的关怀，创造的是一种“能量转化”，释放出不可估量的裂变和聚变式的精神“核效应”，凝聚强大的正能量。</p><p>做好思想政治工作是我党的优良传统、制胜法宝，任何物质财富都无法替代其神奇的功用，它是保障经济发展的动力源、维护社会和谐的稳压器。任何组织、任何单位在任何时候都要以人为本，因人制宜地做好思想政治工作。尤其是边远艰苦地方，各级组织更应将关爱人的思想工作作为一项常态化政治任务，牵挂于心、付诸于行，不管多忙、时间多紧，都不要忘记群众所思所盼的心事，多些雪中送炭、多些心灵鸡汤、多些锦上添花，在点滴之中显现组织关爱，于细微之处体现人文精神，挖掘以情感人的潜力，凝聚干事创业的强大力量，在新的一年里，去战胜一切困难 、创造一切人间奇迹。</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '信息中心', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32809', '准东新闻', '准东开发区零距离', '', '准东开发区三家企业获130万元科技项目资金支持', '', '2020-01-05', '<p>近日，从准东开发区经济发展局传来好消息，开发区三家企业的三个项目分别获得昌吉州科技创新资金奖励，共计130万元。</p><p>这三个项目分别是新疆亘源环保有限公司以电石渣提纯氢氧化钙综合利用项目获得50万元项目资金支持、新疆协鑫新能源材料科技有限公司以科技引领多晶硅产业高效发展专项--提高冷氢化转化效率新工艺研究和应用项目获得50万元资金支持、新疆天池能源有限责任公司以智能防撞系统项目获得30万元项目资金支持。</p><p>昌吉州科技计划项目是昌吉州科学技术局为“整合创新资源，提升财政科技资金使用效率”目标要求，改革设置“科技支撑产业高质量发展专项、“乌昌石”国家自主创新示范区建设专项、创新环境建设专项、协同创新与成果转化专项”四个专项，突出体现科技创新支撑经济高质量发展和科技惠民的能力和效果而设立，由州科技局邀请所有相关领域的专家，对所申报的项目进行评分，最终确定支持项目。</p><p>据了解，所有资助资金均已打入企业账户，准东两家企业是昌吉地区申报的118家企业中资金支持力度最大的企业。此次资金支持是对企业坚持创新驱动的肯定，激励了开发区企业主动提升科技创新水平的积极性，为开发区打造现代煤电煤化工创新示范园区提供资金保障和创新驱动。（王强）</p>', 'http://36.189.242.73:8084/uploads/', '信息中心', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32810', '准东新闻', '准东开发区零距离', '', '准东开发区消防大队开展消防疏散逃生演练', '', '2020-01-06', '<p>近日，准东开发区消防大队联合各部门在准东投资服务中心开展消防安全疏散演练，旨在加强干部职工消防安全意识，提升火灾预防和应急处置能力。</p><p><img src=\"http://36.189.242.73:8085/wehchat_img/wechat_zw_img/577b092d0097449ba4a4e337dd8cd5e0.jpg/\"/></p><p>活动中，消防大队宣讲员为干部职工讲解消防知识，介绍灭火器“一拔二拉三提四喷射”的使用方法，以及灭火过程和疏散逃生中需要注意事项。</p><p><img src=\"http://36.189.242.73:8085/wehchat_img/wechat_zw_img/1b49148710a94f1e9d2ce78aaee85fd8.jpg/\"/></p><p>演练开始，听到警铃后，各部门干部职工听从指挥引导疏散，用湿毛巾、口罩等捂住口鼻，弯腰下楼，按秩序就近从安全出口到空旷处应急避险。<br/></p><p>通过此次消防安全疏散演练活动，各部门明确消防安全重要性，普及了应急救援知识，积累了消防安全的“实战”经验，增强了火灾初期扑救和组织人员安全疏散逃生的能力，营造了人人参与消防，人人关注消防的良好氛围。（陈美君）</p>', 'http://36.189.242.73:8084/uploads/20200307/f1f8ff74157342239434750580512ea7.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/577b092d0097449ba4a4e337dd8cd5e0.jpg, http://36.189.242.73:8085/wehchat_img/wechat_zw_img/1b49148710a94f1e9d2ce78aaee85fd8.jpg, http://36.189.242.73:8085/wehchat_img/wechat_zw_img/5b6094b4d09047fcb3782a6bd87733de.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32811', '准东新闻', '准东开发区零距离', '', '中电投北二电厂1号机组首次并网一次成功', '', '2020-01-07', '<p>1月2日，中电投新疆准东五彩湾北二电厂有限公司1号机组首次并网一次成功！机组各项参数符合设计要求，系统运行正常，为下一步机组满负荷运行及168小时试运行打下坚实基础。<br/></p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583560121590015055.png\" title=\"1583560121590015055.png\" alt=\"企业微信截图_15835600958682.png\"/></p><p>据了解，1号机组的设计供电煤耗、水耗远低于新建电厂准入标准，达到国内先进水平，大幅提高能源、资源综合利用效率，有效改善生态环境质量。同时，为准东开发区在全国形成煤电产业优势奠定了良好基础。（文/图 杨文家）</p>', 'http://36.189.242.73:8084/uploads/20200307/b638e0fc4b9c4c1cac40d73d96654226.png', '信息中心', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32812', '准东新闻', '准东开发区零距离', '', '这场考试“辣味十足” 现场测评“脸红心跳”——开发区党工委（扩大）会上党工委委员及各单位汇报今年工作时“赛马不相马”', '', '2020-01-08', '<p>这场考试“辣味十足” 现场测评“脸红心跳”<br/></p><p>开发区党工委（扩大）会上党工委委员及各单位汇报今年工作时“赛马不相马”</p><p>1月5日，准东开发区召开党工委（扩大）会议，主要内容是党工委委员及各单位汇报今年工作思路、想法和打算，谁也没料到，一场普通的会议成了一个“辣味十足”的考试，不仅发言时间超过6分钟者扣分，今年工作思路说不清、想法不切合实际、打算不能落实的还要扣分，关键是现场进行民主测评令人“脸红心跳”。会后，这种方式的“考试”在开发区引起了强烈反响。</p><p>当天的会场上，大屏幕上显示着“00：06：00”的字样，只要有汇报开始，数字便开始倒计时。每到倒计时还剩30秒时，便传来会务人员“汇报还有30秒”的提示。“财政局2020年的工作思路是积极经济发展和财政工作的新常态，充分发挥财政职能作用，着力推进产业发展和财源建设；调整优化支出结构，提高财政资金使用效率；继续深入推进国资企业改革；强化金融风险管控，进一步落实金融服务实体经济各项政策；持续强化政府采购监督；继续加强政府类投资建设项目审计工作……”当天11时40分许，开发区财政局局长梁晓川正在作汇报，时不时瞅瞅大屏幕上倒计时，还剩25秒时，他的汇报结束了。然而，当一些部门因6分钟时间内未能汇报完毕被叫停时，不禁汗颜、“脸红心跳”。</p><p>“没想到今年汇报规定的这么严，还好我们的汇报时间没超，思路比较清晰。”公安分局副局长周锐代表该局汇报后说，这样的汇报就是一场考试，使每个单位的工作汇报不仅要切合实际，而且简明扼要。无形中督促单位负责人做到对部门工作心里清楚、脑子明白，对今年工作有新思想、新举措，也是督促各部门营造人人有责、人人负责、人人尽责的良好工作氛围。</p><p>开发区管委会主任任建品对在会上做汇报的党工委委员及各部门进行了点评。他对公安分局、规划建设局、水务局在汇报时有思路、有想法的单位进行了表扬，还对一些汇报质量较差的单位进行了点名批评。</p><p>汇报期间，现场每个人都领到了民主测评表，汇报结束时，测评结果也顺应而生，主持当天会议的开发区党工委书记李绍海对被评为优、良和一般的单位、党工委委员名单进行了现场公布。</p><p>李绍海在总结中指出，开发区全体要坚持习近平新时代中国特色社会主义思想为指导，学习贯彻党的十九届四中全会精神，学习习近平总书记关于新疆工作的重要讲话和重要指示精神、新时代党的治疆方略特别是社会稳定和长治久安总目标，贯彻落实区州党委全体会议和区州“两会”精神，贯彻落实中央和区州经济工作精神，推进落实“1+3+3+改革开放”工作部署。今年是落实新疆维吾尔自治区主要领导赋予准东开发区打造现代煤电煤化工创新产业开发区光荣使命的起步之年，所有人员从去年9月份全员竞聘后，不仅身份转变、思想和观念也要转变，此次的汇报是对党工委委员们和各部门负责人的一场考试，同时也要深刻认识到工作是赛马不是相马，而是激活内因、激发动能。</p><p>李绍海强调，开发区党工委是开发区的领导中枢，必须坚持“一套班子抓稳定，一套班子抓发展”的工作机制，工作始终要保持大局意识、全局意识；各部门要在工作中有新思想、新想法、学创新，从而体现党的先进性；自觉运用新发展理念，指导和引领发展，谋划准东发展；坚持高质量发展，努力实现区州党委赋予的使命。</p><p>李绍海要求，要以创优营商环境为目标，切实抓好“三服务”；要坚持法治、强化“三监管”；要形成区域合作发展，融入开发区发展大局；要加强党的建设，为开发区经济发展提供保障；要做好人力资源保障，由招变找，加强人才培养；要加强党对经济工作的领导，坚持党管大局定政策；要抓好廉政建设、从严治党。</p><p>当天的会上，任建品传达学习了自治州“两会”精神；党工委委员、组织部部长、人社局局长王作昌传达学习了习近平总书记在中共中央政治局“不忘初心、牢记使命”主题教育专题民主生活会上的讲话精神；党工委委员、纪工委书记、监察组组长韩军汇报了赴上海交大开展招商引资专题培训情况。</p><p>党工委委员史海生、周友仁、刘肖文、赵斌参加会议并汇报各自的工作。（文/图 王前喜）</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '信息中心', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32813', '准东新闻', '准东开发区零距离', '', '准东开发区召开2020年金融工作首次例会', '', '2020-01-06', '<p><img src=\"/\"/></p><p><br/></p><p>近日，准东开发区召开2020年第一次金融工作例会，财政局、中国建设银行股份有限公司准东支行、中国工商银行准东支行、中国农业银行股份有限公司准东支行相关负责人参加会议。开发区党工委委员、管委会副主任史海生主持会议。</p><p>座谈会上，3家银行先后汇报了2019年工作情况及2020年的工作计划，并从国家宏观金融政策、开发区经济</p><p>高质量发展</p><p>、当前企业融资需求、帮扶中小微企业发展、搭建开发区金融工作交流平台等多个方面进行交流。</p><p>随后，史海生对三家银行为准东经济高质量发展作出的贡献表示感谢和肯定。他希望，金融机构要和企业发展紧密联系在一起，加强政银企三方的交流沟通，</p><p>优</p><p>化营</p><p>商环境，有效率、</p><p>有</p><p>阶段</p><p>性</p><p>地</p><p>召开</p><p>金</p><p>融</p><p>工</p><p>作</p><p>例</p><p>会，实实在在帮助</p><p>企业</p><p>解决实际</p><p>问题。</p><p>（文/图 高泽磊）</p><p>1月6日准东开发区空气质量状况</p><p>空气质量指数AQI：43</p><p>空气质量指数级别：一级</p><p>空气质量指数类别：优</p><p>空气质量颜色预警：绿色</p><p>【昌吉州气象局】</p><p>准东气象台发布2020年1月6日夜间到7日白天天气预报：准东国家经济技术开发区雾，-22到-11℃，风力3级。</p><p>来 源：财政局</p><p>编 辑：刘 歌</p><p>责 编：王新斌</p><p>主 编：王永刚</p><p>投稿邮箱：3480127975@qq.com</p><p>联系电话：0994-6738618</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32814', '准东新闻', '准东开发区零距离', '', '准东开发区财政局三举措强化预算绩效管理', '', '2020-01-07 ', '<p></p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583568503480080856.gif\" title=\"1583568503480080856.gif\" alt=\"微信图片_20200307160808.gif\"/></p><p>2019年，准东开发区财政局建机制、抓重点，多举措扎实推进预算绩效管理工作，不断提高财政资金使用效益，助力经济高质量发展。</p><p>一是深化预算绩效目标管理。将绩效目标申报融入预算编制全过程，对没有编制预算绩效目标的项目支出，一律不纳入往后预算安排；加强绩效目标审核，确保绩效目标指向明确、细化量化、合理可行。</p><p>二是落实绩效监控、绩效评价监督。定期采集绩效运行信息并汇总分析，对绩效目标运行情况进行跟踪管理和督促检查，以每年5月、8月、11月为监控节点，全面开展监控工作，发现问题立即纠偏，对连续监控结果不佳的项目，及时调整预算。</p><p>三是加强部门绩效培训指导。进一步深化预算绩效管理理念，组织各预算绩效业务人员对预算绩效管理的相关文件、政策、梳理预算绩效各个环节开展集中培训学习，使绩效评价业务水平得以进一步提高。</p><p><br/></p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32815', '准东新闻', '准东开发区零距离', '', '货车司机深夜突发疾病，准东交警暖心救助获赞', '', '2020-01-07 ', '<p><br/></p><p>2019年12月28日凌晨5时许，准东交警大队东方希望中队辅警杨龙、安东在Z917线28公里执行夜间勤务时，一名货车司机在同伴的搀扶下向警车走来求助。</p><p><img src=\"http://36.189.242.73:8084/uploads/20200307/1583568688574019906.jpg\" title=\"1583568688574019906.jpg\" alt=\"微信图片_20200307161044.jpg\"/></p><p>“警察同志，能不能帮助我们去医院，他肚子痛得实在受不了了”。只见面前的男子搀扶着同伴，满脸焦急。旁边的同伴单手捂着腹部，身体弯曲，眉头紧蹙，表情痛苦。得知情况后，执勤辅警杨龙、安东迅速将男子扶上警车，火速前往吉木萨尔县人民医院五彩湾分院进行检查。经医生诊断，所幸只是因饮食不当造成的肠胃不适，经过简单处理后，疼痛明显缓解。</p><p>凌晨7点，杨龙和安东将男子送回货车上，再次确定身体无碍后，随即返回工作岗位。</p><p>谢某在临行前说：</p><p>“你们不仅送我就医，还关心我的冷暖、安全，作为一名长期在外的货车司机我非常感动。</p><p>你们真正的诠释了作为人民卫士的职责！</p><p>”（文/图 刘姣）</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200307/e4bd6c5b0c69464399c2a6bd03f13c23.jpg', '', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/147322cc40314a9bb825dc4a9a210368.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32816', '准东新闻', '准东开发区零距离', '', '货车司机深夜突发疾病，准东交警暖心救助获赞', '', '2020-01-07 ', '<p><br/></p><p>2019年12月28日凌晨5时许，准东交警大队东方希望中队辅警杨龙、安东在Z917线28公里执行夜间勤务时，一名货车司机在同伴的搀扶下向警车走来求助。</p><p><img src=\"http://36.189.242.73:8084/uploads/20200307/1583568688574019906.jpg\" title=\"1583568688574019906.jpg\" alt=\"微信图片_20200307161044.jpg\"/></p><p>“警察同志，能不能帮助我们去医院，他肚子痛得实在受不了了”。只见面前的男子搀扶着同伴，满脸焦急。旁边的同伴单手捂着腹部，身体弯曲，眉头紧蹙，表情痛苦。得知情况后，执勤辅警杨龙、安东迅速将男子扶上警车，火速前往吉木萨尔县人民医院五彩湾分院进行检查。经医生诊断，所幸只是因饮食不当造成的肠胃不适，经过简单处理后，疼痛明显缓解。</p><p>凌晨7点，杨龙和安东将男子送回货车上，再次确定身体无碍后，随即返回工作岗位。</p><p>谢某在临行前说：</p><p>“你们不仅送我就医，还关心我的冷暖、安全，作为一名长期在外的货车司机我非常感动。</p><p>你们真正的诠释了作为人民卫士的职责！</p><p>”（文/图 刘姣）</p><p><br/></p>', 'http://36.189.242.73:8084/uploads/20200307/e4bd6c5b0c69464399c2a6bd03f13c23.jpg', '', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/147322cc40314a9bb825dc4a9a210368.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32817', '准东新闻', '准东开发区零距离', '', '自治区机关事业单位新冠肺炎疫情防控措施指南（试行）', '', '2020-03-07', '<p>为指导全区机关事业单位落实好新冠肺炎疫情防控各项工作要求，根据国务院应对新型冠状病毒感染肺炎疫情联防联控机制《关于依法科学精准做好新冠肺炎疫情防控工作的通知》和自治区疫情防控工作总体安排部署，科学组织、稳步有序开展机关事业单位防控工作，制定本防控措施指南。<br/></p><p>一、准备工作</p><p>1.成立疫情防控机构。</p><p>单位主要负责人是疫情防控工作第一贵任人，要建立疫情防控组织领导机制，制定疫情应急预案和工作方案，完善各项防控应对措施，建立报告制度、明确疫情信息报告人。</p><p>2.落实疫情防控责任。</p><p>机关和事业单位是疫情防控的责任主体，负责单位疫情防控措施的落实，保证疫情防控效果。要建立工作责任制，任务分解到部门，工作落实到人；做好防护物资配备保障，准备口罩、消毒剂、洗手液、体温计等防控物资。进行消毒操作规程和疫情防控措施的培训，指定专人负责本单位疫情防控情况的收集和报送工作。</p><p>3.在办公场所入口处设置个人防护提醒。</p><p>在醒目位置张贴防控提示，开展消毒操作规程和疫情防控措施培训，利用各种显示屏宣传新冠肺炎及其他传染病防控知识。</p><p>4.预防性消毒。</p><p>日常以通风换气和清洁卫生为主，同时对接触较多的公用物品和部位进行预防性消毒，必要时对地面、墙壁等进行预防性消毒。</p><p>二、工作人员管理</p><p>5.加强工作人员管理。</p><p>切实掌握工作人员流动情况和身体健康状况，按照自治区疫情防控工作要求进行健康管理，对来自疫情严重地区的人员实行居家或集中隔离医学观察。</p><p>6.加强健康监测。</p><p>开展工作人员个人健康申报和在岗期间健康状况监测，实行每日健康监测制度，建立体温监测登记本，每天上班前应当对工作人员进行体温测量并做好记录，指定专人每天汇总工作人员健康状况，并向辖区疫情防控工作指挥部疫情防控组报告，对发现异常情况要立即报告并采取相应防控措施。</p><p>三、工作场所内卫生要求</p><p>7.工作场所日常防控和管理。</p><p>工作人员在工作期间应当佩戴口罩，提倡网上办公和弹性工作制，减少人员聚集；工作汇报、沟通尽可能采取打电话的方式进行，必要的会面交谈保持1米以上距离。减少开会频次和会议时长，会议期间温度适宜时应当开窗或开门，条件允许的情况下可采用网络视频会议等方式。保持办公区环境清洁，每日至少2次对办公场所进行消毒，每日通风3次，每次20—30分钟，通风时注意保暖。</p><p>8.严管口罩佩戴。</p><p>工作人员在电梯问、会议室、非单人办公室必须佩戴口罩。安保、保洁、服务等人员工作时必须佩戴口罩。</p><p>9.通风换气。</p><p>优先采取开窗通风方式，自然通风，减少使用空调。有条件的可以开启排风扇等抽气装置以加强屋内空气流动。保证厢式电梯的排气扇、地下车库通风系统运转正常。</p><p>10.空调运行。</p><p>使用集中空调通风系统的单位，应当保证集中空调通风系统运转正常，关闭回风，采用全新风方式运行并关闭空调加湿功能，确保新风直接取自室外、进风口清洁、出风口通畅，室内有足够的新风量。定期对空调进风口、出风口消毒采用有效氯500mg/L的消毒液擦拭；加强对风机盘管的凝结水盘、冷却水的清洁消毒；空调通风系统的清洗消毒按照《公共场所集中空调通风系统清洗消毒规范》进行。</p><p>11.规范垃圾收集处理。</p><p>在公共区域增设废弃口罩专用垃圾桶，加强垃圾箱清洁，定期进行消毒处理，及时收集清运。普通垃圾放入黑色塑料袋，口罩等防护用品垃圾按照生活垃圾分类处理；垃圾筒及垃圾点周围无散落，垃圾存放点各类垃圾及时清运，垃圾无超时超量堆放。垃圾转运车和垃圾筒保持清洁，可定期用有效氯500mg/L的含氯消毒剂喷洒或擦拭消毒；垃圾点墙壁、地面应保持清洁，可定期用有效氯500mg/L的含氯消毒液喷洒。</p><p>12.自动扶梯、厢式电梯。</p><p>尽量避免乘坐厢式电梯，控制乘坐人数，乘坐时必须佩戴口罩。厢式电梯的地面、侧壁应当保持清洁，每日消毒2次。电梯按钮、自动扶梯扶手等经常接触部位每日消毒应当不少于3次。</p><p>13.餐厅、食堂。</p><p>保持空气流通，以清洁为主，预防性消毒为辅。采取有效的分流措施，鼓励单位有组织为工作人员配送餐食，避免人员密集和聚餐活动，餐厅每日消毒1次。</p><p>14.卫生间。</p><p>水冲式卫生间要加强空气流通，每日随时进行卫生清洁，保持地面、墙壁清洁，洗手池无污垢，便池无粪便污物积累；物品表面消毒用有效氯500mg/L的含氯消毒剂对公共台面、洗手池、门把手和卫生洁具等物体表面进行擦拭，30分钟后用清水擦拭干净。早厕要加强通风，粪便加盖做到无暴露，加强日常卫生保洁，及时对厕所室内外环境开展消杀灭蝇工作，对粪便表面使用足量生石灰或含氯消毒剂进行消毒。</p><p>四、疫情应对</p><p>15.设置应急区域。</p><p>可在办公场所或公共场所内设立应急区域。当出现疑似症状人员时，及时到该区域进行暂时隔离，再按照相关疫情防控规定处理。</p><p>16.出现疑似病例应对。</p><p>当工作人员出现发热、乏力、干咳等可疑症状时，要及时上报并安排就近就医，在专业人员指导下对其工作活动场所及使用的物品进行消毒处理，对密切接触者开展排查，实施隔离观察。</p>', 'http://36.189.242.73:8084/uploads/D:/wehchat_img/wechat_zw_img/0d17c045facf469faa246eb235fe425c.jpg', '信息中心', '1', '0', '1', '', '', '0', '', '0', 'http://36.189.242.73:8085/wehchat_img/wechat_zw_img/0d17c045facf469faa246eb235fe425c.jpg', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000005', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');
INSERT INTO `zd_news_fb` VALUES ('32818', '准东新闻', '准东开发区零距离', '', '准东开发区纪工委多措并举开展党风廉政教育 扎实推进纪律建设', '', '2020-01-07', '<p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583569155118074690.gif\" title=\"1583569155118074690.gif\" alt=\"微信图片_20200307161735.gif\"/></p><p>去年以来，准东开发区纪工委监察组聚焦主责主业，多措并举开展党风廉政教育，扎实推进纪律建设取得了较为明显的成效。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583569162883086719.jpg\" title=\"1583569162883086719.jpg\" alt=\"微信图片_20200307161303.jpg\"/></p><p>为了切实履行党风廉政建设领导者、执行者、推动者的责任，准东纪工委监察组组织各部门、各产业园党委传达学习习近平总书记系列讲话精神，提高落实全面从严治党的政治站位。认真开展了学习杨晓渡、杨鑫等上级领导讲话及公务活动饮酒、公车管理等文件和一系列党内法规。印发了加强警示教育的工作意见和“不能腐”“不想腐”的两个20条措施，夯实全面从严治党制度基础。下发典型案件通报5期，通报曝光典型案件20件25人。深入开展党纪党规教育，将党纪党规、正反面典型案例等作为重点，扎实开展第二十一个党风廉政教育月活动，采取中心组学习、专题党课、集中观看《蜕变》《叩问初心》等电教片。举办以“纪法铭于心，廉洁伴我行”为主题的知识竞赛，以赛促学。11月初党工委委员、纪工委书记、监察组组长韩军专门到6个产业园和有关企业开展上廉政党课活动。</p><p style=\"text-align: center;\"><img src=\"http://36.189.242.73:8084/uploads/20200307/1583569175138036579.jpg\" title=\"1583569175138036579.jpg\" alt=\"微信图片_20200307161307.jpg\"/></p><p>通过以上有力举措，不断提高开发区干部职工廉洁意识，促使开发区党员干部受警醒、明底线、知敬畏、存戒惧。（文/图 李剑平）</p><p><br/></p><p><br/></p>', 'http://36.189.242.73:8084/uploads/', '', '1', '0', '1', '', '', '0', '', '0', '', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');

-- ----------------------------
-- Table structure for zd_special
-- ----------------------------
DROP TABLE IF EXISTS `zd_special`;
CREATE TABLE `zd_special` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext COMMENT '详情',
  `editor` varchar(50) DEFAULT NULL COMMENT '图片链接',
  `picture` varchar(255) DEFAULT NULL COMMENT '照片',
  `release_time` varchar(25) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `code` int(10) DEFAULT '0',
  `dz_count` int(10) DEFAULT '0',
  `zf_count` int(10) DEFAULT '0',
  `pl_count` int(10) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zd_special
-- ----------------------------

-- ----------------------------
-- Table structure for zy_comment
-- ----------------------------
DROP TABLE IF EXISTS `zy_comment`;
CREATE TABLE `zy_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentator` varchar(50) DEFAULT NULL COMMENT '评论人',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `commentary_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `d_z` int(255) DEFAULT NULL COMMENT '评论点赞',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zy_comment
-- ----------------------------

-- ----------------------------
-- Table structure for zy_news
-- ----------------------------
DROP TABLE IF EXISTS `zy_news`;
CREATE TABLE `zy_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL COMMENT '用于文章图片拼接的绝对路径',
  `code` int(10) DEFAULT '0',
  `d_z` int(11) DEFAULT NULL COMMENT '点赞量',
  `zzqCode` int(1) DEFAULT '0',
  `zyCode` int(1) DEFAULT '0',
  `zdCode` int(1) DEFAULT '0',
  `zCode` int(1) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `caiji_time` datetime DEFAULT NULL COMMENT '数据采集时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zy_news
-- ----------------------------
INSERT INTO `zy_news` VALUES ('1', '1', '1', '1', '1', '1', '1', null, '1', '1', '1', '1', '11', '1', '1', '1', '1', '1', '1', '1', '2020-02-09 16:25:12');

-- ----------------------------
-- Table structure for zy_news_2
-- ----------------------------
DROP TABLE IF EXISTS `zy_news_2`;
CREATE TABLE `zy_news_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(500) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zy_news_2
-- ----------------------------

-- ----------------------------
-- Table structure for zy_news_fb
-- ----------------------------
DROP TABLE IF EXISTS `zy_news_fb`;
CREATE TABLE `zy_news_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `picture_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT '0',
  `d_z` int(255) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zy_news_fb
-- ----------------------------
INSERT INTO `zy_news_fb` VALUES ('1', '准东新闻', null, '', null, '2020-03-23', null, '', '', null, 'http://36.189.242.73:8084/uploads/', '1', '1', '中央新闻', '0', '0', null, '0', null);
INSERT INTO `zy_news_fb` VALUES ('2', '准东新闻', null, '好', null, '2020-03-23', null, '', '好', null, 'http://36.189.242.73:8084/uploads/20200323/d87e9c1fe8a34b8ab0d754cc56f645c2.jpg', '1', '0', '中央新闻', '0', '0', null, '0', null);

-- ----------------------------
-- Table structure for zzq_comment
-- ----------------------------
DROP TABLE IF EXISTS `zzq_comment`;
CREATE TABLE `zzq_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `commentator` varchar(50) DEFAULT NULL COMMENT '评论人',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `commentary_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `d_z` int(255) unsigned zerofill DEFAULT '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000' COMMENT '评论点赞',
  `comment_ID` int(11) DEFAULT NULL COMMENT '新闻ID',
  `comment_category` varchar(50) DEFAULT NULL COMMENT '类别',
  `comment_title` varchar(255) DEFAULT NULL COMMENT '评论标题',
  `user_tx` varchar(255) DEFAULT 'http://36.189.242.73:8083/image/mrtx.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zzq_comment
-- ----------------------------
INSERT INTO `zzq_comment` VALUES ('21', '1', '44', '是个好消息', '2020-04-02 12:44:46', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '1', '准东新闻', '1', 'http://36.189.242.73:8083/image/mrtx.jpg');
INSERT INTO `zzq_comment` VALUES ('22', '1', '44', '希望准东发展越来越好！', '2020-04-02 12:50:04', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001', '1', '准东新闻', '1', 'http://36.189.242.73:8083/image/mrtx.jpg');
INSERT INTO `zzq_comment` VALUES ('23', '1', '44', '你好，艾力', '2020-04-02 12:54:00', '000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002', '1', '准东新闻', '1', 'http://36.189.242.73:8083/image/mrtx.jpg');

-- ----------------------------
-- Table structure for zzq_news
-- ----------------------------
DROP TABLE IF EXISTS `zzq_news`;
CREATE TABLE `zzq_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `picture_url` varchar(255) DEFAULT NULL,
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT '0',
  `d_z` int(11) DEFAULT NULL COMMENT '点赞量',
  `category` varchar(255) DEFAULT NULL,
  `zyCode` int(1) DEFAULT '0',
  `zdCode` int(1) DEFAULT '0',
  `zCode` int(1) DEFAULT '0',
  `zzqCode` int(1) DEFAULT '0',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `zl_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zzq_news
-- ----------------------------

-- ----------------------------
-- Table structure for zzq_news_fb
-- ----------------------------
DROP TABLE IF EXISTS `zzq_news_fb`;
CREATE TABLE `zzq_news_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `picture_url` varchar(255) DEFAULT NULL,
  `article` longtext COMMENT '文章内容',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL,
  `code` int(10) DEFAULT '0' COMMENT '审核状态',
  `d_z` int(255) DEFAULT NULL COMMENT '点赞次数',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(10) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of zzq_news_fb
-- ----------------------------
INSERT INTO `zzq_news_fb` VALUES ('1', null, null, null, null, null, null, null, null, null, null, '0', '1', null, null, '0', null, '0', null);

-- ----------------------------
-- Table structure for z_comment
-- ----------------------------
DROP TABLE IF EXISTS `z_comment`;
CREATE TABLE `z_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `commentator` varchar(50) DEFAULT NULL COMMENT '评论人',
  `comment_content` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `commentary_time` varchar(255) DEFAULT NULL COMMENT '评论时间',
  `d_z` int(255) DEFAULT NULL COMMENT '评论点赞',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of z_comment
-- ----------------------------

-- ----------------------------
-- Table structure for z_news
-- ----------------------------
DROP TABLE IF EXISTS `z_news`;
CREATE TABLE `z_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `article` text COMMENT '文章内容',
  `picture_url` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `abs_url` varchar(255) DEFAULT NULL COMMENT '用于拼接图片的绝对路径',
  `code` int(10) DEFAULT '0',
  `d_z` int(11) DEFAULT NULL COMMENT '点赞量',
  `zzqCode` int(1) DEFAULT '0',
  `zyCode` int(1) DEFAULT '0',
  `zdCode` int(1) DEFAULT '0',
  `zCode` int(1) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT '0',
  `uid` int(10) DEFAULT NULL,
  `caiji_time` datetime DEFAULT NULL COMMENT '采集时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of z_news
-- ----------------------------

-- ----------------------------
-- Table structure for z_news_fb
-- ----------------------------
DROP TABLE IF EXISTS `z_news_fb`;
CREATE TABLE `z_news_fb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) DEFAULT NULL COMMENT '专栏来源',
  `news_column` varchar(255) DEFAULT NULL COMMENT '新闻专栏',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `article_source` varchar(255) DEFAULT NULL COMMENT '文章来源',
  `release_time` varchar(255) DEFAULT NULL COMMENT '发布时间',
  `article` text COMMENT '文章内容',
  `picture_url` varchar(255) DEFAULT NULL COMMENT '图片来源',
  `editor` varchar(255) DEFAULT NULL COMMENT '编辑人',
  `code` int(10) DEFAULT '0',
  `d_z` int(255) DEFAULT '0',
  `category` varchar(255) DEFAULT NULL COMMENT '类别',
  `uid` int(255) DEFAULT NULL,
  `abs_url` varchar(255) DEFAULT NULL COMMENT '拼接绝对路径',
  `status` int(10) DEFAULT '0',
  `pre_url` varchar(255) DEFAULT NULL,
  `differentiate` int(10) DEFAULT '0',
  `local_url` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of z_news_fb
-- ----------------------------
INSERT INTO `z_news_fb` VALUES ('1', null, null, null, null, null, null, null, null, null, '0', '1', null, null, null, '0', null, '0', null);

-- ----------------------------
-- Procedure structure for replaceArticleTags
-- ----------------------------
DROP PROCEDURE IF EXISTS `replaceArticleTags`;
DELIMITER ;;
CREATE DEFINER=`wangyawei`@`%` PROCEDURE `replaceArticleTags`(in tagName varchar(64))
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE idtemp VARCHAR(32);
    DECLARE tagtemp VARCHAR(128);
    declare oldtag varchar(128);
    declare newtag varchar(128);
    DECLARE articles CURSOR FOR SELECT id,tag FROM system_article WHERE INSTR(CONCAT(CONCAT(',',tag),','), concat(concat(',',tagName),','));
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = true;
    
    OPEN articles;
    read_loop : LOOP
        FETCH articles INTO idtemp,tagtemp;
        IF done THEN
            LEAVE read_loop;
        END IF;
	
	set oldtag = CONCAT(CONCAT(',',tagName),',');
	set newtag = concat(concat(',',tagtemp),',');
	
	set newtag = replace(newtag, oldtag, ',');
	set newtag = substring(newtag,1);
	
	if instr(newtag,',') = 1 then 
		SET newtag = SUBSTRING(REVERSE(newtag),1,CHAR_LENGTH(newtag)-1);
		set newtag = reverse(newtag);
	end if;
	if instr(reverse(newtag),',') = 1 then 
		SET newtag = SUBSTRING(newtag,1,char_length(newtag)-1);
	end if;
	
	if newtag is not null then
		update system_article set tag = newtag where id = idtemp;
	end if;
    END LOOP read_loop;
    CLOSE articles;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for updateArticleTag
-- ----------------------------
DROP PROCEDURE IF EXISTS `updateArticleTag`;
DELIMITER ;;
CREATE DEFINER=`wangyawei`@`%` PROCEDURE `updateArticleTag`(IN tagName Varchar(200))
BEGIN
   DECLARE tId VARCHAR(32);
   DECLARE temp VARCHAR(400);
   declare stop int default 0; 
   DECLARE cur CURSOR FOR (SELECT id,tag FROM system_article where tag LIKE  CONCAT('%',tagName,'%'));
   declare CONTINUE HANDLER FOR SQLSTATE '02000' SET stop = null;  
   open cur;
           FETCH cur INTO tid,temp;
	      WHILE ( stop is not null) DO  
	        update system_article set tag= left(replace(concat(temp,','),concat(tagName,','),''),length(REPLACE(CONCAT(temp,','),CONCAT(tagName,','),''))-1)
	        where id = tid;
	        FETCH cur INTO tid,temp;
	      END WHILE;
	   CLOSE cur;  
   END
;;
DELIMITER ;
