package com.rmtzx.mapper;

import com.rmtzx.entity.appentity.zwEntity.WebAnnouncement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
public interface WebAnnouncementMapper extends BaseMapper<WebAnnouncement> {

}
