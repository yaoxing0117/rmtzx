package com.rmtzx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.rmtzx.entity.appentity.zwEntity.GovernmentAffairsFb;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
public interface GovernmentAffairsFbMapper extends BaseMapper<GovernmentAffairsFb> {

}
