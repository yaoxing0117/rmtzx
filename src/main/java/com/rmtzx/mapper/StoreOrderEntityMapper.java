package com.rmtzx.mapper;


import com.rmtzx.entity.store.StoreOrderEntity;

public interface StoreOrderEntityMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(StoreOrderEntity record);

    int insertSelective(StoreOrderEntity record);

    StoreOrderEntity selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(StoreOrderEntity record);

    int updateByPrimaryKey(StoreOrderEntity record);
}