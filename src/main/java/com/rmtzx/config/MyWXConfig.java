package com.rmtzx.config;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.github.wxpay.sdk.WXPayConfig;

public class MyWXConfig implements WXPayConfig {

    private byte[] certData;

    public MyWXConfig() throws Exception {
	       /* String certPath = "";
	        File file = new File(certPath);
	        InputStream certStream = new FileInputStream(file);
	        this.certData = new byte[(int) file.length()];
	        certStream.read(this.certData);
	        certStream.close();*/
    }

    public String getAppID() {
        return "wx22f827f5a9742068";
    }

    public String getMchID() {
        return "1501381771";
    }


    public String getKey() {
        return "c56c92b5e6ed2ede8d36ca7c9e3d084b";
    }

    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    public int getHttpReadTimeoutMs() {
        return 10000;
    }
}
