package com.rmtzx;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.servlet.MultipartConfigElement;
import java.util.logging.Logger;

@Configuration
@SpringBootApplication
@MapperScan(basePackages ={"com.rmtzx.dao","com.rmtzx.mapper"})
@EnableScheduling //启动类启用定时
@Slf4j
public class MySpringBoot_RMT {
    // protected static final Logger logger= (Logger) LoggerFactory.getLogger(MySpringBoot_RMT.class);
    public static void main(String[] args) {
        SpringApplication.run(MySpringBoot_RMT.class);
        System.out.println("服务端启动成功！");
    }
    /**
     * 文件上传配置
     *
     * @return
     */
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        //  单个数据大小
        factory.setMaxFileSize("102400KB"); // KB,MB
        /// 总上传数据大小
        factory.setMaxRequestSize("1024000KB");
        return factory.createMultipartConfig();
    }

}
