package com.rmtzx.service.yqfxservice.impl;

import com.rmtzx.dao.yqdao.YqfxMapper;
import com.rmtzx.entity.yqfxentity.EnergyEntity;
import com.rmtzx.entity.yqfxentity.StatisticsEntity;
import com.rmtzx.entity.yqfxentity.ThesaurusEntity;
import com.rmtzx.service.yqfxservice.YqfxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class YqfxServiceImpl implements YqfxService {
    @Autowired
    private YqfxMapper yqfxMapper;

    ////添加统计数量
    @Override
    public List<StatisticsEntity> allStatistics() {
        return yqfxMapper.allStatistics();
    }

    @Override
    public StatisticsEntity allStatisticszuo() {
        Date today = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleDateFormat.format(today);//获取昨天日期

        Date today1 = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 48);
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String time1 = simpleDateFormat1.format(today1);//获取前天日期
//zNumber zyNumber zzqNumber  wzNumber qtNumber wxNumber nyNumber wbNumber zongNumber
// dangNumber,time,qtNumber bzNumber zdNumber
        StatisticsEntity list = yqfxMapper.allStatisticszuo(time);//昨天
        StatisticsEntity list1 = yqfxMapper.allStatisticszuo(time1);//前天
        StatisticsEntity statistics = new StatisticsEntity();

        statistics.setZNumber(list.getZNumber() - list1.getZNumber());
        statistics.setZyNumber(list.getZyNumber() - list1.getZyNumber());
        statistics.setZzqNumber(list.getZzqNumber() - list1.getZzqNumber());
        statistics.setWzNumber(list.getWzNumber() - list1.getWzNumber());
        statistics.setQtNumber(list.getQtNumber() - list1.getQtNumber());
        statistics.setWxNumber(list.getWxNumber() - list1.getWxNumber());
        statistics.setNyNumber(list.getNyNumber() - list1.getNyNumber());
        statistics.setZongNumber(list.getZongNumber() - list1.getZongNumber());
//        statistics.setDangNumber(list.getDangNumber()-list1.getDangNumber());
        statistics.setTime(time);
        statistics.setWbNumber(list.getWbNumber() - list1.getWbNumber());
        statistics.setBzNumber(list.getBzNumber() - list1.getBzNumber());
        statistics.setZdNumber(list.getZdNumber() - list1.getZdNumber());
        return statistics;
    }

    //热词搜索排行
    @Override
    public List<ThesaurusEntity> allSelects() {
        return yqfxMapper.allSelects();
    }

    @Override
    public List nyAll() {
        List<EnergyEntity> list = new ArrayList<>();
        Date date = new Date();
        SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
        String currSun = dateFm.format(date);
        if (currSun.equals("星期一")) {
            list.add(yqfxMapper.nyAll(1));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期二")) {
            list.add(yqfxMapper.nyAll(2));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期三")) {
            list.add(yqfxMapper.nyAll(3));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期四")) {
            list.add(yqfxMapper.nyAll(4));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期五")) {
            list.add(yqfxMapper.nyAll(5));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期六")) {
            list.add(yqfxMapper.nyAll(6));
            list.add(yqfxMapper.nyAll(8));
            return list;
        } else if (currSun.equals("星期天")) {
            list.add(yqfxMapper.nyAll(7));
            list.add(yqfxMapper.nyAll(8));
            return list;
        }
        return list;
    }


}
