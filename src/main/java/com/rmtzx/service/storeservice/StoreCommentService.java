package com.rmtzx.service.storeservice;

import com.rmtzx.entity.store.StoreCommentEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 店家-菜品 评论业务逻辑层
 */
public interface StoreCommentService {
    int insertComment(@Param("commentCategory") String commentCategory,
                      @Param("commentContent") String commentContent,
                      @Param("commentPicture") MultipartFile file,
                      @Param("commentTime") String commentTime,
                      @Param("commentStar") int commentStar,
                      @Param("userId") int userId,
                      @Param("pid") int pid,
                      @Param("storeId") int storeId);
    int insertnull(@Param("commentCategory") String commentCategory,
                      @Param("commentContent") String commentContent,
                      @Param("commentTime") String commentTime,
                      @Param("commentStar") int commentStar,
                      @Param("userId") int userId,
                      @Param("pid") int pid,
                   @Param("storeId") int storeId);
    List<StoreCommentEntity> selectComment(@Param("commentCategory") String commentCategory,
                                           @Param("pid") int pid);
    //店家评论查询
    List<StoreCommentEntity> selectCommentstore(@Param("storeId") int storeId);
}
