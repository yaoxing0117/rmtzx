package com.rmtzx.service.storeservice.impl;

import com.rmtzx.dao.storedao.StoreCommentMapper;
import com.rmtzx.entity.store.StoreCommentEntity;
import com.rmtzx.service.storeservice.StoreCommentService;
import com.rmtzx.utilsss.FileUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 店家-菜品 评论业务逻辑层
 */
@Service
public class StoreCommentServiceImpl implements StoreCommentService {
    @Value("${server.port}")
    private String port;
    @Value("${web.upload-path}")
    private String path;
    @Value("${fileIP}")
    private String ip;
    @Autowired
    private StoreCommentMapper storeCommentMapper;

    /**
     *有照片评论
     * @param commentCategory 评论类型
     * @param commentContent 评论内容
     * @param file 评论图片
     * @param commentTime 评论时间
     * @param commentStar 评论星级
     * @param userId 用户id
     * @param pid 订单id
     * @return 是否成功
     */
    @Override
    public int insertComment(String commentCategory, String commentContent,
                             MultipartFile file, String commentTime, int commentStar, int userId, int pid, int storeId) {
        //1定义要上传文件 的存放路径
        //2获得文件名字
        String fileName = file.getOriginalFilename();
        //2上传失败提示`
        String userPortrait1 = FileUtils.upload(file, path, fileName);
        String[] split = userPortrait1.split("D:");
        String s1 = split[1];
        String commentPicture = ip + port + s1;
        return storeCommentMapper.insertComment(commentCategory,commentContent,commentPicture,commentTime,commentStar,userId,pid,storeId);
    }

    /**
     * 无照片评论
     * @param commentCategory  评论类型
     * @param commentContent 评论内容
     * @param commentTime 评论时间
     * @param commentStar 评论星级
     * @param userId 用户id
     * @param pid 订单id
     * @return 是否成功
     */
    @Override
    public int insertnull(String commentCategory, String commentContent, String commentTime, int commentStar, int userId, int pid, int storeId) {
        return storeCommentMapper.insertComment(commentCategory,commentContent,"无",commentTime,commentStar,userId,pid,storeId);
    }

    /**
     * 用户评论查询
     * @param commentCategory
     * @param pid
     * @return
     */
    @Override
    public List<StoreCommentEntity> selectComment(String commentCategory, int pid) {
        return storeCommentMapper.selectComment(commentCategory, pid);
    }

    /**
     * 店家评论查询
     * @param storeId
     * @return
     */
    @Override
    public List<StoreCommentEntity> selectCommentstore(int storeId) {
        return storeCommentMapper.selectCommentstore(storeId);
    }
}
