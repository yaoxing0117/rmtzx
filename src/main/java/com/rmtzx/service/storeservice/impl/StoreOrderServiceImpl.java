package com.rmtzx.service.storeservice.impl;

import com.rmtzx.dao.storedao.StoreOrderMapper;
import com.rmtzx.entity.store.StoreOrderEntity;
import com.rmtzx.service.storeservice.StoreOrderService;
import com.rmtzx.util.MakeOrderNum;
import com.rmtzx.utilsss.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.beans.Transient;
import java.util.List;

@Service
public class StoreOrderServiceImpl implements StoreOrderService {

    @Autowired
    private StoreOrderMapper storeOrderMapper;

    /**
     * @param storeOrderEntities 订单数据List
     * @return 返回状态
     */
    @Override
    @Transactional
    public String insertOrder(List<StoreOrderEntity> storeOrderEntities, int orderTotal) {
        String orderNumber = this.returnDH("WM");
        int i = 1 + storeOrderMapper.selectId();
        for (StoreOrderEntity e : storeOrderEntities) {
            storeOrderMapper.insertOrder(e.getUserId(),
                    e.getUserAddress(),
                    e.getUserName(),
                    e.getUserTel(),
                    e.getSpotName(),
                    e.getDishName(),
                    e.getDishNumber(),
                    e.getDishMoney(),
                    orderNumber,
                    e.getOrderState(),
                    e.getDishId(),
                    e.getUserRemarks(),
                    i,
                    orderTotal);
        }
        return orderNumber;
    }

    /**
     * 用户订单查询
     *
     * @param userId 根据用户id查询订单数据
     * @return 返回用户订单数据
     */
    @Override
    public List<StoreOrderEntity> selectOrder(int userId) {
        return storeOrderMapper.selectOrder(userId);
    }

    /**
     * @param orderState 订单状态
     * @param pid        对应订单表中用户表中id
     * @return
     */
    @Override
    public int updateOrder(String orderState, int pid) {
        return storeOrderMapper.updateOrder(orderState, pid);
    }

    /**
     * 订单号生成
     *
     * @param dhstate 店铺首字母
     * @return
     */
    @Override
    public String returnDH(String dhstate) {
        Integer i = storeOrderMapper.selectId();
        String i1 = i.toString();
        return MakeOrderNum.getNewEquipmentNo(dhstate, i1);
    }


}

