package com.rmtzx.service.storeservice.impl;

import com.rmtzx.dao.storedao.StoreTakeawayMapper;
import com.rmtzx.entity.store.StoreTakeawayEntity;
import com.rmtzx.service.storeservice.StoreTakeawayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 外卖店家-UCRD
 */
@Service
public class StoreTakeawayServiceImpl implements StoreTakeawayService {
    @Autowired
    private StoreTakeawayMapper storeTakeawayMapper;

    /**
     *
     * @param pageNo 第一页
     * @param pageSize 一页多少数据
     * @return 所有店家数据
     */
    @Override
    public List<StoreTakeawayEntity> selectAll(int pageNo, int pageSize) {
        return storeTakeawayMapper.selectAll((pageNo-1)*pageSize, pageSize);
    }

    /**
     *
     * @param id 具体商家id
     * @return 商家详情
     */
    @Override
    public StoreTakeawayEntity selectOne(int id) {
        return storeTakeawayMapper.selectOne(id);
    }

    /**
     * 店铺名模糊查询
     * @param shopOwner 店名
     * @return 所有匹配店铺
     */
    @Override
    public List<StoreTakeawayEntity> selectAllStore(String shopOwner) {
        return storeTakeawayMapper.selectAllStore(shopOwner);
    }
}
