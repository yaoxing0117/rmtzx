package com.rmtzx.service.storeservice.impl;

import com.rmtzx.dao.storedao.StoreDishMapper;
import com.rmtzx.entity.store.StoreDishEntity;
import com.rmtzx.service.storeservice.StoreDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 * 外卖店家-菜品UCRD
 */
@Service
public class StoreDishServiceIpml implements StoreDishService {
    @Autowired
    private StoreDishMapper storeDishMapper;

    /**
     * 查询店家下所有菜品
     * @param pid 店家id
     * @return 菜品详情
     */
    @Override
    public List<StoreDishEntity> selectAll(int pid) {
        return storeDishMapper.selectAll(pid);
    }

    /**
     * 模糊搜索菜名
     * @param dishName 菜名
     * @return 所有匹配菜名
     */
    @Override
    public List<StoreDishEntity> selectAlldish(String dishName) {
        return storeDishMapper.selectAlldish(dishName);
    }

    /**
     * 固定店家菜名搜索
     * @param dishName 菜名
     * @param pid 店家id
     * @return 所有匹配
     */
    @Override
    public List<StoreDishEntity> selectAllDishStore(String dishName, int pid) {
        return storeDishMapper.selectAllDishStore(dishName, pid);
    }
}
