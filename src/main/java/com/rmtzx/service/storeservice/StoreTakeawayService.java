package com.rmtzx.service.storeservice;

import com.rmtzx.entity.store.StoreTakeawayEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 外卖店家-UCRD
 */
public interface StoreTakeawayService {
    List<StoreTakeawayEntity> selectAll(@Param("pageNo")int pageNo,
                                        @Param("pageSize")int pageSize);

    StoreTakeawayEntity selectOne(@Param("id")int id);

    List<StoreTakeawayEntity> selectAllStore(@Param("shopOwner") String shopOwner);
}
