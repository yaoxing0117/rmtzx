package com.rmtzx.service.storeservice;

import com.rmtzx.entity.store.StoreOrderEntity;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface StoreOrderService {
    //下单
    String insertOrder(List<StoreOrderEntity> storeOrderEntities, int orderTotal);

    List<StoreOrderEntity> selectOrder(@Param("userId") int userId);

    int updateOrder(@Param("orderState") String orderState,
                    @Param("pid") int pid);
    //单号生成
     String returnDH(String dhstate);


}
