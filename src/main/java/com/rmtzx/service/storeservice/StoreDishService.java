package com.rmtzx.service.storeservice;

import com.rmtzx.entity.store.StoreDishEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
/**
 * 外卖店家-菜品UCRD
 */
public interface StoreDishService {
    //菜品数据查询
    List<StoreDishEntity> selectAll(@Param("pid")int pid);
    //模糊搜索菜名
    List<StoreDishEntity> selectAlldish(@Param("dishName")String dishName);
    //固定店家菜名模糊搜索
    List<StoreDishEntity> selectAllDishStore(@Param("dishName")String dishName,
                                             @Param("pid")int pid);
}
