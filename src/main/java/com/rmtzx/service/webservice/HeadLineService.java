package com.rmtzx.service.webservice;

import com.rmtzx.entity.webentity.HeadLineEntity;

import java.util.List;

public interface HeadLineService {
    //头条
    List<HeadLineEntity> oneHeadLine();
}
