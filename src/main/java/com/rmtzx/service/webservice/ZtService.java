package com.rmtzx.service.webservice;

import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.ZtEntity;
import org.apache.ibatis.annotations.Param;


public interface ZtService {

    //列表(id 预览图 详情接口)
    ShowInfoWeb allZt(@Param("category")String category,
                      @Param("pageNo")int pageNo,
                      @Param("pageSize")int pageSize);
    //详情()
    ZtEntity oneZt(@Param("id")int id);
}
