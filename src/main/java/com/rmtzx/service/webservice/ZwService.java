package com.rmtzx.service.webservice;

import com.rmtzx.entity.pojo.ClassTableEntity;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.Announcement;
import org.apache.ibatis.annotations.Param;


import java.util.List;

public interface ZwService {
    //类别查询
    List<ClassTableEntity> allClass();
    //政务列表展示
    ShowInfoWeb allList(@Param("cid")int cid,@Param("pageNo")int pageNo,@Param("pageSize")int pageSize);
    //政务详情展示
    Announcement details(@Param("id") int id);
}
