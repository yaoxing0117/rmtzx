package com.rmtzx.service.webservice;

import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.ZdtsEntity;
import org.apache.ibatis.annotations.Param;



public interface ZdtsService {
    //准东图说+微视频展示详情
    ZdtsEntity oneZdts(@Param("id")int id);
    //准东图说+微视频列表展示
    ShowInfoWeb allZdts(@Param("category")String category,
                        @Param("pageNo")int pageNo,
                        @Param("pageSize")int pageSize);
    //返回图片+视频分页
    ShowInfoWeb allZdtss(@Param("pageNo")int pageNo,
                              @Param("pageSize")int pageSize);
}
