package com.rmtzx.service.webservice.Impl;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TestTree {
    static class DemoEntity{
        private int id;
        /**
         *
         */
        private String name;
        /**
         *
         */
        private int depth;
        /**
         *
         */
        private int parentId;


        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDepth() {
            return depth;
        }

        public void setDepth(int depth) {
            this.depth = depth;
        }

        public int getParentId() {
            return parentId;
        }

        public void setParentId(int parentId) {
            this.parentId = parentId;
        }

        public DemoEntity() {
        }

        public DemoEntity(int id, String name, int depth, int parentId) {
            this.id = id;
            this.name = name;
            this.depth = depth;
            this.parentId = parentId;
        }
    }

    public static List<DemoEntity> selectFromDb() {
        List<DemoEntity> list = new ArrayList<>();

        DemoEntity demoEntity1 = new DemoEntity(1, "陕西", 1, 0);
        DemoEntity demoEntity2 = new DemoEntity(2, "云南", 1, 0);

        DemoEntity demoEntity3 = new DemoEntity(3, "西安市", 2, 1);
        DemoEntity demoEntity4 = new DemoEntity(4, "韩城市", 2, 1);
        DemoEntity demoEntity5 = new DemoEntity(5, "临沧市", 2, 2);


        DemoEntity demoEntity6 = new DemoEntity(6, "莲湖区", 3, 3);
        DemoEntity demoEntity7 = new DemoEntity(7, "未央区", 3, 3);
        DemoEntity demoEntity8 = new DemoEntity(8, "新城区", 3, 4);
        DemoEntity demoEntity9 = new DemoEntity(9, "老城区", 3, 4);
        DemoEntity demoEntity10 = new DemoEntity(10, "临翔区", 3, 5);
        DemoEntity demoEntity11 = new DemoEntity(11, "耿马县", 3, 5);

        list.add(demoEntity1);
        list.add(demoEntity2);
        list.add(demoEntity3);
        list.add(demoEntity4);
        list.add(demoEntity5);
        list.add(demoEntity6);
        list.add(demoEntity7);
        list.add(demoEntity8);
        list.add(demoEntity9);
        list.add(demoEntity10);
        list.add(demoEntity11);
        return list;
    }


    public static void main(String[] args) {
        List<DemoEntity> demoEntities = selectFromDb();
        List<DemoEntity> topList = new ArrayList<>();

        Map<Integer, List<DemoEntity>> map = new HashMap<>();

        for (DemoEntity demoEntity : demoEntities) {
            if (demoEntity.getParentId() == 0) {
                topList.add(demoEntity);
            }
            List<DemoEntity> subList = map.get(demoEntity.getParentId());
            if (subList == null) {
                subList = new ArrayList<>();
            }
            subList.add(demoEntity);
            map.put(demoEntity.getParentId(), subList);
        }

        for (DemoEntity demoEntity : topList) {
            int id = demoEntity.getId();
            System.out.println(demoEntity.getName());
            List<DemoEntity> subList = map.get(id);
            if (subList != null) {
                for (DemoEntity entity : subList) {
                    List<DemoEntity> sub2 = map.get(entity.getId());
                    if (sub2 != null) {
                        for (DemoEntity demoEntity1 : sub2) {
                            System.out.print(getSpilt(demoEntity1.getDepth()));
                            System.out.println(demoEntity1.getName());
                        }

                    }
                }
            }
        }
    }

    public static String  getSpilt(int depth){
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i <depth; i++) {
            sb.append("----");
        }
        return sb.toString();
    }
}
