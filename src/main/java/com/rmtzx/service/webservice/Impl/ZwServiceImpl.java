package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.ZwMapper;
import com.rmtzx.entity.pojo.ClassTableEntity;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.Announcement;
import com.rmtzx.service.webservice.ZwService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ZwServiceImpl implements ZwService {
    @Autowired
    private ZwMapper zwMapper;
    //返回类别
//    @Override
    public Map allClass1() {
        List<ClassTableEntity> dtopList = new ArrayList<>();
        List<ClassTableEntity> classTableEntities = zwMapper.allClass();//所有数据
        Map<String,List<ClassTableEntity>> map = new HashMap<>();
        for (ClassTableEntity classTableEntity:classTableEntities) {
            if (classTableEntity.getPid()==0){
                List<ClassTableEntity> xdtopList = new ArrayList<>();
                for (ClassTableEntity classTableEntity1:classTableEntities) {
                    if (classTableEntity.getId()==classTableEntity1.getPid()){
                        xdtopList.add(classTableEntity1);
                    }
                }
                map.put(classTableEntity.getLargeCategory(),xdtopList);
                }

            }
        return map;
    }


    @Override
    public List<ClassTableEntity> allClass() {
        return zwMapper.allClass();
    }

    //小类别分页
    @Override
    public ShowInfoWeb allList(int cid,int pageNo, int pageSize) {
        ShowInfoWeb showInfoWeb = new ShowInfoWeb();
        int i = zwMapper.zwPaperPageCount(cid);
        int i1 = i / pageSize;
        if (i1==0) {
            int i2 = i1 + 1;
            showInfoWeb.setTotalPage(i2);
        }else {
            showInfoWeb.setTotalPage(i1);
        }
        showInfoWeb.setTotalCount(i);
        showInfoWeb.setPage(pageNo);
        showInfoWeb.setNewsInfoURL("/zw/details");
        showInfoWeb.setLimitShowList(zwMapper.allList(cid,(pageNo-1)*pageSize,pageSize));

        return showInfoWeb;
    }
//详情查询
    @Override
    public Announcement details(int id) {
        return  zwMapper.details(id);
    }
}
