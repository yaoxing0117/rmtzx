package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.ZtMapper;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.ZtEntity;
import com.rmtzx.service.webservice.ZtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZtServiceImpl implements ZtService {
    @Autowired
    private ZtMapper ztMapper;

//列表
@Override
public ShowInfoWeb allZt(String category,int pageNo, int pageSize) {
    ShowInfoWeb showInfoWeb=new ShowInfoWeb();
    showInfoWeb.setNewsInfoURL("/ztzd/oneZt");
    showInfoWeb.setPage(pageNo);
    int i = ztMapper.paperPageCounts(category);
    int ii = 0;
    int li=i/pageSize;
    if (li==0){
        ii =li+1;
    }
    showInfoWeb.setTotalPage(ii);
    showInfoWeb.setTotalCount(ztMapper.paperPageCounts(category));
    showInfoWeb.setLimitShowList(ztMapper.allZt(category,(pageNo-1)*pageSize, pageSize));
    return showInfoWeb;
}

//详情
    @Override
    public ZtEntity oneZt(int id) {
        return ztMapper.oneZt(id);
    }
}
