package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.LjzdMapper;
import com.rmtzx.entity.webentity.LjzdEntity;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.service.webservice.LjzdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class LjzdServiceImpl implements LjzdService {
    @Autowired
    private LjzdMapper ljzdMapper;
    //详情返回
    @Override
    public LjzdEntity oneLjzd(String category) {

        return ljzdMapper.oneLjzd(category);
    }
    //列表返回
    @Override
    public ShowInfoWeb allLjzd() {
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/ljzd/oneLjzd");
        List<LjzdEntity> ljzdEntities = ljzdMapper.allLjzd();
        showInfoWeb.setLimitShowList(ljzdEntities);
        return showInfoWeb;
    }

    //列表返回
    @Override
    public ShowInfoWeb allCycj() {
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/ljzd/oneLjzd");
        List<LjzdEntity> ljzdEntities = ljzdMapper.allCycj();
        showInfoWeb.setLimitShowList(ljzdEntities);
        return showInfoWeb;
    }
    @Override
    public ShowInfoWeb allCycj3(){
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/ljzd/oneLjzd");
        LjzdEntity ljzdEntity = ljzdMapper.oneLjzd3(1);
        LjzdEntity ljzdEntity1 = ljzdMapper.oneLjzd3(7);
        LjzdEntity ljzdEntity2 = ljzdMapper.oneLjzd3(16);
        List<LjzdEntity> ljzdEntities=new ArrayList<>();
        ljzdEntities.add(ljzdEntity);
        ljzdEntities.add(ljzdEntity1);
        ljzdEntities.add(ljzdEntity2);
        showInfoWeb.setLimitShowList(ljzdEntities);
        return showInfoWeb;
    }
    //投资返回列表数据
    @Override
    public ShowInfoWeb allTZ(String category, int pageNo, int pageSize) {
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setTotalPage(ljzdMapper.paperPageCount(category)/pageSize);
        showInfoWeb.setPage(pageNo);
        showInfoWeb.setNewsInfoURL("");
        showInfoWeb.setLimitShowList(ljzdMapper.allTZ(category, (pageNo-1)*pageSize, pageSize));
        return showInfoWeb;
    }
    //投资返回详情数据
    @Override
    public LjzdEntity oneTZ(int id) {
        return ljzdMapper.oneTZ(id);
    }

    @Override
    public int investment(String investmentUser,
                          String investmentTel,
                          String investmentMail,
                          String investmentWx,
                          String investmentBz) {
        int investment = ljzdMapper.investment(investmentUser, investmentTel, investmentMail, investmentWx, investmentBz);
        return investment;
    }
}
