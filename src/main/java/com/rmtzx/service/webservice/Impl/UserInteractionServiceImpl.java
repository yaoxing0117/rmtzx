package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.UserInteractionMapper;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.UserInteractionEntity;
import com.rmtzx.service.webservice.UserInteractionService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInteractionServiceImpl implements UserInteractionService {
    @Autowired
    private UserInteractionMapper userInteractionMapper;

    //添加信件
    @Override
    public int insertInteraction(String userName, String userDq, String userTel,
                                 String userTjtime, String userNr, String userZt,String userXjlb) {
        int i = userInteractionMapper.insertInteraction(userName, userDq, userTel, userTjtime,
                userNr, userZt, userXjlb);

        return i;
    }
    //分页展示所有信件
    @Override
    public ShowInfoWeb allUserI(String userXjlb, int pageNo, int pageSize) {

        ShowInfoWeb showInfoWeb =new ShowInfoWeb();
        int i = userInteractionMapper.paperPageCount();
        int i1 = i / pageSize;
        if (i1==0){
            showInfoWeb.setTotalPage(i1+1);
        }else {
            showInfoWeb.setTotalPage(i1);
        }

        showInfoWeb.setPage(pageNo);
        showInfoWeb.setTotalCount(i);
        if(userXjlb.equals("全部")){
            System.out.println("全部");
            showInfoWeb.setLimitShowList(userInteractionMapper.allUserI((pageNo-1)*pageSize, pageSize));
            return showInfoWeb;
        }
        showInfoWeb.setLimitShowList(userInteractionMapper.allUser1(userXjlb,(pageNo-1)*pageSize, pageSize));
        return showInfoWeb;
    }
//
    @Override
    public int paperPageCount() {
        return 0;
    }
//模糊查询信件
    @Override
    public ShowInfoWeb allSelect(String userName, String userXjlb, String userZt ,int pageNo, int pageSize) {
        if (userXjlb.equals("全部")&&userName==null&&userZt==null){
            ShowInfoWeb showInfoWeb=new ShowInfoWeb();
            List<UserInteractionEntity> userInteractionEntities = userInteractionMapper.allUserI((pageNo-1)*pageSize, pageSize);
            System.out.println(userInteractionEntities);
            int size = userInteractionEntities.size();
            // showInfoWeb.setTotalPage(i/pageSize);
            showInfoWeb.setPage(pageNo);
            showInfoWeb.setTotalCount(size);
            showInfoWeb.setLimitShowList(userInteractionEntities);
            return showInfoWeb;
        }
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        List<UserInteractionEntity> userInteractionEntities = userInteractionMapper.allSelect3(
                userName, userXjlb, userZt, (pageNo - 1) * pageSize, pageSize);
        showInfoWeb.setPage(pageNo);
        showInfoWeb.setTotalCount(userInteractionEntities.size());
        showInfoWeb.setLimitShowList(userInteractionEntities);
        return showInfoWeb;
    }
//查询详情//增加浏览次数
    @Override
    public UserInteractionEntity oneUser(int id) {
        int i = userInteractionMapper.rdUser(id);
        int XJRd =i+1;
        userInteractionMapper.zjUser(id,XJRd);
        UserInteractionEntity userInteractionEntity = userInteractionMapper.oneUser(id);
        return userInteractionEntity;
    }

    @Override
    public int rdUser(int id) {
        return 0;
    }
}
