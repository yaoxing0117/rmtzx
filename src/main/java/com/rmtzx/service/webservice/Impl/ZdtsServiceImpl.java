package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.ZdtsMapper;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.ZdtsEntity;
import com.rmtzx.service.webservice.ZdtsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZdtsServiceImpl implements ZdtsService {
    @Autowired
    private ZdtsMapper zdtsMapper;

    //详情图说准东
    @Override
    public ZdtsEntity oneZdts(int id) {

        return zdtsMapper.oneZdts(id);
    }
    //列
    @Override
    public ShowInfoWeb allZdts(String category, int pageNo, int pageSize) {
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/tszd_sp/oneZdts");
        showInfoWeb.setPage(pageNo);
        int i = zdtsMapper.paperPageCount(category);
        int ii = 0;
        int li=i/pageSize;
        if (li==0){
            ii =li+1;
            showInfoWeb.setTotalPage(ii);
        }else {
            showInfoWeb.setTotalPage(i/pageSize);
        }
        showInfoWeb.setTotalCount(zdtsMapper.paperPageCount(category));
        showInfoWeb.setLimitShowList(zdtsMapper.allZdts(category, (pageNo-1)*pageSize, pageSize));
        return showInfoWeb;
    }
//返回图片+视频列表
    @Override
    public ShowInfoWeb allZdtss(int pageNo, int pageSize) {
        ShowInfoWeb showInfoWeb=new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/tszd_sp/oneZdts");
        showInfoWeb.setPage(pageNo);
        int i = zdtsMapper.paperPageCounts();
        int ii = 0;
        int li=i/pageSize;
        if (li==0){
            ii =li+1;
        }
        showInfoWeb.setTotalPage(ii);
        showInfoWeb.setTotalCount(zdtsMapper.paperPageCounts());
        showInfoWeb.setLimitShowList(zdtsMapper.allZdtss((pageNo-1)*pageSize, pageSize));
        return showInfoWeb;
    }
}
