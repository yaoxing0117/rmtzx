package com.rmtzx.service.webservice.Impl;

import com.rmtzx.dao.webdao.HeadLineMapper;
import com.rmtzx.entity.webentity.HeadLineEntity;
import com.rmtzx.service.webservice.HeadLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HeadLineServiceImpl implements HeadLineService {
    @Autowired
    private HeadLineMapper headLineMapper;


    @Override
    public List<HeadLineEntity> oneHeadLine() {
        return headLineMapper.oneHeadLine() ;
    }
}
