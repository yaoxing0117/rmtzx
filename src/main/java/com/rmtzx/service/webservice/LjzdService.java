package com.rmtzx.service.webservice;

import com.rmtzx.entity.webentity.LjzdEntity;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface LjzdService {
    //据类别返回详情
    LjzdEntity oneLjzd(@Param("category")String category);
    //列表返回
    ShowInfoWeb allLjzd();
    //产业列表返回
    ShowInfoWeb allCycj();
    //投资列表返回("产业促进")
    ShowInfoWeb allTZ(@Param("category")String category,
                           @Param("pageNo")int pageNo,
                           @Param("pageSize")int pageSize);
    public ShowInfoWeb allCycj3();
    //据类别返回详情
    LjzdEntity oneTZ(@Param("id")int id);
    //投资人信息
    int investment(@Param("investmentUser")String investmentUser,
                   @Param("investmentTel")String investmentTel,
                   @Param("investmentMail")String investmentMail,
                   @Param("investmentWx")String investmentWx,
                   @Param("investmentBz")String investmentBz);
}
