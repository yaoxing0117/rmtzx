package com.rmtzx.service.webservice;

import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.UserInteractionEntity;
import org.apache.ibatis.annotations.Param;


public interface UserInteractionService {
    //提交信件
    int insertInteraction(@Param("userName")String userName,
                          @Param("userDq")String userDq,
                          @Param("userTel")String userTel,
                          @Param("userTjtime")String userTjtime,
                          @Param("userNr")String userNr,
                          @Param("userZt")String userZt,
                          @Param("userXjlb")String userXjlb
    );
    //展示所有信件
    ShowInfoWeb allUserI( @Param("userXjlb")String userXjlb,
                          @Param("pageNo")int pageNo,
                         @Param("pageSize")int pageSize);
    //查询总数据量
    int paperPageCount();
    //查询信件(联系人)
    ShowInfoWeb allSelect(@Param("userName")String userName,
                          @Param("userLb")String userLb,
                          @Param("userZt")String userZt,
                          @Param("pageNo")int pageNo,
                          @Param("pageSize")int pageSize);
    //查看详情信件
    UserInteractionEntity oneUser(@Param("id") int id );
    //查询浏览次数
    int rdUser(@Param("id")int id);

}
