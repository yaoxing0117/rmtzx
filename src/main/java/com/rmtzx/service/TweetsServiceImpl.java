package com.rmtzx.service;

import com.rmtzx.entity.Tweets;
import com.rmtzx.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class TweetsServiceImpl {
    @Autowired
    private RedisUtils redisUtils;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Scheduled(cron="0 */10 * * * ?")
    public void findByLimit(){
        log.info("微博数据统计开始执行");
            Integer   m=1;
            Integer   n=10;
        Query query = new Query();
        query.with(Sort.by(
                Sort.Order.desc("like_num")
        ));
        query.skip((m-1)*n).limit(n);

        List<Tweets> tweets = mongoTemplate.find(query, Tweets.class, "Tweets");
        redisUtils.lSet("Tweets",tweets,600);
        log.info("微博数据统计结束");
    }
}
