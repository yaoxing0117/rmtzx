package com.rmtzx.service.appservice;

import com.rmtzx.entity.CYTelEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CYTelService {

    //大类别返回
    ShowInfo pAll();

    //小列别返回
    ShowInfo cAll(@Param("pid") int pid,
                  @Param("pageNo")int pageNo,
                  @Param("pageSize")int pageSize);
}
