package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.YszsMapper;
import com.rmtzx.entity.appentity.YszsEntity;
import com.rmtzx.entity.appentity.ysEntity.WXGZHEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.YszsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YszsServiceImpl implements YszsService {
    @Autowired
    private YszsMapper yszsMapper;

    @Override
    public ShowInfo allYszs(String category, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setLimitShowList(yszsMapper.allYszs(category, (pageNo-1)*pageSize, pageSize));
        showInfo.setPage(pageNo);
        showInfo.setTotalCount(yszsMapper.xxpaperPageCount(category));
        return showInfo;
    }

    @Override
    public YszsEntity oneYszs(int id) {
        return yszsMapper.oneYszs(id);
    }

    @Override
    public WXGZHEntity oneWX(int id) {
        return yszsMapper.oneWX(id);
    }

    @Override
    public ShowInfo allWX(String differentiate, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setLimitShowList(yszsMapper.allWX(differentiate, (pageNo-1)*pageSize, pageSize));
        showInfo.setPage(pageNo);
        showInfo.setTotalCount(yszsMapper.wxpaperPageCount(differentiate));
        return showInfo;
    }
}
