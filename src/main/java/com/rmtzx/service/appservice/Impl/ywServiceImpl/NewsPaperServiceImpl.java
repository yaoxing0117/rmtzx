package com.rmtzx.service.appservice.Impl.ywServiceImpl;

import com.rmtzx.dao.appdao.ywDao.NewsPaperMapper;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.ywEntity.NewsPaperEntity;
import com.rmtzx.service.appservice.NewsPaperSerivce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsPaperServiceImpl implements NewsPaperSerivce {
    @Autowired
    private NewsPaperMapper newsPaperMapper;

//报纸分页展示
    @Override
    public ShowInfo paperAll(String source,int pageNo,int pageSize) {
        ShowInfo showInfo1 = new ShowInfo();
        List<NewsPaperEntity> newsPaperEntities1 = newsPaperMapper.paperAll(source,(pageNo - 1)*pageSize,pageSize);
        int i = newsPaperMapper.newsPaperPageCount();
        showInfo1.setTotalPage(i);
        showInfo1.setTotalPage(i/pageSize);//总页数
        showInfo1.setPage(pageNo);
        showInfo1.setTotalCount(newsPaperMapper.newsPaperPageCount());
        showInfo1.setLimitShowList(newsPaperEntities1);
            return showInfo1;
    }

    @Override
    public NewsPaperEntity paperID(int id) {
        NewsPaperEntity newsPaperEntity = newsPaperMapper.newsPaperDate(id);
        return newsPaperEntity;
    }

}
