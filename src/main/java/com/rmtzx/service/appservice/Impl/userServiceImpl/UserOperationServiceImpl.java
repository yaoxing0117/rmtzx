package com.rmtzx.service.appservice.Impl.userServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserMapper;
import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.entity.appentity.userEntity.*;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.UserOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserOperationServiceImpl implements UserOperationService {
    @Autowired
    private UserOperationMapper userOperationMapper;
    @Autowired
    private UserMapper userMapper;

    //写入用户历史记录
    @Override
    public int setHistory(String historicalUser,
                          String historicalTitle,
                          String historicalSource,
                          int historicalNewsId,
                          String historicalCategory,
                          String historicalTime) {
        return userOperationMapper.setHistory(historicalUser,
                historicalTitle, historicalSource, historicalNewsId, historicalCategory, historicalTime);
    }

    //用户评论展示
    @Override
    public ShowInfo allComments(String commentator, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<CommentEntity> commentEntities = userOperationMapper.allComments(commentator, (pageNo - 1) * pageSize, pageSize);
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(commentEntities);
        return showInfo;
    }

    //添加收藏
    @Override
    public int userRecord(String collectionUser,
                          String collectionArticle,
                          String collectionSource,
                          String collectionTime,
                          String collectionArticleCategory,
                          int collectionArticleId, int userId) {

        List<UserCollectionEntity> userHistoricalEntities = userOperationMapper.userQueryCollection(userId, collectionArticleId);
        System.out.println(userHistoricalEntities);
        if (userHistoricalEntities.isEmpty()) {
            int i = userOperationMapper.userRecord(collectionUser,
                    collectionArticle, collectionSource, collectionTime,
                    collectionArticleCategory,
                    collectionArticleId, userId);
            UserEntity userEntity = userOperationMapper.userFrequency(userId);
            int userCollection = userEntity.getUserCollection();
            userOperationMapper.frequencyCollection(userCollection + 1,userId);
            return i;
        }

        return 0;
    }

    //分页展示收藏
    @Override
    public ShowInfo allUserRecord(int userId, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<UserCollectionEntity> userHistoricalEntities = userOperationMapper.allUserRecord(userId, (pageNo - 1) * pageSize, pageSize);
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(userHistoricalEntities);
        return showInfo;
    }

    //用户投诉
    @Override
    public int userComplaints(String complainant,//投诉人手机号
                              String department,//投诉部门
                              int userId,//投诉人ID
                              String userMail,//投诉人邮箱
                              String contents,//投诉内容
                              String title,//投诉标题
                              String complaintTime, String category) {//投诉时间
        return userOperationMapper.userComplaints(complainant,
                department,
                userId, userMail, contents, title, complaintTime, category);
    }

    //用户投诉列表展示
    @Override
    public ShowInfo userAllComplaints(int userId, String category, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<UserComplaintEntity> userComplaintEntities =
                userOperationMapper.userAllComplaints(userId, category, (pageNo - 1) * pageSize, pageSize);
        System.out.println(userComplaintEntities);
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(userComplaintEntities);
        return showInfo;
    }

    @Override
    public List<UserComplaintEntity> userOneComplaints(int id) {
        return userOperationMapper.userOneComplaints(id);
    }

    //反馈投诉
    @Override
    public int userFeedback(String feedbackContents, String feedbackTime, String feedbackDepartment) {
        return 0;
    }

    //添加简历
    @Override
    public int setUserResume(String userName, String userAge, String userSex, String userTel,
                             String userMail, String userXl, String userZwms, String userQwxz,
                             String userBz, String userNameTel, int uId) {
        //查询是否有存在简历
        UserResumeEntity userResumeEntity = userOperationMapper.resumeUser(uId);
        // System.out.println(userResumeEntity);
        if (userResumeEntity != null) {
            userOperationMapper.update(userName, userAge, userSex, userTel, userMail, userXl, userZwms, userQwxz, userBz, uId, userResumeEntity.getId());
            int resumeId = userOperationMapper.resumeId(uId);//简历id
            int i = userOperationMapper.editUser(uId, resumeId);
            return i;
        }
        int i = userOperationMapper.setUserResume(userName, userAge, userSex, userTel, userMail, userXl, userZwms, userQwxz, userBz, uId);
//        userOperationMapper.editUser(id,);
        int resumeId = userOperationMapper.resumeId(uId);//简历id
        userOperationMapper.editUser(uId, resumeId);
        return i;
    }

    //查看简历
    @Override
    public UserResumeEntity oneUserR(int uid) {
        return userOperationMapper.oneUserR(uid);
    }

    @Override
    public List<UserGrslEntity> grslAll(String userId) {
        return userOperationMapper.grslAll(userId);
    }

    @Override
    public UserEntity userGetId(int id) {
        return userOperationMapper.userGetId(id);
    }

    @Override
    public int prizeInsert(String reportContent, String reportPicture, String reportTel, String reportWx, String reportTime) {
        return userOperationMapper.prizeInsert(reportContent, reportPicture, reportTel, reportWx, reportTime);
    }
}
