package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.TrainingMapper;
import com.rmtzx.entity.appentity.TrainingEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TrainigServiceImpl implements TrainingService {
    @Autowired
    private TrainingMapper trainingMapper;

    //展示分页所有培训班
    @Override
    public ShowInfo allTraining(int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(trainingMapper.paperPageCount());
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(trainingMapper.allTraining((pageNo-1)*pageSize, pageSize));
        return showInfo;
    }

    @Override
    public TrainingEntity oneTraining(int id) {
        return trainingMapper.oneTraining(id);
    }
}
