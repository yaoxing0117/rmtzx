package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.WjdcMapper;
import com.rmtzx.entity.appentity.WjdcDAEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.WjdcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class WjdcServiceImpl implements WjdcService {
    @Autowired
    private WjdcMapper wjdcMapper;
    //列表返回问卷
    @Override
    public ShowInfo wjdcAll(String category, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(wjdcMapper.wjdcAll(category, (pageNo-1)*pageSize, pageSize));
        return showInfo;
    }
    //详情问卷
    @Override
    public List<WjdcDAEntity> wjdcOne(String wjId) {
        return wjdcMapper.wjdcOne(wjId);
    }
    //开始答题
    @Override
    public int upWjdc(int id) {
        int i = wjdcMapper.seWjdc(id);
        int count = i + 1;
        wjdcMapper.upWjdc(id, count);
        return 0;
    }

    @Override
    public int seWjdc(int id) {
        return 0;
    }
}
