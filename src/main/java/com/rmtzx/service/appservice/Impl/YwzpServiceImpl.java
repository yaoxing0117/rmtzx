package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.ysDao.YwzpMapper;
import com.rmtzx.entity.appentity.ysEntity.YstdEntity;
import com.rmtzx.entity.appentity.ysEntity.YszwEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.YwzpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class YwzpServiceImpl implements YwzpService {
    @Autowired
    private YwzpMapper ywzpMapper;

    //信息公开展示
    @Override
    public List<YstdEntity> xxallSelect() {
        return ywzpMapper.xxallSelects();
    }
    //信息公开展示
    @Override
    public ShowInfo xxallSelect(int cid, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(ywzpMapper.xxpaperPageCount(cid));
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(ywzpMapper.xxallSelect(cid, (pageNo-1)*pageSize, pageSize));
        return showInfo;
    }
    //信息详情展示()
    @Override
    public YstdEntity xxoneSelect(int cid) {
        return ywzpMapper.xxoneSelect(cid);
    }
    //职位列表展示
    @Override
    public ShowInfo zwallSelect(int cid,int pageNo,int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(ywzpMapper.zwPaperPageCount(cid));
        showInfo.setLimitShowList(ywzpMapper.zwallSelect(cid, (pageNo-1)*pageSize, pageSize));
        showInfo.setPage(pageNo);
        return showInfo;
    }
    //职位详情展示
    @Override
    public YszwEntity zwoneSelect(int cid) {
        return ywzpMapper.zwoneSelect(cid);
    }
    //职位投递
    @Override
    public int zwInsert(int gongsiId, int userId, int zhiweiId) {
        return ywzpMapper.zwInsert(gongsiId, userId, zhiweiId);
    }
}
