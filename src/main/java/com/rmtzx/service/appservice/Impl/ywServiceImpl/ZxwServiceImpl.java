package com.rmtzx.service.appservice.Impl.ywServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.dao.appdao.ywDao.CommentMapper;
import com.rmtzx.dao.appdao.ywDao.ZxwMapper;
import com.rmtzx.dao.appdao.ywDao.ZzqxwMapper;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.ZxwEntity;
import com.rmtzx.service.appservice.ZxwService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZxwServiceImpl implements ZxwService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ZxwMapper zxwMapper;
    @Autowired
    private ZzqxwMapper zzqxwMapper;
    @Autowired
    private UserOperationMapper userOperationMapper;
    //审批数据展示
    @Override
    public List<ZxwEntity> zxwAll() {
        return zxwMapper.zxwAll();
    }
    //州新闻批量审核
    @Override
    public String updateState(int id,int code) {
        int count = zxwMapper.updateState(code,id);
//        System.out.print(count+"-------"+ids+"-------"+code);
        if (count == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }
    //app展示列表形式 固定字段
    @Override
    public ShowInfo zxwScreen(int pageNo,int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<ZxwEntity> zxwEntities = zxwMapper.zxwScreen((pageNo - 1)*pageSize, pageSize);
        showInfo.setLimitShowList(zxwEntities);
        showInfo.setTotalCount(zxwMapper.zxwPageCount());
        showInfo.setPage(pageNo);
        return showInfo;
    }
    //分页查询
    @Override
    public ShowInfo zxwPaging(int pageNo, int pageSize) {
        ShowInfo showInfo1 = new ShowInfo();
        //总行数
        int i = zxwMapper.zxwPageCount();
        showInfo1.setTotalCount(i);//总记录数
        showInfo1.setTotalPage(i/pageSize);//总页数
        showInfo1.setPage(pageNo);
        showInfo1.setNewsInfoURL("/zxw/zxwIdDeta");
        List<ZxwEntity> zxwEntities = zxwMapper.zxwPaging((pageNo - 1) * pageSize, pageSize);
        showInfo1.setLimitShowList(zxwEntities);
        return showInfo1;
    }
    //id查询数据（详情）
    public Result singleDate(int id) {
        ZxwEntity zxwEntity = zxwMapper.singleDate(id);
        if (zxwEntity==null){
            return Result.failure(ResultCode.PARAMETER_ERROR);
        }
        List<CommentEntity> commentEntities = commentMapper.queryComments(zxwEntity.getId());
        zxwEntity.setComment(commentEntities);
        return Result.success(zxwEntity);
    }
    //删除数据
    @Override
    public int deleteId(int id) {
        return 0;
    }
    //点赞
    @Override
    public int thumbsUp(int up, int id) {
        int i = zxwMapper.d_zId(id);
        int i1=i+1;
        int i2 = zxwMapper.thumbsUp(i1,id);
        return i2;
    }

    //增加评论
    @Override
    public int commentInsert(String commentator, String commentContent, String commentaryTime, String userTx, int commentID, int userId, String commentCategory) {
        return zzqxwMapper.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory);
    }
    //评论分页展示
    @Override
    public List<CommentEntity> commentPaging(String commentCategory,int commentID,int pageNo, int pageSize) {
        List<CommentEntity> commentEntities = zzqxwMapper.commentPaging(commentCategory,commentID,(pageNo-1)*pageSize, pageSize);
        return commentEntities;
    }
}
