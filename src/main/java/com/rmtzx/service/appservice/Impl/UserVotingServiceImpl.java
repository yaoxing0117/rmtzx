package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.usersDao.UserVotingMapper;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.UserVotingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserVotingServiceImpl implements UserVotingService {
    @Autowired
    private UserVotingMapper userVotingMapper;



    //查询所有投票展示（据投票类别）
    @Override
    public ShowInfo all(String votingCategory,
                        int pageNo,
                        int pageSize) {
        ShowInfo showInfo =new ShowInfo();
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(
                userVotingMapper.all(
                        votingCategory,(pageNo - 1)*pageSize, pageSize));
        return showInfo;
    }
    //查询投票内容详情展示
    @Override
    public ShowInfo oneVoting(String optionId,
                              int pageNo,
                              int pageSize) {
        ShowInfo showInfo =new ShowInfo();
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(
                userVotingMapper.oneVoting(
                        optionId,(pageNo - 1)*pageSize, pageSize));
        return showInfo;
    }
    //用户投票
    @Override
    public Result updateVotes(String userName,
                              int userId,
                              int id) {
        String i = userVotingMapper.selectVoting(id,userName);
        if (i==null){
            int i1 = userVotingMapper.selectVotes(id);
            userVotingMapper.inUser(userName, userId, id);
            int votes=i1+1;
            return Result.success( userVotingMapper.updateVotes(id, votes));
        }

        return Result.failure(ResultCode.FAIL,"已投票");
    }

    @Override
    public int selectVotes() {
        return 0;
    }
}
