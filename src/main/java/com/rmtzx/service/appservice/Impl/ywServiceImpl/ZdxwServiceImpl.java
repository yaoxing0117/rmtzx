package com.rmtzx.service.appservice.Impl.ywServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.dao.appdao.ywDao.CommentMapper;
import com.rmtzx.dao.appdao.ywDao.ZdxwMapper;
import com.rmtzx.dao.appdao.ywDao.ZzqxwMapper;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import com.rmtzx.service.appservice.ZdxwService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZdxwServiceImpl implements ZdxwService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ZdxwMapper zdxwMapper;
    @Autowired
    private ZzqxwMapper zzqxwMapper;
    @Autowired
    private UserOperationMapper userOperationMapper;
    //审批数据展示
    //州新闻批量审核
    @Override
    public String updateState(int id,int code) {
        int count = zdxwMapper.updateState(code,id);
//        System.out.print(count+"-------"+ids+"-------"+code);
        if (count == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }
    //app展示列表形式 固定字段
    @Override
    public ShowInfo zdxwScreen(int pageNo,int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<ZdxwEntity> zdxwEntities = zdxwMapper.zdxwScreen((pageNo - 1)*pageSize, pageSize);
        showInfo.setLimitShowList(zdxwEntities);
        showInfo.setTotalCount(zdxwMapper.zdxwPageCount());
        showInfo.setPage(pageNo);
        return showInfo;
    }
    //分页查询
    @Override
    public ShowInfo zdxwPaging(int pageNo, int pageSize) {
        ShowInfo showInfo1 = new ShowInfo();
        //总行数
        int i = zdxwMapper.zdxwPageCount();
        showInfo1.setTotalPage(i);
        showInfo1.setTotalPage(i/pageSize);//总页数
        showInfo1.setTotalCount(zdxwMapper.zdxwPageCount());
        showInfo1.setPage(pageNo);
        showInfo1.setNewsInfoURL("/zdxw/zdxwIdDeta");
        showInfo1.setTotalPage(zdxwMapper.zdxwPageCount());
        List<ZdxwEntity> zdxwEntities = zdxwMapper.zdxwPaging((pageNo - 1) * pageSize, pageSize);
        showInfo1.setLimitShowList(zdxwEntities);
        return showInfo1;
    }
    //id查询数据（详情）
    public Result singleDate(int id) {
        ZdxwEntity zdxwEntity = zdxwMapper.singleDate(id);
        if (zdxwEntity==null){
            return Result.failure(ResultCode.PARAMETER_ERROR);
        }
        List<CommentEntity> commentEntities = commentMapper.queryComments(zdxwEntity.getId());
        zdxwEntity.setComment(commentEntities);
        return Result.success(zdxwEntity);
    }
    //删除数据
    @Override
    public int deleteId(int id) {
        return 0;
    }
    //点赞
    @Override
    public int thumbsUp(int up, int id) {
        int i = zdxwMapper.d_zId(id);
        int i1=i+1;
        int i2 = zdxwMapper.thumbsUp(i1,id);
        return i2;
    }
    //增加评论
    @Override
    public int commentInsert(String commentator, String commentContent, String commentaryTime, String userTx, int commentID, int userId, String commentCategory) {
        int pl = zdxwMapper.pl(commentID);
        int commentQuantity=pl+1;
        int i = zdxwMapper.plUp(commentQuantity, pl);
        return zzqxwMapper.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory);
    }
    //评论分页展示
//    @Override
//    public List<CommentEntity> commentPaging(String commentCategory,int pageNo, int pageSize) {
//        List<CommentEntity> commentEntities = zzqxwMapper.commentPaging(commentCategory,(pageNo-1)*pageSize, pageSize);
//        return commentEntities;
//    }
}
