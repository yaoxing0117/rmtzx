package com.rmtzx.service.appservice.Impl.ywServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.dao.appdao.ywDao.CommentMapper;
import com.rmtzx.dao.appdao.ywDao.ZzqxwMapper;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.ZyxwEntity;
import com.rmtzx.dao.appdao.ywDao.ZyxwMapper;
import com.rmtzx.service.appservice.ZyxwService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ZyxwServiceImpl implements ZyxwService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ZyxwMapper zyxwMapper;
    @Autowired
    private ZzqxwMapper zzqxwMapper;
    @Autowired
    private UserOperationMapper userOperationMapper;

    @Override
    public List<ZyxwEntity> zyxwAll() {
        return zyxwMapper.zyxwAll();
    }
    //州新闻批量审核
    @Override
    public String updateState(int id,int code) {
        int count = zyxwMapper.updateState(code,id);
//        System.out.print(count+"-------"+ids+"-------"+code);
        if (count == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }
    //详情数据展示
    @Override
    public ShowInfo zyxwScreen(int pageNo,int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<ZyxwEntity> zyxwEntities = zyxwMapper.zyxwScreen((pageNo - 1)*pageSize, pageSize);
        showInfo.setLimitShowList(zyxwEntities);
        showInfo.setPage(pageNo);
        return showInfo;
    }

    @Override
    public ShowInfo zyxwPaging(int pageNo, int pageSize) {
        ShowInfo showInfo1 = new ShowInfo();
        //z总页数
        int i = zyxwMapper.zyxwPageCount();
        showInfo1.setTotalPage(i);
        showInfo1.setTotalPage(i/pageSize);//总页数
        showInfo1.setPage(pageNo);
        showInfo1.setNewsInfoURL("/zyxw/zyxwIdDeta");
        showInfo1.setTotalPage(zyxwMapper.zyxwPageCount());
        List<ZyxwEntity> zyxwEntities = zyxwMapper.zyxwPaging((pageNo - 1)*pageSize, pageSize);
        showInfo1.setLimitShowList(zyxwEntities);
        return showInfo1;
    }
//详情展示
    @Override
    public Result singleDate(int id) {
        ZyxwEntity zyxwEntity = zyxwMapper.singleDate(id);
        if (zyxwEntity==null){
            return Result.failure(ResultCode.PARAMETER_ERROR);
        }
        List<CommentEntity> commentEntities = commentMapper.queryComments(zyxwEntity.getId());
        zyxwEntity.setComment(commentEntities);
        return Result.success(zyxwEntity);
    }

    //通过id删除
    @Override
    public int deleteId(int id) {
        return 0;
    }
    //点赞
    @Override
    public int thumbsUp(int up, int id) {
        int i = zyxwMapper.d_zId(id);
        int up1=i+1;
        int i2 = zyxwMapper.thumbsUp(up1,id);
        System.out.println(up1+"...."+id+"......"+i);
        return i2;
    }
    //增加评论
    @Override
    public int commentInsert(String commentator, String commentContent, String commentaryTime,String userTx, int commentID, int userId, String commentCategory) {
        return zzqxwMapper.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory);
    }
    //评论分页展示
//    @Override
//    public List<CommentEntity> commentPaging(String commentCategory,int pageNo, int pageSize) {
//        List<CommentEntity> commentEntities = zzqxwMapper.commentPaging(commentCategory,(pageNo-1)*pageSize, pageSize);
//        return commentEntities;
//    }
}
