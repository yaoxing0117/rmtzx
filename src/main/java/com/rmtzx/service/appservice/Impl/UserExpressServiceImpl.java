package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.usersDao.UserExpressMapper;
import com.rmtzx.entity.appentity.userEntity.UserExpressEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.UserExpressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
public class UserExpressServiceImpl implements UserExpressService {
    @Autowired
    private UserExpressMapper userExpress;
    //添加用户快递信息
    @Override
    public int userRecord(String userUser,
                          String sender,
                          String mailingAddress,
                          String addressee,
                          String receivingAddress,
                          String waybill,
                          String userOrder,
                          String courierServicesCompany,
                          String uniqueIdentifier) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String expressTime = df.format(new Date());
        return  userExpress.userRecord(userUser,
                sender, mailingAddress, addressee,
                receivingAddress, waybill, userOrder, courierServicesCompany,expressTime,uniqueIdentifier);

    }
    //查询快递信息
    @Override
    public ShowInfo allExpress(String userTel, int pageNo, int pageSize) {
        List<UserExpressEntity> userExpressEntities =
                userExpress.allExpress(userTel, (pageNo - 1) * pageSize, pageSize);
        ShowInfo showInfo = new ShowInfo();
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(userExpressEntities);

        return showInfo;
    }
}
