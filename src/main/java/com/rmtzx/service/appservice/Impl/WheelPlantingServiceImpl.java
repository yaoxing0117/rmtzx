package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.ywDao.WheelPlantingMapper;
import com.rmtzx.entity.appentity.AudioEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.pojo.WheelPlantingEntity;
import com.rmtzx.service.appservice.WheelPlantingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WheelPlantingServiceImpl implements WheelPlantingService {
    @Autowired
    private WheelPlantingMapper wheelPlantingMapper;


    //轮播图片展示
    @Override
    public List<WheelPlantingEntity>allWheel (String category) {
        return wheelPlantingMapper.allWheel(category);
    }
    //轮播新闻详情
    @Override
    public WheelPlantingEntity oneWheel(int id, String category) {
        WheelPlantingEntity wheelPlantingEntity = wheelPlantingMapper.oneWheel(id, category);
        return wheelPlantingMapper.oneWheel(id, category);
    }
    //增加点赞次数
    @Override
    public int dzUpdate(int id,String category) {
        int i = wheelPlantingMapper.dzAll(id, category);
        int i1=i+1;
        int i2 = wheelPlantingMapper.dzUpdate(i1, id);
        return i2;
    }
//MP3展示分页
    @Override
    public ShowInfo allMP3(String category, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        List<AudioEntity> audioEntities = wheelPlantingMapper.allMP3(category, (pageNo-1)*pageSize, pageSize);
        showInfo.setLimitShowList(audioEntities);
        showInfo.setPage(pageNo);
        showInfo.setTotalCount(wheelPlantingMapper.mp3wPageCount(category));
        return showInfo;
    }
    //MP3详情
    @Override
    public AudioEntity oneMP3(int id) {
        return wheelPlantingMapper.oneMP3(id);
    }
}
