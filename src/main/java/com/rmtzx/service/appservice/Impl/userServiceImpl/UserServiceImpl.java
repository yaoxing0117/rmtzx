package com.rmtzx.service.appservice.Impl.userServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserMapper;
import com.rmtzx.entity.appentity.userEntity.UserEntity;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.appservice.UserService;
import com.rmtzx.util.Md5Util;
import com.rmtzx.util.RedisUtils;
import com.rmtzx.util.SMSUtils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;


@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RedisUtils redisUtil;

    @Override
    public UserEntity queryportra(String userNameTel) {
        // System.out.println(userMapper.queryportra(userNameTel));
        return userMapper.queryportra(userNameTel);
    }

    //登录
    @Override
    public Result findLoadByUser(String userNameTel, String passWord) {
        String s = Md5Util.MD5(userNameTel);//使用账号生成MD5加密token
        //获取当前token
//        if (redisUtil.get(userNameTel)==null){
        UserEntity loadByUser = userMapper.findUser(userNameTel);
        if (loadByUser != null) {
            String passWord1 = loadByUser.getPassWord();
            if (passWord.equals(passWord1)) {
                //密码正确
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
                String userHistory = df.format(new Date());// new Date()
                UserEntity loadByUser1 = userMapper.findLoadByUser(userNameTel);
                userMapper.userHistoryUser(userNameTel, userHistory);
//                    redisUtil.set(userNameTel,s,3600);//redis缓存token
                loadByUser1.setUserToken(s);
                log.info("用户：" + userNameTel + "登陆成功");
                return Result.success(loadByUser1);
            } else {
                //账号不存在
                return Result.failure(ResultCode.FAIL, "密码错误");
            }
        }
        return Result.failure(ResultCode.FAIL, "服务器异常");
//        }
//        return Result.failure(ResultCode.FAIL,"用户已登陆");
    }

    //退出登录
    @Override
    public Result signOut(String userNameTel) {
        redisUtil.del(userNameTel);
        log.info("用户：" + userNameTel + "退出已登陆");
        return Result.success("退出登陆成功");
    }

    //自动登录
    @Override
    public Result automaticLogon(String userNameTel, String token) {
        try {
            String s = redisUtil.get(userNameTel).toString();
            if (s.equals(token)) {
                return Result.success("自动登录成功");
            } else if (s != token) {
                return Result.failure(ResultCode.FAIL, "未知错误！请重新登录。");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.failure(ResultCode.FAIL, "登录失效。");
    }

    //发送短信
    @Override
    public Result SMSlogin(String userNameTel) throws IOException {
        Random random = new Random();
        Integer i = random.nextInt(1000000);
        String sms = i.toString();
        redisUtil.set(userNameTel, sms, 300);
        int i1 = SMSUtils.sendMsg(userNameTel, sms);
        if (i1 == 1) {
            return Result.success("发送成功");
        }
        return Result.failure(ResultCode.FAIL);
    }

    //注册
    @Override
    public Result addUser(String userName, String passWord, String Sms) {
        String s = redisUtil.get(userName).toString();
        if (Sms.equals(s)) {
            UserEntity user = userMapper.findUser(userName);
            if (user == null) {
                userMapper.addUser(userName, passWord);
                //注册成功
                log.info("用户：" + userName + "注册成功");
                return Result.success();
            } else {
                //已存在
                return Result.failure(ResultCode.UNAUTHORISE, "账号已存在");
            }
        }
        //验证码逻辑
        //String userNameTel = user.getUserNameTel();
        return Result.failure(ResultCode.FAIL, "验证码错误");
    }

    //修改成功
    @Override
    public Result editUser(String userNameTel, String passWord, String Sms) {
        //验证码逻辑未完成~
        String s = redisUtil.get(userNameTel).toString();
        if (s.equals(Sms)) {
            if (userNameTel == null) {
                return Result.failure(ResultCode.FAIL, "验证码不存在");
            }
            userMapper.editUser(userNameTel, passWord);
            return Result.success();
        }
        return Result.failure(ResultCode.FAIL, "验证码错误");
    }

    //修改个人信息
    @Override
    public int updatePersona(String userNameTel,
                             String userNickName,
                             String userAge,
                             String userSex,
                             String userBirthday,
                             String userCity,
                             String userPortrait,
                             String userTel,
                             String userName) {
        log.info("用户：" + userNameTel + "修改信息~");
        int i = userMapper.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
        return i;
    }

    //查询昵称是否存在
    @Override
    public UserEntity queryNickName(String userNickName) {
        return userMapper.queryNickName(userNickName);
    }

    //查询修改后用户信息
    @Override
    public UserEntity userDate(String userNameTel) {
        return userMapper.userDate(userNameTel);
    }


}
