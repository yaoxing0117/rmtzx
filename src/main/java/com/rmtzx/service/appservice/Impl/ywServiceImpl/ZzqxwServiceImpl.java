package com.rmtzx.service.appservice.Impl.ywServiceImpl;

import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.dao.appdao.ywDao.CommentMapper;
import com.rmtzx.dao.appdao.ywDao.ZzqxwMapper;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.ZzqxwEntity;
import com.rmtzx.service.appservice.ZzqxwService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ZzqxwServiceImpl implements ZzqxwService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private ZzqxwMapper zzqxwMapper;
    @Autowired
    private UserOperationMapper userOperationMapper;

    //州新闻批量审核
    @Override
    public String updateState(int[] id,int code) {
        for (int i = 0; i <id.length ; i++) {
            int i1 = zzqxwMapper.updateState(id[i], code);
        }
        return "成功";
    }

    //列表固定字段查询分页
    @Override
    public ShowInfo zzqxwScreen(int pageNo,int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        List<ZzqxwEntity> zzqxwEntities = zzqxwMapper.zzqScreen((pageNo - 1) * pageSize, pageSize);
        showInfo.setLimitShowList(zzqxwEntities);
        showInfo.setTotalCount(zzqxwMapper.zzqxwPageCount());
        showInfo.setPage(pageNo);
        return showInfo;
    }

    //分页展示 pageNo页数 pageSize展示多少数据
    @Override
    public ShowInfo zzqxwPaging(int pageNo, int pageSize) {
        ShowInfo showInfo1 = new ShowInfo();
        //总条数
        int i = zzqxwMapper.zzqxwPageCount();
        showInfo1.setTotalPage(i);
        showInfo1.setTotalPage(i/pageSize);//总页数
        showInfo1.setPage(pageNo);
        showInfo1.setNewsInfoURL("/zyxw/zzqxwIdDeta");
        showInfo1.setTotalPage(zzqxwMapper.zzqxwPageCount());
        List<ZzqxwEntity> zzqxwEntities = zzqxwMapper.zzqxwPaging((pageNo - 1) * pageSize, pageSize);
        showInfo1.setLimitShowList(zzqxwEntities);
        return showInfo1;
    }
    //通过Id查询数据-详情页面
    @Override
    public Result singleDate(int id) {
        ZzqxwEntity zzqxwEntity = zzqxwMapper.singleDate(id);
        if (zzqxwEntity==null){
            return Result.failure(ResultCode.PARAMETER_ERROR);
        }
        List<CommentEntity> commentEntities = commentMapper.queryComments(zzqxwEntity.getId());
        zzqxwEntity.setComment(commentEntities);
        return Result.success(zzqxwEntity);
    }
    //通过id删除
    @Override
    public int deleteId(int id) {
        return 0;
    }

    @Override
    public int thumbsUp(int up, int id) {
        int i = zzqxwMapper.d_zId(id);
        int i1=i+1;
        int i2 = zzqxwMapper.thumbsUp(i1,id);
        return i2;
    }
    //添加评论
    @Override
    public int commentInsert(String commentator, String commentContent, String commentaryTime, String userTx, int commentID, int userId, String commentCategory) {
        if (commentCategory.equals("ZDXW")){
            int i = zzqxwMapper.commentZD(commentID);//该新闻评论总数
            zzqxwMapper.upComment(i+1,commentID);
            return zzqxwMapper.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory);
        }
        return zzqxwMapper.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory);
    }
    //评论分页展示
    @Override
    public List<CommentEntity> commentPaging(String commentCategory,int commentID,int pageNo, int pageSize) {
        List<CommentEntity> commentEntities = zzqxwMapper.commentPaging(commentCategory,commentID,(pageNo-1)*pageSize, pageSize);
        return commentEntities;
    }
}
