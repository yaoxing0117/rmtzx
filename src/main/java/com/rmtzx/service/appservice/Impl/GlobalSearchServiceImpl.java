package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.GlobalSearchMapper;
import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.GlobalSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GlobalSearchServiceImpl implements GlobalSearchService {
    @Autowired
    private GlobalSearchMapper globalSearch;


    @Override
    public ShowInfo allGlobalSearch(String article, int pageNo, int pageSize) {
        ShowInfo showInfo =new ShowInfo();
        List<ZdxwEntity> zdxwEntities = globalSearch.allGlobalSearch(article, pageNo, pageSize);
        showInfo.setLimitShowList(zdxwEntities);
        showInfo.setPage(pageNo);
        return showInfo;
    }
}
