package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.usersDao.UserOperationMapper;
import com.rmtzx.dao.appdao.ywDao.UserDZMapper;
import com.rmtzx.dao.appdao.ywDao.ZdxwMapper;
import com.rmtzx.dao.appdao.ywDao.ZxwMapper;
import com.rmtzx.dao.appdao.ywDao.ZyxwMapper;
import com.rmtzx.dao.appdao.ywDao.ZzqxwMapper;
import com.rmtzx.entity.appentity.ywEntity.UserDZEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.UserDZService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@Slf4j
public class UserDZServiceImpl implements UserDZService {
    @Autowired
    private UserDZMapper userDZMapper;
    @Autowired
    private ZdxwMapper zdxwMapper;
    @Autowired
    private ZzqxwMapper zzqxwMapper;
    @Autowired
    private ZyxwMapper zyxwMapper;
    @Autowired
    private ZxwMapper zxwMapper;
    @Autowired
    private UserOperationMapper userOperationMapper;

    /**
     * 查询点赞是否存在
     *
     * @param userId 用户id
     * @param newsId 新闻id
     * @return 用户是否点赞该id 0未点赞 1已经点赞
     */
    @Override
    public int selectAll(int userId, int newsId, String newsCategory) {
        UserDZEntity userDZEntity = userDZMapper.selectOne(userId, newsCategory, newsId);
        if (userDZEntity == null) {
            return 0;
        }
        if (userDZEntity.getNewsCategory().equals(newsCategory)&&userDZEntity.getNewsId().equals(newsId)) {
            return 1;
        } else return 0;
    }

    /**
     * 添加点赞
     * @param newsTitle    新闻标题
     * @param newsTime     新闻时间
     * @param newsCategory 新闻类别
     * @param userId       用户id
     * @param newsId       新闻id
     * @return 是否成功
     */
    @Override
    public int insertDZ(String newsTitle, String newsTime, String newsCategory, int userId, int newsId) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String reserveOne = df.format(new Date());
        return userDZMapper.insertDZ(newsTitle, newsTime, newsCategory, userId, reserveOne, newsId);
    }

    /**
     * 用户查询自己id下所有点赞
     *
     * @param userId   用户id
     * @param pageNo   第几页
     * @param pageSize 一页显示多少行
     * @return 查询数据
     */
    @Override
    public ShowInfo selectAll(int userId, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        showInfo.setLimitShowList(userDZMapper.selectAll(userId, (pageNo - 1) * pageSize, pageSize));
        showInfo.setTotalCount(userDZMapper.newsPaperPageCount(userId));
        return showInfo;
    }

    /**
     * 通用点赞接口
     *
     * @param newsCategory 新闻类别
     * @param newsId       新闻id
     * @return 是否成功
     */
    @Override
    public int insDZ(String newsCategory, int newsId, int userId) {
        if (newsCategory.equals("ZDXW")) {
            userOperationMapper.frequencyDZ(userOperationMapper.userFrequency(userId).getUserDz() + 1, userId);
            return zdxwMapper.plUp(zdxwMapper.pl(newsId) + 1, newsId);
        } else if (newsCategory.equals("ZZQXW")) {
            userOperationMapper.frequencyDZ(userOperationMapper.userFrequency(userId).getUserDz() + 1, userId);
            return zzqxwMapper.thumbsUp(zzqxwMapper.d_zId(newsId) + 1, newsId);
        } else if (newsCategory.equals("ZXW")) {
            userOperationMapper.frequencyDZ(userOperationMapper.userFrequency(userId).getUserDz() + 1, userId);
            return zxwMapper.thumbsUp(zxwMapper.d_zId(newsId) + 1, newsId);
        } else if (newsCategory.equals("ZYXW")) {
            userOperationMapper.frequencyDZ(userOperationMapper.userFrequency(userId).getUserDz() + 1, userId);
            return zyxwMapper.thumbsUp(zyxwMapper.d_zId(newsId) + 1, newsId);
        } else {
            return 0;
        }
    }

}
