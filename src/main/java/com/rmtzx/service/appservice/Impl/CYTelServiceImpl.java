package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.CYTelMapper;
import com.rmtzx.entity.CYTelEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.CYTelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CYTelServiceImpl implements CYTelService {
    @Autowired
    private CYTelMapper cyTelMapper;
    @Override
    public ShowInfo pAll() {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(cyTelMapper.paperPageCount());
        showInfo.setLimitShowList(cyTelMapper.pAll());
        return showInfo;
    }

    @Override
    public ShowInfo cAll(int pid,
                         int pageNo,
                         int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(cyTelMapper.cpaperPageCount(pid));
        showInfo.setLimitShowList(cyTelMapper.cAll(pid, (pageNo-1)*pageSize, pageSize));
        return showInfo;
    }
}
