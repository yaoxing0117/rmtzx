package com.rmtzx.service.appservice.Impl;

import com.rmtzx.dao.appdao.YsZjkMapper;
import com.rmtzx.entity.appentity.YsZjkEntity;
import com.rmtzx.entity.appentity.YsZjkNameEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.YsZjkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YsZjkServiceImpl implements YsZjkService {
    @Autowired
    private YsZjkMapper ysZjkMapper;

    //类别返回
    @Override
    public List<YsZjkEntity> zjAll() {
        return ysZjkMapper.zjAll();
    }
    //专家分页展示
    @Override
    public ShowInfo zjNameAll(int cid, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setPage(pageNo);
        showInfo.setLimitShowList(ysZjkMapper.zjNameAll(cid, (pageNo-1)*pageSize, pageSize));
        return showInfo;
    }
    //专家详情展示
    @Override
    public YsZjkNameEntity zjone(int id) {
        return ysZjkMapper.zjone(id);
    }
//查询专家
    @Override
    public List<YsZjkNameEntity> zjAll1(String expertName) {
        return ysZjkMapper.zjAll1(expertName);
    }
}
