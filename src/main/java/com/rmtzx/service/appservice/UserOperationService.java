package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.userEntity.UserComplaintEntity;
import com.rmtzx.entity.appentity.userEntity.UserEntity;
import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import com.rmtzx.entity.appentity.userEntity.UserResumeEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserOperationService {
         int setHistory(String historicalUser,
                              String historicalTitle,
                              String historicalSource,
                              int historicalNewsId,
                              String historicalCategory,
                              String historicalTime);
        //用户分页展示评论
        ShowInfo allComments(@Param("commentator") String commentator,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);//评论人
        //添加收藏
        int userRecord(@Param("collectionUser")String collectionUser,
                       @Param("collectionArticle")String collectionArticle,
                       @Param("collectionSource")String collectionSource,
                       @Param("collectionTime")String collectionTime,
                       @Param("collectionArticleCategory")String collectionArticleCategory,
                       @Param("collectionArticleId")int collectionArticleId,
                       @Param("userId")int userId);
        ShowInfo allUserRecord(@Param("userId") int userId,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);//评论人
        //用户投诉
        int userComplaints(@Param("complainant")String complainant,//投诉人手机号
                           @Param("department")String department,//投诉部门
                           @Param("userId")int userId,//投诉人ID
                           @Param("userMail")String userMail,//投诉人邮箱
                           @Param("contents")String contents,//投诉内容
                           @Param("title")String title,//投诉标题
                           @Param("complaintTime")String complaintTime,//投诉时间
                           @Param("category")String category);//类别
        //用户投诉列表展示
        ShowInfo userAllComplaints(
                @Param("userId")int userId,
                @Param("category")String category,
                @Param("pageNo")int pageNo,
                @Param("pageSize")int pageSize
        );
        //通过ID查询投诉内容反馈
        List<UserComplaintEntity> userOneComplaints(@Param("id")int id);
        //投诉反馈接口
        int userFeedback(@Param("feedbackContents")String feedbackContents,
                         @Param("feedbackTime")String feedbackTime,
                         @Param("feedbackDepartment")String feedbackDepartment);
        //添加简历
        int setUserResume(@Param("userName") String userName,
                          @Param("userAge") String userAge,
                          @Param("userSex") String userSex,
                          @Param("userTel") String userTel,
                          @Param("userMail") String userMail,
                          @Param("userXl") String userXl,
                          @Param("userZwms") String userZwms,
                          @Param("userQwxz") String userQwxz,
                          @Param("userBz") String userBz,
                          @Param("userNameTel") String userNameTel,
                          @Param("id")int id);
        //查看简历
        UserResumeEntity oneUserR(@Param("uid") int uid);
        //个人申领查询
        List<UserGrslEntity> grslAll(@Param("userId")String userId);
        UserEntity userGetId(@Param("id")int id);
        int prizeInsert(@Param("reportContent")String reportContent,
                        @Param("reportPicture")String reportPicture,
                        @Param("reportTel")String reportTel,
                        @Param("reportWx")String reportWx,
                        @Param("reportTime")String reportTime

        );
}
