package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.YsZjkEntity;
import com.rmtzx.entity.appentity.YsZjkNameEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YsZjkService {
    //类别返回
    List<YsZjkEntity> zjAll();
    //专家分页展示
    ShowInfo zjNameAll(@Param("cid")int cid,
                       @Param("pageNo")int pageNo,
                       @Param("pageSize")int pageSize);
    //专家详情展示
    YsZjkNameEntity zjone(@Param("id") int id);
    //专家查询展示
    List<YsZjkNameEntity> zjAll1(@Param("expertName")String expertName);
}
