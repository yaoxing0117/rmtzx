package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.TrainingEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;



public interface TrainingService {
    ShowInfo allTraining(@Param("pageNo")int pageNo,
                         @Param("pageSize")int pageSize);
    TrainingEntity oneTraining(@Param("id")int id);

}
