package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.ysEntity.YstdEntity;
import com.rmtzx.entity.appentity.ysEntity.YszwEntity;
import com.rmtzx.entity.pojo.ShowInfo;

import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface YwzpService {
    //信息公开展示
    List<YstdEntity> xxallSelect();
    //信息公开展示
    ShowInfo xxallSelect(@Param("cid")int cid,
                         @Param("pageNo")int pageNo,
                         @Param("pageSize")int pageSize);
    //信息详情展示()
    YstdEntity xxoneSelect(@Param("cid")int cid);
    //职位列表展示
    ShowInfo zwallSelect(@Param("cid")int cid,
                         @Param("pageNo")int pageNo,
                         @Param("pageSize")int pageSize);
    //职位详情展示
    YszwEntity zwoneSelect(@Param("cid")int cid);
    //职位投递
    int zwInsert(@Param("gongsiId")int gongsiId,
                 @Param("userId")int userId,
                 @Param("zhiweiId")int zhiweiId);
}
