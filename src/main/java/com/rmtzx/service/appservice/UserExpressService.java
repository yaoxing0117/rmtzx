package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.userEntity.UserExpressEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserExpressService {
    int userRecord(@Param("userUser")String userUser,//用户
                   @Param("sender")String sender,//寄件人
                   @Param("mailingAddress")String mailingAddress,//寄件地址
                   @Param("addressee")String addressee,//收件人
                   @Param("receivingAddress")String receivingAddress,//收件地址
                   @Param("waybill")String waybill,//运单号
                   @Param("userOrder")String userOrder,//订单号
                   @Param("courierServicesCompany")String courierServicesCompany,
                   @Param("uniqueIdentifier")String uniqueIdentifier);//快递公司
    ShowInfo allExpress(@Param("userTel")String userTel,
                        @Param("pageNo")int pageNo,
                        @Param("pageSize")int pageSize );

}
