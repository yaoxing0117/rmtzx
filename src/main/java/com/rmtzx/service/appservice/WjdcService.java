package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.WjdcDAEntity;
import com.rmtzx.entity.appentity.WjdcNameEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WjdcService {
    //列表返回问卷
    ShowInfo wjdcAll(@Param("category")String category,
                    @Param("pageNo")int pageNo,
                    @Param("pageSize")int pageSize);
    //详情问卷
    List<WjdcDAEntity> wjdcOne(@Param("wjId")String wjId);
    //开始答题
    int upWjdc(@Param("id")int id);
    //
    int seWjdc(@Param("id")int id);
}
