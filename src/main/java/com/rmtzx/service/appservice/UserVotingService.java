package com.rmtzx.service.appservice;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

public interface UserVotingService {
    //查询所有投票展示（据投票类别）
    ShowInfo all(@Param("votingCategory")String votingCategory,
                 @Param("pageNo")int pageNo,
                 @Param("pageSize")int pageSize);
    //查询投票内容详情展示
    ShowInfo oneVoting(@Param("optionId")String optionId,
                       @Param("pageNo")int pageNo,
                       @Param("pageSize")int pageSize);
    //用户投票
    Result updateVotes(@Param("userName")String userName,
                       @Param("userId")int userId,
                       @Param("cid")int id);
    //查询当前投票数
    int selectVotes();
}
