package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.AudioEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.pojo.WheelPlantingEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WheelPlantingService {
    //轮播图片展示
    List<WheelPlantingEntity> allWheel(@Param("category")String category);
    //轮播新闻详情
    WheelPlantingEntity oneWheel(@Param("id")int id,@Param("category")String category);
    //增加点赞次数
    int dzUpdate(@Param("id")int id,@Param("category")String category);
   ShowInfo allMP3(@Param("category")String category,
                   @Param("pageNo")int pageNo,
                   @Param("pageSize")int pageSize);
    AudioEntity oneMP3(@Param("id")int id);
}
