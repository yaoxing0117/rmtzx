package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.ywEntity.UserDZEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 点赞业务逻辑
 */
public interface UserDZService {
    int selectAll(@Param("userId")int userId,
                           @Param("newsId")int newsId,
                     String newsCategory);
    int insertDZ(@Param("newsTitle")String newsTitle,
                 @Param("newsTime")String newsTime,
                 @Param("newsCategory")String newsCategory,
                 @Param("userId")int userId,
                 @Param("newsId")int newsId);
    ShowInfo selectAll(@Param("userId") int userId,
                       @Param("pageNo") int pageNo,
                       @Param("pageSize") int pageSize);
    int insDZ(String newsCategory,
              int newsId, int userId);
}
