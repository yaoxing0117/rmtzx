package com.rmtzx.service.appservice;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.ywEntity.ZxwEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ZxwService {
    List<ZxwEntity> zxwAll();
    //审核
    String updateState(@Param("ids") int ids,@Param("code") int code);
    //列表固定字段ios/安卓
    ShowInfo zxwScreen(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize);
    //分页查询
    ShowInfo zxwPaging(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize );
    //id查询数据
    Result singleDate(@Param("id")int id);
    //删除数据
    int deleteId(@Param("id")int id);
    //点赞
    int thumbsUp(@Param("up") int up,@Param("id")int id);
    //评论
    int commentInsert(@Param("commentator") String commentator,
                      @Param("commentContent")String commentContent,
                      @Param("commentaryTime")String commentaryTime,
                      @Param("userTx")String userTx,
                      @Param("commentID")int commentID,
                      @Param("userId")int userId,
                      @Param("commentCategory")String commentCategory);
    //评论分页展示
    List<CommentEntity> commentPaging( @Param("commentCategory")String commentCategory,
                                       @Param("commentID")int commentID,
                                       @Param("pageNo")int pageNo,
                                       @Param("pageSize")int pageSize );
}
