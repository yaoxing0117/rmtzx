package com.rmtzx.service.appservice.htService.Impl;


import com.rmtzx.dao.appdao.htDao.UserAdminMapper;
import com.rmtzx.entity.appentity.htEntity.UserAdmin;
import com.rmtzx.service.appservice.htService.UserAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserAdminServiceImpl implements UserAdminService {

    @Autowired
    private UserAdminMapper userAdminMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return 0;
    }

    @Override
    public int insert(UserAdmin record) {
        return 0;
    }

    @Override
    public int insertSelective(UserAdmin record) {
        return 0;
    }

    @Override
    public UserAdmin selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(UserAdmin record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(UserAdmin record) {
        return 0;
    }
}
