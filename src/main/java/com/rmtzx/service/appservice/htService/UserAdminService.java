package com.rmtzx.service.appservice.htService;

import com.rmtzx.entity.appentity.htEntity.UserAdmin;

public interface UserAdminService {
    int deleteByPrimaryKey(Integer id);

    int insert(UserAdmin record);

    int insertSelective(UserAdmin record);

    UserAdmin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserAdmin record);

    int updateByPrimaryKey(UserAdmin record);
}