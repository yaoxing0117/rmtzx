package com.rmtzx.service.appservice.htService;

import com.rmtzx.entity.appentity.htEntity.UserPower;

public interface UserPowerService {
    int deleteByPrimaryKey(Integer id);

    int insert(UserPower record);

    int insertSelective(UserPower record);

    UserPower selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserPower record);

    int updateByPrimaryKey(UserPower record);
}