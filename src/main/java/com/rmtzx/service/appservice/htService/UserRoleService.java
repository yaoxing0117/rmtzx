package com.rmtzx.service.appservice.htService;

import com.rmtzx.entity.appentity.htEntity.UserRole;

public interface UserRoleService {
    int deleteByPrimaryKey(Integer rid);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer rid);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
}