package com.rmtzx.service.appservice.htService.Impl;

import com.rmtzx.dao.appdao.htDao.UserPowerMapper;
import com.rmtzx.entity.appentity.htEntity.UserPower;
import com.rmtzx.service.appservice.htService.UserPowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserPowerServiceImpl implements UserPowerService {

    @Autowired
    private UserPowerMapper userPowerMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        int i = userPowerMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(UserPower record) {
        return 0;
    }

    @Override
    public int insertSelective(UserPower record) {
        return 0;
    }

    @Override
    public UserPower selectByPrimaryKey(Integer id) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(UserPower record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(UserPower record) {
        return 0;
    }
}
