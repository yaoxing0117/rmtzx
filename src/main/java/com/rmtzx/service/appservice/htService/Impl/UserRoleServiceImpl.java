package com.rmtzx.service.appservice.htService.Impl;

import com.rmtzx.entity.appentity.htEntity.UserRole;
import com.rmtzx.service.appservice.htService.UserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl  implements UserRoleService {
    @Override
    public int deleteByPrimaryKey(Integer rid) {
        return 0;
    }

    @Override
    public int insert(UserRole record) {
        return 0;
    }

    @Override
    public int insertSelective(UserRole record) {
        return 0;
    }

    @Override
    public UserRole selectByPrimaryKey(Integer rid) {
        return null;
    }

    @Override
    public int updateByPrimaryKeySelective(UserRole record) {
        return 0;
    }

    @Override
    public int updateByPrimaryKey(UserRole record) {
        return 0;
    }
}
