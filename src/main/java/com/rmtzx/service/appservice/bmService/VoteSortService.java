package com.rmtzx.service.appservice.bmService;


import com.rmtzx.entity.appentity.bmEntity.VoteSort;

public interface VoteSortService {
    /**
     * 根据投票id删除投票分类
     * @param id
     * @return
     */
    String deleteByPrimaryKey(Integer id);

    /**
     * 插入新的record投票分类
     * @param record
     * @return
     */
    String insert(VoteSort record);

    /**
     * 添加新的投票分类record，做非空判断
     * @param record
     * @return
     */
    String insertSelective(VoteSort record);

    /**
     * 根据分类id查询
     * @param id
     * @return
     */
    VoteSort selectByPrimaryKey(Integer id);

    /**
     * 根据分类id修改投票分类信息，做非空判断
     * @param record
     * @return
     */
    String updateByPrimaryKeySelective(VoteSort record);

    /**
     * 根据分类id修改投票分类信息，不做分控判断
     * @param record
     * @return
     */

    String updateByPrimaryKey(VoteSort record);


}