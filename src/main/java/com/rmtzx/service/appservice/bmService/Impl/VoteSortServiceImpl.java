package com.rmtzx.service.appservice.bmService.Impl;

import com.rmtzx.dao.appdao.bmDao.VoteSortMapper;
import com.rmtzx.entity.appentity.bmEntity.VoteSort;
import com.rmtzx.service.appservice.bmService.VoteSortService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VoteSortServiceImpl implements VoteSortService {

    @Autowired
    private VoteSortMapper voteSortMapper;

    @Override
    public String deleteByPrimaryKey(Integer id) {
        int i = voteSortMapper.deleteByPrimaryKey(id);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public String insert(VoteSort record) {
        int i = voteSortMapper.insert(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public String insertSelective(VoteSort record) {
        int i = voteSortMapper.insertSelective(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public VoteSort selectByPrimaryKey(Integer id) {
        VoteSort voteOption = voteSortMapper.selectByPrimaryKey(id);
        return voteOption;
    }

    @Override
    public String updateByPrimaryKeySelective(VoteSort record) {
        int i = voteSortMapper.updateByPrimaryKeySelective(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public String updateByPrimaryKey(VoteSort record) {
        int i = voteSortMapper.updateByPrimaryKey(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }
}
