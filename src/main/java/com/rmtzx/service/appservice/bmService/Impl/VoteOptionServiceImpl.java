package com.rmtzx.service.appservice.bmService.Impl;

import com.rmtzx.dao.appdao.bmDao.VoteOptionMapper;
import com.rmtzx.entity.appentity.bmEntity.VoteOption;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.appservice.bmService.VoteOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VoteOptionServiceImpl implements VoteOptionService {

    @Autowired
    VoteOptionMapper voteOptionMapper;

    @Override
    public String deleteByPrimaryKey(Integer id) {
        int i = voteOptionMapper.deleteByPrimaryKey(id);
        if (i==0){
            return "失败";
        }
            return "成功";
    }

    @Override
    public String insert(VoteOption record) {
        int i = voteOptionMapper.insert(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public String insertSelective(VoteOption record) {
        int i = voteOptionMapper.insertSelective(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public Result selectByPrimaryKey(String title) {
        List<VoteOption> voteOption = voteOptionMapper.selectByPrimaryKey(title);
        if(voteOption==null){
            return Result.failure(ResultCode.NOT_FIND);
        }
            return Result.success(voteOption);
    }

    @Override
    public String updateByPrimaryKeySelective(VoteOption record) {
        int i = voteOptionMapper.updateByPrimaryKeySelective(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public String updateByPrimaryKey(VoteOption record) {
        int i = voteOptionMapper.updateByPrimaryKey(record);
        if (i==0){
            return "失败";
        }
        return "成功";
    }

    @Override
    public Result findBySortId(int sortId) {
        List<VoteOption> voteOptions = voteOptionMapper.findBySortId(sortId);
        if (voteOptions==null){
           return   Result.failure(ResultCode.NOT_FIND);
        }
           return Result.success(voteOptions);
    }
}
