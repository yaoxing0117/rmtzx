package com.rmtzx.service.appservice.bmService;

import com.rmtzx.entity.appentity.bmEntity.VoteOption;
import com.rmtzx.entity.bo.Result;


public interface VoteOptionService {

    /**
     * 根据投票选项id删除投票选项信息
     * @param id
     * @return
     */
    String deleteByPrimaryKey(Integer id);

    /**
     * 插入新的投票选项
     * @param record
     * @return
     */
    String insert(VoteOption record);

    /**
     * 插入新的投票选项并做非空判断
     * @param record
     * @return
     */
    String insertSelective(VoteOption record);

    /**
     * 根据投票选项的id查询投票选项的内容
     * @param title
     * @return
     */
    Result selectByPrimaryKey(String title);

    /**
     * 根据投票选项的id修改投票选项的内容，并做非空判断
     * @param record
     * @return
     */
    String updateByPrimaryKeySelective(VoteOption record);

    /**
     * 根据投票选项的id修改投票选项的内容，不做非空判断
     * @param record
     * @return
     */
    String updateByPrimaryKey(VoteOption record);

    /**
     * 根据分类的sortId查询所有该分类下的所有投票信息
     * @param sortId
     * @return
     */
    Result findBySortId(int sortId);
}