package com.rmtzx.service.appservice;

import com.rmtzx.entity.appentity.YszsEntity;
import com.rmtzx.entity.appentity.ysEntity.WXGZHEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

public interface YszsService {
    //展示数据列表
    ShowInfo allYszs(@Param("category")String category,
                     @Param("pageNo")int pageNo,
                     @Param("pageSize")int pageSize);
    //详情数据
    YszsEntity oneYszs(@Param("id")int id);

    WXGZHEntity oneWX(@Param("id")int id);

    ShowInfo allWX(@Param("differentiate")String differentiate,
                            @Param("pageNo")int pageNo,
                            @Param("pageSize")int pageSize);
}
