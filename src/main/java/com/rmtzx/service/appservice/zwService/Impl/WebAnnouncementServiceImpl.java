package com.rmtzx.service.appservice.zwService.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.rmtzx.entity.appentity.zwEntity.GovernmentAffairsFb;
import com.rmtzx.entity.appentity.zwEntity.WebAnnouncement;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.mapper.WebAnnouncementMapper;
import com.rmtzx.service.appservice.zwService.IWebAnnouncementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Service
public class WebAnnouncementServiceImpl extends ServiceImpl<WebAnnouncementMapper, WebAnnouncement> implements IWebAnnouncementService {

    @Autowired
    private WebAnnouncementMapper webAnnouncementMapper;

    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2) {
        QueryWrapper<WebAnnouncement> q = new QueryWrapper<WebAnnouncement>();
        IPage<WebAnnouncement> page = new Page<>(1,15);
        q.eq("code",1).or().eq("category","通知公告").or().eq("category","信息公示").or().eq("category","人事任免").or().eq("category","重大决策").orderByDesc("id");
        page = this.page(page,q);
        return new ShowInfo((int)page.getPages(),(int)page.getCurrent(),(int)page.getTotal(),page.getRecords());
    }

    @Override
    public Result findByCategoryAndId(String cateGory, int id) {
        WebAnnouncement governmentAffairsFb = webAnnouncementMapper.selectById(id);
        if (governmentAffairsFb==null){
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(governmentAffairsFb);
    }
}
