package com.rmtzx.service.appservice.zwService;

import com.baomidou.mybatisplus.extension.service.IService;
import com.rmtzx.entity.appentity.zwEntity.GovernmentAffairsFb;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
public interface IGovernmentAffairsFbService extends IService<GovernmentAffairsFb> {

    /**
     * 根据当前页数arg1和每页展示数arg2获取  总页数 总记录数 当前页数 分页展示内容
     * @param arg1
     * @param arg2
     * @return
     */
    ShowInfo limitShowInfo(int arg0, int arg1, int arg2);

    /**
     * 根据政务分类和详情id查询政务详情
     * @param cateGory
     * @param id
     * @return
     */
    Result findByCategoryAndId(String cateGory, int id);
}
