package com.rmtzx.service.appservice.zwService.Impl;

import com.rmtzx.dao.appdao.zwDao.ProposeMapper;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.Propose;
import com.rmtzx.service.appservice.zwService.ProposeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProposeServiceImpl implements ProposeService {

    @Autowired
    private ProposeMapper proposeMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        int i = proposeMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(Propose record) {
        int insert = proposeMapper.insert(record);
        return insert;
    }
    @Override
    public int insertSelective(Propose record) {
        int i = proposeMapper.insertSelective(record);
        return i;
    }

    @Override
    public Propose selectByPrimaryKey(Integer id) {
        Propose partyWork = proposeMapper.selectByPrimaryKey(id);
        return partyWork;
    }

    @Override
    public int updateByPrimaryKeySelective(Propose record) {
        int i = proposeMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public String updateByPrimaryKey(int arg1,String  arg0) {
        int j = proposeMapper.updateByPrimaryKey(arg1, arg0);
        if (j == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }

    @Override
    public String updateByCode(int arg1, int[] arg0) {
        int i = proposeMapper.updateByCode(arg1, arg0);
        if (i == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }


    @Override
    public List<Propose> findAll(int arg0, int arg1, int arg2,int arg3) {
        List<Propose> all = proposeMapper.findAll(arg0, (arg1-1)*arg2, arg2,arg3);
        return all;
    }


    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = proposeMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Propose> all = findAll(arg0, arg1, arg2,arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }


    @Override
    public ShowInfo findMyJy(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = proposeMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Propose> all = findAll(arg0, arg1, arg2,arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }
}
