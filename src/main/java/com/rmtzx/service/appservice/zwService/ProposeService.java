package com.rmtzx.service.appservice.zwService;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.Propose;

import java.util.List;

public interface ProposeService {

    /**
     * 根据主键id删除公告信息
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加公告信息不做非空
     * @param record
     * @return
     */
    int insert(Propose record);

    /**
     * 添加公告信息做非空判断
     * @param record
     * @return
     */
    int insertSelective(Propose record);

    /**
     * 根据主键id查询公告信息
     * @param id
     * @return
     */
    Propose selectByPrimaryKey(Integer id);

    /**
     * 根据主键id修改公告信息做非空判断
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(Propose record);

    /**
     * 根据主键id修改公告信息不做非空判断
     * @param record
     * @return
     */
    String updateByPrimaryKey(int arg1, String arg0);
    /**
     * 根据code值和传递数组id值批量修改政策信息
     * @param code
     * @param ids
     * @return
     */
    String updateByCode(int arg1, int[] arg0);

    /**
     * 根据每页m条查询当前N页的所有code为1审核通过的信息
     * @param code
     * @param m
     * @param n
     * @return
     */
    List<Propose> findAll(int arg0, int arg1, int arg2, int arg3);

    /**
     * 根据当前页数arg1和每页展示数arg2获取  总页数 总记录数 当前页数 分页展示内容
     * @param arg1
     * @param arg2
     * @return
     */
    ShowInfo limitShowInfo(int arg0, int arg1, int arg2, int arg3);

    /**
     * 根据当前页数arg1和每页展示数arg2用户编号arg0以及处理状态arg3获取  总页数 总记录数 当前页数 分页展示我的建议内容
     * @param arg0
     * @param arg1
     * @param arg2
     * @param arg3
     * @return
     */
    ShowInfo findMyJy(int arg0, int arg1, int arg2, int arg3);
}
