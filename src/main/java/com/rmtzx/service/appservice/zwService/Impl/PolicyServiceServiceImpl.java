package com.rmtzx.service.appservice.zwService.Impl;

import com.alibaba.fastjson.JSONArray;
import com.rmtzx.dao.appdao.zwDao.PolicyMapper;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.Policy;
import com.rmtzx.service.appservice.zwService.PolicyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 政务业务
 */
@Service
public class PolicyServiceServiceImpl implements PolicyService {

    @Autowired
    private PolicyMapper policyMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        int i = policyMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(Policy record) {
        int i = policyMapper.insert(record);
        return i;
    }

    @Override
    public int insertSelective(Policy record) {
        int i = policyMapper.insertSelective(record);
        return i;
    }

    @Override
    public Policy selectByPrimaryKey(Integer id) {
        Policy policy = policyMapper.selectByPrimaryKey(id);
        return policy;
    }

    @Override
    public int updateByPrimaryKeySelective(Policy record) {
        int i = policyMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public String updateByPrimaryKey(int arg1,Integer[] arg0) {
        int k =0;
        for (int i=0;i<arg0.length;i++) {
            int j = policyMapper.updateByPrimaryKey(arg1, arg0[i]);
            System.out.println(j);
            if (j==0){
                k=0;
            }else {
                k=1;
            }
        }
        if (k == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }

    @Override
    public String updateByCode(int arg1, int[] arg0) {
        JSONArray array = null;
        int i = policyMapper.updateByCode(arg1, arg0);

        if (i == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }

    @Override
    public List<Policy> findAll(int arg0, int arg1, int arg2) {
        List<Policy> all = policyMapper.findAll(arg0, (arg1-1)*arg2, arg2);
        return all;
    }

    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2) {
        //获取总记录数
        int totalCount = policyMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Policy> all = findAll(arg0, arg1, arg2);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }

    @Override
    public Result findByCategoryAndId(String cateGory, int id) {
        //根据政务分类和政务详情id获取改id下的政务详情
        Policy byCategoryAndId = policyMapper.findByCategoryAndId(cateGory, id);
        //判断政务详情对象是否为空
        if (byCategoryAndId==null){
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(byCategoryAndId);
    }


}
