package com.rmtzx.service.appservice.zwService.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rmtzx.entity.appentity.zwEntity.GovernmentAffairsFb;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.mapper.GovernmentAffairsFbMapper;
import com.rmtzx.service.appservice.zwService.IGovernmentAffairsFbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Service
public class GovernmentAffairsFbServiceImpl extends ServiceImpl<GovernmentAffairsFbMapper, GovernmentAffairsFb> implements IGovernmentAffairsFbService {

    @Autowired
    private GovernmentAffairsFbMapper governmentAffairsFbMapper;

    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2) {
        QueryWrapper<GovernmentAffairsFb> q = new QueryWrapper<GovernmentAffairsFb>();
        IPage<GovernmentAffairsFb> page = new Page<>(arg1,arg2);
        q.eq("code",arg0);
        page = this.page(page,q);
        return new ShowInfo((int)page.getPages(),(int)page.getCurrent(),(int)page.getTotal(),page.getRecords());
    }

    @Override
    public Result findByCategoryAndId(String cateGory, int id) {
        GovernmentAffairsFb governmentAffairsFb = governmentAffairsFbMapper.selectById(id);
        if (governmentAffairsFb==null){
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(governmentAffairsFb);
    }
}
