package com.rmtzx.service.appservice.zwService.Impl;

import com.rmtzx.dao.appdao.zwDao.ComplaintsMapper;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.Complaints;
import com.rmtzx.service.appservice.zwService.ComplaintsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ComplaintsServiceImpl implements ComplaintsService {
    @Autowired
    private ComplaintsMapper complaintsMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        int i = complaintsMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(Complaints record) {
        int insert = complaintsMapper.insert(record);
        return insert;
    }
    @Override
    public int insertSelective(Complaints record) {
        int i = complaintsMapper.insertSelective(record);
        return i;
    }

    @Override
    public Complaints selectByPrimaryKey(Integer id) {
        Complaints partyWork = complaintsMapper.selectByPrimaryKey(id);
        return partyWork;
    }

    @Override
    public int updateByPrimaryKeySelective(Complaints record) {
        int i = complaintsMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public String updateByPrimaryKey(int arg1,String  arg0) {

        int j = complaintsMapper.updateByPrimaryKey(arg1, arg0);

        if (j == 0) {
            //失败
            return "失败";
        }
            //成功
            return "成功";
    }

    @Override
    public String updateByCode(int arg1, int[] arg0) {
        int i = complaintsMapper.updateByCode(arg1, arg0);
        if (i == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }

    @Override
    public List<Complaints> findAll(int arg0, int arg1, int arg2,int arg3) {
        List<Complaints> all = complaintsMapper.findAll(arg0, (arg1-1)*arg2, arg2,arg3);
        return all;
    }
    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = complaintsMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Complaints> all = findAll(arg0, arg1, arg2, arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }

    @Override
    public ShowInfo findMyTs(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = complaintsMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Complaints> all = findAll(arg0, arg1, arg2, arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }

}
