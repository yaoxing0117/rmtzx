package com.rmtzx.service.appservice.zwService.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rmtzx.entity.appentity.zwEntity.PolicyFb;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.mapper.PolicyFbMapper;
import com.rmtzx.service.appservice.zwService.IPolicyFbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Service
public class PolicyFbServiceImpl extends ServiceImpl<PolicyFbMapper, PolicyFb> implements IPolicyFbService {

    @Autowired
    private PolicyFbMapper policyFbMapper;

    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2) {
        QueryWrapper<PolicyFb> q = new QueryWrapper<PolicyFb>();
        IPage<PolicyFb> page = new Page<>(arg1,arg2);
        q.eq("code",arg0);
        page = this.page(page,q);
        return new ShowInfo((int)page.getPages(),(int)page.getCurrent(),(int)page.getTotal(),page.getRecords());

    }

    @Override
    public Result findByCategoryAndId(String cateGory, int id) {
        PolicyFb partyWorkFb = policyFbMapper.selectById(id);
        if (partyWorkFb==null){
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(partyWorkFb);
    }
}
