package com.rmtzx.service.appservice.zwService.Impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.rmtzx.entity.appentity.zwEntity.AnnouncementFb;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.mapper.AnnouncementFbMapper;
import com.rmtzx.service.appservice.zwService.IAnnouncementFbService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author DT.wom6666666666666
 * @since 2020-02-07卧槽  是不是上传的时候吧这个删了。。。。。。。
 */
@Service
public class AnnouncementFbServiceImpl extends ServiceImpl<AnnouncementFbMapper, AnnouncementFb> implements IAnnouncementFbService {

    @Autowired
    private AnnouncementFbMapper announcementFbMapper;

    /**
     * 根据当前页数arg1和每页记录数arg2一级审核状态arg0分页列表展示公告信息
     * @param arg0
     * @param arg1
     * @param arg2
     * @return
     */
    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2) {
        QueryWrapper<AnnouncementFb> q = new QueryWrapper<AnnouncementFb>();
        IPage<AnnouncementFb> page = new Page<>(arg1,arg2);
        q.eq("code",arg0);
        page = this.page(page,q);
        return new ShowInfo((int)page.getPages(),(int)page.getCurrent(),(int)page.getTotal(),page.getRecords());
    }

    /**
     * 根据公告id查询公告详情卧槽  太坑了  我这边都好好的6666实在不行就打包给我
     * 我重新起一个 你稍微等下我在 看下jiabao
     * 包 配置文件kk
     * 估计跟那个没关系 之前是mapper到不进来 这次好像是service的问题
     * @param cateGory
     * @param id
     * @return
     */
    @Override
    public Result findByCategoryAndId(String cateGory, int id) {
        AnnouncementFb announcementFb = announcementFbMapper.selectById(id);
        if (announcementFb==null){
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(announcementFb);
    }
}
