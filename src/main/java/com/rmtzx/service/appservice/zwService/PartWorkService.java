package com.rmtzx.service.appservice.zwService;


import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.PartyWork;

import java.util.List;

/**
 * 党务业务逻辑接口
 */
public interface PartWorkService {
    /**
     * 根据ID删除党务信息
     * @param id
     * @return
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * 添加党务信息
     * @param record
     * @return
     */
    int insert(PartyWork record);

    /**
     * 添加党务信息，会对传进来的值进行非空判断
     * @param record
     * @return
     */
    int insertSelective(PartyWork record);

    /**
     * 根据id主键查询党务信息
     * @param id
     * @return
     */
    PartyWork selectByPrimaryKey(Integer id);

    /**
     * 根据主键id修改党务信息（会做非空判断）
     * @param record
     * @return
     */
    int updateByPrimaryKeySelective(PartyWork record);

    /**
     * 修改信息不做非空判断
     * @param record
     * @return
     */
    String updateByPrimaryKey(int arg1, Integer[] arg0);
    /**
     * 根据code值和传递数组id值批量修改政策信息
     * @param code
     * @param ids
     * @return
     */
    String  updateByCode(int arg1, Integer[] arg0);

    /**
     * 根据每页(m-1)*n条查询当前N页的所有code为1审核通过的信息
     * @param code
     * @param m
     * @param n
     * @return
     */
    List<PartyWork> findAll(int arg0, int arg1, int arg2);

    /**
     * 根据当前页数arg1和每页展示数arg2获取  总页数 总记录数 当前页数 分页展示内容
     * @param arg1
     * @param arg2
     * @return
     */
    ShowInfo limitShowInfo(int arg0, int arg1, int arg2);

    /**
     * 根据政务分类和详情id查询政务详情
     * @param cateGory
     * @param id
     * @return
     */
    Result findByCategoryAndId(String cateGory, int id);
}
