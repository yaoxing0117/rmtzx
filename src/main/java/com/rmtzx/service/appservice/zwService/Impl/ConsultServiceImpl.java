package com.rmtzx.service.appservice.zwService.Impl;

import com.rmtzx.dao.appdao.zwDao.ConsultMapper;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.zwEntity.Consult;
import com.rmtzx.service.appservice.zwService.ConsultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultServiceImpl implements ConsultService {
    @Autowired
    private ConsultMapper consultMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        int i = consultMapper.deleteByPrimaryKey(id);
        return i;
    }

    @Override
    public int insert(Consult record) {
        int insert = consultMapper.insert(record);
        return insert;
    }
    @Override
    public int insertSelective(Consult record) {
        int i = consultMapper.insertSelective(record);
        return i;
    }

    @Override
    public Consult selectByPrimaryKey(Integer id) {
        Consult partyWork = consultMapper.selectByPrimaryKey(id);
        return partyWork;
    }

    @Override
    public int updateByPrimaryKeySelective(Consult record) {
        int i = consultMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public String updateByPrimaryKey(int arg0,String  arg1) {
        int j = consultMapper.updateByPrimaryKey(arg0, arg1);
        if (j == 0) {
            //失败
            System.out.println(j+"失败");
            return "失败";
        }
        //成功
        System.out.println(j+"成功");
        return "成功";
    }

    @Override
    public String updateByCode(int arg1, int[] arg0) {
        int i = consultMapper.updateByCode(arg1, arg0);
        if (i == 0) {
            //失败
            return "失败";
        }
        //成功
        return "成功";
    }

    @Override
    public List<Consult> findAll(int arg0, int arg1, int arg2 ,int arg3) {
        List<Consult> all = consultMapper.findAll(arg0, (arg1-1)*arg2, arg2,arg3);
        return all;
    }
    @Override
    public ShowInfo limitShowInfo(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = consultMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Consult> all = findAll(arg0, arg1, arg2,arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }

    @Override
    public ShowInfo findMyZx(int arg0, int arg1, int arg2, int arg3) {
        //获取总记录数
        int totalCount = consultMapper.findTotalCountAndPage();
        //获取总页数
        int totalPage = totalCount/arg2;
        //获取当前页的所有内容
        List<Consult> all = findAll(arg0, arg1, arg2,arg3);
        //将内容插入实体类
        ShowInfo showInfo=new ShowInfo(totalPage,arg1,totalCount,  all);
        return showInfo;
    }
}
