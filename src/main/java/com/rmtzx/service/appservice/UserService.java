package com.rmtzx.service.appservice;


import com.rmtzx.entity.appentity.userEntity.UserEntity;
import com.rmtzx.entity.bo.Result;
import org.apache.ibatis.annotations.Param;

import java.io.IOException;


public interface UserService {
    //查询用户头像是否存在
    UserEntity queryportra(@Param("userNameTel")String userNameTel);
    //     登录
    Result findLoadByUser(String userNameTel, String passWord);
    //     验证码发送接口
    Result SMSlogin(String userNameTel) throws IOException;
    //     注册
    Result addUser(String userName,String passWord,String Sms);
    //    修改
    Result editUser(String userNameTel, String passWord, String Sms);
    // 修改个人信息
    int updatePersona(@Param("userNameTel")String collectionUser,
                      @Param("userNickName")String userNickName,
                      @Param("userAge")String userAge,
                      @Param("userSex")String userSex,
                      @Param("userBirthday")String userBirthday,
                      @Param("userCity")String userCity,
                      @Param("userPortrait")String userPortrait,
                      @Param("userTel")String userTel,
                      @Param("userName")String userName
    );
    //查询昵称是否存在
    UserEntity queryNickName(@Param("userNickName")String userNickName);
    //返回更改数据
    UserEntity userDate(@Param("userNameTel")String userNameTel);
    //退出登录
    Result signOut(String userNameTel);
    //自动登录
    Result automaticLogon(String userNameTel,String token);

}
