package com.rmtzx.service.appservice;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.ywEntity.NewsPaperEntity;
import org.apache.ibatis.annotations.Param;

public interface NewsPaperSerivce {
    //报纸数据展示
    ShowInfo paperAll(@Param("code")String source,
                      @Param("pageNo")int pageNo,
                      @Param("pageSize")int pageSize);
    //id展示详情数据
    NewsPaperEntity paperID(int id);
}
