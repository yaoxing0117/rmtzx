package com.rmtzx.service.appservice;

import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

public interface GlobalSearchService {
    ShowInfo allGlobalSearch(@Param("article")String article,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);
}
