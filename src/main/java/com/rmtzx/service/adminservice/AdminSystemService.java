package com.rmtzx.service.adminservice;

import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;


public interface AdminSystemService {
    ShowInfo selectQuery(@Param("reportExamine") int reportExamine,
                         @Param("content") String content,
                         @Param("pageNo") int pageNo,
                         @Param("pageSize") int pageSize);

    //分页列表

    ShowInfo selectAll(@Param("reportExamine") int reportExamine,
                       @Param("pageNo") int pageNo,
                       @Param("pageSize") int pageSize);

    //修改审核状态
    int updateOne(@Param("reportExamine") int reportExamine,
                  @Param("id") int id);

}
