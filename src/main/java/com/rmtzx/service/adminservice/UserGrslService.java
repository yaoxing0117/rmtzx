package com.rmtzx.service.adminservice;

import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserGrslService {
    int insertGrsl(@Param("title")String title,
                   @Param("content")String content,
                   @Param("slRemarks")String slRemarks,
                   @Param("cjTime")String cjTime);

    ShowInfo selectAll(@Param("pageNo") int pageNo,
                       @Param("pageSize") int pageSize);
    int updateCode(@Param("id")int id,
                   @Param("code")int code);
    ShowInfo selectTitle(@Param("title")String title,
                                     @Param("pageNo") int pageNo,
                                     @Param("pageSize") int pageSize);
    int updateNR(@Param("id")int id,
                 @Param("title") String title,
                 @Param("content") String content,
                 @Param("slRemarks") String slRemarks);
    int deleteOne(@Param("id")int id,@Param("type")int type);
    ShowInfo selectSL(@Param("pid") int pid,
                                  @Param("pageNo") int pageNo,
                                  @Param("pageSize") int pageSize);
    int insertSl(@Param("userUser") String userUser,
                 @Param("userId") String userId,
                 @Param("slRemarks") String slRemarks,
                 @Param("pid") int pid,
                 @Param("lqTime") String lqTime);
    int updateSLR(@Param("code")int code,
                  @Param("lqTime") String lqTime,
                  @Param("id")int id);
    ShowInfo selectCodeSl(@Param("pid")int pid,
                                      @Param("code")int code,
                                      @Param("userUser")String userUser,@Param("pageNo") int pageNo,
                          @Param("pageSize") int pageSize);
     ShowInfo selectCodeSlNoUser(int pid, int code,@Param("pageNo") int pageNo,
                                 @Param("pageSize") int pageSize);
}
