package com.rmtzx.service.adminservice.impl;

import com.rmtzx.dao.admindao.VotingAdminMapper;
import com.rmtzx.entity.appentity.userEntity.UserVotingEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.adminservice.VotingAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class VotingAdminServiceImpl implements VotingAdminService {
    @Autowired
    private VotingAdminMapper admin;
    @Override
    public ShowInfo selectVotingAll(int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        showInfo.setTotalCount(admin.paperPageCount());
        showInfo.setLimitShowList(admin.selectVotingAll((pageNo-1)*pageSize,pageSize));
        return showInfo;
    }

    @Override
    public int insertOne(String title, String picture, String content, String startDate, String stopDate,  int votingCategory, String tpRemarks, int codePicture) {
        return admin.insertOne(title, picture, content, startDate, stopDate,  votingCategory, tpRemarks, codePicture);
    }

    @Override
    public int updateVoting(String title, String picture, String content, String stopDate, int votingCategory, String tpRemarks, int id) {
        return admin.updateVoting(title, picture, content, stopDate, votingCategory, tpRemarks, id);
    }

    @Override
    public int deleteId(int id) {
       int i= admin.deleteId(id);
       if (i==1){
           return admin.deletePid(id);
       }
        return 0;
    }

    @Override
    public int deletePid(int id) {
        return admin.deleteId(id);
    }

    @Override
    public ShowInfo selectMH(String title, int votingCategory, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        if (votingCategory==0){//全部查询
            List<UserVotingEntity> list= admin.selectAllMH(title, (pageNo-1)*pageSize,pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }else {
            List<UserVotingEntity> list= admin.selectMH(title, votingCategory,(pageNo-1)*pageSize,pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }

    }

    @Override
    public ShowInfo selectMHw( int votingCategory, int pageNo, int pageSize) {
        ShowInfo showInfo=new ShowInfo();
        if (votingCategory==0){
            List<UserVotingEntity> list= admin.selectAllMHw((pageNo-1)*pageSize,pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }else {
            List<UserVotingEntity> list= admin.selectMHw( votingCategory,(pageNo-1)*pageSize,pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }
    }

    @Override
    public int updateCode(int code, int id) {
        return admin.updateCode(code, id);
    }


}
