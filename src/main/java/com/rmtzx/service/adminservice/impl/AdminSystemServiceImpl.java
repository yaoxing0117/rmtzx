package com.rmtzx.service.adminservice.impl;

import com.rmtzx.dao.admindao.AdminSystemMapper;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.adminservice.AdminSystemService;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminSystemServiceImpl implements AdminSystemService {
    @Autowired
    private AdminSystemMapper adminSystemMapper;

    /**
     * 模糊查询
     *
     * @param reportExamine 审核状态
     * @param content       内容
     * @param pageNo        分页
     * @param pageSize      分页
     * @return 数据集合
     */
    @Override
    public ShowInfo selectQuery(int reportExamine, String content, int pageNo, int pageSize) {
        if (reportExamine == 3 && content == null) {
            ShowInfo showInfo = new ShowInfo();
            showInfo.setTotalCount(adminSystemMapper.paperPageCountAll());
            showInfo.setLimitShowList(adminSystemMapper.selectQueryAll2((pageNo - 1) * pageSize, pageSize));
            return showInfo;
        }
        if (reportExamine == 3) {
            ShowInfo showInfo = new ShowInfo();
            showInfo.setTotalCount(adminSystemMapper.paperPageCountQueryall(content));
            showInfo.setLimitShowList(adminSystemMapper.selectQueryAll(content, (pageNo - 1) * pageSize, pageSize));
            return showInfo;
        }
        ShowInfo showInfo = new ShowInfo();
        showInfo.setTotalCount(adminSystemMapper.paperPageCountQuery(reportExamine, content));
        showInfo.setLimitShowList(adminSystemMapper.selectQuery(reportExamine, content, (pageNo - 1) * pageSize, pageSize));
        return showInfo;
    }

    /**
     * 分页数据
     *
     * @param reportExamine 审核状态
     * @param pageNo        分页
     * @param pageSize      分页
     * @return 返回数据集合
     */
    @Override
    public ShowInfo selectAll(int reportExamine, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        if (reportExamine == 3) {
            showInfo.setTotalCount(adminSystemMapper.paperPageCountAll());
            showInfo.setLimitShowList(adminSystemMapper.selectAll((pageNo - 1) * pageSize, pageSize));
            return showInfo;
        }
        showInfo.setTotalCount(adminSystemMapper.paperPageCount(reportExamine));
        showInfo.setLimitShowList(adminSystemMapper.selectReportExamine(reportExamine, (pageNo - 1) * pageSize, pageSize));
        return showInfo;
    }

    /**
     * 修改数据状态
     *
     * @param reportExamine 状态类型
     * @param id            修改id
     * @return 返回修改状态
     */
    @Override
    public int updateOne(int reportExamine, int id) {
        return adminSystemMapper.updateOne(reportExamine, id);
    }
}
