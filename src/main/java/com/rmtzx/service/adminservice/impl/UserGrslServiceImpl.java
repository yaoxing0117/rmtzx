package com.rmtzx.service.adminservice.impl;

import com.rmtzx.dao.admindao.UserGrslMapper;
import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.adminservice.UserGrslService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGrslServiceImpl implements UserGrslService {
    @Autowired
    private UserGrslMapper userGrslMapper;

    /**
     * 新增申领
     *
     * @param title     标题
     * @param content   内容
     * @param slRemarks 备注
     * @param cjTime    创建时间
     * @return 是否成功
     */
    @Override
    public int insertGrsl(String title, String content, String slRemarks, String cjTime) {
        return userGrslMapper.insertGrsl(title, content, slRemarks, cjTime);
    }

    /**
     * 所有申领分页展示
     *
     * @param pageNo   1
     * @param pageSize 10
     * @return 数据展示
     */
    @Override
    public ShowInfo selectAll(int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        showInfo.setTotalCount(userGrslMapper.paperPageCountAll());
        showInfo.setLimitShowList(userGrslMapper.selectAll((pageNo - 1) * pageSize, pageSize));
        return showInfo;
    }

    /**
     * 修改申领状态
     *
     * @param id   申领id
     * @param code 状态
     * @return 结果状态
     */
    @Override
    public int updateCode(int id, int code) {
        return userGrslMapper.updateCode(id, code);
    }

    /**
     * 模糊查询
     *
     * @param title    内容或标题数据
     * @param pageNo   1
     * @param pageSize 10
     * @return 数据展示
     */
    @Override
    public ShowInfo selectTitle(String title, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        showInfo.setTotalCount(userGrslMapper.paperPageCountTitle(title));
        List<UserGrslEntity> list1 = userGrslMapper.selectTitle(title, (pageNo - 1) * pageSize, pageSize);
        showInfo.setLimitShowList(list1);
        if (list1.isEmpty()) {
            List<UserGrslEntity> list2 = userGrslMapper.selectContent(title, (pageNo - 1) * pageSize, pageSize);
            showInfo.setTotalCount(userGrslMapper.paperPageCountContent(title));
            showInfo.setLimitShowList(list2);
            return showInfo;
        }
        return showInfo;
    }

    /**
     * 修改申领内容
     *
     * @param id        id
     * @param title     标题
     * @param content   内容
     * @param slRemarks 备注
     * @return 修改状态
     */
    @Override
    public int updateNR(int id, String title, String content, String slRemarks) {
        return userGrslMapper.updateNR(id, title, content, slRemarks);
    }

    /**
     * 删除申领
     *
     * @param id
     * @return
     */
    @Override
    public int deleteOne(int id, int type) {
        if (type == 1) {
            int i = userGrslMapper.selectOne(userGrslMapper.selectOne(id).getPid()).getSlNumber();
            int slNumber = i - 1;
            userGrslMapper.updateSL(slNumber, userGrslMapper.selectOne(userGrslMapper.selectOne(id).getPid()).getId());
            return userGrslMapper.deleteOne(id);
        }
        userGrslMapper.deleteOneSLR(id);
        return userGrslMapper.deleteOne(id);
    }

    @Override
    public ShowInfo selectSL(int pid, int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        showInfo.setTotalCount(userGrslMapper.paperPageCountAllSL(pid));
        List<UserGrslEntity> list= userGrslMapper.selectSL(pid, (pageNo - 1) * pageSize, pageSize);
        showInfo.setLimitShowList(list);
        showInfo.setTotalCount(list.size());
        return showInfo;
    }

    @Override
    public int insertSl(String userUser, String userId, String slRemarks, int pid, String lqTime) {
        for (UserGrslEntity e : userGrslMapper.selectALLSl(userId)) {
            if (pid == e.getPid()) {
                return 2;
            }
        }
        UserGrslEntity e= userGrslMapper.selectonme(pid);
        int i = userGrslMapper.insertSl(userUser, userId,e.getTitle(),e.getContent(), slRemarks, pid, lqTime);
        if (i == 1) {
            UserGrslEntity userGrslEntity = userGrslMapper.selectOne(pid);
            return userGrslMapper.updateSL(userGrslEntity.getSlNumber() + 1, pid);
        }
        return 0;
    }


    @Override
    public int updateSLR(int code, String lqTime, int id) {
        return userGrslMapper.updateSLR(code, lqTime, id);
    }

    @Override
    public ShowInfo selectCodeSl(int pid, int code, String userUser,int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        if (code == 0) {
            List<UserGrslEntity> list = userGrslMapper.selectAllSl(pid, userUser, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }
        if (code == 1) {
            List<UserGrslEntity> list = userGrslMapper.selectCodeSl(pid, 1, userUser, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        } else {
            List<UserGrslEntity> list = userGrslMapper.selectCodeSl(pid, 2, userUser, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }
    }

    @Override
    public ShowInfo selectCodeSlNoUser(int pid, int code,int pageNo, int pageSize) {
        ShowInfo showInfo = new ShowInfo();
        if (code == 0) {
            List<UserGrslEntity> list = userGrslMapper.selectAllSlNOUser(pid, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }
        if (code == 1) {
            List<UserGrslEntity> list = userGrslMapper.selectCodeSlNOUser(pid, 1, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        } else {
            List<UserGrslEntity> list = userGrslMapper.selectCodeSlNOUser(pid, 2, (pageNo - 1) * pageSize, pageSize);
            showInfo.setLimitShowList(list);
            showInfo.setTotalCount(list.size());
            return showInfo;
        }
    }

}
