package com.rmtzx.service.adminservice;

import com.rmtzx.entity.appentity.userEntity.UserVotingEntity;
import com.rmtzx.entity.pojo.ShowInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface VotingAdminService {
    ShowInfo selectVotingAll(@Param("pageNo") int pageNo,
                             @Param("pageSize") int pageSize);
    int insertOne(@Param("title") String title,
                  @Param("picture") String picture,
                  @Param("content") String content,
                  @Param("startDate") String startDate,
                  @Param("stopDate") String stopDate,
                  @Param("votingCategory") int votingCategory,
                  @Param("tpRemarks") String tpRemarks,
                  @Param("codePicture") int codePicture);
    int updateVoting(@Param("title") String title,
                     @Param("picture") String picture,
                     @Param("content") String content,
                     @Param("stopDate") String stopDate,
                     @Param("votingCategory") int votingCategory,
                     @Param("tpRemarks") String tpRemarks,
                     @Param("id")int id);
    int deleteId(@Param("id")int id);
    int deletePid(@Param("id")int id);
    ShowInfo selectMH(@Param("title") String title,
                                    @Param("votingCategory")int votingCategory,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    ShowInfo selectMHw(@Param("votingCategory")int votingCategory,
                       @Param("pageNo") int pageNo,
                       @Param("pageSize") int pageSize);
    int updateCode(@Param("code")int code,
                   @Param("id")int id);
}
