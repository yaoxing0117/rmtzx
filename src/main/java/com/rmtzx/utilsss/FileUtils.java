package com.rmtzx.utilsss;
import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;
/**
 *
 * @author gaoxuyang
 *
 */
public class FileUtils {
    @Value("${web.upload-path}")
    private String path;
    /**
     *
     * @param file 文件
     * @param path   文件存放路径
     * @param fileName 原文件名
     * @return
     */
    public static String upload(MultipartFile file, String path, String fileName){

        // 生成新的文件名
//        String realPath = path + "/" + FileNameUtils.getFileName(fileName);

        //使用原文件名
         String realPath = path + fileName;
        File dest = new File(realPath);
        //判断文件父目录是否存在
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdir();
        }
        try {
            //保存文件
            file.transferTo(dest);
            return realPath;
        } catch (IllegalStateException e) {
            e.printStackTrace();
            return realPath;
        } catch (IOException e) {
            e.printStackTrace();
            return realPath;
        }

    }
}
