package com.rmtzx.entity.yqfxentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 词库实体
 */
@Data
public class ThesaurusEntity implements Serializable {
    private int id;
    private String thesauruOne;
    private String addingTime;
    private String addingName;
    private int heat;

}
