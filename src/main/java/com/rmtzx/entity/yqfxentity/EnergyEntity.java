package com.rmtzx.entity.yqfxentity;

import lombok.Data;

/**
 *
 * 能源统计表
 */
@Data
public class EnergyEntity {
    private int id;
    private int mtEnergy;
    private int nyEnergy;
    private int syEnergy;
    private int dlEnergy;
    private int trqEnergy;
    private int jrEnergy;
    private int bzEnergy;
    private int zongEnergy;
    private String category;

}
