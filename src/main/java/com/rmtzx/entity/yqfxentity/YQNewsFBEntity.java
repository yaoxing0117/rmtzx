package com.rmtzx.entity.yqfxentity;

import lombok.Data;

@Data
public class YQNewsFBEntity {
    private Integer id;

    private String article;

    private String articleSource;

    private String author;

    private Integer code;

    private String editor;

    private String newsColumn;

    private String pictureUrl;

    private String releaseTime;

    private String title;

    private Integer newsClick;

    private String localUrl;

    private String absUrl;

    private String category;

    private String differentiate;

    private Integer newsComment;

    private String newsReprinted;

}