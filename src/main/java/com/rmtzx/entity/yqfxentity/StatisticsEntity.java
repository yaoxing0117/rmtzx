package com.rmtzx.entity.yqfxentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 统计表数据实体
 *
 */
@Data
public class StatisticsEntity implements Serializable {
    private int id;
    private String category;
    private int wxNumber;
    private int nyNumber;
    private int qtNumber;
    private int bzNumber;
    private int zNumber;
    private int zzqNumber;
    private int zyNumber;
    private int wbNumber;
    private int zdNumber;
    private int wzNumber;

    private int zongNumber;
    private int dangNumber;
    private int orderNumber;
    private String time;

}
