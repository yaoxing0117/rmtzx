package com.rmtzx.entity.yqfxentity;

import lombok.Data;

/**
 *
 * 微博数据实体
 */
@Data
public class TweetsEntity {

    private String id; //= Field()  # 微博id
    private String weibo_url;// = Field()  # 微博URL
    private  String created_at; //= Field()  # 微博发表时间
    private  int  like_num;// = Field()  # 点赞数
    private  int  repost_num;// = Field()  # 转发数
    private  int comment_num; //= Field()  # 评论数
    private  String content; //= Field()  # 微博内容
    private  String user_id;// = Field()  # 发表该微博用户的id
    private  String tool; //= Field()  # 发布微博的工具
    private  String image_url;// = Field()  # 图片
    private  String video_url; //= Field()  # 视频
    private  String location;// = Field()  # 定位信息
    private  String origin_weibo;// = Field()  # 原始微博，只有转发的微博才有这个字段
    private  String crawled_at;// = Field()  # 抓取时间戳

}
