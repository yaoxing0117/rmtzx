package com.rmtzx.entity;

import lombok.Data;

@Data
public class StoreEntity1 {

    // dishId":1,"dishMoney":100,"di
    //     shName":"菜名","dishNumber":10,"orderNumber":"","orderState":"下单","pid":1,"spo
    //     tName":"店名","userAddress":"用户地址","userId":44,"userName":"用户名称","userRe
    //     marks":"不加饭","userTel":"18888888888"}]
    private int dishId;
    private int dishMoney;
    private int dishNumber;
    private String orderNumber;
    private String orderState;
    private String spotName;
    private int pid;
    private String userAddress;
    private int userId;
    private String userName;
    private String userRemarks;
    private int userTel;
}
