package com.rmtzx.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CYTelEntity implements Serializable {
    private int id;
    private int pid;
    private int code;
    private String bmName;
    private String phone;
    private String comment;
    private String txTime;
    private String jzrTime;
}
