package com.rmtzx.entity.appentity.bmEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteOption {
    private Integer id;

    private String optionNr;

    private Integer tpCount;

    private String picture;

    private String xxCommit;

    private Integer sortId;

    private String yls;

    private Integer yli;

}