package com.rmtzx.entity.appentity.bmEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteSort {
    private Integer id;

    private String sort;

    private Integer tpCode;

    private Date sTime;

    private Date eTime;

    private String picture;

    private String tpCommit;

}