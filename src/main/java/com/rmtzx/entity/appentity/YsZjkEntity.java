package com.rmtzx.entity.appentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 专家分类实体
 */
@Data
public class YsZjkEntity implements Serializable {
    private int id;
    private String expertsCategory;
    private String categories;
    private int pid;
}
