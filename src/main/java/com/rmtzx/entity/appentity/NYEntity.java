package com.rmtzx.entity.appentity;

import lombok.Data;

/**
 * 能源数据实体
 */
@Data
public class NYEntity {
    private int id;
    private String title;
    private String releaseTime;
    private String source;
    private String newsColumn;
    private String articleSource;
    private String article;
    private String editor;
    private String absUrl;
    private String caijiTime;

}
