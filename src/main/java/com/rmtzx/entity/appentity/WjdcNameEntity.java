package com.rmtzx.entity.appentity;

import lombok.Data;

import java.io.Serializable;

/**
 *
 * 问卷表（详情）
 */
@Data
public class WjdcNameEntity implements Serializable {
    private int id;
    private String title;
    private String content;
    private String picture;
    private String startTime;
    private String stopTime;
    private String category;
    private int uid;
    private int code;
}
