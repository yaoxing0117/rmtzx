package com.rmtzx.entity.appentity.ysEntity;

import lombok.Data;

/**
 *
 * 微信公众号数据实体
 */
@Data
public class WXGZHEntity {
    private int id;
    private String title;
    private String source;
    private String article;
    private String releaseTime;
    private String author;
    private String articleLink;
    private String pingLun;
    private String yueDu;
    private String dianZan;
    private String uid;
    private String status;
    private String code;
    private String pictureUrl;
    private String category;
    private String localUrl;
    private String differentiate;

}