package com.rmtzx.entity.appentity.ysEntity;

import lombok.Data;

/**
 * 招聘职位数据实体
 */
@Data
public class YszwEntity {
    private int id;
    private String position;
    private String place;
    private String experience;
    private String education;
    private String partTime;
    private String expectation;
    private String releaseTime;
    private String releaseId;
    private String bright;
    private String description;
    private int cid;
    private int code;



}
