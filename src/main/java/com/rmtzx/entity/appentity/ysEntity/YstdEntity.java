package com.rmtzx.entity.appentity.ysEntity;

import lombok.Data;

/**
 * 机关单位/公司详情与发布实体
 */
@Data
public class YstdEntity {
    private int id;
    private String title;
    private String content;
    private String time;
    private String picture;
    private int code;
    private int cid;
    private String adminId;

}
