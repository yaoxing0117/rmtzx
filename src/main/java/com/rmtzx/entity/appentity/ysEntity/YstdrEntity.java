package com.rmtzx.entity.appentity.ysEntity;

import lombok.Data;

/**
 *
 * 投递人实体
 */
@Data
public class YstdrEntity {
    private int id;
    private int gongsiId;
    private int userId;
    private int zhiweiId;

}
