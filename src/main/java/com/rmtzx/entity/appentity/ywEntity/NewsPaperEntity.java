package com.rmtzx.entity.appentity.ywEntity;

import com.rmtzx.entity.pojo.CommentEntity;
import lombok.Data;

import java.util.List;

/**
 * @author yaoxing
 * @create 2019-12-21 14:13
 * 报纸公共实体
 */

@Data
public class NewsPaperEntity {
    //主键
    private int id;
    //标题_h1
    private String titleH1;
    //标题_h2
    private String titleH2;
    //标题_h3
    private String titleH3;
    //标题_h4
    private String titleH4;
    //编辑人
    private String editor;
    //报纸内容
    private String newsArticle;
    //每版报纸首页图片地址
    private String pictureUrl;
    //发布时间
    private String releaseTime;
    private String localUrl;
    //来源
    private String source;
    //点赞
    private String d_z;
    //审核状态
    private int code;
    //评论
    private List<CommentEntity> comment;
    //类别
    private String category="BZ";

    private String absUrl;
    private int differentiate;
}
