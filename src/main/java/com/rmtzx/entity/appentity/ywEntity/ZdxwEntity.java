package com.rmtzx.entity.appentity.ywEntity;

import com.rmtzx.entity.pojo.CommentEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 准东新闻数据实体
 */
@Data
public class ZdxwEntity implements Serializable {
    //主键
    private int id;
    //大类别
    private String bigCategory="准东动态";
    //类别
    private String category="ZDXW";
    //正文
    private String article;
    //文章来源
    private String article_source;
    //作者
    private String author;
    //状态码
    private int code;
    //编辑人
    private String editor;
    //新闻类型
    private String news_column;
    //图片URL
    private String pictureUrl;
    //发布时间
    private String releaseTime;
    //来源
    private String source;
    //标题
    private String  title;
    private String localUrl;
    //点赞
    private String d_z;
    //评论
    private List<CommentEntity> comment;
    //请求前缀
    private String absUrl;
    private int differentiate;
    private int clicks;
    private int reload;
    private int commentQuantity;
}
