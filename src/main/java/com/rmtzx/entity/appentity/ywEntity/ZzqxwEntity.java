package com.rmtzx.entity.appentity.ywEntity;

import com.rmtzx.entity.pojo.CommentEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yaoxing
 * @create 2019-12-18 17:13
 * 自治区新闻数据实体
 */
@Data
public class ZzqxwEntity implements Serializable {
    //主键
    private int id;
    //正文
    private String article;
    //文章来源
    private String articleSource;
    //作者
    private String author;
    //状态码
    private int code;
    //编辑人
    private String editor;
    //新闻类型
    private String newsColumn;
    //图片URL
    private String pictureUrl;
    //发布时间
    private String releaseTime;
    //来源
    private String source;
    //标题
    private String  title;
    //点赞
    private String d_z;
    private String localUrl;
    //评论
    private List<CommentEntity> comment;
    //类别
    private String category="ZZQXW";
    private String absUrl;
    private int differentiate;

}
