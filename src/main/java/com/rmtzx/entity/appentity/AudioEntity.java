package com.rmtzx.entity.appentity;

import lombok.Data;

/**
 * mp3数据实体
 */
@Data
public class AudioEntity {
    private int id;
    private String category;
    private String upTime;
    private String picture;
    private String mp3Url;
    private String editor;
    private String article;
    private int code;
}
