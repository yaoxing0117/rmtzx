package com.rmtzx.entity.appentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 匿名投诉数据实体
 */
@Data
public class PrizeEntiy implements Serializable {
    private int id;
    private String reportContent ;
    private String reportPicture;
    private String reportTel;
    private String reportWx;
    private String reportTime;
    private int reportExamine;

}
