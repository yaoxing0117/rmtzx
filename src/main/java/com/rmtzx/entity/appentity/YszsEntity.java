package com.rmtzx.entity.appentity;

import com.rmtzx.entity.pojo.CommentEntity;
import lombok.Data;

import java.util.List;

/**
 * 招商政策-经济信息-成果展示-产销咨询公共数据实体
 *
 */
@Data
public class YszsEntity {
    //主键
    private int id;
    //正文
    private String article;
    //文章来源
    private String articleSource;
    //作者
    private String author;
    //状态码
    private int code;
    //编辑人
    private String editor;
    //新闻类型
    private String newsColumn;
    //图片URL
    private String pictureUrl;
    //发布时间
    private String releaseTime;
    //来源
    private String source;
    //标题
    private String  title;
    //点赞
    private String d_z;
    //评论
    private List<CommentEntity> comment;
    //类别
    private String category;
    private String localUrl;
    //
    private String absUrl;
    private int cid;
    private int differentiate;

}
