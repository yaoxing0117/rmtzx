package com.rmtzx.entity.appentity.zwEntity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class AnnouncementFb implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 正文内容
     */
    @TableField("article")
    private String article;

    /**
     * 编辑人
     */
    @TableField("editor")
    private String editor;

    /**
     * 预览图
     */
    @TableField("picture")
    private String picture;

    /**
     * 发布时间
     */
    @TableField("release_time")
    private LocalDateTime releaseTime;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 状态1审核0未审核
     */
    @TableField("code")
    private Integer code;

    /**
     * 点赞数
     */
    @TableField("dz_count")
    private Integer dzCount;

    /**
     * 转发数
     */
    @TableField("zf_count")
    private Integer zfCount;

    /**
     * 评论数
     */
    @TableField("pl_count")
    private Integer plCount;

    /**
     * 修改人
     */
    @TableField("uid")
    private Integer uid;

    /**
     * 分类
     */
    @TableField("category")
    private String category;

    /**
     * 状态
     */
    @TableField("status")
    private Integer status;

    /**
     * 图片前缀
     */
    @TableField("pre_url")
    private String preUrl;

    /**
     * 网站图片绝对路径
     */
    @TableField("abs_url")
    private String absUrl;

    @TableField("differentiate")
    private Integer differentiate;

    @TableField("local_url")
    private String localUrl;


}
