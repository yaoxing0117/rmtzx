package com.rmtzx.entity.appentity.zwEntity;

import lombok.Data;

/**
 * 用户收藏实体
 */
@Data
public class UserCollectionEntity {
   private int id;
   //收藏人
   private String collection_user;
   //收藏标题
   private String collection_title;
   //收藏时间
   private String collection_time;
   //类别
   private String collection_category;
}
