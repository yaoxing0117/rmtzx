package com.rmtzx.entity.appentity.zwEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Propose {
    private Integer id;

    private String editor;

    private String picture;

    private Date releaseTime;

    private Integer bmId;

    private String article;

    private Integer userId;

    private String clContent;

    private Integer jyCode;
}