package com.rmtzx.entity.appentity.zwEntity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class WebAnnouncement implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 详情
     */
    @TableField("article")
    private String article;

    /**
     * 图片链接
     */
    @TableField("editor")
    private String editor;

    /**
     * 照片
     */
    @TableField("picture")
    private String picture;

    /**
     * 发布时间
     */
    @TableField("release_time")
    private String releaseTime;

    /**
     * 标题
     */
    @TableField("title")
    private String title;

    /**
     * 状态0未审核1审核
     */
    @TableField("code")
    private Integer code;

    /**
     * 点赞数
     */
    @TableField("dz_count")
    private Integer dzCount;

    /**
     * 转发数
     */
    @TableField("zf_count")
    private Integer zfCount;

    /**
     * 评论数
     */
    @TableField("pl_count")
    private Integer plCount;

    /**
     * 类别名
     */
    @TableField("category")
    private String category;

    @TableField("cid")
    private Integer cid;

    @TableField("pre_url")
    private String preUrl;

    /**
     * 区分数组json
     */
    @TableField("differentiate")
    private Integer differentiate;

    @TableField("local_url")
    private String localUrl;

    /**
     * 文章来源
     */
    @TableField("source")
    private String source;


}
