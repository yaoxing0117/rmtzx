package com.rmtzx.entity.appentity.zwEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Complaints {
    private Integer id;

    private String article;

    private String editor;

    private String picture;

    private Date releaseTime;

    private Integer bmId;

    private Integer userId;

    private String clContent;

    private Integer tsCode;
}