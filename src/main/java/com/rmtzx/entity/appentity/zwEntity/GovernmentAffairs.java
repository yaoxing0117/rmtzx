package com.rmtzx.entity.appentity.zwEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 政务信息实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GovernmentAffairs {

    //主键id
    private Integer id;
    //详情
    private String article;
    //编辑者
    private String editor;
    //图片链接
    private String picture;
    //发布时间
    private Date releaseTime;
    //标题
    private String title;
    //审核是否通过o:未审核，1：审核通过
    private Integer zwCode;
    //点赞次数
    private Integer dzCount;
    //转发次数
    private Integer zfCount;
    //评论次数
    private Integer plCount;
    //政务详情种类
    private String  cateGory;
}