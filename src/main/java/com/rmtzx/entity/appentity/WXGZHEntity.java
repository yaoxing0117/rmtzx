package com.rmtzx.entity.appentity;

import lombok.Data;

import javax.persistence.Column;

/**
 * 微信公众号数据实体
 */
@Data
public class WXGZHEntity {
    @Column(name = "id")
    private int id;
    @Column(name = "code")
    private int code;
    @Column(name = "title")
    private String title;
    @Column(name = "source")
    private String source;
    @Column(name = "article")
    private String article;
    @Column(name = "release_Time")
    private String release_Time;
    @Column(name = "author")
    private String author;
    @Column(name = "article_link")
    private String articleLink;
    @Column(name = "picture_lrl")
    private String pictureUrl;
    @Column(name = "category")
    private String category;
    @Column(name = "local_url")
    private String localUrl;
    @Column(name = "differentiate")
    private String differentiate;

}
