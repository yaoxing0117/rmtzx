package com.rmtzx.entity.appentity.htEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRole {
    private Integer rid;

    private String rname;

    private Integer pid1;

    private Integer pid2;

    private Integer pid3;

    private Integer pid4;

    private Integer pid5;

    private Integer pid6;

    private String commit;

}