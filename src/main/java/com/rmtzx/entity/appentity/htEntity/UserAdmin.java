package com.rmtzx.entity.appentity.htEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAdmin {
    private Integer id;

    private String username;

    private String pwd;

    private Date ptime;

    private Integer bmid;

    private String phone;

    private Integer rid;

    private String commit;

    private String softdog;

    private String yls;

    private Integer yli;

    private String token;

}