package com.rmtzx.entity.appentity.htEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserPower {
    private Integer id;

    private String pname;

    private String commit;

}