package com.rmtzx.entity.appentity;

import lombok.Data;

/**
 * 问卷调查（答案表）
 */
@Data
public class WjdcDAEntity {
    private int id;
    private int pid;
    private int count;
    private int wjId;
    private String title;
    private String picture;
    private String category;
}
