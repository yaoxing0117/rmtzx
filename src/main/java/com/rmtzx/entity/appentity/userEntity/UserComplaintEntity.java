package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 投诉+咨询+建议数据实体
 */
@Data
public class UserComplaintEntity implements Serializable {
    private int id;
    //用户id
    private int userId;
    //投诉人手机号
    private String complainant;
    //用户邮箱
    private String userMail;
    //投诉部门
    private String department;
    //标题
    private String title;
    //投诉内容
    private String contents;
    //投诉时间
    private String complaintTime;
    //回馈内容
    private String feedbackContents;
    //回馈时间
    private String feedbackTime;
    //回馈部门
    private String feedbackDepartment;
    //处理状态
    private int code;
    //类别
    private String category;
}
