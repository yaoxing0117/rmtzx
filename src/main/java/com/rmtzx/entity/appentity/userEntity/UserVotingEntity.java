package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 投票数据实体
 */
@Data
public class UserVotingEntity implements Serializable {
    private int id;
    //主题id
    private int optionId;
    //发起人
    private String originator;
    //标题
    private String title;
    //内容
    private String content;
    //投票图片
    private String picture;
    //开始时间
    private String startDate;
    //结束日期
    private String stopDate;
    //投票选项标题
    private String optionTitle;
    //选项图片
    private String optionPicture;
    //投票选项内容
    private String optionContent;
    //票数
    private String votes;
    //投票类别
    private int votingCategory;
    //备注
    private String tpRemarks;
    //投票图片开启状态
    private String codePicture;

}
