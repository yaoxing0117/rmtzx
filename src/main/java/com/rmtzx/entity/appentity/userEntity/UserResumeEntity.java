package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户简历实体
 */
@Data
public class UserResumeEntity implements Serializable {
    private int id;
    private String userName;
    private String userAge;
    private String userSex;
    private String userTel;
    private String userMail;
    private String userXl;
    private String userZwms;
    private String userQwxz;
    private String userBz;
}
