package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 投票人
 */
@Data
public class UserVotingNameEntity  implements Serializable {
    private int id;
    private String userName;
    private int userId;
    private int cid;
}
