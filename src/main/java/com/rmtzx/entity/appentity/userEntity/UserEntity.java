package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * 用户资料实体
 */
@Data
public class UserEntity implements Serializable {
    private String userToken;
    //用户Id
    private int id;
    //用户账号-手机号
    private String userNameTel;
    //密码
    private String passWord;
    //用户身份证
    private String userId;
    //用户昵称
    private String userNickName;
    //用户年龄
    private String userAge;
    //性别
    private String userSex;
    //用户生日
    private String userBirthday;
    //用户地址
    private String userCity;
    //头像URl
    private String userPortrait;
    //用户电话
    private String userTel;
    //用户姓名
    private String userName;
    //收藏次数
    private int userCollection;
    //点赞次数
    private int userDz;
    //历史记录
    private String userHistory;
    private int resumeId;

}
