package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户历史记录实体
 */
@Data
public class UserHistoricalEntity implements Serializable {
    private int id;
    //浏览用户
    private String historicalUser;
    //浏览标题
    private String historicalTitle;
    //浏览来源
    private String historicalSource;
    //浏览新闻ID
    private String historicalNewsId;
    //浏览新闻类别
    private String historicalCategory;
    //浏览时间
    private String historicalTime;
}
