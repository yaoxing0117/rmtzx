package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 收藏数据实体
 */
@Data
public class UserCollectionEntity implements Serializable {
    //id
    private int id;
    //收藏人
    private String collectionUser;
    //收藏内容标题
    private String collectionArticle;
    //来源
    private String collectionSource;
    //收藏时间
    private String collectionTime;
    //收藏类别
    private String collectionArticleCategory;
    //收藏新闻ID
    private int collectionArticleId;
    private int userId;

}
