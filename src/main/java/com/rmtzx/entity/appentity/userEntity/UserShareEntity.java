package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户分享数据实体
 */
@Data
public class UserShareEntity implements Serializable {
    //
    private int id;
    //分享人
    private String user;
    //分享时间
    private String time;
    //分享内容
    private String news;
    //分享内容ID
    private String news_id;
    //分享方向
    private String sharing_direction;
    //预留字段
    private String reserve_one;
    //预留字段
    private String reserve_two;
    //预留字段
    private String reserve_three;
}
