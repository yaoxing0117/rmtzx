package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户快递实体
 */
@Data
public class UserExpressEntity implements Serializable {
    private int id;
    //用户手机号
    private String userUser;
    //寄件人
    private String sender;
    //寄件地址
    private String mailingAddress;
    //收件人
    private String addressee;
    //收件地址
    private String receivingAddress;
    //运单号
    private String waybill;
    //订单号
    private String userOrder;
    //快递公司
    private String courierServicesCompany;
    //订单时间
    private String expressTime;
    //唯一标示
    private String uniqueIdentifier;
}
