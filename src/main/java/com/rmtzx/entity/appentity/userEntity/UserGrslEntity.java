package com.rmtzx.entity.appentity.userEntity;

import lombok.Data;

import java.io.Serializable;

/**
 * 个人申领数据实体
 */
@Data
public class UserGrslEntity implements Serializable {
    private int id;
    private String userUser;
    private String userId;
    private String content;
    private String title;
    private int code;
    private int slNumber;
    private String slRemarks;
    private int yslNumber;
    private int pid;
    private String cjTime;
    private String lqTime;
    private int adminId;
    private int yl1;
    private int yl2;
    private String yl3;
    private String yl4;

}
