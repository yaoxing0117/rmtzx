package com.rmtzx.entity.appentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 专家详情实体
 */
@Data

public class YsZjkNameEntity implements Serializable {
    private int id;
    private String expertName;
    private String expertUnit;
    private String expertPost;
    private String expertTitle;
    private String expertField;
    private String expertBrief;
    private int cid;
}
