package com.rmtzx.entity.appentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 培训班数据实体
 *
 */
@Data
public class TrainingEntity implements Serializable {
    private int id;
    private String className;
    private String classBegins;
    private String organizationName;
    private String registrationTime;
    private String classCost;
    private String content;
    private String entryRequirements;
    private String contactInformation;
    private String companyIntroduction;
    private int code;


}
