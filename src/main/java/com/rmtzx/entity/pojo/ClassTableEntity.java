package com.rmtzx.entity.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassTableEntity implements Serializable {
    private int id;
     //大分类
    private String largeCategory;
    //小类别
    private String smallCategory;
    //预留字段
    private String y_l1;
    //预留字段
    private String y_l2;
    //预留字段
    private String y_l3;
    //
    private int pid;
    private String absUrl;
}
