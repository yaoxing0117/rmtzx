package com.rmtzx.entity.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 详情公共实体
 */
@Data
@NoArgsConstructor
public class SpecificEntities {
    private List specificEntities;
}
