package com.rmtzx.entity.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 前端分页展示公共实体类
 */
@Data
@NoArgsConstructor
public class ShowInfo implements Serializable {
    private int totalPage;//总页数
    private int page;//当前页数
    private int totalCount;//总记录数
    private List limitShowList;//当前页的所有内容
    private String newsInfoURL;//请求url
    public ShowInfo(int totalPage,int page,int totalCount, List limitShowList){
        this.limitShowList=limitShowList;
        this.page=page;
        this.totalCount=totalCount;
        this.totalPage=totalPage;
    }
}
