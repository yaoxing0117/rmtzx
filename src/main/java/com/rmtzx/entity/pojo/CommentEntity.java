package com.rmtzx.entity.pojo;

import lombok.Data;
import java.io.Serializable;

/**
 * 评论公共实体
 */
@Data
public class CommentEntity implements Serializable {
    //
    private int id;
    private int userId;
    //评论人
    private String commentator;
    //评论内容
    private String  commentContent;
    //评论时间
    private String commentaryTime;
    //评论点赞
    private int DZ;
    //新闻ID
    private int commentID;
    //类别
    private String commentCategory;
    //评论标题
    private String commentTitle;
    private String userTx;
}
