package com.rmtzx.entity.pojo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.metadata.BaseRowModel;
import lombok.Data;

import java.util.Date;

/**
 * excel表格实体
 */
@Data
public class ExcelModel extends BaseRowModel {
    private int id;
    //部门
    @ExcelProperty(value = "部门", index = 0)
    private String fullDepartment;
    //姓名
    @ExcelProperty(value = "姓名", index = 1)
    private String fullName;
    //岗位
    @ExcelProperty(value = "岗位", index = 2)
    private String fullPost;
    //身份
    @ExcelProperty(value = "身份", index = 3)
    private String fullIdentity;
    //请假时间
    @ExcelProperty(value = "请假时间(X月X日—X月X日)（几天）", index = 4)
    private String fullLeaveTime;
    //请假原因
    @ExcelProperty(value = "请假原因", index = 5)
    private String fullTakingLeave;
    //参加工作时间
    @ExcelProperty(value = "参加工作时间(X年X月X日)", index = 6,format = "yyyy/MM/dd")
    private Date workingHours;
    //入职准东工作时间
    @ExcelProperty(value = "入职准东工作时间[X年X月X日]", index = 7,format = "yyyy/MM/dd")
    private Date inWork;
    //年假时间
    @ExcelProperty(value = "年休假天数（0天，5天，10天，15天）", index = 8)
    private String leaveTime;
    //累计请假;
    @ExcelProperty(value = "累计请假天数", index = 9)
    private String accumulatedTime;
    //备注;
    @ExcelProperty(value = "备注", index = 10)
    private String remarks;
}
