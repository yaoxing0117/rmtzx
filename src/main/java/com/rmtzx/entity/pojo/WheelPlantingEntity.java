package com.rmtzx.entity.pojo;

import lombok.Data;

import java.io.Serializable;
@Data
public class WheelPlantingEntity implements Serializable {
    private int id;
    //标题
    private String title;
    //文章
    private String text;
    //图片
    private String picture;
    //发布人
    private String publisher;
    //时间
    private String time;
    //审核状态
    private String  code;
    //点赞
    private int d_z;
    //评论
    private String comment;
    //轮播类型
    private String category;
    //论播图片
    private String titlePicture;
    private String preUrl;
    private int differentiate;
}
