package com.rmtzx.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "Tweets")
public class Tweets implements Serializable {

    private String id;
    private String created_at ; // 微博发表时间
    private Integer like_num;//点赞数
    private Integer repost_num;//转发数
    private Integer comment_num;//评论数
    private String content;//微博内容
    private String user_id;//发表该微博用户的id
    private String tool;//发布微博的工具
    private String image_url;//图片
    private String video_url;//视频
    private String location;//定位信息
    private String origin_weibo;//原始微博
    private String crawled_at;//抓取时间戳
    private String weibo_url;
}
