package com.rmtzx.entity.bo;

public enum ResultCode {
    // 自定义状态码
    SUCCESS(0, "成功"),
    FAIL(1,"失败"),
    UNAUTHENTICATED(1,"账号不存在！"),
    UNAUTHORISE(1,"已存在"),
    SERVER_ERROR(1,"密码不相同"),
    PARAMETER_ERROR(1,"参数不存在"),
    INVALID_PARAM(1,"参数为空"),
    INVALID_PARAM1(1,"暂无数据"),
    INVALID_Entities(1,"收藏存在"),
    INVALID_Entitie(1,"参数错误"),
    NOT_FIND(1,"昵称已存在"),
    NOT_FIND2(1,"用户已添加申领"),
    DZ_FIND(1,"用户已点赞");

    private Integer code;

    private String message;

    ResultCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer code() {
        return this.code;
    }

    public String message() {
        return this.message;
    }

    public static String getMessage(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.message;
            }
        }
        return name;
    }

    public static Integer getCode(String name) {
        for (ResultCode item : ResultCode.values()) {
            if (item.name().equals(name)) {
                return item.code;
            }
        }
        return null;
    }
}
