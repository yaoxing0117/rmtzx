package com.rmtzx.entity.bo;

import lombok.Data;

@Data
public class WXPayEntity {
    private int id;
    private String transaction_id;//微信支付订单号
    private String nonce_str;//随机字符串
    private String bank_type;//付款银行
    private String openid;//用户标识
    private String sign;//签名
    private String fee_type;//货币种类
    private String mch_id;//商户号
    private int cash_fee;//现金支付金额
    private String outTradeNo;//商户订单号
    private String appid;//小程序ID
    private int total_fee;//订单金额
    private String trade_type;//交易类型
    private String result_code; //返回状态码
    private String time_end;//支付完成时间
    private String is_subscribe;//是否关注公众账号
    private String orderCode;//订单状态 支付成功 or 支付中
    private String userTel;//用户手机号
    private int userMoney;//充值金额
    private int user_oder;//前端生成订单号
}
