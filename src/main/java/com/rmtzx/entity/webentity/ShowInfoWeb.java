package com.rmtzx.entity.webentity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * web前端分页展示公共实体类
 */
@Data
@NoArgsConstructor
public class ShowInfoWeb implements Serializable {
    private int totalPage;//总页数
    private int page;//当前页数
    private int totalCount;//总记录数
    private List limitShowList;//当前页的所有内容
    private String newsInfoURL;//请求url
    public ShowInfoWeb(int totalPage, int page, int totalCount, List limitShowList){
        this.limitShowList=limitShowList;
        this.page=page;
        this.totalCount=totalCount;
        this.totalPage=totalPage;
    }
}
