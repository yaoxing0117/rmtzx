package com.rmtzx.entity.webentity;

import lombok.Data;

/**
 * 投资人数据实体
 */
@Data
public class InvestmentEntity {
    private int id;
    private String investmentUser;
    private String investmentTel;
    private String investmentMail;
    private String investmentWx;
    private String investmentBz;
    private int differentiate;

}
