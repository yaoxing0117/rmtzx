package com.rmtzx.entity.webentity;

import lombok.Data;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * 专栏数据实体
 */
@Data

public class ZtEntity implements Serializable {
    @Column(name = "id")
    private Integer id;
    @Column(name = "editor")
    private String editor;
    @Column(name = "picture")
    private String picture;
    @Column(name = "release_time")
    private String releaseTime;
    @Column(name = "title")
    private String title;
    @Column(name = "code")
    private Integer code;
    @Column(name = "dz_count")
    private Integer dzCount;
    @Column(name = "zf_count")
    private Integer zfCount;
    @Column(name = "pl_count")
    private Integer plCount;
    @Column(name = "category")
    private String category;
    @Column(name = "uid")
    private Integer uid;
    @Column(name = "article")
    private String article;
    private String absUrl;
    private String localUrl;
    private int differentiate;
}
