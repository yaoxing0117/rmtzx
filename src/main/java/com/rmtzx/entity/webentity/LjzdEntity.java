package com.rmtzx.entity.webentity;

import lombok.Data;

import java.io.Serializable;

/**
 * 了解准东-投资公用实体
 */
@Data
public class LjzdEntity implements Serializable {

    private int id;
    //修改人ID
    private int changeId;
    //状态
    private int code;
    //预留
    private int shid;
    //标题
    private String title;
    //编辑内容
    private String article;
    //编辑时间
    private String cjTime;
    //编辑人
    private String editor;

    //类别
    private String category;
    //作者
    private String author;
    private String localUrl;
    //展示图片
    private String picture;
    //修改时间
    private String endTime;
    //预留
    private String yl;
    private String absUrl;
    private int differentiate;
}
