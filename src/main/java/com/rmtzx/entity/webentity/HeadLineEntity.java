package com.rmtzx.entity.webentity;

import lombok.Data;

import java.io.Serializable;

@Data
public class HeadLineEntity implements Serializable {
    private int id;
    //标题
    private String title;
    //文章内容
    private String article;
    //发布时间
    private String releaseTime;
    //作者
    private String author;
    //编辑人
    private String editor;
    //类别
    private String category;
    //状态码
    private int code;
    //编辑人ID
    private int uid;
    private String localUrl;

    //来源
    private String source;
    //url
    private String newsInfoURL="/tt/oneHeadLine";
    private int differentiate;

}
