package com.rmtzx.entity.webentity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * 用户互动交流数据实体
 */
@Data

public class UserInteractionEntity implements Serializable {

    private int id;
    //姓名
    private String userName;
    //地区
    private String userDq;
    //手机号
    private String userTel;
    //提交时间
    private String userTjtime;
    //内容
    private String userNr;
    //主题
    private String userZt;
    //内容类别
    private String userLb;
    //信件类别
    private String userXjlb;
    //回复部门
    private String adminHfbm;
    //回复时间
    private String adminHftime;
    //回复内容
    private String adminHfnr;
    //信件id
    private int XJId;

    private int XJRd;

    private String state;
    private String absUrl;
    private int differentiate;
}
