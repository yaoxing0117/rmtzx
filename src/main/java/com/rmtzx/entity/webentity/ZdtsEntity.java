package com.rmtzx.entity.webentity;

import com.rmtzx.entity.pojo.CommentEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 准东图说-微视频数据实体
 */
@Data
public class ZdtsEntity implements Serializable {
    //主键
    private int id;
    //类别
    private String category ;
    private String bigCategory="";
    //正文
    private String article;
    //文章来源
    private String articleSource;
    //作者
    private String author;
    //状态码
    private int code;
    //编辑人
    private String editor;
    //新闻类型
    private String news_column;
    //图片URL
    private String pictureUrl;
    //发布时间
    private String releaseTime;
    //来源
    private String source;
    //标题
    private String  title;
    //点赞
    private String d_z;
    //评论
    private List<CommentEntity> comment;
    //请求前缀
    private String absUrl;

    //
    private int uid;
    private int differentiate;
}
