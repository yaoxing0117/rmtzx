package com.rmtzx.entity.webentity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * app公告信息实体类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Announcement implements Serializable {
    //主键id
    private Integer id;
    //文章内容
    private String article;
   //编辑人
    private String editor;
    //图片链接
    private String picture;
    //发布时间
    private String releaseTime;
   //标题
    private String title;
    //审核是否通过o:未审核，1：审核通过
    private int code;
   //点赞次数
    private int dzCount;
    private String localUrl;
   //转发次数
    private int zfCount;
    //评论次数
    private int plCount;
    //政务详情种类
    private String  category;
    //关联id
    private int cid;

    private String preUrl;
    private int differentiate;
}