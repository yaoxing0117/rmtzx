package com.rmtzx.entity.store;

import lombok.Data;

import java.io.Serializable;

/**
 * 店家-菜品 评论公共数据实体
 */
@Data
public class StoreCommentEntity implements Serializable {
    private Integer id;
    private Integer storeId;

    private String commentCategory;

    private String commentContent;

    private String commentPicture;

    private String commentTime;

    private Integer commentStar;

    private Integer userId;

    private String commentReply;

    private String commentTimeReply;

    private String commentStore;

    private Integer pid;

    private String reserveOne;

    private String reserveTwo;

    private String reserveThree;

    private String reserveFour;

}