package com.rmtzx.entity.store;

import lombok.Data;

import java.io.Serializable;

/**
 * 订单数据实体
 */
@Data
public class StoreOrderEntity implements Serializable {
    private Integer id;

    private Integer orderTotal;//合计mapper

    private Integer userId;

    private String userAddress;

    private String userName;

    private String userTel;

    private String spotName;

    private Integer dishId;

    private String dishName;

    private Integer dishNumber;

    private Integer dishMoney;

    private String orderNumber;

    private String orderState;

    private String orderTime;

    private String userRemarks;

    private Integer orderStar;

    private String orderPicture;

    private String orderComment;

    private String orderCommentTime;

    private String orderReply;

    private String orderReplyTime;

    private String orderReplyName;

    private Integer pid;

    private String reserveOne;

    private String reserveTwo;

    private String reserveThree;

    private String reserveFour;
}