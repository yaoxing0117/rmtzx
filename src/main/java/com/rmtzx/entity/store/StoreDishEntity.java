package com.rmtzx.entity.store;

import lombok.Data;

import java.io.Serializable;

/**
 * 外卖店家-菜品数据实体
 */
@Data
public class StoreDishEntity implements Serializable {
    private Integer id;

    private String dishName;

    private Integer dishPrice;

    private String dishPicture;

    private String dishGrade;

    private Integer dishComment;

    private Integer dishTimes;

    private Integer pid;

    private String reserveOne;

    private String reserveTwo;

    private String reserveThree;

    private String reserveFour;

}