package com.rmtzx.entity.store;

import lombok.Data;

import java.io.Serializable;

/**
 * 外卖店家数据实体
 */
@Data
public class StoreTakeawayEntity implements Serializable {
    private Integer id;

    private String shopOwner;

    private String mainCamp;

    private String storeIntroduce;

    private String storeTel;

    private String storeHours;

    private String storePicture;

    private String storeIncreaseTime;

    private String storeLocation;

    private Integer storePriority;

    private String storeArea;

    private Integer pid;

    private String reserveOne;

    private String reserveTwo;

    private String reserveThree;

    private String reserveFour;

    private String storeNotice;

    private Integer storeStar;

    private String storeAddress;

    private Integer storeCode;
}