package com.rmtzx.util.ExcelPoi;

import com.rmtzx.entity.pojo.ExcelModel;
import com.rmtzx.entity.pojo.ExcelUtil;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

public class EasyexcelApplication {
    public static void main(String[] args) {
        try (FileInputStream inputStream = new FileInputStream("F:\\准东经济技术开发区组织部（人社局）考勤电子信息录入工作1224 - test纪工委.xlsx")) {
            List<ExcelModel> excelModel = ExcelUtil.readExcel(new BufferedInputStream(inputStream), ExcelModel.class);
            System.out.println(excelModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}