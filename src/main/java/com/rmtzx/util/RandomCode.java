package com.rmtzx.util;

import java.util.Random;

public class RandomCode {
    public void randomCodeTest() {
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            int randomNum = random.nextInt(1000000);
            String randomCode = String.format("%06d", randomNum);
            System.out.println(randomCode);
        }
    }
}
