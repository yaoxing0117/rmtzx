package com.rmtzx.util;
/**
 * 订单号生成工具
 */
public class MakeOrderNum {
    /**
     * 生成规则设备编号:单号类型+五位编号（从1开始，不够前补0）
     *
     * @param equipmentType 设备类型
     * @param equipmentNo   最新编号
     * @return
     */
    public static String getNewEquipmentNo(String equipmentType, String equipmentNo) {
        String newEquipmentNo = "00001";
        if (equipmentNo != null && !equipmentNo.isEmpty()) {
            int newEquipment = Integer.parseInt(equipmentNo) + 1;
            newEquipmentNo = String.format(equipmentType + "%08d", newEquipment);
        }

        return newEquipmentNo;
    }
}
