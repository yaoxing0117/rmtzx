package com.rmtzx.manager;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;

import com.rmtzx.util.MakeOrderNum;
import com.rmtzx.util.Md5Util;

import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShoujiHuaFei {
    // HttpClient请求的相关设置，可以不用配置，用默认的参数，这里设置连接和超时时长(毫秒)
    public  RequestConfig config = RequestConfig.custom()
            .setConnectTimeout(30000).setSocketTimeout(30000).build();
    @Value("${tel.AppKey}")
    public  String key ;//申请的接口Appkey
    @Value("${tel.OpenId}")
    public  String openId;//在个人中心查询
    public  String telCheckUrl = "http://op.juhe.cn/ofpay/mobile/telcheck?cardnum=*&phoneno=!&key="+key;
    public  String telQueryUrl="http://op.juhe.cn/ofpay/mobile/onlineorder";
    public  String onlineUrl="http://op.juhe.cn/ofpay/mobile/onlineorder?key="+key+"&phoneno=!&cardnum=*&orderid=@&sign=$";
    public  String yueUrl="http://op.juhe.cn/ofpay/mobile/yue?key="+key+"&"+"timestamp=%&sign=$";
    public  String orderstaUrl="http://op.juhe.cn/ofpay/mobile/ordersta?key="+key+"&orderid=!";
//---------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * 2.根据手机号和面值查询商品信息
     * @param phone 手机号码
     * @param cardnum 充值金额,目前可选：5、10、20、30、50、100、300
     * @return String类型结果
     * @throws Exception
     */
    //校验值，md5(OpenID+key+phoneno+cardnum+orderid)，OpenID在个人中心查询
    //phoneno=15193869953
    // &cardnum=1
    // &orderid=825644945
    // &sign=3337DBDCFF386A04A33C04868B9D672F
    // &key=678de9b139284535203d21a656b2ece2
    public String telQuery(String phone,int cardnum,String orderid) throws Exception{
        // String orderid= MakeOrderNum.getNewEquipmentNo("JH","1");//不可重复订单号,String orderid
        String param0=openId+key+phone+cardnum+orderid;
        String s = Md5Util.MD532(param0);

        String param="phoneno="+phone+"&cardnum="+cardnum+"&orderid="+orderid+"&sign="+s+"&key="+key;
        String s1 = this.sendPost(param);
        System.out.println(s1);
        return s1;
    }
    /**
     * 3.依据用户提供的请求为指定手机直接充值
     * @param phone 手机号码
     * @param cardnum 充值金额,目前可选：5、10、20、30、50、100、300
     * @param
     * @return 返回String结果
     * @throws Exception
     */
    public  String onlineOrder(String phone,int cardnum) throws Exception{
        String result = null;
//Md5Util工具类
        String sign = Md5Util.MD5(openId+"678de9b139284535203d21a656b2ece2"+phone+cardnum+openId);
        result = get(onlineUrl.replace("*", cardnum+"").replace("!", phone).replace("@", openId).replace("$", sign),0);
        return result;
    }
    /**
     * 4.查询账户余额
     * @return
     * @throws Exception
     */
    public  String yuE() throws Exception{
        String timestamp = System.currentTimeMillis()/1000+"";
        String sign = Md5Util.MD5(openId+key+timestamp);
        String result =get(yueUrl.replace("%", timestamp).replace("$", sign),0);
        return result;
    }
    /**
     * 5.订单状态查询
     * @param orderid 商家订单号
     * @return 订单结果
     * @throws Exception
     */
    public  String orderSta(String orderid) throws Exception{
        return get(orderstaUrl.replace("!", orderid), 0);
    }
    /**工具类方法
     * get 网络请求
     * @param url 接收请求的网址
     * @param tts 重试
     * @return String类型 返回网络请求数据
     * @throws Exception 网络异常
     */
    public  String get(String url,int tts) throws Exception{
        if(tts>3){//重试3次
            return null;
        }
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        String result = null;
        try {
            HttpGet httpGet = new HttpGet(url);
            httpGet.setConfig(config);
            response = httpClient.execute(httpGet);
            HttpEntity resEntity = response.getEntity();
            if (resEntity != null) {
                result = ConvertStreamToString(resEntity.getContent(), "UTF-8");
            }
            EntityUtils.consume(resEntity);
            return result;
        }catch(IOException e){
            return get(url, tts++);
        }finally {
            response.close();
            httpClient.close();
        }
// 得到的是JSON类型的数据需要第三方解析JSON的jar包来解析
    }
    /**工具类方法
     * 此方法是把传进的字节流转化为相应的字符串并返回，此方法一般在网络请求中用到
     * @param is 输入流
     * @param charset 字符格式
     * @return String 类型
     * @throws Exception
     */
    public  String ConvertStreamToString(InputStream is, String charset)
            throws Exception {
        StringBuilder sb = new StringBuilder();
        try (InputStreamReader inputStreamReader = new InputStreamReader(is,
                charset)) {
            try (BufferedReader reader = new BufferedReader(inputStreamReader)) {
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\r\n");
                }
            }
        }
        return sb.toString();
    }

    /**
     * 向指定 URL 发送POST方法的请求
     *
     * @param
     * @param param 请求参数，请求参数应该是 name1=value1&name2=value2 的形式。
     * @return 所代表远程资源的响应结果
     */
    public String sendPost( String param) {
        PrintWriter out = null;
        BufferedReader in = null;
        String result = "";
        try {
            URL realUrl = new URL(telQueryUrl);
            // 打开和URL之间的连接
            URLConnection conn = realUrl.openConnection();
            // 设置通用的请求属性
            conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent",
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");
            // 发送POST请求必须设置如下两行
            conn.setDoOutput(true);
            conn.setDoInput(true);
            // 获取URLConnection对象对应的输出流
            out = new PrintWriter(conn.getOutputStream());
            // 发送请求参数
            out.print(param);
            // flush输出流的缓冲
            out.flush();
            // 定义BufferedReader输入流来读取URL的响应
            in = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = in.readLine()) != null) {
                result += line;
            }
        } catch (Exception e) {
            System.out.println("发送 POST 请求出现异常！" + e);
            e.printStackTrace();
        }
        // 使用finally块来关闭输出流、输入流
        finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }
}


