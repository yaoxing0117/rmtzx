package com.rmtzx.manager;

import com.rmtzx.dao.yqdao.YqfxMapper;
import com.rmtzx.entity.yqfxentity.EnergyEntity;
import com.rmtzx.entity.yqfxentity.StatisticsEntity;
import com.rmtzx.entity.yqfxentity.ThesaurusEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class YqfxQuartz {
    @Autowired
    private YqfxMapper yqfxMapper;

    //定时写入抓去数据
    @Scheduled(cron = "30 13 * * * ?")
    public void all() {
        log.info("---定时写入抓取数据统计执行---");
        int bzNumber = yqfxMapper.bzPaperPageCount();
        int wxNumber = yqfxMapper.wxPaperPageCount();
        int zNumber = yqfxMapper.zPaperPageCount();
        int zyNumber = yqfxMapper.zyPaperPageCount();
        int zzqNumber = yqfxMapper.zzqPaperPageCount();
        int qtNumber = yqfxMapper.qtPaperPageCount();
        int nyNumber = yqfxMapper.nyPaperPageCount();
        int zdNumber = yqfxMapper.zdPaperPageCount();
        int wbNumber = yqfxMapper.wbAll();
        int wzNumber = zNumber + zyNumber + zdNumber + zzqNumber;
        int orderNumber = yqfxMapper.allSelect() + 1;
        List<StatisticsEntity> allls = yqfxMapper.allls();
        int orderNmber1 = allls.get(0).getZongNumber();
        int orderNmber2 = allls.get(1).getZongNumber();
        int dangNumber = orderNmber1 - orderNmber2;
        int zongNumber = wxNumber + nyNumber + qtNumber + bzNumber + zzqNumber + zyNumber + zNumber + zdNumber + wbNumber;//历史
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        int i = yqfxMapper.allPaperPageCount(
                wxNumber,
                nyNumber,
                qtNumber,
                bzNumber,
                zzqNumber,
                zyNumber,
                zNumber,
                zongNumber,
                orderNumber,
                dangNumber,
                zdNumber,
                wzNumber, time);
        log.info("---写入抓取数据结束---" + i);
    }

    //定时写入热度
    @Scheduled(cron = "30 10 * * * ?")
    public void addSelect() {
        log.info("---定时执行热度查询---");
        String thesauruOne = "";
        List<ThesaurusEntity> thesaurusEntities = yqfxMapper.allThe();
        for (ThesaurusEntity thesaurusEntity : thesaurusEntities) {
            thesauruOne = thesaurusEntity.getThesauruOne();
            int i1 = yqfxMapper.nyallSelect(thesauruOne);
            int i2 = yqfxMapper.wxallSelect(thesauruOne);
            int i3 = yqfxMapper.zallSelect(thesauruOne);
            int i4 = yqfxMapper.zyallSelect(thesauruOne);
            int i5 = yqfxMapper.zzqallSelect(thesauruOne);
            int i6 = yqfxMapper.bzallSelect(thesauruOne);
            int i7 = yqfxMapper.qtallSelect(thesauruOne);
            int heat = i1 + i2 + i3 + i4 + i5 + i6 + i7;
            int i = yqfxMapper.addSelect(heat, thesauruOne);
        }
        log.info("---热度查询数据结束---");
    }

    @Scheduled(cron = "40 10 1 * * ?")//每天23点50分30秒触发任务
    public void alla() {
        log.info("能源表总量查询开始");
        int mtEnergy = yqfxMapper.nybOne("煤炭");
        int nyEnergy = yqfxMapper.nybOne("新能源");
        int syEnergy = yqfxMapper.nybOne("石油");
        int dlEnergy = yqfxMapper.nybOne("电力");
        int trqEnergy = yqfxMapper.nybOne("天然气");
        int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量
        yqfxMapper.tjAll(9, mtEnergy, nyEnergy, syEnergy, dlEnergy, trqEnergy, zongEnergy);
        log.info("能源表总量查询结束");
    }

    @Scheduled(cron = "30 10 1 * * ?")
    public void alle() {
        Date date = new Date();
        SimpleDateFormat dateFm = new SimpleDateFormat("EEEE");
        String currSun = dateFm.format(date);
        log.info("每日能源查询执行");
        if (currSun.equals("星期一")) {
            log.info("星期一");
            //周一清除数据
            yqfxMapper.zongUpdate(8, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(1, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(2, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(3, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(4, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(5, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(6, 0, 0, 0, 0, 0, 0);
            yqfxMapper.zongUpdate(7, 0, 0, 0, 0, 0, 0);
            //查询前一天更新数据
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量
            //查询历史总量
            EnergyEntity energyEntity = yqfxMapper.nyAll(9);
            // 当日总量-历史总量=当天更新
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(1, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            yqfxMapper.zongUpdate(8, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
        } else if (currSun.equals("星期二")) {
            log.info("星期二");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(2, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        } else if (currSun.equals("星期三")) {
            log.info("星期三");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(3, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        } else if (currSun.equals("星期四")) {
            log.info("星期四");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(4, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        } else if (currSun.equals("星期五")) {
            log.info("星期五");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(5, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        } else if (currSun.equals("星期六")) {
            log.info("星期六");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(6, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        } else if (currSun.equals("星期天")) {
            log.info("星期天");
            int mtEnergy = yqfxMapper.nybOne("煤炭");
            int nyEnergy = yqfxMapper.nybOne("新能源");
            int syEnergy = yqfxMapper.nybOne("石油");
            int dlEnergy = yqfxMapper.nybOne("电力");
            int trqEnergy = yqfxMapper.nybOne("天然气");
            int zongEnergy = mtEnergy + nyEnergy + syEnergy + dlEnergy + trqEnergy;//总量

            EnergyEntity energyEntity = yqfxMapper.nyAll(9);//查询总量
            int mtEnergy1 = mtEnergy - energyEntity.getMtEnergy();
            int nyEnergy1 = nyEnergy - energyEntity.getNyEnergy();
            int syEnergy1 = syEnergy - energyEntity.getSyEnergy();
            int dlEnergy1 = dlEnergy - energyEntity.getDlEnergy();
            int trqEnergy1 = trqEnergy - energyEntity.getTrqEnergy();
            int zongEnergy1 = zongEnergy - energyEntity.getZongEnergy();
            yqfxMapper.zongUpdate(7, mtEnergy1, nyEnergy1, syEnergy1, dlEnergy1, trqEnergy1, zongEnergy1);
            //写入本周抓取量
            EnergyEntity energyEntity1 = yqfxMapper.nyAll(8);
            int mtEnergy2 = mtEnergy1 + energyEntity1.getMtEnergy();
            int nyEnergy2 = nyEnergy1 + energyEntity1.getNyEnergy();
            int syEnergy2 = syEnergy1 + energyEntity1.getSyEnergy();
            int dlEnergy2 = dlEnergy1 + energyEntity1.getDlEnergy();
            int trqEnergy2 = trqEnergy1 + energyEntity1.getTrqEnergy();
            int zongEnergy2 = zongEnergy1 + energyEntity1.getZongEnergy();
            yqfxMapper.zongUpdate(8, mtEnergy2, nyEnergy2, syEnergy2, dlEnergy2, trqEnergy2, zongEnergy2);
        }
        log.info("每日能源查询结束");
    }
}
