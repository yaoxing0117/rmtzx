package com.rmtzx.controller.yqfxcontroller;

import com.rmtzx.dao.yqdao.YQZdxwServiceImpl;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.TweetsServiceImpl;
import com.rmtzx.service.yqfxservice.impl.YqfxServiceImpl;
import com.rmtzx.util.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags = "舆情")
@RequestMapping("/yq")
public class YqfxController {
    @Autowired
    private YQZdxwServiceImpl zdxwService;
    @Autowired
    private YqfxServiceImpl yqfxService;
    @Autowired
    private TweetsServiceImpl tweetsService;
    @Autowired
    private RedisUtils redisUtils;

    //统计数量
    @ApiOperation(value = "统计数量", notes = "数据展示")
    @RequestMapping(value = "/allStatistics", method = RequestMethod.POST)
    public Result allStatistics() {
        return Result.success(yqfxService.allStatistics());
    }

    @ApiOperation(value = "前一天统计数量", notes = "数据展示")
    @RequestMapping(value = "/allStatisticszuo", method = RequestMethod.POST)
    public Result allStatisticszuo() {
        return Result.success(yqfxService.allStatisticszuo());
    }

    //热词搜索排行
    @ApiOperation(value = "热词搜索排行", notes = "数据展示")
    @RequestMapping(value = "/allSelect", method = RequestMethod.POST)
    public Result allSelect() {

        return Result.success(yqfxService.allSelects());
    }

    @ApiOperation(value = "点击热评", notes = "数据展示")
    @RequestMapping(value = "/clicks", method = RequestMethod.POST)
    public Result clicks() {
        return Result.success(zdxwService.zuigaodianji());
    }

    @ApiOperation(value = "转载热评", notes = "数据展示")
    @RequestMapping(value = "/reload", method = RequestMethod.POST)
    public Result reload() {
        return Result.success(zdxwService.zuigaozhuanzai());
    }

    @ApiOperation(value = "评论最多", notes = "数据展示")
    @RequestMapping(value = "/zuigaopinglun", method = RequestMethod.POST)
    public Result zuigaopinglun() {
        return Result.success(zdxwService.zuigaopinglun());
    }

    @ApiOperation(value = "准东热讯展示", notes = "数据展示")
    @RequestMapping(value = "/zdAll", method = RequestMethod.POST)
    public Result zdAll() {
        return Result.success(zdxwService.zdAll());
    }

    @ApiOperation(value = "能源统计", notes = "数据展示")
    @RequestMapping(value = "/nyAll", method = RequestMethod.POST)
    public Result nyAll() {
        return Result.success(yqfxService.nyAll());
    }

    @ApiOperation(value = "热点新闻", notes = "数据展示")
    @RequestMapping(value = "/tweetsService", method = RequestMethod.POST)
    public Result tweetsService() {
        Object tweets = redisUtils.lGet("Tweets", 0, 0);
        return Result.success(tweets);
    }


}
