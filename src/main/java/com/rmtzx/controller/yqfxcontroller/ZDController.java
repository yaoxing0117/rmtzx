package com.rmtzx.controller.yqfxcontroller;

import com.rmtzx.dao.yqdao.YQZdxwServiceImpl;
import com.rmtzx.entity.bo.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="准东点击量统计")
@RequestMapping("/djl")
public class ZDController {
    @Autowired
    private YQZdxwServiceImpl zdxwService;

    @ApiOperation(value = "点击量",notes="数据展示")
    @RequestMapping(value = "/clicks",method = RequestMethod.POST)
    public Result clicks(int id) {
        return Result.success(zdxwService.clicks(id)) ;
    }
    @ApiOperation(value = "转载量",notes="数据展示")
    @RequestMapping(value = "/reload",method = RequestMethod.POST)
    public Result reload(int id) {
        return Result.success(zdxwService.reload(id)) ;
    }
    @ApiOperation(value = "总量（评论，点击，转载）",notes="数据展示")
    @RequestMapping(value = "/commentDJ",method = RequestMethod.POST)
    public Result commentDJ() {
        return Result.success(zdxwService.commentAll()) ;
    }


}
