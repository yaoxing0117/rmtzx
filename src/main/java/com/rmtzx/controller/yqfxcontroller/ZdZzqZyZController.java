package com.rmtzx.controller.yqfxcontroller;

import com.rmtzx.dao.yqdao.YQZdxwServiceImpl;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZdxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZyxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZzqxwServiceImpl;
import com.rmtzx.service.webservice.ZdtsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="H5页面返回数据")
@RequestMapping("/fxx")
public class ZdZzqZyZController {
    @Autowired
    private ZdxwServiceImpl zdxwService;
    @Autowired
    private ZzqxwServiceImpl zzqxwService;
    @Autowired
    private ZyxwServiceImpl zyxwService;
    @Autowired
    private ZxwServiceImpl zxwService;
    @Autowired
    private YQZdxwServiceImpl zService;
    @ApiOperation(value = "详情",notes="数据展示")
    @RequestMapping(value = "/oneZt",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类(ZDXW,ZZQXW,ZYXW,ZXW)", dataType = "String"),
            @ApiImplicitParam(name = "id", value = "新闻id", dataType = "id"),
    })
    public Result oneZt(int id,String category){

        if (category.equals("ZDXW")){
            zService.clicks(id);
         return Result.success(   zdxwService.singleDate(id));
        }else if (category.equals("ZZQXW")){
            return Result.success(zzqxwService.singleDate(id));
        }else if (category.equals("ZYXW")){
            return Result.success(zyxwService.singleDate(id));
        }else if (category.equals("ZXW")){
            return Result.success(zxwService.singleDate(id));
        }
        return Result.failure(ResultCode.FAIL,"新闻不存在");
    }

}
