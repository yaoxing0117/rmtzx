package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.webservice.Impl.ZtServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="专题")
@RequestMapping("/ztzd")
public class Ztcontroller {
    @Autowired
    private ZtServiceImpl ztService;

    @ApiOperation(value = "详情数据展示",notes = "")
    @RequestMapping(value = "/oneZt",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result oneZt(int id){

        return Result.success(ztService.oneZt(id));
    }
    @ApiOperation(value = "类别分页展示",notes="数据展示")
    @RequestMapping(value = "/allZt",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "String")
    })
    public Result allZt(String category, int pageNo, int pageSize){
        return Result.success(ztService.allZt(category,pageNo, pageSize));
    }

}
