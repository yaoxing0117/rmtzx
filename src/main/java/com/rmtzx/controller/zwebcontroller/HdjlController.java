package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.entity.webentity.UserInteractionEntity;
import com.rmtzx.service.webservice.Impl.UserInteractionServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@ResponseBody
@Api(tags ="PC互动交流")
@RequestMapping("/hdjl")
public class HdjlController {
    @Autowired
    private UserInteractionServiceImpl userInte;
    @ApiOperation(value = "提交信件",notes="提交")
    @RequestMapping(value = "/insertInteraction",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "姓名", dataType = "String"),
            @ApiImplicitParam(name = "userDq", value = "地区", dataType = "String"),
            @ApiImplicitParam(name = "userTel", value = "手机号", dataType = "String"),
            @ApiImplicitParam(name = "userNr", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "userZt", value = "主题", dataType = "String"),
            // @ApiImplicitParam(name = "userLb", value = "类别", dataType = "String"),
            @ApiImplicitParam(name = "userXjlb", value = "信件类别", dataType = "String")
    })
    public Result insertInteraction(String userName, String userDq, String userTel,
                                    String userNr,
                                    String userZt,
                                    String userXjlb) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String userTjtime = df.format(new Date());// new Date()为获取当前系统时间
        int i = userInte.insertInteraction(
                userName, userDq, userTel, userTjtime,
                userNr, userZt,userXjlb);
        if (i!=0){
            return Result.success();
        }
        return Result.failure(ResultCode.FAIL) ;
    }
    @ApiOperation(value = "分页展示",notes="数据展示")
    @RequestMapping(value = "/allUserI",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userXjlb", value = "类别", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result allUserI(String userXjlb, int pageNo, int pageSize) {

        return Result.success(userInte.allUserI(userXjlb,pageNo, pageSize));
    }
    @ApiOperation(value = "查询信件",notes="数据展示")
    @RequestMapping(value = "/allSelect",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "联系人", dataType = "String"),
            @ApiImplicitParam(name = "userXjlb", value = "类别", dataType = "String"),
            @ApiImplicitParam(name = "userZt", value = "主题", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "String")
    })
    public Result allSelect(String userName,
                            String userXjlb,
                            String userZt ,
                            int pageNo,
                            int pageSize) {
        if (userName==null&&userXjlb==null){
            Result.failure(ResultCode.FAIL,"联系人，类别，主题 不能为空");
        }
        ShowInfoWeb showInfoWeb = userInte.allSelect(userName, userXjlb, userZt, pageNo, pageSize);
        return Result.success(showInfoWeb) ;
    }
    @ApiOperation(value = "详情数据展示",notes = "")
    @RequestMapping(value = "/oneUser",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result oneUser(int id) {
     return Result.success(userInte.oneUser(id));
    }

}
