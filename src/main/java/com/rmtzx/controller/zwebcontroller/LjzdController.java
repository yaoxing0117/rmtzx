package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.webservice.Impl.LjzdServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="了解准东-产业促进")
@RequestMapping("/ljzd")
public class LjzdController {
    @Autowired
    private LjzdServiceImpl ljzdService;
    @ApiOperation(value = "详情返回",notes="数据展示")
    @RequestMapping(value = "/oneLjzd",method = RequestMethod.POST)
    public Result oneLjzd(String category) {
        return Result.success(ljzdService.oneLjzd(category));
    }
    @ApiOperation(value = "1了解准东列表返回",notes="数据展示")
    @RequestMapping(value = "/allLjzd",method = RequestMethod.POST)
    public Result allLjzd() {
        return Result.success(ljzdService.allLjzd());
    }
    @ApiOperation(value = "2了解准东列表返回",notes="数据展示")
    @RequestMapping(value = "/allCycj",method = RequestMethod.POST)
    public Result allCycj() {
        return Result.success(ljzdService.allCycj());
    }
    @ApiOperation(value = "发展规划.投资环境.主导产业",notes="数据展示")
    @RequestMapping(value = "/allCycj3",method = RequestMethod.POST)
    public Result allCycj3() {
        return Result.success(ljzdService.allCycj3());
    }
    @ApiOperation(value = "投资返回列表数据",notes="数据展示")
    @RequestMapping(value = "/allTZ",method = RequestMethod.POST)
    public Result allTZ(String category, int pageNo, int pageSize) {
        return Result.success(ljzdService.allTZ(category, pageNo, pageSize));
    }

    @ApiOperation(value = "投资返回详情数据",notes="数据展示")
    @RequestMapping(value = "/oneTZ",method = RequestMethod.POST)
    public Result oneTZ(int id) {
        return Result.success(ljzdService.oneTZ(id));
    }
    @RequestMapping(value = "/investment",method = RequestMethod.POST)
    @ApiOperation(value = "我要投资",notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "investmentUser", value = "用户", dataType = "String"),
            @ApiImplicitParam(name = "investmentTel", value = "手机号", dataType = "String"),
            @ApiImplicitParam(name = "investmentMail", value = "邮箱", dataType = "String"),
            @ApiImplicitParam(name = "investmentWx", value = "微信", dataType = "String"),
            @ApiImplicitParam(name = "investmentBz", value = "备注", dataType = "String")
    })
    public Result investment(String investmentUser,
                          String investmentTel,
                          String investmentMail,
                          String investmentWx,
                          String investmentBz) {
  return Result.success(
          ljzdService.investment(
                  investmentUser, investmentTel, investmentMail, investmentWx, investmentBz));
   }

}
