package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;

import com.rmtzx.service.webservice.Impl.ZwServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="PC政务分类")
@RequestMapping("/zw")
public class ZwController {
    @Autowired
    private ZwServiceImpl zwService;
    @ApiOperation(value = "分类返回",notes="数据展示")
    @RequestMapping(value = "/allClass",method = RequestMethod.POST)
    public Result allClass(){
       return Result.success(zwService.allClass());
    }
//    @ApiOperation(value = "分类返回1",notes="数据展示")
//    @RequestMapping(value = "/allClass1",method = RequestMethod.POST)
//    public Result allClass1(){
//       return Result.success(zwService.allClass1());
//    }
    @ApiOperation(value = "详情数据展示PC",notes = "")
    @RequestMapping(value = "/details",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result details(@RequestParam("id")int id){

        return Result.success(zwService.details(id));
    }
    @ApiOperation(value = "分页展示",notes="数据展示")
    @RequestMapping(value = "/allList",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cid", value = "分类id", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "String")
    })
    public Result allList(int cid, int pageNo, int pageSize){
        return Result.success(zwService.allList(cid,pageNo, pageSize));
    }

}
