package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.webservice.Impl.HeadLineServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="头条接口")
@RequestMapping("/tt")
public class HeadLineController {
    @Autowired
    private HeadLineServiceImpl headLineService;

    @ApiOperation(value = "头条展示",notes="数据展示")
    @RequestMapping(value = "/oneHeadLine",method = RequestMethod.POST)
    public Result oneHeadLine(){
        return Result.success(headLineService.oneHeadLine());
    }


}
