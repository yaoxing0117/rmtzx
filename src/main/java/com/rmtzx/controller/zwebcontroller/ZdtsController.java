package com.rmtzx.controller.zwebcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.webservice.ZdtsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="PC+app图说准东+微视频")
@RequestMapping("/tszd_sp")
public class ZdtsController {
    @Autowired
    private ZdtsService zdtsService;

    @ApiOperation(value = "详情数据展示",notes = "")
    @RequestMapping(value = "/oneZdts",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result oneZdts(int id){

        return Result.success(zdtsService.oneZdts(id));
    }
    @ApiOperation(value = "app分页展示",notes="数据展示")
    @RequestMapping(value = "/allZdts",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类(1.图说准东,2.微视频,3.全部)", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "String")
    })
    public Result allZdts(String category, int pageNo, int pageSize){
        return Result.success(zdtsService.allZdts(category,pageNo, pageSize));
    }

    @ApiOperation(value = "PC分页展示",notes="数据展示")
    @RequestMapping(value = "/allZdtss",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类(1.图说准东,2.微视频,3.全部,4.宣传图片)", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "String")
    })
    public Result allZdtss(int category,int pageNo, int pageSize){
        if (category==1){
            return Result.success(zdtsService.allZdts("图片",pageNo,pageSize));
        }else if (category==2){
            return Result.success(zdtsService.allZdts("视频",pageNo,pageSize));
        }else if (category==3) {
            return Result.success(zdtsService.allZdtss(pageNo, pageSize));
        }else if (category==4) {
            return Result.success(zdtsService.allZdts("宣传图片",pageNo, pageSize));
        }
        return Result.failure(ResultCode.FAIL);
    }


}
