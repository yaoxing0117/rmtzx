package com.rmtzx.controller.storecontroller;

import com.alibaba.fastjson.JSON;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.store.StoreCommentEntity;
import com.rmtzx.entity.store.StoreDishEntity;
import com.rmtzx.entity.store.StoreOrderEntity;
import com.rmtzx.entity.store.StoreTakeawayEntity;
import com.rmtzx.service.storeservice.StoreCommentService;
import com.rmtzx.service.storeservice.StoreDishService;
import com.rmtzx.service.storeservice.StoreOrderService;
import com.rmtzx.service.storeservice.StoreTakeawayService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Controller
@ResponseBody
@Api(tags = "外卖_app")
@RequestMapping("/store")
@Slf4j
public class StoreTakeawayController {
    @Autowired
    private StoreTakeawayService storeTakeawayService;
    @Autowired
    private StoreDishService storeDishService;
    @Autowired
    private StoreOrderService storeOrderService;
    @Autowired
    private StoreCommentService storeCommentService;

    @ApiOperation(value = "外卖店家列表展示", notes = "数据展示")
    @RequestMapping(value = "/storeSelect", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result storeSelect(int pageNo, int pageSize) {
        return Result.success(storeTakeawayService.selectAll(pageNo, pageSize));
    }

    @ApiOperation(value = "外卖店家菜品列表展示", notes = "数据展示")
    @RequestMapping(value = "/dishOne", method = RequestMethod.POST)
    @ApiImplicitParam(name = "pid", value = "店家ID", dataType = "int")
    public Result dishOne(int pid) {
        return Result.success(storeDishService.selectAll(pid));
    }

    @ApiOperation(value = "外卖店家详情展示", notes = "数据展示")
    @RequestMapping(value = "/storeOne", method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "店家详情", dataType = "int")
    public Result storeOne(int id) {
        return Result.success(storeTakeawayService.selectOne(id));
    }

    @ApiOperation(value = "外卖下单", notes = "数据添加")
    @RequestMapping(value = "/insertOrder", method = RequestMethod.POST)
    @ApiImplicitParam(name = "storeOrderEntities", value = "订单数据", dataType = "String")
    public Result insertOrder(@RequestParam(value = "storeOrderEntities")  String storeOrderEntitie) {
        List<StoreOrderEntity> storeOrderEntities = JSON.parseArray(storeOrderEntitie, StoreOrderEntity.class);
        System.out.println(storeOrderEntities);
        int orderTotal = 0;
        for (StoreOrderEntity e : storeOrderEntities) {
            orderTotal = (e.getDishNumber() * e.getDishMoney()) + orderTotal;
        }
        return Result.success(storeOrderService.insertOrder(storeOrderEntities, orderTotal));
    }

    @ApiOperation(value = "用户订单查询", notes = "数据展示")
    @RequestMapping(value = "/selectOrder", method = RequestMethod.POST)
    @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int")
    public Result selectOrder(int userId) {
        return Result.success(storeOrderService.selectOrder(userId));
    }

    @ApiOperation(value = "外卖取消订单", notes = "数据添加")
    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orderState", value = "订单状态（传参:取消订单）", dataType = "String"),
            @ApiImplicitParam(name = "pid", value = "对应id同为pid", dataType = "int")
    })
    public Result updateOrder(String orderState, int pid) {
        return Result.success(storeOrderService.updateOrder(orderState, pid));
    }

    @ApiOperation(value = "用户订单评论", notes = "数据添加")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentCategory", value = "评论类型", dataType = "String"),
            @ApiImplicitParam(name = "commentContent", value = "评论内容", dataType = "String"),
            // @ApiImplicitParam(name = "commentPicture", value = "评论图片", dataType = "String"),
            @ApiImplicitParam(name = "commentTime", value = "评论时间", dataType = "String"),
            @ApiImplicitParam(name = "commentStar", value = "评论星级", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "pid", value = "订单id", dataType = "int"),
            @ApiImplicitParam(name = "storeId", value = "店铺id", dataType = "int")
    })
    public Result update(String commentCategory, String commentContent,
                         MultipartFile file, String commentTime, int commentStar, int userId, int pid, int storeId) {
        if (file == null) {
            return Result.success(storeCommentService.insertnull(commentCategory, commentContent, commentTime, commentStar, userId, pid, storeId));
        } else {
            return Result.success(storeCommentService.insertComment(commentCategory, commentContent, file, commentTime, commentStar, userId, pid, storeId));
        }
    }

    @ApiOperation(value = "用户订单评论查询", notes = "数据查询")
    @RequestMapping(value = "/selectComment", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentCategory", value = "评论类型", dataType = "String"),
            @ApiImplicitParam(name = "pid", value = "订单id", dataType = "int")
    })
    public Result selectComment(String commentCategory, int pid) {
        return Result.success(storeCommentService.selectComment(commentCategory, pid));
    }

    @ApiOperation(value = "店铺or菜名模糊查询", notes = "数据展示")
    @RequestMapping(value = "/selectAllStore", method = RequestMethod.POST)
    @ApiImplicitParam(name = "shopOwner", value = "店名或菜名", dataType = "String")
    public Result selectAllStore(String shopOwner) {
        //差菜名搜索
        List<StoreTakeawayEntity> storeTakeawayEntities = storeTakeawayService.selectAllStore(shopOwner);
        System.out.println(storeTakeawayEntities);
        List<StoreDishEntity> storeDishEntities = storeDishService.selectAlldish(shopOwner);
        if (storeTakeawayEntities.isEmpty() && storeDishEntities.isEmpty()) {
            return Result.failure(ResultCode.PARAMETER_ERROR);
        }
        if (storeTakeawayEntities.isEmpty() == false) {
            log.info("店铺查询");
            return Result.success(storeTakeawayEntities);
        } else {
            log.info("菜名查询");
            return Result.success(storeDishEntities);
        }
    }

    @ApiOperation(value = "菜名模糊查询", notes = "数据展示")
    @RequestMapping(value = "/selectAllDishStore", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dishName", value = "菜名", dataType = "String"),
            @ApiImplicitParam(name = "pid", value = "店铺id", dataType = "int")
    })
    public Result selectAllDishStore(String dishName, int pid) {
        List<StoreDishEntity> storeDishEntities = storeDishService.selectAllDishStore(dishName, pid);

        if (storeDishEntities.isEmpty()) {
            return Result.failure(ResultCode.PARAMETER_ERROR);
        } else {
            return Result.success(storeDishEntities);
        }
    }

    @ApiOperation(value = "店铺评论展示接口", notes = "数据展示")
    @RequestMapping(value = "/selectCommentstore", method = RequestMethod.POST)
    @ApiImplicitParam(name = "storeId", value = "商家id", dataType = "int")
    public Result selectCommentstore(int storeId) {
        List<StoreCommentEntity> storeCommentEntities = storeCommentService.selectCommentstore(storeId);
        if (storeCommentEntities.isEmpty()) {
            return Result.success("无评论");
        }
        return Result.success(storeCommentEntities);

    }
}
