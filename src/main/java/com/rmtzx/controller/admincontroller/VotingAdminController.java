package com.rmtzx.controller.admincontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.adminservice.VotingAdminService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@Component
@Api(tags = "投票后台管理")
@ResponseBody
@RequestMapping("/tpadmin")
public class VotingAdminController {
    @Autowired
    private VotingAdminService service;

    @ApiOperation(value = "所有投票分页展示", notes = "数据展示")
    @RequestMapping(value = "/selectVotingAll", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectVotingAll(int pageNo, int pageSize) {
        return Result.success(service.selectVotingAll(pageNo, pageSize));
    }

    @ApiOperation(value = "添加投票", notes = "数据添加")
    @RequestMapping(value = "/insertOne", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", dataType = "String"),
            @ApiImplicitParam(name = "picture", value = "照片", dataType = "String"),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "stopDate", value = "起止日期", dataType = "String"),
            @ApiImplicitParam(name = "votingCategory", value = "投票类别(1生活投票,2服务投票)", dataType = "int"),
            @ApiImplicitParam(name = "tpRemarks", value = "备注", dataType = "String"),
            @ApiImplicitParam(name = "codePicture", value = "投票图片状态(0不开启,1开启)", dataType = "int")
    })
    public Result insertOne(String title, String picture, String content, String stopDate, int votingCategory, String tpRemarks, int codePicture) {
        Date day = new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String startDate = time.format(day);
        int i = service.insertOne(title, picture, content, startDate, stopDate, votingCategory, tpRemarks, codePicture);
        if (i == 2) {
            return Result.failure(ResultCode.NOT_FIND2);
        }
        return Result.success(i);
    }

    @ApiOperation(value = "修改投票内容", notes = "数据添加")
    @RequestMapping(value = "/updateVoting", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", dataType = "String"),
            @ApiImplicitParam(name = "picture", value = "照片", dataType = "String"),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "stopDate", value = "起止日期", dataType = "String"),
            @ApiImplicitParam(name = "votingCategory", value = "投票类别(1生活投票,2服务投票)", dataType = "int"),
            @ApiImplicitParam(name = "tpRemarks", value = "备注", dataType = "String"),
            @ApiImplicitParam(name = "id", value = "修改id", dataType = "int")
    })
    public Result updateVoting(String title, String picture, String content, String stopDate, int votingCategory, String tpRemarks, int id) {
        int i = service.updateVoting(title, picture, content, stopDate, votingCategory, tpRemarks, id);
        return Result.success(i);
    }

    @ApiOperation(value = "数据删除", notes = "数据展示")
    @RequestMapping(value = "/deleteId", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", dataType = "id"),
            @ApiImplicitParam(name = "type", value = "0投票删除 1投票选项删除", dataType = "int")

    })
    public Result deleteId(int id, int type) {
        if (type == 0) {
            int i = service.deleteId(id);
            return Result.success(i);
        } else {
            int i = service.deletePid(id);
            if (i == 0) {
                return Result.failure(ResultCode.FAIL);
            }
            return Result.success(i);
        }
    }

    @ApiOperation(value = "投票模糊查询分页展示", notes = "数据展示")
    @RequestMapping(value = "/selectMH", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "votingCategory", value = "(0全部,1生活投票,2服务投票)", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectTitle(String title, int votingCategory, int pageNo, int pageSize) {
        if (votingCategory != 1 && votingCategory != 0 && votingCategory != 2) {
            return Result.failure(ResultCode.INVALID_Entitie);
        }
        if (null == title || "".equals(title)) {
            return Result.success(service.selectMHw(votingCategory, pageNo, pageSize));
        } else {
            return Result.success(service.selectMH(title, votingCategory, pageNo, pageSize));
        }
    }

    @ApiOperation(value = "数据删除", notes = "数据展示")
    @RequestMapping(value = "/updateCode", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "0未启用1启用", dataType = "id"),
            @ApiImplicitParam(name = "id", value = "id", dataType = "int")

    })
    public Result updateCode(int code, int id) {
        if (code != 1 && code != 0 && code != 2) {
            return Result.failure(ResultCode.INVALID_Entitie);
        }
        int i = service.updateCode(code, id);
        if (i == 0) {
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(i);
    }


}
