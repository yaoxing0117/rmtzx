package com.rmtzx.controller.admincontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.adminservice.UserGrslService;
import com.rmtzx.service.adminservice.impl.UserGrslServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@ResponseBody
@Api(tags = "申领中心后台管理")
@RequestMapping("/grsl")
public class UserGrslController {
    @Autowired
    private UserGrslServiceImpl userGrslService;

    @ApiOperation(value = "申领展示", notes = "数据展示")
    @RequestMapping(value = "/selectAll", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectQuery(int pageNo, int pageSize) {
        return Result.success(userGrslService.selectAll(pageNo, pageSize));
    }

    @ApiOperation(value = "修改数据状态", notes = "数据展示")
    @RequestMapping(value = "/updateCode", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "状态（已停用 / 已启用；用0 / 1 表示）", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "修改id", dataType = "int")
    })
    public Result updateCode(int id, int code) {
        if (code != 1 && code != 0) {
            return Result.failure(ResultCode.INVALID_Entitie);
        }
        int i = userGrslService.updateCode(id, code);
        if (i == 0) {
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success();
    }

    @ApiOperation(value = "申领添加", notes = "数据添加")
    @RequestMapping(value = "/insertGrsl", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "标题", dataType = "int"),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "slRemarks", value = "备注", dataType = "String")
    })
    public Result insertGrsl(String title, String content, String slRemarks) {
        Date day = new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String cjTime = time.format(day);
        int i = userGrslService.insertGrsl(title, content, slRemarks, cjTime);
        if (i == 0) {
            Result.failure(ResultCode.FAIL);
        }
        return Result.success(i);
    }

    @ApiOperation(value = "模糊查询分页展示", notes = "数据展示")
    @RequestMapping(value = "/selectTitle", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "title", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectTitle(String title, int pageNo, int pageSize) {
        if (userGrslService.selectTitle(title, pageNo, pageSize) == null) {
            return Result.failure(ResultCode.INVALID_PARAM1);
        }
        return Result.success(userGrslService.selectTitle(title, pageNo, pageSize));
    }

    @ApiOperation(value = "修改申领内容", notes = "数据展示")
    @RequestMapping(value = "/updateNR", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", dataType = "id"),
            @ApiImplicitParam(name = "title", value = "标题", dataType = "String"),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "slRemarks", value = "备注", dataType = "String")
    })
    public Result updateNR(int id, String title, String content, String slRemarks) {
        return Result.success(userGrslService.updateNR(id, title, content, slRemarks));
    }

    @ApiOperation(value = "数据删除", notes = "数据展示")
    @RequestMapping(value = "/deleteOne", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "id", dataType = "id"),
            @ApiImplicitParam(name = "type", value = "0表示申领删除 1表示申领人删除", dataType = "int")

    })
    public Result deleteOne(int id, int type) {
        int i = userGrslService.deleteOne(id, type);
        if (i == 0) {
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(i);
    }

    @ApiOperation(value = "查询申领人展示", notes = "数据展示")
    @RequestMapping(value = "/selectSL", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pid", value = "对应申领id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectSL(int pid, int pageNo, int pageSize) {
        return Result.success(userGrslService.selectSL(pid, pageNo, pageSize));
    }

    @ApiOperation(value = "申领人添加", notes = "数据添加")
    @RequestMapping(value = "/insertSl", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userUser", value = "姓名", dataType = "String"),
            @ApiImplicitParam(name = "userId", value = "身份证号码", dataType = "String"),
            @ApiImplicitParam(name = "slRemarks", value = "备注", dataType = "String"),
            @ApiImplicitParam(name = "pid", value = "申领id", dataType = "int")
    })
    public Result insertSl(String userUser, String userId, String slRemarks, int pid) {
        Date day = new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lqTime = time.format(day);
        int i = userGrslService.insertSl(userUser, userId, slRemarks, pid, lqTime);
        if (i == 2) {
            return Result.failure(ResultCode.NOT_FIND2);
        }
        return Result.success(i);
    }

    @ApiOperation(value = "修改申领人数据状态", notes = "数据展示")
    @RequestMapping(value = "/updateSLR", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "状态（未领取 / 已领取；用0 / 1 表示）", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "修改id", dataType = "int")
    })
    public Result updateSLR(int id, int code) {
        Date day = new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lqTime = time.format(day);
        if (code != 1 && code != 0 && code != 2) {
            return Result.failure(ResultCode.INVALID_Entitie);
        }
        int i = userGrslService.updateSLR(code, lqTime, id);
        return Result.success(lqTime);
    }

    @ApiOperation(value = "申领人查询", notes = "数据展示")
    @RequestMapping(value = "/selectCodeSl", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pid", value = "对应申领id", dataType = "int"),
            @ApiImplicitParam(name = "code", value = "0 全部,1未领取,2已领取", dataType = "int"),
            @ApiImplicitParam(name = "userUser", value = "用户姓名", dataType = "String")
    })
    public Result selectCodeSl(int pid, int code, String userUser,int pageNo, int pageSize) {
        if (code != 1 && code != 0 && code != 2) {
            return Result.failure(ResultCode.INVALID_Entitie);
        }
        if(null==userUser||"".equals(userUser)){
            ShowInfo s=  userGrslService.selectCodeSlNoUser(pid, code,pageNo,pageSize);
            if (s == null) {
                return Result.failure(ResultCode.INVALID_PARAM1);
            }
            return Result.success(s);
        }
        ShowInfo s = userGrslService.selectCodeSl(pid, code, userUser,pageNo,pageSize);
        if (s == null) {
            return Result.failure(ResultCode.INVALID_PARAM1);
        }
        return Result.success(s);
    }
}
