package com.rmtzx.controller.admincontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.adminservice.AdminSystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@Controller
@ResponseBody
@Api(tags = "有奖举报后台")
@RequestMapping("/yjjb")
public class AdminSystemController {
    @Autowired
    private AdminSystemService adminSystemService;

    @ApiOperation(value = "模糊查询分页展示", notes = "数据展示")
    @RequestMapping(value = "/selectQuery", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "reportExamine", value = "状态（未处理/正在处理/已处理；用0/1/2表示 3全部）", dataType = "int"),
            @ApiImplicitParam(name = "content", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectQuery(int reportExamine, String content, int pageNo, int pageSize) {
        return Result.success(adminSystemService.selectQuery(reportExamine, content, pageNo, pageSize));
    }

    @ApiOperation(value = "分页展示", notes = "数据展示")
    @RequestMapping(value = "/selectAll", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "reportExamine", value = "状态（未处理/正在处理/已处理；用0/1/2表示 3全部）", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result selectAll(int reportExamine, int pageNo, int pageSize) {
//        String session1 = session.getSession().getId();
//        System.out.println(session1);
//        System.out.println(id);
        return Result.success(adminSystemService.selectAll(reportExamine, pageNo, pageSize));
    }

    @ApiOperation(value = "修改数据状态", notes = "数据展示")
    @RequestMapping(value = "/updateOne", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "reportExamine", value = "状态（未处理/正在处理/已处理；用0/1/2表示）", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "修改id", dataType = "int")
    })
    public Result updateOne(int reportExamine, int id) {
        if (reportExamine != 0 && reportExamine != 1 && reportExamine != 2) {
            return Result.failure(ResultCode.FAIL);
        }
        return Result.success(adminSystemService.updateOne(reportExamine, id));
    }


}
