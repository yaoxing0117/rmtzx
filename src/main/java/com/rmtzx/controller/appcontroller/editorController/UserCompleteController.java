//package com.rmtzx.controller.editorController;
//
//import com.sun.deploy.net.URLEncoder;
//import io.swagger.annotations.Api;
//import org.apache.commons.io.FileUtils;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//
//import javax.servlet.http.HttpServletRequest;
//import java.io.*;
//
//@Controller
//@Api(tags ="图片下载")
//@ResponseBody
//@RequestMapping("/corporation")
//public class UserCompleteController {
//    @RequestMapping(value = "/download",method = RequestMethod.POST)
//    public ResponseEntity<byte[]> fileDownload(String filename, HttpServletRequest request) throws IOException {
//        String path = request.getServletContext().getRealPath("/upload/");
//        File file = new File(path + filename);
//        //        System.out.println("转码前" + filename);
//        filename = this.getFilename(request, filename);
//        //        System.out.println("转码后" + filename);
//        // 设置响应头通知浏览器下载
//        HttpHeaders headers = new HttpHeaders();
//        // 将对文件做的特殊处理还原
//        filename = filename.substring(filename.indexOf("_") + 1);
//        headers.setContentDispositionFormData("attachment", filename);
//        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
//        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file), headers, HttpStatus.OK);
//    }
//
//    // 根据不同的浏览器进行编码设置，返回编码后的文件名
//    public String getFilename(HttpServletRequest request, String filename) throws UnsupportedEncodingException {
//        String[] IEBrowerKeyWords = {"MSIE", "Trident", "Edge"};
//        String userAgent = request.getHeader("User-Agent");
//        for (String keyword : IEBrowerKeyWords) {
//            if (userAgent.contains(keyword)) {
//                return URLEncoder.encode(filename, "UTF-8");
//            }
//        }
//        return new String(filename.getBytes("UTF-8"), "ISO-8859-1");
//    }
//
//}
