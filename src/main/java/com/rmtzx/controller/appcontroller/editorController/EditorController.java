//package com.rmtzx.controller.editorController;
//
//
//import com.rmtzx.util.FileUtils;
//import io.swagger.annotations.Api;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MaxUploadSizeExceededException;
//import org.springframework.web.multipart.MultipartFile;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.core.io.ResourceLoader;
//import org.apache.catalina.servlet4preview.http.HttpServletRequest;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//
//
//import java.io.*;
//import java.net.URLEncoder;
//import java.util.List;
//import java.util.UUID;
//
//
//
//
//@Controller
//@Api(tags ="图片上传")
//@ResponseBody
//@RequestMapping("/editor")
//@Slf4j
//public class EditorController {
//
//    private final ResourceLoader resourceLoader;
//
//    @Autowired
//    public EditorController(ResourceLoader resourceLoader) {
//        this.resourceLoader = resourceLoader;
//    }
//
//
//    @ResponseBody
//    @RequestMapping(value = "/fileUpload",method = RequestMethod.POST)
//    public String upload(@RequestParam("file")MultipartFile file ){
//        //1定义要上传文件 的存放路径
//        String localPath="D:/image";
//        //2获得文件名字
//        String fileName=file.getOriginalFilename();
//        //2上传失败提示
//        String warning="";
//        if(FileUtils.upload(file, localPath, fileName)){
//            //上传成功
//            warning="上传成功";
//
//        }else{
//            warning="上传失败";
//        }
//        System.out.println(warning);
//        return "上传成功";
//    }
//    @GetMapping(value = "/file")
//    public String file() {
//        return "file";
//    }
//
//    @PostMapping(value = "/fileUpload1")
//    public String fileUpload(@RequestParam(value = "file") MultipartFile file, Model model, HttpServletRequest request) {
//        if (file.isEmpty()) {
//            System.out.println("文件为空空");
//        }
//        String fileName = file.getOriginalFilename();  // 文件名
//        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
//        String filePath = "D://temp-rainy//"; // 上传后的路径
//        fileName = UUID.randomUUID() + suffixName; // 新文件名
//        System.out.println(fileName);
//        File dest = new File(filePath + fileName);
//        if (!dest.getParentFile().exists()) {
//            dest.getParentFile().mkdirs();
//        }
//        try {
//            file.transferTo(dest);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        String filename = "/temp-rainy/" + fileName;
//        model.addAttribute("filename", filename);
//        return "file";
//    }
//    @ExceptionHandler(MaxUploadSizeExceededException.class)
//    public String handException(MaxUploadSizeExceededException e, HttpServletRequest request) {
//        request.setAttribute("msg", "文件超过了指定大小，上传失败！");
//        return "fileupload";
//    }
//    @PostMapping(value = "/fileupload")
//    public String fileUpload(@RequestParam(value = "file") List<MultipartFile> files, HttpServletRequest request) {
//        String msg = "";
//        // 判断文件是否上传
//        if (!files.isEmpty()) {
//            // 设置上传文件的保存目录
//            String basePath = request.getServletContext().getRealPath("/upload/");
//            // 判断文件目录是否存在
//            File uploadFile = new File(basePath);
//            if (!uploadFile.exists()) {
//                uploadFile.mkdirs();
//            }
//            for (MultipartFile file : files) {
//                String originalFilename = file.getOriginalFilename();
//                if (originalFilename != null && !originalFilename.equals("")) {
//                    try {
//                        // 对文件名做加UUID值处理
//                        originalFilename = UUID.randomUUID() + "_" + originalFilename;
//                        file.transferTo(new File(basePath + originalFilename));
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                        msg = "文件上传失败！";
//                    }
//                } else {
//                    msg = "上传的文件为空！";
//                }
//            }
//            msg = "文件上传成功！";
//        } else {
//            msg = "没有文件被上传！";
//        }
//        request.setAttribute("msg", msg);
//        return "fileupload";
//    }
//    // 根据不同的浏览器进行编码设置，返回编码后的文件名
//    public String getFilename(HttpServletRequest request, String filename) throws UnsupportedEncodingException {
//        String[] IEBrowerKeyWords = {"MSIE", "Trident", "Edge"};
//        String userAgent = request.getHeader("User-Agent");
//        for (String keyword : IEBrowerKeyWords) {
//            if (userAgent.contains(keyword)) {
//                return URLEncoder.encode(filename, "UTF-8");
//            }
//        }
//        return new String(filename.getBytes("UTF-8"), "ISO-8859-1");
//    }
//
//
//}
