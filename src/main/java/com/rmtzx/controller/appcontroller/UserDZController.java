package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.appservice.UserDZService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="用户点赞接口")
@RequestMapping("/userDZ")
public class UserDZController {
    @Autowired
    private UserDZService userDZService;

    @ApiOperation(value = "用户点赞通用接口",notes="数据添加")
    @RequestMapping(value = "/insertDZ",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "newsTitle", value = "新闻标题", dataType = "String"),
            @ApiImplicitParam(name = "newsTime", value = "新闻时间", dataType = "String"),
            @ApiImplicitParam(name = "newsCategory", value = "新闻类型(ZZQXW,ZYXW,ZDXW,ZXW)", dataType = "String"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "newsId", value = "新闻id", dataType = "int")
    })
    public Result insertDZ(String newsTitle, String newsTime, String newsCategory, int userId, int newsId) {
        int i = userDZService.selectAll(userId, newsId,newsCategory);
        if (i==0){
            int i1 = userDZService.insDZ(newsCategory, newsId,userId);
            if (i1==0){
                return Result.failure(ResultCode.FAIL);
            }
            return Result.success(userDZService.insertDZ(newsTitle, newsTime, newsCategory, userId, newsId));
        }
        return Result.failure(ResultCode.DZ_FIND);
    }
    @ApiOperation(value = "用户点赞展示接口",notes="数据展示")
    @RequestMapping(value = "/selectAll",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
          @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页显示多少", dataType = "int")
    })
    public Result selectAll(int userId, int pageNo, int pageSize) {
        return Result.success(userDZService.selectAll(userId, pageNo, pageSize));
    }
}
