package com.rmtzx.controller.appcontroller;


import com.github.wxpay.sdk.WXPay;
import com.rmtzx.config.MyWXConfig;
import com.rmtzx.entity.appentity.userEntity.UserExpressEntity;
import com.rmtzx.entity.bo.CityMap;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.manager.KdGoldAPIDemoO;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.manager.KdniaoTrackQueryAPI;
import com.rmtzx.manager.ShoujiHuaFei;
import com.rmtzx.manager.ShoujiHuaFei1;
import com.rmtzx.service.appservice.Impl.UserExpressServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller

@Component
@Api(tags ="快递查询/预约取件/手机缴费")
@ResponseBody
@RequestMapping("/express")
public class ExpressController {
    //查询
    @Autowired
    private KdniaoTrackQueryAPI kdnAPI;
    //上门寄件
    @Autowired
    private KdGoldAPIDemoO kdGoldAPIDemo;
    @Autowired
    private UserExpressServiceImpl userExpressService;
    @Autowired
    private ShoujiHuaFei shoujiHuaFei;
    //用户快递展示接口
    @RequestMapping(value = "/allExpress",method = RequestMethod.POST)
    @ApiOperation(value = "用户快递展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userTel", value = "查询快递用户", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "int")
    })
    public Result allExpress(String userTel, int pageNo, int pageSize){
        ShowInfo showInfo = userExpressService.allExpress(userTel, pageNo, pageSize);
        return Result.success(showInfo);
    }
    @RequestMapping(value = "/expressInquiry", method = RequestMethod.POST)
    @ApiOperation(value = "快递查询", notes = "单号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "expCode", value = "快点公司", dataType = "String"),
            @ApiImplicitParam(name = "expNo", value = "单号", dataType = "String")
    })
    public Result expressInquiry(String expCode, String expNo) {
        String orderTracesByJson = null;
        try {
            orderTracesByJson = kdnAPI.getOrderTracesByJson(expCode, expNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Result.success(orderTracesByJson);
    }
    @RequestMapping(value = "/doorToDoorMail", method = RequestMethod.POST)
    @ApiOperation(value = "预约上门", notes = "订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "requestData", value = "订单详情Json", dataType = "String"),
    })
    public Result doorToDoorMail(String requestData) {
        try {
            String requestData1 = kdGoldAPIDemo.orderOnlineByJson(requestData);
            return Result.success(requestData1);
        } catch (Exception e) {
            e.printStackTrace();
        }
       return Result.failure(ResultCode.FAIL);
    }
    @RequestMapping(value = "/userRecord",method = RequestMethod.POST)
    @ApiOperation(value = "添加快递信息",notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userUser", value = "用户", dataType = "String"),
            @ApiImplicitParam(name = "sender", value = "寄件人", dataType = "String"),
            @ApiImplicitParam(name = "mailingAddress", value = "寄件地址", dataType = "String"),
            @ApiImplicitParam(name = "addressee", value = "收件人", dataType = "String"),
            @ApiImplicitParam(name = "receivingAddress", value = "寄件地址", dataType = "String"),
            @ApiImplicitParam(name = "waybill", value = "运单号", dataType = "String"),
            @ApiImplicitParam(name = "userOrder", value = "订单号", dataType = "String"),
            @ApiImplicitParam(name = "courierServicesCompany", value = "快递公司", dataType = "String"),
            @ApiImplicitParam(name = "uniqueIdentifier", value = "唯一标示", dataType = "String")
    })
    public Result userRecord(String userUser,
                             String sender,
                             String mailingAddress,
                             String addressee,
                             String receivingAddress,
                             String waybill,
                             String userOrder,
                             String courierServicesCompany,
                             String uniqueIdentifier){
        int i = userExpressService.userRecord(userUser,
                sender, mailingAddress, addressee,
                receivingAddress, waybill, userOrder,
                courierServicesCompany,uniqueIdentifier);
            if (i==0){
              return   Result.failure(ResultCode.FAIL);
            }
        return Result.success();
    }
   @RequestMapping(value = "/tel", method = RequestMethod.POST)
   @ApiOperation(value = "手机充值", notes = "手机号+充值金额")
   @ApiImplicitParams({
           @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String"),
           @ApiImplicitParam(name = "orderid", value = "sasd", dataType = "String"),
           @ApiImplicitParam(name = "cardnum", value = "充值额度", dataType = "String")
   })
   public Result tel(String phone, int cardnum,String orderid) throws Exception {

       return Result.success(shoujiHuaFei.telQuery(phone, cardnum,orderid));
   }
    /**
     * 订单查询
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "trackOrder",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> trackOrder(@RequestParam("out_trade_no") String out_trade_no)
            throws Exception {
        MyWXConfig config = new MyWXConfig();
        WXPay wxpay = new WXPay(config);

        Map<String, String> data = new HashMap<String, String>();
        data.put("out_trade_no", out_trade_no);
        try {
            Map<String, String> resp = wxpay.orderQuery(data);
            System.out.println(resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
