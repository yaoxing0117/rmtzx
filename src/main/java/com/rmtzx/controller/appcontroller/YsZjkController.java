package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.Impl.CYTelServiceImpl;
import com.rmtzx.service.appservice.Impl.YsZjkServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="专家库")
@RequestMapping("/zjk")
public class YsZjkController {

    @Autowired
    private YsZjkServiceImpl ysZjkService;
    @Autowired
    private CYTelServiceImpl cyTelService;
    //类别返回
    @ApiOperation(value = "类别表返回",notes="id=0大类别,小类别pid对应大类别id.")
    @RequestMapping(value = "/zjAll",method = RequestMethod.POST)
    public Result zjAll() {
        return Result.success(ysZjkService.zjAll());
    }
    //专家分页展示
    @ApiOperation(value = "专家分页展示",notes="cid对应类别表小分类id")
    @RequestMapping(value = "/zjNameAll",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cid", value = "类别id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result zjNameAll(int cid, int pageNo, int pageSize) {
        return Result.success(ysZjkService.zjNameAll(cid, pageNo, pageSize));
    }
    //专家详情展示
    @ApiOperation(value = "专家详情展示",notes="数据展示")
    @RequestMapping(value = "/zjone",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "专家ID", dataType = "String")
    public Result zjone(int id) {
        return Result.success(ysZjkService.zjone(id));
    }//专家详情展示
    @ApiOperation(value = "专家查询",notes="数据展示")
    @RequestMapping(value = "/zjAll1",method = RequestMethod.POST)
    @ApiImplicitParam(name = "expertName", value = "专家姓名", dataType = "String")
    public Result zjAll1(String expertName) {
        return Result.success(ysZjkService.zjAll1(expertName));
    }
    //类别返回
    @ApiOperation(value = "列表展示机构（常用电话）",notes=".")
    @RequestMapping(value = "/pAll",method = RequestMethod.POST)
    public Result pAll() {
        return Result.success(cyTelService.pAll());
    }
    //专家分页展示
    @ApiOperation(value = "常用电话列表展示",notes="")
    @RequestMapping(value = "/cAll",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pid", value = "类别id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result cAll(int cid, int pageNo, int pageSize) {
        return Result.success(cyTelService.cAll(cid, pageNo, pageSize));
    }
}
