package com.rmtzx.controller.appcontroller.usersController;

import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.userServiceImpl.UserOperationServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZdxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZyxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZzqxwServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@Api(tags ="用户操作")
@ResponseBody
@RequestMapping("/userOperation")
public class UserOperationController {
    @Autowired
    private UserOperationServiceImpl userOperationService;
    @Autowired
    private ZzqxwServiceImpl zzqxwService;
    @Autowired
    private ZyxwServiceImpl zyxwService;
    @Autowired
    private ZxwServiceImpl zxwService;
    @Autowired
    private ZdxwServiceImpl zdxwService;



    //评论展示接口
    @RequestMapping(value = "/allComments",method = RequestMethod.POST)
    @ApiOperation(value = "用户评论展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentator", value = "评论人", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "String")
    })
    public Result allComments(String commentator, int pageNo, int pageSize){
        return Result.success( userOperationService.allComments(commentator,pageNo,pageSize));
    }
    //评论详情展示
    @RequestMapping(value = "/detailsComments",method = RequestMethod.POST)
    @ApiOperation(value = "公共请求数据接口",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentCategory", value = "类别", dataType = "String"),
            @ApiImplicitParam(name = "commentID", value = "ID", dataType = "String")
    })
    public Result detailsComments(String commentCategory,int commentID){
            if (commentCategory.equals("ZZQXW")){
                return zzqxwService.singleDate(commentID);
            }else if (commentCategory.equals("ZXW")){
                return zxwService.singleDate(commentID);
            }else if (commentCategory.equals("ZYXW")){
                return zyxwService.singleDate(commentID);
            }else if (commentCategory.equals("ZDXW")){
                return zdxwService.singleDate(commentID);
            }
        return Result.failure(ResultCode.FAIL);
    }
    //添加收藏
    @RequestMapping(value = "/userRecord",method = RequestMethod.POST)
    @ApiOperation(value = "用户添加收藏",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "collectionUser", value = "收藏人账号", dataType = "String"),
            @ApiImplicitParam(name = "collectionArticle", value = "收藏标题", dataType = "String"),
            @ApiImplicitParam(name = "collectionSource", value = "收藏来源", dataType = "String"),
            @ApiImplicitParam(name = "collectionTime", value = "收藏时间", dataType = "String"),
            @ApiImplicitParam(name = "collectionArticleCategory", value = "收藏类别", dataType = "String"),
            @ApiImplicitParam(name = "collectionArticleId", value = "收藏内容ID", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int")
    })
    public Result userRecord(String collectionUser,
                             String collectionArticle,
                             String collectionSource,
                             String collectionTime,
                             String collectionArticleCategory,
                             int collectionArticleId,
                             int userId){
        int i = userOperationService.userRecord(collectionUser,
                collectionArticle,
                collectionSource,
                collectionTime,
                collectionArticleCategory,
                collectionArticleId,userId);
        System.out.println(i);
        if (i==0){
            return Result.failure(ResultCode.INVALID_Entities);
        }
        return Result.success(i);
    }
    //收藏分页展示
    @RequestMapping(value = "/allUserRecord",method = RequestMethod.POST)
    @ApiOperation(value = "收藏分页展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "收藏人id", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "String"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "String")
    })
    public Result allUserRecord(int userId, int pageNo, int pageSize){
        return Result.success( userOperationService.allUserRecord(userId,pageNo,pageSize));
    }
    //收藏详情展示

    //用户投诉
    @RequestMapping(value = "/userComplaints",method = RequestMethod.POST)
    @ApiOperation(value = "咨询-投诉-建议通用写入",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "complainant", value = "手机号", dataType = "String"),
            @ApiImplicitParam(name = "department", value = "部门", dataType = "String"),
            @ApiImplicitParam(name = "userId", value = "用户ID", dataType = "int"),
            @ApiImplicitParam(name = "userMail", value = "邮箱", dataType = "String"),
            @ApiImplicitParam(name = "contents", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "title", value = "标题", dataType = "String"),
            @ApiImplicitParam(name = "category", value = "类别(咨询-投诉-建议)", dataType = "String")
    })
    public Result userComplaints(String complainant,//投诉人手机号
                                 String department,//投诉部门
                                 int userId,//投诉人ID
                                 String userMail,//投诉人邮箱
                                 String contents,//投诉内容
                                 String title,//投诉标题
                                 String category) {
        Date day=new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd");
        String complaintTime = time.format(day);
        return Result.success(userOperationService.userComplaints(complainant, department,
                userId, userMail, contents, title, complaintTime, category));

    }
    //用户投诉展示
    @RequestMapping(value ="/userAllComplaints" ,method = RequestMethod.POST)
    @ApiOperation(value = "咨询-投诉-建议通用分页展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户", dataType = "int"),
            @ApiImplicitParam(name = "category", value = "类别(咨询-投诉-建议)", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "显示行数", dataType = "int")
    })
    public Result userAllComplaints(int userId,
                                    String category,
                                    int pageNo,
                                    int pageSize){
        ShowInfo showInfo = userOperationService.userAllComplaints(userId,category, pageNo, pageSize);
        return Result.success(showInfo);
    }
    //用户投诉详情
    @RequestMapping(value ="/userOneComplaints" ,method = RequestMethod.POST)
    @ApiOperation(value = "咨询-投诉-建议通用详情展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "投诉ID", dataType = "int")
    })
    public Result userOneComplaints(int id){
        return Result.success( userOperationService.userOneComplaints(id));
    }

    @RequestMapping(value ="/setUserResume" ,method = RequestMethod.POST)
    @ApiOperation(value = "添加简历",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "姓名", dataType = "String"),
            @ApiImplicitParam(name = "userAge", value = "年龄", dataType = "String"),
            @ApiImplicitParam(name = "userSex", value = "性别", dataType = "String"),
            @ApiImplicitParam(name = "userTel", value = "电话", dataType = "String"),
            @ApiImplicitParam(name = "userMail", value = "邮箱", dataType = "String"),
            @ApiImplicitParam(name = "userXl", value = "学历", dataType = "String"),
            @ApiImplicitParam(name = "userZwms", value = "自我描述", dataType = "String"),
            @ApiImplicitParam(name = "userQwxz", value = "期望薪资", dataType = "String"),
            @ApiImplicitParam(name = "userBz", value = "备注", dataType = "String"),
            @ApiImplicitParam(name = "uId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "userNameTel", value = "用户账号", dataType = "String")
    })
    public Result setUserResume(String userName, String userAge, String userSex, String userTel,
                             String userMail, String userXl, String userZwms, String userQwxz, String userBz,int uId,
                                String userNameTel) {
        int i = userOperationService.setUserResume(userName, userAge, userSex, userTel, userMail, userXl, userZwms, userQwxz, userBz,userNameTel,uId);
        return Result.success();
    }
    //查看简历
    @RequestMapping(value ="/oneUserR" ,method = RequestMethod.POST)
    @ApiOperation(value = "查看简历",notes = "")
    @ApiImplicitParam(name = "uid", value = "用户id", dataType = "String")
    public Result oneUserR(int uid) {
        return Result.success(userOperationService.oneUserR(uid));
    }
    @RequestMapping(value ="/userGetId" ,method = RequestMethod.POST)
    @ApiOperation(value = "通过id查询昵称与头像",notes = "")
    @ApiImplicitParam(name = "id", value = "用户id", dataType = "String")
    public Result userGetId(int id) {
        return Result.success(userOperationService.userGetId(id));
    }
    @RequestMapping(value ="/grslAll" ,method = RequestMethod.POST)
    @ApiOperation(value = "个人申领查询",notes = "")
    @ApiImplicitParam(name = "userId", value = "身份证号", dataType = "String")
    public Result grslAll(String userId) {
        List<UserGrslEntity> userGrslEntities = userOperationService.grslAll(userId);
//        System.out.println(userGrslEntities);
        if (userGrslEntities.isEmpty()){
            return Result.failure(ResultCode.FAIL,"身份证不正确或不存在");
        }else {
            return Result.success(userGrslEntities);
        }
    }
    @RequestMapping(value ="/prizeInsert" ,method = RequestMethod.POST)
    @ApiOperation(value = "匿名投诉",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "reportContent", value = "内容", dataType = "String"),
            @ApiImplicitParam(name = "reportPicture", value = "图片", dataType = "String"),
            @ApiImplicitParam(name = "reportTel", value = "电话", dataType = "String"),
            @ApiImplicitParam(name = "reportWx", value = "微信", dataType = "String"),
    })
    public Result prizeInsert(String reportContent,
                              String reportPicture,
                              String reportTel,
                              String reportWx) {
        Date day=new Date();
        SimpleDateFormat time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String reportTime = time.format(day);
        return Result.success(userOperationService.prizeInsert(reportContent, reportPicture, reportTel, reportWx, reportTime));
    }
}
