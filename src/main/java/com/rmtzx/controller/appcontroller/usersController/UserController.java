package com.rmtzx.controller.appcontroller.usersController;

import com.rmtzx.entity.appentity.userEntity.UserEntity;
import com.rmtzx.entity.bo.*;
import com.rmtzx.service.appservice.Impl.userServiceImpl.UserServiceImpl;
import com.rmtzx.util.RedisUtils;
import com.rmtzx.utilsss.FileUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@Api(tags = "用户登录操作")
@ResponseBody
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Value("${server.port}")
    private String port;
    @Value("${web.upload-path}")
    private String path;
    @Value("${fileIP}")
    private String ip;
    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private RedisUtils redisUtils;

    @RequestMapping(value = "/SMSlogin", method = RequestMethod.POST)
    @ApiOperation(value = "手机短信接口", notes = "手机号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNameTel", value = "手机号", dataType = "String")
    })
    public Result loginUser(@RequestParam("userNameTel") String userNameTel) throws IOException {
        Result loadByUser = userService.SMSlogin(userNameTel);
        return loadByUser;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "登录,所需参数:用户名(userNameTel),密码（passWord）", notes = "0账号不存在，1密码成功，2密码错误;")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNameTel", value = "账号", dataType = "String"),
            @ApiImplicitParam(name = "passWord", value = "密码", dataType = "String")
    })
    public Result loginUser(@RequestParam("userNameTel") String userNameTel,
                            @RequestParam("passWord") String passWord) {
//        System.out.println(userNameTel+"---------"+passWord);
        Result loadByUser = userService.findLoadByUser(userNameTel, passWord);
        return loadByUser;
    }

    //自动登录
    @RequestMapping(value = "/automaticLogon", method = RequestMethod.POST)
    @ApiOperation(value = "自动登录", notes = "账号+token")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNameTel", value = "账号", dataType = "String"),
            @ApiImplicitParam(name = "token", value = "token", dataType = "String")
    })
    public Result automaticLogon(@RequestParam("userNameTel") String userNameTel,
                                 @RequestParam("token") String token) {
        return userService.automaticLogon(userNameTel, token);
    }

    //退出登录
    @RequestMapping(value = "/signOut", method = RequestMethod.POST)
    @ApiOperation(value = "退出登录,所需参数:用户名(userNameTel)", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNameTel", value = "账号", dataType = "String"),
    })
    public Result signOut(@RequestParam("userNameTel") String userNameTel
    ) {

        return userService.signOut(userNameTel);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ApiOperation(value = "注册:所需参数：用户名(userName),密码(passWord),验证码(Sms)", notes = "0账号存在，1注册成功")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号", dataType = "String"),
            @ApiImplicitParam(name = "passWord", value = "首次密码", dataType = "String"),
            @ApiImplicitParam(name = "Sms", value = "验证码", dataType = "String")
    })
    public Result addUser(@RequestParam("userName") String userName,
                          @RequestParam("passWord") String passWord,
                          @RequestParam("Sms") String Sms) {
        if (userName.equals(null)) {
            return Result.failure(ResultCode.INVALID_PARAM);
        }
        Result result = userService.addUser(userName, passWord, Sms);
        return result;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ApiOperation(value = "修改密码:所需参数：用户手机号(userName),短信验证码(passWord),新密码（passWord）", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号", dataType = "String"),
            @ApiImplicitParam(name = "passWord", value = "新密码", dataType = "String"),
            @ApiImplicitParam(name = "Sms", value = "验证码", dataType = "String")
    })
    public Result updateUser(@RequestParam("userName") String userName,
                             @RequestParam("passWord") String passWord,
                             @RequestParam("Sms") String Sms) {
        System.out.println(userName + passWord);
        if (userName == null) {
            return Result.failure(ResultCode.INVALID_PARAM);
        }
        Result result = userService.editUser(userName, passWord, Sms);
        return result;
    }

    //返回当前用户信息
    @RequestMapping(value = "/currentUser", method = RequestMethod.POST)
    @ApiOperation(value = "返回当前用户信息", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "账号", dataType = "String")
    })
    public Result currentUser(@RequestParam("userName") String userName) {
        redisUtils.get(userName);
        UserEntity userEntity = userService.userDate(userName);
        return Result.success(userEntity);
    }

    @RequestMapping(value = "/updatePersona", method = RequestMethod.POST)
    @ApiOperation(value = "修改个人信息：头像 昵称 用户姓名 手机号 地址 电话 生日 性别 ", notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userNameTel", value = "账号", dataType = "String"),
            @ApiImplicitParam(name = "userNickName", value = "昵称", dataType = "String"),
            @ApiImplicitParam(name = "userAge", value = "年龄", dataType = "String"),
            @ApiImplicitParam(name = "userSex", value = "性别", dataType = "String"),
            @ApiImplicitParam(name = "userBirthday", value = "生日", dataType = "String"),
            @ApiImplicitParam(name = "userCity", value = "地址", dataType = "String"),
            @ApiImplicitParam(name = "userTel", value = "电话", dataType = "String"),
            @ApiImplicitParam(name = "userName", value = "用户姓名", dataType = "String")
    })
    public Result updatePersona(String userNameTel,
                                String userNickName,
                                String userAge,
                                String userSex,
                                String userBirthday,
                                String userCity,
                                MultipartFile file,
                                String userTel,
                                String userName) {
        UserEntity s = userService.queryNickName(userNickName);//查询昵称是否存在
        UserEntity queryportra = userService.queryportra(userNameTel);//查询是否上传头像

        if (s == null) {
            if (file != null) {//判断头像是否为空
                //1定义要上传文件 的存放路径
                //2获得文件名字
                String fileName = file.getOriginalFilename();
                //2上传失败提示`
                String userPortrait1 = FileUtils.upload(file, path, fileName);
                String[] split = userPortrait1.split("D:");
                String s1 = split[1];
                String userPortrait = ip + port + s1;
                userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
                UserEntity userEntity = userService.userDate(userNameTel);
                return Result.success(userEntity);
            }
        } else if (s.getUserNickName().equals(userNickName)&&file==null) {
            String userPortrait = queryportra.getUserPortrait();
            userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
            UserEntity userEntity = userService.userDate(userNameTel);
            return Result.success(userEntity);

        }
        if (file == null) {
            //头像为空时 查询历史头像
            String userPortrait = queryportra.getUserPortrait();
            userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
            UserEntity userEntity = userService.userDate(userNameTel);
            return Result.success(userEntity);
        } else {
            //1定义要上传文件 的存放路径
            //2获得文件名字
            String fileName = file.getOriginalFilename();
            //2上传失败提示`
            String userPortrait1 = FileUtils.upload(file, path, fileName);
            String[] split = userPortrait1.split("D:");
            String s1 = split[1];
            String userPortrait = ip + port + s1;
            userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
            UserEntity userEntity = userService.userDate(userNameTel);
            return Result.success(userEntity);
        }


        // return Result.failure(ResultCode.FAIL);

//
//
//
//
//         System.out.println(empty);
//         if(file.isEmpty()&&empty)
//
//     {
//         log.info("第一次修改&&未上传头像并");
//         String userPortrait = "未上传头像";
//         userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
//         UserEntity userEntity = userService.userDate(userNameTel);
//         return Result.success(userEntity);
//     } else if(s !=null)
//
//     {
//         //1定义要上传文件 的存放路径
//         //2获得文件名字
//         String fileName = file.getOriginalFilename();
//         //2上传失败提示`
//         String userPortrait1 = FileUtils.upload(file, path, fileName);
//         String[] split = userPortrait1.split("D:");
//         String s1 = split[1];
//         String userPortrait = ip + port + s1;
//         userService.updatePersona(userNameTel, userNickName, userAge, userSex, userBirthday, userCity, userPortrait, userTel, userName);
//         UserEntity userEntity = userService.userDate(userNameTel);
//         return Result.success(userEntity);
//     } else
//
//     {
//         return Result.success(ResultCode.FAIL);
//     }
// }
    }

}
