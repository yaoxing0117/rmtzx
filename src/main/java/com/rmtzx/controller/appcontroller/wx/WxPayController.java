package com.rmtzx.controller.appcontroller.wx;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.util.DHUtil;
import com.rmtzx.util.MakeOrderNum;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

@Api(tags = "微信-支付宝-话费充值")
@RestController
@RequestMapping("/wxPay")
@Slf4j
public class WxPayController {

    @Autowired
    private WxPayService wxPayService;

    /**
     * 微信支付异步通知
     */
    @RequestMapping(value = "/notify")
    public void payNotify(HttpServletRequest request) {
        InputStream is = null;
        try {
            is = request.getInputStream();
            // 将InputStream转换成String
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
           wxPayService.notify(sb.toString());
        } catch (Exception e) {
            log.error("微信手机支付回调通知失败：", e);
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @ApiOperation(value = "微信支付", notes = "微信订单保存接口")
    @RequestMapping(value = "/wxPay", method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userTel", value = "用户手机号", dataType = "String"),
            @ApiImplicitParam(name = "userMoney", value = "充值金额", dataType = "int")
    })
    public Result wxPay(String userTel, int userMoney) {
        return Result.success(wxPayService.insereWXXR(userTel, userMoney));
    }

}
