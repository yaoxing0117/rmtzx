package com.rmtzx.controller.appcontroller.wx;

import com.github.wxpay.sdk.WXPayUtil;
import com.rmtzx.dao.appdao.WXPayMapper;
import com.rmtzx.entity.bo.WXPayEntity;
import com.rmtzx.manager.ShoujiHuaFei;
import com.rmtzx.util.MakeOrderNum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
@Slf4j
public class WxPayServiceImpl implements WxPayService {
    @Autowired
    private ShoujiHuaFei shoujiHuaFei;
    @Autowired
    private WXPayMapper wxPayMapper;

    @Override
    public String notify(String notifyStr) {
        try {
            // 转换成map
            Map<String, String> resultMap = WXPayUtil.xmlToMap(notifyStr);
            String returnCode = resultMap.get("return_code");  //状态
            String outTradeNo = resultMap.get("out_trade_no");//商户订单号
            WXPayEntity wxPayEntity = wxPayMapper.selectWX(outTradeNo);
            if (returnCode.equals("SUCCESS")) {
                log.info("订单成功支付");
                System.out.println(wxPayEntity);
                if (wxPayEntity.getOrderCode().equals("支付成功")) {
                    log.info("充值订单重复信息忽略");
                    return "重复订单";
                }
                log.info("调用聚合充值");
                int i = wxPayMapper.updateWXCZ(outTradeNo);//修改数据库订单状态
                log.info("修改数据订单状态:" + i);
                String s = shoujiHuaFei.telQuery(wxPayEntity.getUserTel(), wxPayEntity.getUserMoney(), wxPayEntity.getOutTradeNo());
                log.info("聚合话费充值调用完成：" + s);
                //聚合充值话费业务流程
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 用户订单保存
     *
     * @param userTel   用户手机号
     * @param userMoney 充值金额
     * @return 是否成功
     */
    @Override
    public String insereWXXR(String userTel, int userMoney) {

        Integer id = wxPayMapper.newId1();
        String outTradeNo = MakeOrderNum.getNewEquipmentNo("WX", id.toString());
        log.info("下单接口单号为：" + outTradeNo);
        wxPayMapper.insereWXXR(outTradeNo, userTel, userMoney);
        return outTradeNo;
    }
    //public String notify(String notifyStr) {
    //        String xmlBack = "<xml><return_code><![CDATA[FAIL]]></return_code><return_msg><![CDATA[报文为空]]></return_msg></xml> ";
    //        try {
    //            // 转换成map
    //            Map<String, String> resultMap = WXPayUtil.xmlToMap(notifyStr);
    //            WXPay wxpayApp = new WXPay(wxPayAppConfig);
    //            if (wxpayApp.isPayResultNotifySignatureValid(resultMap)) {
    //                String returnCode = resultMap.get("return_code");  //状态
    //                String outTradeNo = resultMap.get("out_trade_no");//商户订单号
    //                String transactionId = resultMap.get("transaction_id");
    //                if (returnCode.equals("SUCCESS")) {
    //                    if (StringUtils.isNotBlank(outTradeNo)) {
    //                        /**
    //                         * 注意！！！
    //                         * 请根据业务流程，修改数据库订单支付状态，和其他数据的相应状态
    //                         *
    //                         */
    //                        logger.info("微信手机支付回调成功,订单号:{}", outTradeNo);
    //                        xmlBack = "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
    //                    }
    //                }
    //            }
    //        } catch (Exception e) {
    //            e.printStackTrace();
    //        }
    //        return xmlBack;
    //    }

}
