package com.rmtzx.controller.appcontroller.wx;


import org.apache.ibatis.annotations.Param;

/**
 * 微信支付服务接口
 */
public interface WxPayService {


    /**
     * @param notifyStr: 微信异步通知消息字符串
     * @return
     * @Description: 订单支付异步通知
     * @Author:
     * @Date: 2019/8/1
     */
    String notify(String notifyStr) throws Exception;

    /**
     * 用户订单保存

     * @param userTel 用户手机号
     * @param userMoney 充值金额
     * @return 返回订单号
     */
    String insereWXXR(
                   @Param("userTel") String userTel,
                   @Param("userMoney") int userMoney);

}
