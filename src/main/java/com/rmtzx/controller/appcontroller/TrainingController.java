package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.Impl.TrainigServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

@Component
@Api(tags ="机构培训班")
@ResponseBody
@RequestMapping("/jgpx")
public class TrainingController {
@Autowired
private TrainigServiceImpl trainigService;
    @RequestMapping(value = "/allTraining",method = RequestMethod.POST)
    @ApiOperation(value = "培训班列表展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "int")
    })
    public Result allTraining(int pageNo, int pageSize) {
        return Result.success(trainigService.allTraining(pageNo, pageSize));
    }
    @RequestMapping(value = "/oneTraining", method = RequestMethod.POST)
    @ApiOperation(value = "培训班详情", notes = "订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "详情Id", dataType = "int"),
    })
    public Result oneTraining(int id) {
        return Result.success(trainigService.oneTraining(id));
    }
}
