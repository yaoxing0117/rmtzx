package com.rmtzx.controller.appcontroller.journalismController;


import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.entity.appentity.ywEntity.NewsPaperEntity;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.NewsPaperServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 报纸数据展示
 */

@Controller
@ResponseBody
@Api(tags ="报纸数据")
@RequestMapping("/bz")
public class NewsPaperController {
    @Autowired
    private NewsPaperServiceImpl newsPaperService;

    @ApiOperation(value = "报纸查询展示数据展示",notes="传输报纸类别")
    @RequestMapping(value = "/bzall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "source", value = "查询来源", dataType = "String")
    })
    public Result paperAll(String source,int pageNo, int pageSize){
        if (source==null){
            return Result.failure(ResultCode.INVALID_PARAM);
        }
        ShowInfo showInfo = newsPaperService.paperAll(source, pageNo, pageSize);
        return Result.success(showInfo);
    }
    @ApiOperation(value = "详情数据展示(PC/app通用)",notes = "报纸ID")
    @RequestMapping(value = "bzIdDeta",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result bzIdDate(@RequestParam("id")int id){
        NewsPaperEntity newsPaperEntity = newsPaperService.paperID(id);
        return Result.success(newsPaperEntity) ;
    }
}
