package com.rmtzx.controller.appcontroller.journalismController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZdxwServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 准东新闻
 * */
@Controller
@ResponseBody
@Api(tags ="准东新闻")
@RequestMapping("/zdxw")
public class ZdxwController {
    @Autowired
    private ZdxwServiceImpl zdxwService;

    @ApiOperation(value = "列表(app)分页展示",notes="数据展示")
    @RequestMapping(value = "/zdxwScreenAll",method = RequestMethod.POST)
    public Result zdxwScreenAll(int pageNo,int pageSize){
        ShowInfo showInfo = zdxwService.zdxwScreen(pageNo, pageSize);
        return Result.success(showInfo);
    }
    @ApiOperation(value = "PC分页展示",notes="分页展示")
    @RequestMapping(value = "/zdxwPageAll",method = RequestMethod.POST)
    public Result zyxwPageAll(int pageNo,int pageSize){
        return Result.success(zdxwService.zdxwPaging(pageNo,pageSize));
    }
    @ApiOperation(value = "详情数据展示(PC/app通用)",notes = "")
    @RequestMapping(value = "/zdxwIdDeta",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result zxwIdDate(@RequestParam("id")int id){
        Result result = zdxwService.singleDate(id);
        return result;
    }
    @ApiOperation(value = "点赞接口")
    @RequestMapping(value = "/d_z",method = RequestMethod.POST)
    public Result thunbsUp(int idy,int idx){
        return Result.success(zdxwService.thumbsUp(idy,idx));
    }
    @ApiOperation(value = "评论接口")
    @RequestMapping(value = "comment",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentator", value = "评论用户账号", dataType = "String"),
            @ApiImplicitParam(name = "commentContent", value = "评论内容", dataType = "String"),
            @ApiImplicitParam(name = "userTx", value = "用户头像", dataType = "String"),
            @ApiImplicitParam(name = "commentID", value = "新闻ID", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "commentCategory", value = "类别", dataType = "String")
    })
    public Result commentInsert(String commentator, String commentContent,String userTx, int commentID, int userId, String commentCategory){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String commentaryTime = df.format(new Date());// new Date()为获取当前系统时间
        return Result.success(zdxwService.commentInsert(commentator,commentContent,
                commentaryTime,userTx,commentID,userId,commentCategory));
    }
//    @ApiOperation(value = "评论分页展示")
//    @RequestMapping(value = "commentDisplay",method = RequestMethod.POST)
//    public Result commentDisplay(String commentCategory,int pageNo,int pageSize ){
//        return Result.success(zdxwService.commentPaging(commentCategory,pageNo,pageSize));
//    }
}
