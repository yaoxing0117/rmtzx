package com.rmtzx.controller.appcontroller.journalismController;

import com.rmtzx.entity.bo.Result;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZzqxwServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 要闻—自治区新闻
 * */
@Controller
@RequestMapping("/ywzzq")
@ResponseBody
@Api(tags ="要闻—自治区新闻")
public class ZzqxwController {
    @Autowired
    private ZzqxwServiceImpl zzqxwService;

    @ApiOperation(value = "列表(app)分页展示",notes="自治区新闻数据展示")
    @RequestMapping(value = "/zzqxwScreenAll",method = RequestMethod.POST)
    public Result zzqxwScreenAll(int pageNo,int pageSize){
        ShowInfo showInfo = zzqxwService.zzqxwScreen(pageNo, pageSize);
        return  Result.success(showInfo);
    }
    @ApiOperation(value = "PC分页展示",notes="自治区新闻分页展示")
    @RequestMapping(value = "/zzqxwPageAll",method = RequestMethod.POST)
    public Result zzqxwPageAll(@RequestParam("pageNo")int pageNo,
                              @RequestParam("pageSize") int pageSize){
        return  Result.success(zzqxwService.zzqxwPaging(pageNo, pageSize));
    }
    @ApiOperation(value = "详情数据展示(PC/app通用)",notes = "")
    @RequestMapping(value = "zzqxwIdDeta",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "historicalUser", value = "访问用户账号", dataType = "String"),
            @ApiImplicitParam(name = "historicalTitle", value = "访问标题", dataType = "String"),
            @ApiImplicitParam(name = "historicalSource", value = "访问来源", dataType = "String"),
            @ApiImplicitParam(name = "id", value = "访问新闻ID", dataType = "String"),
            @ApiImplicitParam(name = "historicalCategory", value = "新闻类别", dataType = "String"),
            @ApiImplicitParam(name = "historicalTime", value = "访问时间", dataType = "String"),
    })
    public Result zzqIdDate(@RequestParam("id")int id,
                            String historicalUser,
                            String historicalTitle,
                            String historicalSource,
                            String historicalCategory,
                            String historicalTime){
        Result result = zzqxwService.singleDate(id);
        return result;
    }
    @ApiOperation(value = "点赞接口")
    @RequestMapping(value = "d_z",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "idy", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "idx", value = "新闻id", dataType = "int")
    })
    public Result thunbsUp(int idy,int idx){
        return Result.success(zzqxwService.thumbsUp(idy,idx));
    }
    @ApiOperation(value = "评论接口")
    @RequestMapping(value = "comment",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentator", value = "评论用户名称", dataType = "String"),
            @ApiImplicitParam(name = "commentContent", value = "评论内容", dataType = "String"),
            @ApiImplicitParam(name = "userTx", value = "用户头像", dataType = "String"),
            @ApiImplicitParam(name = "commentID", value = "新闻ID", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "commentCategory", value = "类别", dataType = "String")
    })
    public Result commentInsert(@RequestParam("commentator") String commentator,
                                @RequestParam("commentContent")String commentContent,
                                @RequestParam("userTx")String userTx,
                                @RequestParam("commentID")int commentID,
                                @RequestParam("userId")int userId,
                                @RequestParam("commentCategory")String commentCategory){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String commentaryTime = df.format(new Date());// new Date()为获取当前系统时间
        return Result.success(zzqxwService.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory));
    }
    @ApiOperation(value = "评论分页展示")
    @RequestMapping(value = "commentDisplay",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentCategory", value = "新闻类别", dataType = "String"),
            @ApiImplicitParam(name = "commentID", value = "新闻id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "页数", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少行", dataType = "int")
    })
    public Result commentDisplay(String commentCategory,int commentID,int pageNo,int pageSize ){
        return Result.success(zzqxwService.commentPaging(commentCategory,commentID,pageNo,pageSize));
    }
}
