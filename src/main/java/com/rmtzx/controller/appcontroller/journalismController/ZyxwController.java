package com.rmtzx.controller.appcontroller.journalismController;


import com.rmtzx.entity.bo.Result;

import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZyxwServiceImpl;
import com.rmtzx.service.appservice.Impl.ywServiceImpl.ZzqxwServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 要闻—中央新闻
 * */
@Controller
@RequestMapping("/zyxw")
@ResponseBody
@Api(tags ="要闻—中央新闻")
public class ZyxwController {
    @Autowired
    private ZyxwServiceImpl zyxwService;
    @Autowired
    private ZzqxwServiceImpl zzqxwService;

    @ApiOperation(value = "列表(app)分页展示",notes="数据展示")
    @RequestMapping(value = "/zyxwScreenAll",method = RequestMethod.POST)
    public Result zyxwScreenAll(int pageNo,int pageSize){
        ShowInfo showInfo = zyxwService.zyxwScreen(pageNo, pageSize);
        return Result.success(showInfo);
    }
    @ApiOperation(value = "PC分页展示",notes="分页展示")
    @RequestMapping(value = "/zyxwPageAll",method = RequestMethod.POST)
    public Result zyxwPageAll(int pageNo,int pageSize){
        return Result.success(zyxwService.zyxwPaging(pageNo,pageSize));
    }
    @ApiOperation(value = "详情数据展示(PC/app通用)",notes = "")
    @RequestMapping(value = "/zyxwIdDeta",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id",value = "查询id",dataType = "int")
    public Result zyxwIdDate(@RequestParam("id")int id){
        Result result = zyxwService.singleDate(id);
        return result;
    }
    @ApiOperation(value = "点赞接口")
    @RequestMapping(value = "/d_z",method = RequestMethod.POST)
    public Result thunbsUp(int idy,int idx){
        return Result.success(zyxwService.thumbsUp(idy,idx));
    }
    @ApiOperation(value = "评论接口")
    @RequestMapping(value = "/comment",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "commentator", value = "评论用户账号", dataType = "String"),
            @ApiImplicitParam(name = "commentContent", value = "评论内容", dataType = "String"),
            @ApiImplicitParam(name = "userTx", value = "用户头像", dataType = "String"),
            @ApiImplicitParam(name = "commentID", value = "新闻ID", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "commentCategory", value = "类别", dataType = "String")
    })
    public Result commentInsert(@RequestParam("commentator") String commentator,
                                @RequestParam("commentContent")String commentContent,
                                @RequestParam("userTx")String userTx,
                                @RequestParam("commentID")int commentID,
                                @RequestParam("userId")int userId,
                                @RequestParam("commentCategory")String commentCategory){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        String commentaryTime = df.format(new Date());// new Date()为获取当前系统时间
        return Result.success(zzqxwService.commentInsert(commentator,commentContent,commentaryTime,userTx,commentID,userId,commentCategory));
    }
    @ApiOperation(value = "评论分页展示")
    @RequestMapping(value = "commentDisplay",method = RequestMethod.POST)
    public Result commentDisplay(String commentCategory,int commentID,int pageNo,int pageSize ){
        return Result.success(zzqxwService.commentPaging(commentCategory,commentID,pageNo,pageSize));
    }
}
