// package com.rmtzx.controller.appcontroller.BM;
//
// import com.rmtzx.entity.appentity.bmEntity.VoteOption;
// import com.rmtzx.service.appservice.bmService.VoteOptionService;
// import io.swagger.annotations.Api;
// import io.swagger.annotations.ApiImplicitParam;
// import io.swagger.annotations.ApiOperation;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.*;
//
// @Controller
// @ResponseBody
// @Api(tags ="投票选项")
// @RequestMapping("/tpxx")
// public class VoteOptionController {
//
//     @Autowired
//     VoteOptionService voteOptionService;
//
//     @ApiOperation(value = "添加投票选项",notes="添加投票选项")
//     @RequestMapping(value = "/tpadd",method = RequestMethod.POST)
//     public String addOption(@ModelAttribute VoteOption voteOption){
//         String i = voteOptionService.insertSelective(voteOption);
//         return i;
//     }
//
//     @ApiOperation(value = "删除投票选项",notes="删除投票选项")
//     @RequestMapping(value = "/tpdelete",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "id",value = "需要删除投票选项的id",dataType = "int")
//     public String deleteOption(@RequestParam("id")int id){
//         String s = voteOptionService.deleteByPrimaryKey(id);
//         return s;
//     }
//
//     @ApiOperation(value = "修改投票选项",notes="修改投票选项")
//     @RequestMapping(value = "/tpupdate",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "id",value = "需要删除投票选项的id",dataType = "int")
//     public String deleteOption(@ModelAttribute VoteOption voteOption){
//         String s = voteOptionService.updateByPrimaryKeySelective(voteOption);
//         return s;
//     }
//
// }
