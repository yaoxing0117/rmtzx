// package com.rmtzx.controller.appcontroller.BM;
//
// import com.rmtzx.entity.bo.Result;
// import com.rmtzx.service.appservice.bmService.VoteOptionService;
// import io.swagger.annotations.Api;
// import io.swagger.annotations.ApiImplicitParam;
// import io.swagger.annotations.ApiOperation;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.RequestMapping;
// import org.springframework.web.bind.annotation.RequestMethod;
// import org.springframework.web.bind.annotation.ResponseBody;
//
// @Controller
// @ResponseBody
// @Api(tags ="投票展示")
// @RequestMapping("/tpzs")
// public class VoteSelectController {
//
//     @Autowired
//     private VoteOptionService voteOptionService;
//
//     @ApiOperation(value = "根据投票标题查询投票内容",notes="根据标题查询投票内容")
//     @RequestMapping(value = "/findByTitle",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "title",value = "根据title查询",dataType = "String")
//     public Result findByTitle(String title){
//
//         return voteOptionService.selectByPrimaryKey(title);
//     }
//
//     @ApiOperation(value = "后台根据投票分类id查询所有投票进行分页展示",notes = "按分类展示该类所有投票标题")
//     @RequestMapping(value = "/findBySortId",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "sortId",value = "根据分类id查询该分类")
//     public Result findBySortId(int sortId){
//         Result result = voteOptionService.findBySortId(sortId);
//         return result;
//     }
// }
