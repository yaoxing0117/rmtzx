// package com.rmtzx.controller.appcontroller.BM;
//
//
// import com.rmtzx.entity.appentity.bmEntity.VoteSort;
// import com.rmtzx.service.appservice.bmService.VoteSortService;
// import io.swagger.annotations.Api;
// import io.swagger.annotations.ApiImplicitParam;
// import io.swagger.annotations.ApiOperation;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Controller;
// import org.springframework.web.bind.annotation.*;
//
// @Controller
// @ResponseBody
// @Api(tags ="投票分类")
// @RequestMapping("/tp")
// public class VoteSortController {
//
//     @Autowired
//     private VoteSortService voteSortService;
//
//     @ApiOperation(value = "添加投票分类",notes="添加投票分类")
//     @RequestMapping(value = "/fladd",method = RequestMethod.POST)
//     public String addOption(@ModelAttribute VoteSort voteSort){
//         String i = voteSortService.insertSelective(voteSort);
//         return i;
//     }
//
//     @ApiOperation(value = "删除投票分类",notes="删除投票分类")
//     @RequestMapping(value = "/fldelete",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "id",value = "需要删除投票分类的id",dataType = "int")
//     public String deleteOption(@RequestParam("id")int id){
//         String s = voteSortService.deleteByPrimaryKey(id);
//         return s;
//     }
//
//     @ApiOperation(value = "修改投票分类",notes="修改投票分类")
//     @RequestMapping(value = "/tpupdate",method = RequestMethod.POST)
//     @ApiImplicitParam(name = "id",value = "需要删除投票选项的id",dataType = "int")
//     public String updateOption(@ModelAttribute VoteSort voteSort){
//         String s = voteSortService.updateByPrimaryKeySelective(voteSort);
//         return s;
//     }
// }
