package com.rmtzx.controller.appcontroller.exception;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.utilsss.FileUtils;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author gaoxuyang
 * @user 图片上传及显示
 */
@Controller
@Api(tags = "文件上传")
@ResponseBody
@RequestMapping("/editor")
@Slf4j
public class FileUploadController {


    private final ResourceLoader resourceLoader;

    @Autowired
    public FileUploadController(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Value("${server.port}")
    private String port;
    @Value("${web.upload-path}")
    private String path;
    @Value("${fileIP}")
    private String ip;


    /**
     * @param file 上传的文件
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
    public String upload(@RequestParam("file") MultipartFile file) {
        System.out.println(file);
        //2获得文件名字
        String fileName = file.getOriginalFilename();
        //2上传失败提示`
        String userPortrait1 = FileUtils.upload(file, path, fileName);
        String[] split = userPortrait1.split("D:");
        String s1 = split[1];
        String userPortrait = ip + port + s1;
        System.out.println(userPortrait);
        return userPortrait;
    }
    @ResponseBody
    @RequestMapping(value = "/file", method = RequestMethod.POST)
    public Result file(@RequestParam("file") MultipartFile file) {
        System.out.println(file);
        //2获得文件名字
        String fileName = file.getOriginalFilename();
        //2上传失败提示`
        String userPortrait1 = FileUtils.upload(file, path, fileName);
        String[] split = userPortrait1.split("D:");
        String s1 = split[1];
        String userPortrait = ip + port + s1;
        System.out.println(userPortrait);
        return Result.success(userPortrait);
    }

    /**
     * 显示图片
     *
     * @param fileName 文件名
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "show", method = RequestMethod.POST)
    public ResponseEntity show(String fileName) {
        try {
            // 由于是读取本机的文件，file是一定要加上的， path是在application配置文件中的路径
            return ResponseEntity.ok(resourceLoader.getResource(fileName));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

    }
}