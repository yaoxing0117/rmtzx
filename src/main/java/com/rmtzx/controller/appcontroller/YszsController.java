package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.Impl.YszsServiceImpl;
import com.rmtzx.service.appservice.YszsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="营商-资讯-微信公众号")
@RequestMapping("/yszxs")
public class YszsController {
    @Autowired
    private YszsServiceImpl yszsService;


    @ApiOperation(value = "展示数据列表",notes="cid对应类别表小分类id")
    @RequestMapping(value = "/allYszs",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "类别（招商政策，经济信息，成果展示，产销咨询，准东环保，准东产业，）", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result allYszs(String category, int pageNo, int pageSize) {
        return Result.success(yszsService.allYszs(category, pageNo, pageSize));
    }
    //
    @ApiOperation(value = "详情展示",notes="数据展示")
    @RequestMapping(value = "/zjone",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "咨询ID", dataType = "String")
    public Result zjone(int id) {
        return Result.success(yszsService.oneYszs(id));
    }
    @ApiOperation(value = "微信公众号列表展示",notes="")
    @RequestMapping(value = "/allWX",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "differentiate", value = "类别（政务，企业）", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result allWX(String differentiate, int pageNo, int pageSize) {
        return Result.success(yszsService.allWX(differentiate, pageNo, pageSize));
    }
    //
    @ApiOperation(value = "微信详情展示",notes="数据展示")
    @RequestMapping(value = "/oneWX",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "咨询ID", dataType = "int")
    public Result oneWX(int id) {
        return Result.success(yszsService.oneWX(id));
    }
    //微信公众平台
}
