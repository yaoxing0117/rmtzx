package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.service.appservice.Impl.YwzpServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="营商-简历投递")
@RequestMapping("/ywzp")
public class YwzpController {
    @Autowired
    private YwzpServiceImpl ywzpService;


    //信息公开展示
    @ApiOperation(value = "信息公开展示",notes="")
    @RequestMapping(value = "/xxallSelects",method = RequestMethod.POST)
    public Result xxallSelects() {
        return Result.success(ywzpService.xxallSelect());
    }
    //信息公开展示
    @ApiOperation(value = "信息公开展示",notes="cid对应类别表小分类id")
    @RequestMapping(value = "/xxallSelect",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cid", value = "类别id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result xxallSelect(int cid, int pageNo, int pageSize) {
        return Result.success(ywzpService.xxallSelect(cid, pageNo, pageSize));
    }
    //信息详情展示()
    @ApiOperation(value = "信息详情展示",notes="数据展示")
    @RequestMapping(value = "/xxoneSelect",method = RequestMethod.POST)
    @ApiImplicitParam(name = "cid", value = "详情cid", dataType = "String")
    public Result xxoneSelect(int cid) {
        return Result.success(ywzpService.xxoneSelect(cid));
    }
    //职位列表展示
    @ApiOperation(value = "职位列表展示",notes="cid对应类别表小分类id")
    @RequestMapping(value = "/zwallSelect",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cid", value = "公司id", dataType = "int"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result zwallSelect(int cid,int pageNo,int pageSize) {
        return Result.success(ywzpService.zwallSelect(cid, pageNo, pageSize));
    }
    //职位详情展示
    @ApiOperation(value = "职位详情展示",notes="数据展示")
    @RequestMapping(value = "/zwoneSelect",method = RequestMethod.POST)
    @ApiImplicitParam(name = "cid", value = "详情cid", dataType = "String")
    public Result zwoneSelect(int cid) {
        return Result.success(ywzpService.zwoneSelect(cid));
    }
    //职位投递
    @ApiOperation(value = "职位投递",notes="")
    @RequestMapping(value = "/zwInsert",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "gongsiId", value = "公司id", dataType = "int"),
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "int"),
            @ApiImplicitParam(name = "zhiweiId", value = "职位id", dataType = "int")
    })
    public Result zwInsert(int gongsiId, int userId, int zhiweiId) {
        int i = ywzpService.zwInsert(gongsiId, userId, zhiweiId);
        if (i==0){
           return Result.failure(ResultCode.FAIL,"投递失败");
        }
        return Result.success(i);
    }


}
