package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.GlobalSearchServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Api(tags ="搜索服务")
@ResponseBody
@RequestMapping("/globalSearch")
public class GlobalSearchController {
    @Autowired
    private GlobalSearchServiceImpl searchService;

    @RequestMapping(value = "/allGlobalSearch",method = RequestMethod.POST)
    @ApiOperation(value = "准东搜索",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "article", value = "搜索关键词", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "int")
    })
    public Result allGlobalSearch(String article, int pageNo, int pageSize) {
        if (searchService.allGlobalSearch(article, pageNo, pageSize)==null){
            return Result.failure(ResultCode.FAIL,"暂无数据");
        }
        return Result.success(searchService.allGlobalSearch(article, pageNo, pageSize));
    }


}
