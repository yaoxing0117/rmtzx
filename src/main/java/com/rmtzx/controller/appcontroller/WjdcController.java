package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.Impl.WjdcServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="问卷调查")
@RequestMapping("/wjdc")
public class WjdcController {
    @Autowired
    private WjdcServiceImpl wjdcService;

    //列表返回问卷
    @ApiOperation(value = "问卷列表返回",notes="数据展示")
    @RequestMapping(value = "/wjdcAll",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "分类(生产SH,生活SC)", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几页", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "多少页", dataType = "int")
    })
    public Result wjdcAll(String category, int pageNo, int pageSize) {
        return Result.success( wjdcService.wjdcAll(category, pageNo, pageSize));
    }
    //详情问卷
    @ApiOperation(value = "详情返回",notes="数据展示")
    @RequestMapping(value = "/wjdcOne",method = RequestMethod.POST)
    @ApiImplicitParam(name = "wjId", value = "问卷详情", dataType = "String")
    public Result wjdcOne(String wjId) {
        return Result.success(wjdcService.wjdcOne(wjId));
    }

    @ApiOperation(value = "答案返回",notes="数据展示")
    @RequestMapping(value = "/upWjdc",method = RequestMethod.POST)
    @ApiImplicitParam(name = "idAll", value = "返回格式1,1,1,1,1", dataType = "String")
    public Result upWjdc(String idAll) {
        String[] split = idAll.split(",");
        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            int id = Integer.valueOf(s);
             wjdcService.upWjdc(id);
        }
        return Result.success();
    }

}
