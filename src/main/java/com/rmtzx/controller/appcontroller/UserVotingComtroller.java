package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.Impl.UserVotingServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@Component
@Api(tags ="投票")
@ResponseBody
@RequestMapping("/tp")
public class UserVotingComtroller {
    @Autowired
    private UserVotingServiceImpl userVoting;


    @RequestMapping(value = "/all",method = RequestMethod.POST)
    @ApiOperation(value = "投票展示",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "votingCategory", value = "投票分类", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "int")
    })
    public Result all(String votingCategory,
                      int pageNo,
                      int pageSize) {
        return Result.success(userVoting.all(
                votingCategory,pageNo, pageSize));
    }
    @RequestMapping(value = "/oneVoting",method = RequestMethod.POST)
    @ApiOperation(value = "投票详情",notes = "")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "optionId", value = "详情展示", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "第几行", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "一页展示数据", dataType = "int")
    })
    public Result oneVoting(String optionId,
                      int pageNo,
                      int pageSize) {
        return Result.success(userVoting.oneVoting(optionId,pageNo, pageSize));
    }
    @RequestMapping(value = "/updateVotes", method = RequestMethod.POST)
    @ApiOperation(value = "投票", notes = "id")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userName", value = "投票人", dataType = "String "),
            @ApiImplicitParam(name = "userId", value = "投票id", dataType = "int"),
            @ApiImplicitParam(name = "id", value = "选项id", dataType = "int")
    })
    public Result updateVotes(String userName,
                              int userId,
                              int id) {
            return userVoting.updateVotes(userName, userId, id);
        }
    }


