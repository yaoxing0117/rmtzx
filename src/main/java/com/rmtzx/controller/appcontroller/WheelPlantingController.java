package com.rmtzx.controller.appcontroller;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.pojo.WheelPlantingEntity;
import com.rmtzx.entity.webentity.ShowInfoWeb;
import com.rmtzx.service.appservice.Impl.WheelPlantingServiceImpl;
import com.rmtzx.service.appservice.WheelPlantingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@Api(tags ="轮播展示")
@RequestMapping("/wheel")
public class WheelPlantingController {
    @Autowired
    private WheelPlantingServiceImpl wheelPlantingService;

    @ApiOperation(value = "轮播展示",notes="数据展示")
    @RequestMapping(value = "/allWheel",method = RequestMethod.POST)
    @ApiImplicitParam(name = "category", value = "PC/app轮播类型", dataType = "String")
    public Result allWheel(@RequestParam("category")String category){
        List<WheelPlantingEntity> wheelPlantingEntities = wheelPlantingService.allWheel(category);
        ShowInfoWeb showInfoWeb =new ShowInfoWeb();
        showInfoWeb.setNewsInfoURL("/wheel/oneWheel");
        showInfoWeb.setLimitShowList(wheelPlantingEntities);
        return Result.success(showInfoWeb);
    }
    @ApiOperation(value = "轮播详情",notes="category分类,id新闻id")
    @RequestMapping(value = "/oneWheel",method = RequestMethod.POST)
    public Result oneWheel(@RequestParam("category")String category,
                           @RequestParam("id")int id){
        WheelPlantingEntity wheelPlantingEntity = wheelPlantingService.oneWheel(id, category);
        return Result.success(wheelPlantingEntity);
    }
    @ApiOperation(value = "点赞",notes="新闻id,类别")
    @RequestMapping(value = "/dzUpdate",method = RequestMethod.POST)
    public Result dzUpdate(int id,String category){
        return Result.success(wheelPlantingService.dzUpdate(id, category));
    } @ApiOperation(value = "mp3列表",notes="category分类")
    @RequestMapping(value = "/allMP3",method = RequestMethod.POST)
    public Result allMP3(String category, int pageNo, int pageSize){

        return Result.success(wheelPlantingService.allMP3(category, pageNo, pageSize));
    }
    @ApiOperation(value = "详情Mp3",notes="新闻id,类别")
    @RequestMapping(value = "/oneMP3",method = RequestMethod.POST)
    public Result oneMP3(int id){
        return Result.success(wheelPlantingService.oneMP3(id));
    }


}
