package com.rmtzx.controller.appcontroller.zwController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.zwService.IAnnouncementFbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="公告信息")
@RequestMapping("/gg")
public class AnnouncementController {

    @Autowired
    private IAnnouncementFbService announcementService;

    @ApiOperation(value = "展示审核通过公告信息",notes="公告数据分页展示")
    @RequestMapping(value = "/ggall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "审核状态（0：未审核，1：审核通过）", dataType = "int"),
            @ApiImplicitParam(name = "m", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "n", value = "每页几条", dataType = "int")
    })
    public Result findAll(@RequestParam("code") int arg0, @RequestParam("m") int arg1, @RequestParam("n") int arg2){
        Result success = Result.success(announcementService.limitShowInfo(arg0,arg1,arg2));
        return success;
    }

    // @ApiOperation(value = "审核",notes="公告信息批量审核")
    // @RequestMapping(value = "/update.gg",method =RequestMethod.POST)
    //
    // public String  ggUpdate(@RequestParam("ids") Integer[]  arg0,
    //                         @RequestParam("code") int arg1){
    //     String s = announcementService.updateByPrimaryKey(arg1, arg0);
    //     return s;
    // }
    @ApiOperation(value = "展示公告详情",notes="公告详情展示")
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "公告详情的id", dataType = "int")
    public Result ggDetail(int id){
        Result result = announcementService.findByCategoryAndId("公告", id);
        return result;
    }


}
