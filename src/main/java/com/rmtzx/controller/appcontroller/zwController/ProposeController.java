package com.rmtzx.controller.appcontroller.zwController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.zwService.ProposeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="建议信息")
@RequestMapping("/jy")
public class ProposeController {
    @Autowired
    private ProposeService proposeService;

    @ApiOperation(value = "展示建议信息",notes="建议信息展示")
    @RequestMapping(value = "/jyall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "arg0", value = "部门id", dataType = "int"),
            @ApiImplicitParam(name = "arg1", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "arg2", value = "每页几条", dataType = "int"),
            @ApiImplicitParam(name = "arg3", value = "处理状态（0:未处理,1:已处理）", dataType="int")
    })
    public Result findAll(@RequestParam("arg0") int arg0, @RequestParam("arg1") int arg1, @RequestParam("arg2") int arg2, @RequestParam("arg3")int arg3){
        Result success = Result.success(proposeService.limitShowInfo(arg0,arg1,arg2,arg3));
        return success;
    }

    @ApiOperation(value = "处理",notes="建议信息处理")
    @RequestMapping(value = "/update.jy",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "arg0",value = "处理内容",dataType = "String"),
            @ApiImplicitParam(name = "arg1",value = "建议id",dataType = "int")
    })
    public String  ggUpdate(@RequestParam("arg0") String  arg0,
                            @RequestParam("arg1") int arg1){
        String s = proposeService.updateByPrimaryKey(arg1, arg0);
        return s;
    }

    @ApiOperation(value = "展示个人咨询信息",notes="个人咨询数据展示")
    @RequestMapping(value = "/myZx",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "arg3", value = "审核状态（0：未审核，1：审核通过）", dataType = "int"),
            @ApiImplicitParam(name = "arg1", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "arg2", value = "每页几条", dataType = "int"),
            @ApiImplicitParam(name = "arg0", value = "用户id",dataType = "int")
    })
    public Result findMyJy(@RequestParam("arg0") int arg0, @RequestParam("arg1") int arg1, @RequestParam("arg2") int arg2, @RequestParam("arg3")int arg3){
        Result success = Result.success(proposeService.findMyJy(arg0,arg1,arg2,arg3));
        return success;
    }
}
