package com.rmtzx.controller.appcontroller.zwController;


import com.rmtzx.entity.bo.Result;
import com.rmtzx.entity.bo.ResultCode;
import com.rmtzx.entity.pojo.ShowInfo;
import com.rmtzx.service.appservice.zwService.IWebAnnouncementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 *  近期公告前端控制器
 * </p>
 *
 * @author DT.wom
 * @since 2020-02-07
 */
@Controller
@ResponseBody
@Api(tags ="近期公告")
@RequestMapping("/jqgg")
public class WebAnnouncementController {

    @Autowired
    private IWebAnnouncementService webAnnouncementService;

    @ApiOperation(value = "近期公示列表展示",notes = "近期公示列表展示")
    @RequestMapping(value = "/list",method = RequestMethod.POST)
    public Result findAll(){
        ShowInfo showInfo = webAnnouncementService.limitShowInfo(1, 1, 20);
        if (showInfo==null){
            Result.failure(ResultCode.FAIL);
        }
        return Result.success(showInfo.getLimitShowList());
    }

    @ApiOperation(value = "详情展示",notes = "根据id查详情")
    @RequestMapping(value = "/findone",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "近期公告详情的id", dataType = "int")
    public Result findOne(Integer id){
        Result byCategoryAndId = webAnnouncementService.findByCategoryAndId("", id);
        return byCategoryAndId;
    }
}
