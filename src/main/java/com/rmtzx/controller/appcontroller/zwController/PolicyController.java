package com.rmtzx.controller.appcontroller.zwController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.zwService.IPolicyFbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 政策信息控制器方法
 */
@Controller
@ResponseBody
@RequestMapping("/zc")
@Api(tags = "政策信息")
public class PolicyController {

    @Autowired
    private IPolicyFbService policyService;

    @ApiOperation(value = "展示审核通过政策信息",notes="政策数据分页展示")
    @RequestMapping(value = "/zcall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "审核状态（0：未审核，1：审核通过）", dataType = "int"),
            @ApiImplicitParam(name = "m", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "n", value = "每页几条", dataType = "int")
    })
    public Result findAll(@RequestParam("code") int arg0, @RequestParam("m") int arg1, @RequestParam("n") int arg2){
        Result success = Result.success(policyService.limitShowInfo(arg0,arg1,arg2));
        return success;
    }

    // @ApiOperation(value = "审核",notes="政策信息批量审核")
    // @RequestMapping(value = "/update.zc",method =RequestMethod.POST)
    // public String  ggUpdate(@RequestParam("ids") Integer[]  arg0,
    //                         @RequestParam("code") int arg1){
    //     String s = policyService.updateByPrimaryKey(arg1, arg0);
    //     return s;
    // }

    @ApiOperation(value = "展示政策详情",notes="政策详情展示")
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "党务详情的id", dataType = "int")
    public Result ggDetail(int id){
        Result result = policyService.findByCategoryAndId("公告", id);
        return result;
    }
}
