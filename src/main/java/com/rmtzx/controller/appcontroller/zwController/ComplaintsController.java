package com.rmtzx.controller.appcontroller.zwController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.zwService.ComplaintsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
@Api(tags ="投诉信息")
@RequestMapping("/ts")
public class ComplaintsController {
    @Autowired
    private ComplaintsService complaintsService;

    @ApiOperation(value = "展示投诉信息",notes="投诉数据展示")
    @RequestMapping(value = "/tsall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "arg3", value = "处理状态（0：未处理，1：已处理）", dataType = "int"),
            @ApiImplicitParam(name = "arg1", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "arg2", value = "每页几条", dataType = "int"),
            @ApiImplicitParam(name = "arg0", value = "部门id",dataType = "int")
    })
    public Result findAll(@RequestParam("arg0") int arg0, @RequestParam("arg1") int arg1, @RequestParam("arg2") int arg2, @RequestParam("arg3")int arg3){
        Result success = Result.success(complaintsService.limitShowInfo(arg0,arg1,arg2,arg3));
        return success;
    }

    @ApiOperation(value = "审核",notes="公告信息批量审核")
    @RequestMapping(value = "/update.ts",method =RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "arg0",value = "处理内容",dataType = "String"),
            @ApiImplicitParam(name = "arg1",value = "投诉id",dataType = "int")
    })
    public String  ggUpdate(@RequestParam("arg0") String  arg0,
                            @RequestParam("arg1") int arg1){
        String s = complaintsService.updateByPrimaryKey(arg1, arg0);
        return s;
    }
    @ApiOperation(value = "个人投诉信息展示",notes="个人投诉展示")
    @RequestMapping(value = "/myTs",method = RequestMethod.POST)
    @ApiImplicitParams({
                    @ApiImplicitParam(name = "arg3", value = "处理状态（0：未处理，1：已处理）", dataType = "int"),
                    @ApiImplicitParam(name = "arg1", value = "当前第几页", dataType = "int"),
                    @ApiImplicitParam(name = "arg2", value = "每页几条", dataType = "int"),
                    @ApiImplicitParam(name = "arg0", value = "用户id",dataType = "int")
            })
    public Result findMyTs(@RequestParam("arg0") int arg0, @RequestParam("arg1") int arg1, @RequestParam("arg2") int arg2, @RequestParam("arg3")int arg3){
        Result success = Result.success(complaintsService.findMyTs(arg0,arg1,arg2,arg3));
        return success;
    }


}
