package com.rmtzx.controller.appcontroller.zwController;

import com.rmtzx.entity.bo.Result;
import com.rmtzx.service.appservice.zwService.IPartyWorkFbService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 党务信息控制器方法
 */
@Controller
@ResponseBody
@RequestMapping("/dw")
@Api(tags = "党务信息")
public class PartWorkController {

    @Autowired
    private IPartyWorkFbService partWorkService;

    @ApiOperation(value = "展示审核通过党务信息",notes="党务数据分页展示")
    @RequestMapping(value = "/dwall",method = RequestMethod.POST)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "审核状态（0：未审核，1：审核通过）", dataType = "int"),
            @ApiImplicitParam(name = "m", value = "当前第几页", dataType = "int"),
            @ApiImplicitParam(name = "n", value = "每页几条", dataType = "int")
    })
    public Result findAll(@RequestParam("code") int arg0, @RequestParam("m") int arg1, @RequestParam("n") int arg2){

        Result success = Result.success(partWorkService.limitShowInfo(arg0, arg1, arg2));

        return success;
    }

    // @ApiOperation(value = "审核",notes="党务信息批量审核")
    // @RequestMapping(value = "/update.dw",method =RequestMethod.POST)
    // public String  ggUpdate(@ApiParam("用户数组id") @RequestParam Integer[]  arg0,
    //                         @ApiParam("审核通过的")@RequestParam int arg1){
    //     String s = partWorkService.updateByPrimaryKey(arg1,arg0);
    //     return s;
    // }

    @ApiOperation(value = "展示党务详情",notes="党务详情展示")
    @RequestMapping(value = "/detail",method = RequestMethod.POST)
    @ApiImplicitParam(name = "id", value = "党务详情的id", dataType = "int")
    public Result ggDetail(int id){
        Result result = partWorkService.findByCategoryAndId("公告", id);
        return result;
    }
}
