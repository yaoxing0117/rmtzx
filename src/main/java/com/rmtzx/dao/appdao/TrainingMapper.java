package com.rmtzx.dao.appdao;

import com.rmtzx.entity.appentity.TrainingEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface TrainingMapper {
    //展示
    @Select("SELECT id,class_name as className,class_begins as classBegins,organization_name as organizationName," +
            "registration_time as registrationTime,class_cost as classCost,content," +
            "entry_requirements as entryRequirements,contact_information as contactInformation,company_introduction as " +
            " companyIntroduction FROM institutional_training where code=1  ORDER BY class_begins desc LIMIT #{pageNo},#{pageSize}")
    List<TrainingEntity> allTraining( @Param("pageNo")int pageNo,
                                      @Param("pageSize")int pageSize);
    //详情
    @Select("SELECT id,class_name as className,class_begins as classBegins,organization_name as organizationName, " +
            " registration_time as registrationTime,class_cost as classCost,content," +
            " entry_requirements as entryRequirements,contact_information as contactInformation,company_introduction as " +
            " companyIntroduction FROM institutional_training where id=#{id}")
    TrainingEntity oneTraining(@Param("id")int id);

    @Select("SELECT COUNT(*) FROM institutional_training")
    int paperPageCount();

}
