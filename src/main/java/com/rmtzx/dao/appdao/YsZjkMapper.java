package com.rmtzx.dao.appdao;

import com.rmtzx.entity.appentity.YsZjkEntity;
import com.rmtzx.entity.appentity.YsZjkNameEntity;
import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface YsZjkMapper {
    //类别返回
    @Select("SELECT id, experts_category as expertsCategory,pid FROM expert_library ")
    List<YsZjkEntity> zjAll();
    //专家分页展示
    @Select("SELECT id,expert_name as expertName,expert_unit as expertUnit,expert_post as expertPost," +
            "expert_title as expertTitle,expert_field as expertField,expert_brief as expertBrief " +
            "FROM expert_library_name where cid=#{cid}  LIMIT #{pageNo},#{pageSize}")
    List<YsZjkNameEntity> zjNameAll(@Param("cid")int cid,
                                    @Param("pageNo")int pageNo,
                                    @Param("pageSize")int pageSize);
    //专家详情展示
    @Select("SELECT id,expert_name as expertName,expert_unit as expertUnit,expert_post as expertPost," +
            "expert_title as expertTitle,expert_field as expertField,expert_brief as expertBrief " +
            "FROM expert_library_name where id=#{id}")
    YsZjkNameEntity zjone(@Param("id") int id);
    //专家查询展示
    @Select("SELECT id,expert_name as expertName,expert_unit as expertUnit,expert_post as expertPost," +
            "expert_title as expertTitle,expert_field as expertField,expert_brief as expertBrief " +
            "FROM expert_library_name where expert_name=#{expertName}  LIMIT 0,10")
    List<YsZjkNameEntity> zjAll1(@Param("expertName")String expertName);




}
