package com.rmtzx.dao.appdao.htDao;

import com.rmtzx.entity.appentity.htEntity.UserPower;
import org.mapstruct.Mapper;

@Mapper
public interface UserPowerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserPower record);

    int insertSelective(UserPower record);

    UserPower selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserPower record);

    int updateByPrimaryKey(UserPower record);
}