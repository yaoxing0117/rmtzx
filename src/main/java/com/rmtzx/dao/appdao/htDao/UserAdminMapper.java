package com.rmtzx.dao.appdao.htDao;

import com.rmtzx.entity.appentity.htEntity.UserAdmin;
import org.mapstruct.Mapper;

@Mapper
public interface UserAdminMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserAdmin record);

    int insertSelective(UserAdmin record);

    UserAdmin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserAdmin record);

    int updateByPrimaryKey(UserAdmin record);
}