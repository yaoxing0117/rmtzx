package com.rmtzx.dao.appdao.htDao;

import com.rmtzx.entity.appentity.htEntity.UserRole;
import org.mapstruct.Mapper;

@Mapper
public interface UserRoleMapper {
    int deleteByPrimaryKey(Integer rid);

    int insert(UserRole record);

    int insertSelective(UserRole record);

    UserRole selectByPrimaryKey(Integer rid);

    int updateByPrimaryKeySelective(UserRole record);

    int updateByPrimaryKey(UserRole record);
}