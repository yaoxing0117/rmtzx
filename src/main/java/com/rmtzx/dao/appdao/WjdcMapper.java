package com.rmtzx.dao.appdao;

import com.rmtzx.entity.appentity.WjdcDAEntity;
import com.rmtzx.entity.appentity.WjdcNameEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface WjdcMapper {
    //列表返回问卷
    @Select("SELECT id,title,content,picture,start_time as startTime," +
            "stop_time as stopTime,uid,category FROM wjdc_name where category=#{category} " +
            " ORDER BY stop_time desc LIMIT #{pageNo},#{pageSize}")
    List<WjdcNameEntity> wjdcAll(@Param("category")String category,
                                 @Param("pageNo")int pageNo,
                                 @Param("pageSize")int pageSize);
    //详情问卷
    @Select("SELECT id,title,picture,category,pid from wjdc_da where wj_id=#{wjId}")
    List<WjdcDAEntity> wjdcOne(@Param("wjId")String wjId);
    //开始答题
    @Update("UPDATE wjdc_da SET count=#{count} WHERE id=#{id}")
    int upWjdc(@Param("id")int id,
             @Param("count")int count);
    //
    @Select("select count from wjdc_da where id=#{id}")
    int seWjdc(@Param("id")int id);

}
