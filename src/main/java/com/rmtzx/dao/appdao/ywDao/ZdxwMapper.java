package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;
@Mapper
public interface ZdxwMapper {
    //ios查询固定字段
    @Select("select id,title,source,release_time as releaseTime,d_z,picture_url as pictureUrl,differentiate from zd_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZdxwEntity> zdxwScreen(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize);
    //审核
    @Update("update zd_news_fb set code=#{code} where id=#{ids}")
    int updateState(@Param("ids") int id,@Param("code") int code);
    //表总数据
    @Select("SELECT COUNT(*) FROM zd_news_fb WHERE `code`=1")
    int zdxwPageCount();
    //数据分页查询
    @Select("SELECT id,source,title,article_source,release_time as releaseTime,picture_url as pictureUrl,differentiate FROM zd_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZdxwEntity> zdxwPaging(@Param("pageNo")int pageNo,
                                  @Param("pageSize")int pageSize );
    //id查询数据
    @Select("select id,source,title,article_source,article,release_time as releaseTime,abs_url as absUrl,differentiate,local_url as localUrl,d_z from zd_news_fb where code=1 and id=#{id}")
    ZdxwEntity singleDate(@Param("id")int id);
    //删除数据
    @Delete("delete from zd_news_fb where id=#{id}")
    int deleteId(@Param("id")int id);
    //查询当前点赞
    @Select("select d_z from zd_news_fb where id=#{id}")
    int d_zId(@Param("id")int id);
    //点赞
    @Update("update zd_news_fb set d_z=#{up} where id=#{id}")
    int thumbsUp(@Param("up")int up,@Param("id")int id);
    //评论
    @Insert("INSERT INTO zzq_comment (commentator,comment_content," +
            "commentary_time,comment_ID,comment_category)VALUES" +
            "(#{commentator},#{commentContent},#{commentaryTime},#{commentID},#{commentCategory})")
    int commentInsert(@Param("commentator") String commentator,
                      @Param("commentContent")String commentContent,
                      @Param("commentaryTime")String commentaryTime,
                      @Param("commentID")int commentID,
                      @Param("commentCategory")String commentCategory);
    //评论展示分页
    @Select("SELECT * FROM zzq_comment  LIMIT #{pageNo},#{pageSize}")
    List<CommentEntity> commentPaging(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize );
    //查询当前点赞
    @Select("select d_z from zd_news_fb where id=#{id}")
    int pl(@Param("id")int commentID);
    //点赞
    @Update("update zd_news_fb set d_z=#{commentQuantity} where id=#{id}")
    int plUp(@Param("commentQuantity")int commentQuantity,@Param("id")int id);
}
