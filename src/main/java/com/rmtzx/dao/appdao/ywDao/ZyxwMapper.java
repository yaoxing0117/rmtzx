package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.appentity.ywEntity.ZyxwEntity;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ZyxwMapper {
    //审核过
    @Select("select * from zy_news_fb where code=1")
    List<ZyxwEntity> zyxwAll();
    //ios查询固定字段
    @Select("select id,title,source,release_time as releaseTime,d_z,picture_url as pictureUrl,differentiate from zy_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZyxwEntity> zyxwScreen(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize);
    //审核数据
    @Update("update zy_news_fb set code=#{code} where id=#{ids}")
    int updateState(@Param("code") int code, @Param("ids") int ids);
    //表总数据
    @Select("SELECT COUNT(*) FROM zy_news_fb WHERE `code`=1")
    int zyxwPageCount();
    //数据分页查询
    @Select("SELECT id,source,title,article_source,release_time as releaseTime,picture_url as pictureUrl,differentiate FROM zy_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZyxwEntity> zyxwPaging(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize );
    //id查询数据
    @Select("select id,source,title,article_source,article,release_time as releaseTime,abs_url as absUrl,differentiate,local_url as localUrl,d_z from zy_news_fb where code=1 and id=#{id}")
    ZyxwEntity singleDate(@Param("id")int id);
    //删除数据
    @Delete("delete from zy_news_fb where id=#{id}")
    int deleteId(@Param("id")int id);
    //查询当前点赞
    @Select("select d_z from zy_news_fb where id=#{id}")
    int d_zId(@Param("id")int id);
    //点赞
    @Update("update zy_news_fb set d_z=#{up1} where id=#{id}")
    int thumbsUp(@Param("up1")int up1,@Param("id")int id);
}
