package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.appentity.AudioEntity;
import com.rmtzx.entity.pojo.WheelPlantingEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface WheelPlantingMapper {
    //轮播图片展示
    @Select("select id,title,time,title_picture as titlePicture,category,differentiate " +
            "from wheel_planting where code=1 and category=#{category} ORDER BY time desc  LIMIT 0,4")
    List<WheelPlantingEntity> allWheel(@Param("category")String category);
    //轮播新闻详情
    @Select("select title,time,text,title_picture as titlePicture,publisher,picture,d_z" +
            ",pre_url as preUrl,differentiate from wheel_planting where code=1 and category=#{category} and id=#{id} ")
    WheelPlantingEntity oneWheel(@Param("id")int id,@Param("category")String category);
    //查询点赞次数
    @Select("select d_z from wheel_planting where code=1 and id=#{id} and category=#{category}")
    int dzAll(@Param("id")int id,@Param("category")String category);
    //增加点赞次数
    @Update("UPDATE wheel_planting SET d_z=#{d_z} where id=#{id}")
    int dzUpdate(@Param("d_z")int d_z,@Param("id")int id);
    //表总数据
    @Select("SELECT COUNT(*) FROM wheel_planting WHERE code=1")
    int zxwPageCount();
    //MP3展示分页
    @Select("select id,category,up_time as upTime,picture,mp3_url as mp3Url,editor,article" +
            " from mp3_table where code=1 and category=#{category} ORDER BY id desc  " +
            "LIMIT #{pageNo},#{pageSize}")
    List<AudioEntity> allMP3(@Param("category")String category,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);
    //MP3详情
    @Select("select id,category,up_time as upTime,picture,mp3_url as mp3Url,editor,article " +
            "from mp3_table where id=#{id} ")
    AudioEntity oneMP3(@Param("id")int id);
    @Select("SELECT COUNT(*) FROM mp3_table WHERE code=1 and category=#{category}")
    int mp3wPageCount(@Param("category")String category);
}
