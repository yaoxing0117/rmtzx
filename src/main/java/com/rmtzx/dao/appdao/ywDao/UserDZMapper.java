package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.appentity.ywEntity.UserDZEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserDZMapper {
    //查询用户是否点赞
    @Select("select * from user_dz where user_id=#{userId} and user_id=#{newsId} and news_category=#{newsCategory} ")
    UserDZEntity selectOne(@Param("userId") int userId,
                           @Param("newsCategory") String newsCategory,
                           @Param("newsId") int newsId);

    //点赞
    @Insert("INSERT INTO `user_dz` (news_title,news_time,news_category,user_id,reserve_one,news_id)" +
            " VALUES (#{newsTitle},#{newsTime},#{newsCategory},#{userId},#{reserveOne},#{newsId})")
    int insertDZ(@Param("newsTitle") String newsTitle,
                 @Param("newsTime") String newsTime,
                 @Param("newsCategory") String newsCategory,
                 @Param("userId") int userId,
                 @Param("reserveOne") String reserveOne,
                 @Param("newsId") int newsId);

    // 根据用户id查询点赞列表
    @Select("select * from user_dz where user_id=#{userId} ORDER BY `reserve_one` DESC LIMIT #{pageNo},#{pageSize} ")
    List<UserDZEntity> selectAll(@Param("userId") int userId,
                                 @Param("pageNo") int pageNo,
                                 @Param("pageSize") int pageSize);

    @Select("SELECT COUNT(id) FROM user_dz where user_id=#{userId}")
    int newsPaperPageCount(@Param("userId")int userId);


}
