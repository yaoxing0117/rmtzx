package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.pojo.CommentEntity;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CommentMapper {
    //新闻评论查询
    @Select("select comment_content,commentator,commentary_time from praise where journalism_id=#{journalismID}")
    List<CommentEntity> queryComments(int journalismID);
    //删除评论
    int delComment(int journalismID,int comment);
    //评论审核

}
