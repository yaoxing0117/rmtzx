package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.NewsPaperEntity;

import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface NewsPaperMapper {
    //来源+分页查询
    @Select("select id,title_h1 as titleH1,title_h2 as titleH2,title_h3 as titleH3," +
            "title_h4 as titleH4,picture_url as pictureUrl,release_time as releaseTime,source " +
            ",differentiate from newspaper_fb  " +
            "where code=1 and source=#{source} " +
            " ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<NewsPaperEntity> paperAll(@Param("source")String source,
                                   @Param("pageNo")int pageNo,
                                   @Param("pageSize")int pageSize );

    //表总条数查询
    @Select("SELECT COUNT(*) FROM newspaper_fb where code=1")
    int newsPaperPageCount();
    //查询通过id查询详情数据
    @Select("select id,title_h1 as titleH1,title_h2 as titleH2,title_h3 as titleH3,title_h4 as titleH4, \n" +
            "picture_url as pictureUrl,release_time as releaseTime ,source,news_article as newsArticle \n" +
            ",abs_url as absUrl,differentiate,local_url as localUrl,d_z from newspaper_fb where code=1 and id=#{id}")
    NewsPaperEntity  newsPaperDate(@Param("id")int id);
    //删除数据
    @Delete("delete from newspaper_fb where id=#{id}")
    int deleteId(@Param("id")int id);
    //查询当前点赞
    @Select("select d_z from newspaper_fb where id=#{id}")
    int d_zId(@Param("id")int id);
    //点赞
    @Update("update newspaper set d_z=#{up} where id=#{id}")
    int thumbsUp(@Param("up")int up,@Param("id")int id);
    //评论
    @Insert("INSERT INTO zzq_comment (commentator,comment_content," +
            "commentary_time,comment_ID,comment_category)VALUES(#{commentator},#{commentContent},#{commentaryTime},#{commentID},#{commentCategory})")
    int commentInsert(@Param("commentator") String commentator,
                      @Param("commentContent")String commentContent,
                      @Param("commentaryTime")String commentaryTime,
                      @Param("commentID")String commentID,
                      @Param("commentCategory")String commentCategory);
    //评论展示分页
    @Select("SELECT * FROM newspaper_fb LIMIT #{pageNo},#{pageSize}")
    List<CommentEntity> commentPaging(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize );
}
