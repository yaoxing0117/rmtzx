package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.pojo.CommentEntity;
import com.rmtzx.entity.appentity.ywEntity.ZzqxwEntity;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ZzqxwMapper {
    //ios查询固定字段
    @Select("select id,title,source,release_time,d_z,picture_url as pictureUrl,differentiate from zzq_news_fb " +
            "where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZzqxwEntity> zzqScreen(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize);
    //审核
    @Update("update zzq_news_fb set code=#{code} where id=#{ids}")
    int updateState(@Param("ids") int id,@Param("code") int code);
    //表总数据
    @Select("SELECT COUNT(*) FROM zzq_news_fb WHERE `code`=1")
    int zzqxwPageCount();
    //数据分页查询
    @Select("SELECT id,source,title,article_source,release_time as releaseTime " +
            ",picture_url as pictureUrl,differentiate FROM zzq_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZzqxwEntity> zzqxwPaging(@Param("pageNo")int pageNo,
                                  @Param("pageSize")int pageSize );
    //id查询数据
    @Select("select id,source,title,article_source,article,release_time as releaseTime," +
            "abs_url as absUrl,differentiate,local_url as localUrl,d_z from zzq_news_fb where code=1 and id=#{id}")
    ZzqxwEntity singleDate(@Param("id")int id);
    //删除数据
    @Delete("delete from zzq_news_fb where id=#{id}")
    int deleteId(@Param("id")int id);
    //查询当前点赞
    @Select("select d_z from zzq_news_fb where id=#{id}")
    int d_zId(@Param("id")int id);
    //点赞
    @Update("update zzq_news_fb set d_z=#{up} where id=#{id}")
    int thumbsUp(@Param("up")int up,@Param("id")int id);
    //评论
    @Insert("INSERT INTO zzq_comment (commentator,comment_content," +
            "commentary_time,comment_ID,comment_category,userId,user_tx)VALUES(#{commentator},#{commentContent}," +
            "#{commentaryTime},#{commentID},#{commentCategory},#{userId},#{userTx})")
    int commentInsert(@Param("commentator") String commentator,
                      @Param("commentContent")String commentContent,
                      @Param("commentaryTime")String commentaryTime,
                      @Param("userTx")String userTx,
                      @Param("commentID")int commentID,
                      @Param("userId")int userId,
                      @Param("commentCategory")String commentCategory);
    //准东新闻 评论数量统计
    @Select("SELECT  COUNT(comment_ID) FROM zzq_comment where comment_ID=#{commentID} ")
    int commentZD(@Param("commentID")int commentID);

    //添加评论数量
    @Update("UPDATE zd_news_fb SET comment_quantity=#{commentQuantity} WHERE (id=#{id})")
    int upComment(@Param("commentQuantity")int commentQuantity,
                  @Param("id")int id);
    //评论展示分页
    @Select("SELECT * FROM zzq_comment where comment_category=#{commentCategory}  " +
            "  and comment_ID=#{commentID} ORDER BY commentary_time desc LIMIT #{pageNo},#{pageSize}")
    List<CommentEntity> commentPaging( @Param("commentCategory")String commentCategory,
                                       @Param("commentID")int commentID,
                                       @Param("pageNo")int pageNo,
                                       @Param("pageSize")int pageSize );

}
