package com.rmtzx.dao.appdao.ywDao;

import com.rmtzx.entity.appentity.ywEntity.ZxwEntity;
import org.apache.ibatis.annotations.*;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ZxwMapper {
    //审核过
    @Select("select * from z_news_fb where code=1")
    List<ZxwEntity> zxwAll();
    //审核数据
    @Update("update z_news_fb set code=#{code} where id=#{ids}")
    int updateState(@Param("code") int code,@Param("ids") int ids);
    //app展示列表形式 固定字段
    @Select("select id,title,source,release_time as releaseTime,d_z,picture_url as pictureUrl,differentiate from z_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZxwEntity> zxwScreen(@Param("pageNo")int pageNo, @Param("pageSize")int pageSize);
    //表总数据
    @Select("SELECT COUNT(*) FROM z_news_fb WHERE code=1")
    int zxwPageCount();
    //数据分页查询
    @Select("SELECT id,source,title,article_source,release_time as releaseTime,abs_url as absUrl" +
            ",picture_url as pictureUrl,differentiate FROM z_news_fb where code=1 ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<ZxwEntity> zxwPaging(@Param("pageNo")int pageNo,@Param("pageSize")int pageSize );
    //id查询数据
    @Select("select id,source,title,article_source,article,release_time as releaseTime,abs_url as absUrl,differentiate,local_url as localUrl,d_z from z_news_fb where code=1 and id=#{id}")
    ZxwEntity singleDate(@Param("id")int id);
    //删除数据
    @Delete("delete from z_news_fb where id=#{id}")
    int deleteId(@Param("id")int id);
    //查询当前点赞
    @Select("select d_z from z_news_fb where id=#{id}")
    int d_zId(@Param("id")int id);
    //点赞
    @Update("update z_news_fb set d_z=#{up} where id=#{id}")
    int thumbsUp(@Param("up")int up,@Param("id")int id);
}
