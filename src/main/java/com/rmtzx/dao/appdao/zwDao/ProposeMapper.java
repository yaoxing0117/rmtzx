package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.Propose;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ProposeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Propose record);

    int insertSelective(Propose record);

    Propose selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Propose record);

    int updateByPrimaryKeyWithBLOBs(Propose record);

    int updateByPrimaryKey(int arg1, String arg0);

    int updateByCode(int arg1, int[] arg0);

    List<Propose> findAll(int arg0, int arg1, int arg2, int arg3);

    int findTotalCountAndPage();

    List<Propose> findMyJy(int arg0, int arg1, int arg2, int arg3);
}