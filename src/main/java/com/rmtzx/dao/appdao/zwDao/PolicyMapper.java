package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.Policy;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface PolicyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Policy record);

    int insertSelective(Policy record);

    Policy selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Policy record);

    int updateByPrimaryKey(int arg1, int arg0);

    int updateByCode(int arg1, int[] arg0);

    List<Policy> findAll(int arg0, int arg1, int arg2);

    int findTotalCountAndPage();

    Policy findByCategoryAndId(@Param("cateGory") String cateGory, @Param("id") int id);
}