package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.GovernmentAffairs;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface GovernmentAffairsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(GovernmentAffairs record);

    int insertSelective(GovernmentAffairs record);

    GovernmentAffairs selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(GovernmentAffairs record);

    int updateByPrimaryKey(int arg1, int arg0);

    int updateByCode(int arg1, int[] arg0);

    List<GovernmentAffairs> findAll(int arg0, int arg1, int arg2);

    int findTotalCountAndPage();

    GovernmentAffairs findByCategoryAndId(@Param("cateGory") String cateGory, @Param("id") int id);
}