package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.Announcement;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface AnnouncementMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Announcement record);

    int insertSelective(Announcement record);

    Announcement selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Announcement record);

    int updateByPrimaryKey(int arg1, int arg0);

    int updateByCode(int arg1, int[] arg0);

    List<Announcement> findAll(int arg0, int arg1, int arg2);

    int findTotalCountAndPage();

    Announcement findByCategoryAndId(@Param("cateGory") String cateGory, @Param("id") int id);
}