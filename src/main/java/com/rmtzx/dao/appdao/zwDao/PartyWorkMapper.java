package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.PartyWork;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;


import java.util.List;

@Mapper
public interface PartyWorkMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(PartyWork record);

    int insertSelective(PartyWork record);

    PartyWork selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(PartyWork record);

    int updateByPrimaryKey(int arg1, Integer arg0);

    int updateByCode(@Param("arg1") int arg1, @Param("arg0") Integer[] arg0);

    List<PartyWork> findAll(int arg0, int arg1, int arg2);

    int findTotalCountAndPage();

    PartyWork findByCategoryAndId(@Param("cateGory") String cateGory, @Param("id") int id);
}