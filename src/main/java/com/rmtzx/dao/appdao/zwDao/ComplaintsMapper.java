package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.Complaints;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ComplaintsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Complaints record);

    int insertSelective(Complaints record);

    Complaints selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Complaints record);

    int updateByPrimaryKey(Complaints record);

    int updateByPrimaryKey(int arg0, String arg1);

    int updateByCode(int arg1, int[] arg0);

    List<Complaints> findAll(int arg0, int arg1, int arg2, int arg3);

    int findTotalCountAndPage();

    List<Complaints> findMyTs(int arg0, int arg1, int arg2, int arg3);
}