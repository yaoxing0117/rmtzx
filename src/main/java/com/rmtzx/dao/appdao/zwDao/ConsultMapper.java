package com.rmtzx.dao.appdao.zwDao;

import com.rmtzx.entity.appentity.zwEntity.Consult;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * 咨询Mapper
 */
@Mapper
public interface ConsultMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Consult record);

    int insertSelective(Consult record);

    Consult selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Consult record);

    int updateByPrimaryKeyWithBLOBs(Consult record);

    int updateByPrimaryKey(Consult record);

    int updateByPrimaryKey(int arg0, String arg1);

    int updateByCode(int arg1, int[] arg0);

    List<Consult> findAll(int arg0, int arg1, int arg2, int arg3);

    int findTotalCountAndPage();

    List<Consult> findMyZx(int arg0, int arg1, int arg2, int arg3);
}