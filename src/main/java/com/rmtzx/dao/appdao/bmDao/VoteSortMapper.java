package com.rmtzx.dao.appdao.bmDao;

import com.rmtzx.entity.appentity.bmEntity.VoteSort;
import org.mapstruct.Mapper;

@Mapper
public interface VoteSortMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(VoteSort record);

    int insertSelective(VoteSort record);

    VoteSort selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(VoteSort record);

    int updateByPrimaryKey(VoteSort record);
}