package com.rmtzx.dao.appdao.bmDao;

import com.rmtzx.entity.appentity.bmEntity.VoteOption;
import org.apache.ibatis.annotations.Param;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface VoteOptionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(VoteOption record);

    int insertSelective(VoteOption record);

    List<VoteOption> selectByPrimaryKey(@Param("title") String title);

    int updateByPrimaryKeySelective(VoteOption record);

    int updateByPrimaryKey(VoteOption record);

    List<VoteOption> findBySortId(@Param("sortId") int sortId);
}