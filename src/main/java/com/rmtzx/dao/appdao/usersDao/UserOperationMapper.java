package com.rmtzx.dao.appdao.usersDao;

import com.rmtzx.entity.appentity.userEntity.*;
import com.rmtzx.entity.pojo.CommentEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserOperationMapper {
    //增加收藏
    @Insert("INSERT INTO user_collection_record(collection_user,collection_article," +
            "collection_source,collection_time,collection_article_category,collection_article_id,user_id)" +
            "VALUES(#{collectionUser},#{collectionArticle}," +
            "#{collectionSource},#{collectionTime}," +
            "#{collectionArticleCategory},#{collectionArticleId},#{userId})")
    int userRecord(@Param("collectionUser") String collectionUser,
                   @Param("collectionArticle") String collectionArticle,
                   @Param("collectionSource") String collectionSource,
                   @Param("collectionTime") String collectionTime,
                   @Param("collectionArticleCategory") String collectionArticleCategory,
                   @Param("collectionArticleId") int collectionArticleId,
                   @Param("userId") int userId);

    //查询收藏是否存在
    @Select("SELECT collection_article as collectionArticle,collection_source as collectionSource," +
            "collection_time as collectionTime ,collection_article_id as collectionArticleId" +
            ",collection_article_category as collectionArticleCategory from user_collection_record WHERE " +
            "collection_article_id=#{collectionArticleId} and user_id=#{userId}")
    List<UserCollectionEntity> userQueryCollection(@Param("userId") int userId,
                                                   @Param("collectionArticleId") int collectionArticleId);

    //收藏分页展示
    @Select("select collection_user as collectionUser,collection_article as collectionArticle,collection_source as collectionSource," +
            "collection_time as collectionTime ,collection_article_id as collectionArticleId" +
            ",collection_article_category as collectionArticleCategory," +
            "user_id as userId from user_collection_record where user_id=#{userId} LIMIT #{pageNo},#{pageSize} ")
    List<UserCollectionEntity> allUserRecord(@Param("userId") int userId,
                                             @Param("pageNo") int pageNo,
                                             @Param("pageSize") int pageSize);

    //查询历史，收藏，点赞，次数
    @Select("SELECT user_collection,user_dz,user_history FROM user_user where id=#{id} ")
    UserEntity userFrequency(@Param("id")int id);

    //收藏次数
    @Update("UPDATE `user_user` SET user_collection=#{userCollection}  WHERE (id=#{id})")
    int frequencyCollection(@Param("userCollection")int userCollection,
                                   @Param("id")int id);

    //点赞次数
    @Update("UPDATE `user_user` SET user_dz=#{userDz}  WHERE (id=#{id})")
    int frequencyDZ(@Param("userDz") int userDz,@Param("id") int id);

    //历史次数
    // @Insert("INSERT INTO user_user (user_history)VALUES(#{userHistory})")
    // int frequencyHistory(int userHistory);

    //观点（分页展示）
    @Select("select comment_content as commentContent,comment_title as commentTitle," +
            "comment_category as commentCategory ,comment_ID as commentID" +
            ",commentator as commentator,commentary_time as commentaryTime from zzq_comment" +
            " where commentator=#{commentator} and category=#{category}  LIMIT #{pageNo},#{pageSize} ")
    List<CommentEntity> allComments(@Param("commentator") String commentator,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);

    //用户投诉
    @Insert("INSERT INTO complaint (complainant,department,contents,complaint_time,user_id," +
            "user_mail,title,category)" +
            "VALUES(#{complainant},#{department},#{contents},#{complaintTime},#{userId}," +
            "#{userMail},#{title},#{category})")
    int userComplaints(@Param("complainant") String complainant,//投诉人手机号
                       @Param("department") String department,//投诉部门
                       @Param("userId") int userId,//投诉人ID
                       @Param("userMail") String userMail,//投诉人邮箱
                       @Param("contents") String contents,//投诉内容
                       @Param("title") String title,//投诉标题
                       @Param("complaintTime") String complaintTime,//投诉时间
                       @Param("category") String category);//类别

    //用户列表展示
    @Select("select id,department,contents,complaint_time as complaintTime,user_id as userId," +
            "feedback_contents as feedbackContents,feedback_time as feedbackTime,feedback_department as " +
            "feedbackDepartment,category,complainant,user_mail as userMail,title from complaint where user_id=#{userId} and " +
            "category=#{category}" +
            " ORDER BY complaint_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserComplaintEntity> userAllComplaints(
            @Param("userId") int userId,
            @Param("category") String category,
            @Param("pageNo") int pageNo,
            @Param("pageSize") int pageSize
    );

    //通过ID查询内容反馈
    @Select("select id,department,contents,complaint_time as complaintTime," +
            "title,feedback_contents as feedbackContents,feedback_time as feedbackTime," +
            "feedback_department as feedbackDepartment,title,complainant,user_mail as userMail," +
            "title,category from complaint " +
            "where id=#{id} ")
    List<UserComplaintEntity> userOneComplaints(@Param("id") int id);

    //通过ID查询内容反馈
    @Select("select user_nick_name as userNickName,user_portrait as userPortrait from user_user where id=#{id}")
    UserEntity userGetId(@Param("id") int id);

    //用户浏览记录写入
    @Insert("INSERT INTO user_browsing_history (historical_user,historical_title,historical_source," +
            "historical_news_id,historical_category,historical_time)VALUES(#{historicalUser},#{historicalTitle}," +
            "#{historicalSource},#{historicalNewsId},#{historicalCategory},#{historicalTime})")
    int setHistory(@Param("historicalUser") String historicalUser,
                   @Param("historicalTitle") String historicalTitle,
                   @Param("historicalSource") String historicalSource,
                   @Param("historicalNewsId") int historicalNewsId,
                   @Param("historicalCategory") String historicalCategory,
                   @Param("historicalTime") String historicalTime
    );

    //添加简历
    @Insert("INSERT INTO user_resume (user_name,user_age,user_sex," +
            "user_tel,user_mail,user_xl,user_zwms,user_qwxz,user_bz,uid)VALUES(#{userName},#{userAge}," +
            "#{userSex},#{userTel},#{userMail},#{userXl},#{userZwms},#{userQwxz},#{userBz},#{uId})")
    int setUserResume(@Param("userName") String userName,
                      @Param("userAge") String userAge,
                      @Param("userSex") String userSex,
                      @Param("userTel") String userTel,
                      @Param("userMail") String userMail,
                      @Param("userXl") String userXl,
                      @Param("userZwms") String userZwms,
                      @Param("userQwxz") String userQwxz,
                      @Param("userBz") String userBz,
                      int uId);

    //查询简历id
    @Select("select id from user_resume where uid=#{uId}")
    int resumeId(@Param("uId") int uId);

    //查询是否存在简历
    @Select("select user_name as userName,id from user_resume where uid=#{uId}")
    UserResumeEntity resumeUser(@Param("uId") int uId);

    //添加简历id
    @Update("update user_user set resume_id=#{resumeId} where id=#{uid} ")
    int editUser(@Param("uid") int id, @Param("resumeId") int resumeId);

    //查看简历
    @Select("select id,user_name as userName,user_age as userAge,user_sex as userSex ,user_tel as userTel," +
            "user_mail as userMail ,user_xl as userXl,user_zwms as userZwms,user_qwxz as userQwxz," +
            "user_bz as userBz from user_resume where uid=#{uId}")
    UserResumeEntity oneUserR(@Param("uId") int uId);

    //个人申领查询
    @Select("select id,user_user as userUser,user_id as userId,title,content,code,pid from user_grsl " +
            "where user_id=#{userId}")
    List<UserGrslEntity> grslAll(@Param("userId") String userId);
    @Select("select id,user_user as userUser,user_id as userId,title,content,code from user_grsl")
    List<UserGrslEntity> grslOne(@Param("id")int id);
    //有奖举报
    @Insert("INSERT INTO `prize_peport` (report_content, report_picture," +
            " report_tel, report_wx,report_time) VALUES " +
            "(#{reportContent},#{reportPicture},#{reportTel},#{reportWx},#{reportTime})")
    int prizeInsert(@Param("reportContent") String reportContent,
                    @Param("reportPicture") String reportPicture,
                    @Param("reportTel") String reportTel,
                    @Param("reportWx") String reportWx,
                    @Param("reportTime") String reportTime
    );

    //修改简历
    @Update("UPDATE user_resume SET user_name=#{userName}, user_age=#{userAge}, user_sex=#{userSex}," +
            " user_tel=#{userTel}, user_mail=#{userMail}, user_xl=#{userXl}, user_zwms=#{userZwms}, user_qwxz=#{userQwxz}," +
            " user_bz=#{userBz}, uid=#{uId} WHERE (id=#{id})")
    int update(@Param("userName") String userName,
               @Param("userAge") String userAge,
               @Param("userSex") String userSex,
               @Param("userTel") String userTel,
               @Param("userMail") String userMail,
               @Param("userXl") String userXl,
               @Param("userZwms") String userZwms,
               @Param("userQwxz") String userQwxz,
               @Param("userBz") String userBz,
               @Param("uId")int uId,
               @Param("id")int id);
}
