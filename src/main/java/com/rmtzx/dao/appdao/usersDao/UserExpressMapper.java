package com.rmtzx.dao.appdao.usersDao;

import com.rmtzx.entity.appentity.userEntity.UserExpressEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserExpressMapper {
    //添加用户快递信息
        @Insert("INSERT INTO user_express(user_user,sender," +
                "mailing_address,addressee,receiving_address,waybill," +
                "user_order,courier_services_company,express_time,unique_identifier)" +
                "VALUES(#{userUser},#{sender}," +
                "#{mailingAddress},#{addressee}," +
                "#{receivingAddress},#{waybill},#{userOrder}" +
                ",#{courierServicesCompany},#{expressTime},#{uniqueIdentifier})")
    int userRecord(@Param("userUser")String userUser,//用户
                   @Param("sender")String sender,//寄件人
                   @Param("mailingAddress")String mailingAddress,//寄件地址
                   @Param("addressee")String addressee,//收件人
                   @Param("receivingAddress")String receivingAddress,//收件地址
                   @Param("waybill")String waybill,//运单号
                   @Param("userOrder")String userOrder,//订单号
                   @Param("courierServicesCompany")String courierServicesCompany,//快递公司
                   @Param("expressTime")String expressTime,
                   @Param("uniqueIdentifier")String uniqueIdentifier);

    //查询用户快递信息列表
    @Select("select id,user_user as userUser,sender,mailing_address as mailingAddress," +
            "addressee,receiving_address as receivingAddress,waybill,user_order as userOrder," +
            "courier_services_company as courierServicesCompany from user_express " +
            "where user_user=#{userTel} ORDER BY express_time desc LIMIT #{pageNo},#{pageSize} ")
    List<UserExpressEntity> allExpress(@Param("userTel")String userTel,
                                       @Param("pageNo")int pageNo,
                                       @Param("pageSize")int pageSize );


}
