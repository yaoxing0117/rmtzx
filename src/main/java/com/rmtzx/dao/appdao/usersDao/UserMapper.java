package com.rmtzx.dao.appdao.usersDao;

import com.rmtzx.entity.appentity.userEntity.UserEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;
/**
 * 用户登录注册
 */

@Mapper
public interface UserMapper {
    //      登录
    @Select("SELECT id,user_name_tel AS userNameTel,user_nick_name AS userNickName,user_age AS userAge," +
            "user_birthday AS userBirthday,user_id as userId,user_city AS userCity,user_portrait AS userPortrait,user_sex as userSex," +
            "user_tel as userTel,user_name as userName,user_history as userHistory,user_dz as userDz,user_collection as userCollection, " +
            " resume_id as resumeId from user_user " +
            "where user_name_tel=#{userNameTel}")
    UserEntity findLoadByUser(String userNameTel);
    //      手机号查询是否存在,返回密码
    @Select("SELECT pass_word as passWord from user_user where user_name_tel=#{userNameTel}")
    UserEntity findUser(@Param("userNameTel") String userNameTel);
    //      注册
    @Insert("INSERT INTO user_user (user_name_tel,pass_word)VALUES(#{userName},#{passWord})")
    int addUser(@Param("userName")String userName,
                @Param("passWord")String passWord);
    //      修改
    @Update("update user_user set pass_word=#{passWord} where user_name_tel=#{userNameTel} ")
    int editUser(@Param("userNameTel") String userNameTel,@Param("passWord") String passWord);
    @Update("update user_user set user_history=#{userHistory} where user_name_tel=#{userNameTel} ")
    int userHistoryUser(@Param("userNameTel") String userNameTel,@Param("userHistory") String userHistory);
    //修改个人资料
    @Update("update user_user set user_nick_name=#{userNickName},user_age=#{userAge},user_birthday=#{userBirthday}," +
            "user_city=#{userCity},user_portrait=#{userPortrait},user_sex=#{userSex},user_tel=#{userTel}" +
            " ,user_name=#{userName} where user_name_tel=#{userNameTel}")
    int updatePersona(@Param("userNameTel")String userNameTel,
                      @Param("userNickName")String userNickName,
                      @Param("userAge")String userAge,
                      @Param("userSex")String userSex,
                      @Param("userBirthday")String userBirthday,
                      @Param("userCity")String userCity,
                      @Param("userPortrait")String userPortrait,
                      @Param("userTel")String userTel,
                      @Param("userName")String userName
    );
    //查询用户头像是否存在
    @Select("select user_portrait from user_user where user_name_tel=#{userNameTel}")
    UserEntity queryportra(@Param("userNameTel")String userNameTel);

    //查询用户昵称是否存在
    @Select("select * from user_user where user_nick_name=#{userNickName}")
    UserEntity queryNickName(@Param("userNickName")String userNickName);
    //查询账号是否存在
    @Select("select user_name_tel from user_user where user_name_tel=#{userNameTel}")
    int queryUserNameTel(@Param("userNameTel")String userNameTel);
    //返回更新用户信息
    @Select("select id,user_name_tel AS userNameTel,user_nick_name AS userNickName,user_age AS userAge," +
            "user_birthday AS userBirthday,user_city AS userCity,user_portrait AS userPortrait,user_sex as userSex," +
            "user_tel as userTel,user_history as userHistory,user_dz as userDz,user_collection as userCollection,user_name as userName from user_user where user_name_tel=#{userNameTel} ")
    UserEntity userDate(@Param("userNameTel")String userNameTel);


}
