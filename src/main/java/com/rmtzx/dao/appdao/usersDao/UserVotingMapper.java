package com.rmtzx.dao.appdao.usersDao;

import com.rmtzx.entity.appentity.userEntity.UserVotingEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface UserVotingMapper {
    //查询所有投票展示（据投票类别）
    @Select("select id,originator,title,content,picture,start_date as startDate,stop_date as stopDate, \n" +
            " option_id as optionId from voting_detailss where option_id=0 and code=1 and voting_category=#{votingCategory}\n" +
            " ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> all(@Param("votingCategory")String votingCategory,
                               @Param("pageNo")int pageNo,
                               @Param("pageSize")int pageSize);
    //查询投票内容详情展示
    @Select("select id,title,content,picture,votes from " +
            "voting_detailss where option_id=#{optionId} LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> oneVoting(@Param("optionId")String optionId,
                                     @Param("pageNo")int pageNo,
                                     @Param("pageSize")int pageSize);
    //用户投票
    @Update("UPDATE voting_detailss SET votes=#{votes} WHERE (id=#{id})")
    int updateVotes(@Param("id")int id,
                   @Param("votes")int votes);
    //查询当前投票数
    @Select("select votes from voting_detailss where id=#{id}")
    int selectVotes(@Param("id")int id);
    //增加投票数
    @Insert("INSERT INTO voting_user (user_name, user_id, cid) VALUES (#{userName},#{userId},#{cid})")
    int inUser(@Param("userName")String userName,
               @Param("userId")int userId,
               @Param("cid")int cid);
    //查询是否投票
    @Select("select user_name from voting_user where cid=#{id} and user_name=#{userName}")
    String selectVoting(@Param("id")int id,
                        @Param("userName")String userName);
}
