package com.rmtzx.dao.appdao;


import com.rmtzx.entity.bo.WXPayEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface WXPayMapper {
    //订单号写入 状态写入 INSERT INTO `rmt_zx`.`wx_pay`(`out_trade_no`, `order_code`) VALUES ('dasd', '支付成功')
    @Insert("INSERT INTO wx_pay(out_trade_no,order_code,user_tel,user_money) " +
            "VALUES (#{outTradeNo}, '支付中',#{userTel},#{userMoney})")
    int insereWXXR(@Param("outTradeNo") String outTradeNo,
                   @Param("userTel") String userTel,
                   @Param("userMoney") int userMoney);

    //查询订单是否成功 SELECT * FROM `rmt_zx`.`wx_pay` LIMIT 0, 1000
    @Select("SELECT * FROM  wx_pay where out_trade_no=#{outTradeNo}")
    WXPayEntity selectWX(@Param("outTradeNo") String outTradeNo);
//UPDATE `wx_pay` SET `order_code`='支付成功' WHERE (`id`='28')
    @Insert("UPDATE wx_pay SET order_code='支付成功' WHERE (out_trade_no=#{outTradeNo})")
    int updateWXCZ(@Param("outTradeNo") String outTradeNo);
    //查询最新数据id
    @Select("select COUNT(id) FROM wx_pay LIMIT 0,1")
    int newId1();
}