package com.rmtzx.dao.appdao;

import com.rmtzx.entity.CYTelEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface CYTelMapper {
    //大类别返回
    @Select("select * from re_phone where pid=0 and code=1 ")
    List<CYTelEntity> pAll();

    //小列别返回
    @Select("select * from re_phone where id=#{id} and code=1 LIMIT #{pageNo},#{pageSize}")
    List<CYTelEntity> cAll(@Param("id") int id,
                           @Param("pageNo")int pageNo,
                           @Param("pageSize")int pageSize);

    @Select("SELECT COUNT(*) FROM re_phone where pid=0 and code=1")
    int paperPageCount();
    @Select("SELECT COUNT(*) FROM re_phone where pid=#{pid} and code=1")
    int cpaperPageCount(@Param("pid")int pid);
}
