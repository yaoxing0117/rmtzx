package com.rmtzx.dao.appdao;

import com.rmtzx.entity.appentity.YszsEntity;
import com.rmtzx.entity.appentity.ysEntity.WXGZHEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface YszsMapper {
    //展示数据列表
    @Select("select id,article_source as articleSource,picture_url as pictureUrl," +
            "release_time as release_time ,title from ys_zszc where code=1 and " +
            "category=#{category} ORDER BY release_time desc LIMIT #{pageNo},#{pageSize} ")
    List<YszsEntity> allYszs(@Param("category")String category,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);
    //详情数据
    @Select("select id,article_source as articleSource,picture_url as pictureUrl," +
            "release_time as releaseTime ,title,article,author,abs_url as absUrl from ys_zszc where " +
            " id=#{id} ")
    YszsEntity oneYszs(@Param("id")int id);
    //表总条数查询(信息公示)
    @Select("SELECT COUNT(*) FROM ys_zszc where category=#{category} ")
    int xxpaperPageCount(@Param("category")String category);

    //微信公众号列表展示
    @Select("select id,title,source,article,release_time as releaseTime,picture_url as pictureUrl " +
            ",local_url as localUrl from weixin_gzh_fb where differentiate=#{differentiate} LIMIT #{pageNo},#{pageSize}")
    List<WXGZHEntity> allWX(@Param("differentiate")String differentiate,
                            @Param("pageNo")int pageNo,
                            @Param("pageSize")int pageSize);
    //微信公众号详情展示
    @Select("select id,title,source,article,release_time as releaseTime,picture_url as pictureUrl " +
            "from weixin_gzh_fb where id=#{id}")
    WXGZHEntity oneWX(@Param("id")int id);

    //表总条数查询(信息公示)
    @Select("SELECT COUNT(*) FROM weixin_gzh_fb where differentiate=#{differentiate} ")
    int wxpaperPageCount(@Param("differentiate")String differentiate);
}
