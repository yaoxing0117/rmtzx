package com.rmtzx.dao.appdao.ysDao;

import com.rmtzx.entity.appentity.ysEntity.YstdEntity;
import com.rmtzx.entity.appentity.ysEntity.YszwEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface YwzpMapper {
    //信息公开展示
    @Select("select id,title,content,picture,time,cid from company_content where cid=0")
    List<YstdEntity> xxallSelects();
    //信息公开展示
    @Select("select id,title from company_content where code=1 LIMIT #{pageNo},#{pageSize}")
    List<YstdEntity> xxallSelect(@Param("cid")int cid,
                               @Param("pageNo")int pageNo,
                               @Param("pageSize")int pageSize);
    //信息详情展示()
    @Select("select id,title,content,picture,time,cid from " +
            "company_content where code=1 and id=#{id}")
    YstdEntity xxoneSelect(@Param("id")int cid);
    //职位列表展示
    @Select("select id,position,place,experience,education,part_time as partTime,release_time as releaseTime from " +
            " company_description where cid=#{cid} ORDER BY release_time desc LIMIT #{pageNo},#{pageSize}")
    List<YszwEntity> zwallSelect(@Param("cid")int cid,
                                 @Param("pageNo")int pageNo,
                                 @Param("pageSize")int pageSize);
    //职位详情展示
    @Select("select id,position,place,experience,education,part_time as partTime, " +
            "expectation,release_time as releaseTime,release_id as releaseId,bright,description from " +
            "company_description where cid=#{cid}")
    YszwEntity zwoneSelect(@Param("cid")int cid);
    //职位投递
    @Insert("INSERT INTO company_id (gongsi_id,user_id,zhiwei_id)VALUES(#{gongsiId},#{userId},#{zhiweiId})")
    int zwInsert(@Param("gongsiId")int gongsiId,
                 @Param("userId")int userId,
                 @Param("zhiweiId")int zhiweiId);
    //表总条数查询(信息公示)
    @Select("SELECT COUNT(*) FROM company_content where cid=#{cid} ")
    int xxpaperPageCount(@Param("cid")int cid);
    //查询职位表
    @Select("SELECT COUNT(*) FROM company_description where cid=#{cid} ")
    int zwPaperPageCount(@Param("cid")int cid);

}
