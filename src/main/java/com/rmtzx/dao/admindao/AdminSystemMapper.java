package com.rmtzx.dao.admindao;

import com.rmtzx.entity.appentity.PrizeEntiy;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface AdminSystemMapper {
    //举报内容搜索（状态）
    @Select("select id, report_content as  reportContent ,report_picture as reportPicture,report_tel " +
            "as reportTel,report_wx as reportWx,report_time as reportTime,report_examine as reportExamine " +
            "from prize_peport where report_examine=#{reportExamine} and report_content like CONCAT('%',#{content},'%') " +
            "ORDER BY report_time desc LIMIT #{pageNo},#{pageSize} ")
    List<PrizeEntiy> selectQuery(@Param("reportExamine") int reportExamine,
                                 @Param("content") String content,
                                 @Param("pageNo") int pageNo,
                                 @Param("pageSize") int pageSize);

    @Select("select id,report_content as  reportContent ,report_picture as reportPicture,report_tel" +
            " as reportTel,report_wx as reportWx,report_time as reportTime,report_examine as reportExamine " +
            "from prize_peport where report_content like CONCAT('%',#{content},'%') " +
            "ORDER BY report_time desc LIMIT #{pageNo},#{pageSize} ")
    List<PrizeEntiy> selectQueryAll(@Param("content") String content,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    @Select("select id,report_content as  reportContent ,report_picture as reportPicture,report_tel" +
            " as reportTel,report_wx as reportWx,report_time as reportTime,report_examine as reportExamine " +
            "from prize_peport " +
            "ORDER BY report_time desc LIMIT #{pageNo},#{pageSize} ")
    List<PrizeEntiy> selectQueryAll2(
            @Param("pageNo") int pageNo,
            @Param("pageSize") int pageSize);

    //分页列表
    @Select("SELECT * FROM prize_peport ORDER BY report_time desc LIMIT #{pageNo},#{pageSize}")
    List<PrizeEntiy> selectAll(@Param("pageNo") int pageNo,
                               @Param("pageSize") int pageSize);

    //根据审核状态分页列表
    @Select("SELECT id,report_content as  reportContent ,report_picture as reportPicture,report_tel as reportTel," +
            "report_wx as reportWx,report_time as reportTime,report_examine as reportExamine FROM prize_peport where report_examine=#{reportExamine} ORDER BY report_time desc LIMIT #{pageNo},#{pageSize}")
    List<PrizeEntiy> selectReportExamine(@Param("reportExamine") int reportExamine,
                                         @Param("pageNo") int pageNo,
                                         @Param("pageSize") int pageSize);

    //修改审核状态
    @Update("UPDATE prize_peport SET report_examine=#{reportExamine} WHERE id=#{id}")
    int updateOne(@Param("reportExamine") int reportExamine,
                  @Param("id") int id);

    //查询总条数
    @Select("SELECT COUNT(*) FROM prize_peport ")
    int paperPageCountAll();

    @Select("SELECT COUNT(*) FROM prize_peport where report_examine=#{reportExamine}")
    int paperPageCount(@Param("reportExamine") int reportExamine);

    //模糊查询查询总条数
    @Select("SELECT COUNT(*) FROM prize_peport where report_examine=#{reportExamine} or report_content like CONCAT('%',#{content},'%')")
    int paperPageCountQuery(@Param("reportExamine") int reportExamine,
                            @Param("content") String content);

    @Select("SELECT COUNT(*) FROM prize_peport where  report_content like CONCAT('%',#{content},'%')")
    int paperPageCountQueryall(@Param("content") String content);

}
