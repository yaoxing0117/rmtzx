package com.rmtzx.dao.admindao;

import com.rmtzx.entity.appentity.userEntity.UserGrslEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserGrslMapper {
    //创建申领
    @Insert("INSERT INTO `user_grsl` (title,content,sl_remarks,cj_time)" +
            " VALUES (#{title},#{content},#{slRemarks},#{cjTime})")
    int insertGrsl(@Param("title") String title,
                   @Param("content") String content,
                   @Param("slRemarks") String slRemarks,
                   @Param("cjTime") String cjTime);

    //所有展示
    @Select("SELECT id,title,content,code,sl_number as slNumber,ysl_number as yslNumber,sl_remarks as slRemarks," +
            "cj_time as cjTime FROM `user_grsl` where pid=0 ORDER BY cj_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectAll(@Param("pageNo") int pageNo,
                                   @Param("pageSize") int pageSize);

    //查询总条数
    @Select("SELECT COUNT(*) FROM user_grsl where pid=0 ")
    int paperPageCountAll();

    //修改状态
    @Update("UPDATE user_grsl SET `code`=#{code} WHERE id=#{id}")
    int updateCode(@Param("id") int id,
                   @Param("code") int code);

    //模糊查询标题
    @Select("SELECT * FROM `user_grsl` where title like CONCAT('%',#{title},'%') and  pid=0 LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectTitle(@Param("title") String title,
                                     @Param("pageNo") int pageNo,
                                     @Param("pageSize") int pageSize);

    @Select("SELECT COUNT(*) FROM user_grsl where title like CONCAT('%',#{title},'%') and  pid=0 ")
    int paperPageCountTitle(@Param("title") String title);

    //模糊查询内容
    @Select("SELECT * FROM `user_grsl` where content like CONCAT('%',#{title},'%') and pid=0   LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectContent(@Param("title") String title,
                                       @Param("pageNo") int pageNo,
                                       @Param("pageSize") int pageSize);

    @Select("SELECT COUNT(*) FROM user_grsl where title like CONCAT('%',#{title},'%') and  pid=0 ")
    int paperPageCountContent(@Param("title") String title);

    //修改内容
    @Update("UPDATE `user_grsl` SET title=#{title},content=#{content},sl_remarks=#{slRemarks} WHERE (id=#{id})")
    int updateNR(@Param("id") int id,
                 @Param("title") String title,
                 @Param("content") String content,
                 @Param("slRemarks") String slRemarks);

    //删除申领
    @Delete("DELETE FROM `user_grsl` WHERE (id=#{id})")
    int deleteOne(@Param("id") int id);

    //删除同申领下所有申领人
    @Delete("DELETE FROM `user_grsl` WHERE (pid=#{id})")
    int deleteOneSLR(@Param("id") int id);


    //申领人查询
    @Select("SELECT id,user_user as userUser,user_id as userId,code,sl_remarks as slRemarks, " +
            "lq_time as lqTime,cj_time as cjTime FROM `user_grsl` where pid=#{pid} ORDER BY cj_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectSL(@Param("pid") int pid,
                                  @Param("pageNo") int pageNo,
                                  @Param("pageSize") int pageSize);

    //申领总人数
    @Select("SELECT COUNT(*) FROM user_grsl where pid=#{pid} ")
    int paperPageCountAllSL(@Param("pid") int pid);

    //添加申领人
    @Insert("INSERT INTO `user_grsl` (user_user,user_id,title,content,code,sl_remarks,pid,cj_time) VALUES" +
            " (#{userUser},#{userId},#{title},#{content},1,#{slRemarks},#{pid},#{lqTime})")
    int insertSl(@Param("userUser") String userUser,
                 @Param("userId") String userId,
                 @Param("title") String title,
                 @Param("content") String content,
                 @Param("slRemarks") String slRemarks,
                 @Param("pid") int pid,
                 @Param("lqTime") String lqTime);

    //查询当前用户身份证是否存在
    @Select("SELECT pid,sl_number as slNumber FROM user_grsl where user_id=#{userId}")
    List<UserGrslEntity> selectALLSl(@Param("userId") String userId);

    //查询父级申领
    @Select("SELECT id,sl_number as slNumber,pid FROM user_grsl where id=#{id}")
    UserGrslEntity selectOne(@Param("id") int id);

    //更新申领总人数
    @Update("UPDATE `user_grsl` SET sl_number=#{slNumber} WHERE id=#{id}")
    int updateSL(@Param("slNumber") int slNumber,
                 @Param("id") int id);

    //修改申领人状态
    @Update("UPDATE `user_grsl` SET code=#{code},lq_time=#{lqTime}  WHERE id=#{id}")
    int updateSLR(@Param("code") int code,
                  @Param("lqTime") String lqTime,
                  @Param("id") int id);

    @Select("SELECT id,user_user as userUser,user_id as userId,code,sl_remarks as slRemarks, cj_time as cjTime,lq_time as lqTime" +
            " FROM `user_grsl` where pid=#{pid} and code=#{code} and user_user=#{userUser} ORDER BY lq_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectCodeSl(@Param("pid") int pid,
                                      @Param("code") int code,
                                      @Param("userUser") String userUser,@Param("pageNo") int pageNo,
                                      @Param("pageSize") int pageSize);

    @Select("SELECT id,user_user as userUser,user_id as userId,code,sl_remarks as slRemarks,cj_time as cjTime,lq_time as lqTime" +
            "  FROM `user_grsl` where pid=#{pid} and user_user=#{userUser} ORDER BY lq_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectAllSl(@Param("pid") int pid,
                                     @Param("userUser") String userUser,@Param("pageNo") int pageNo,
                                     @Param("pageSize") int pageSize);
    @Select("SELECT id,user_user as userUser,user_id as userId,code,sl_remarks as slRemarks, cj_time as cjTime,lq_time as lqTime" +
            " FROM `user_grsl` where pid=#{pid} and code=#{code}  ORDER BY lq_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectCodeSlNOUser(@Param("pid") int pid,
                                      @Param("code") int code,@Param("pageNo") int pageNo,
                                            @Param("pageSize") int pageSize);

    @Select("SELECT id,user_user as userUser,user_id as userId,code,sl_remarks as slRemarks,cj_time as cjTime,lq_time as lqTime" +
            "  FROM `user_grsl` where pid=#{pid}  ORDER BY lq_time desc LIMIT #{pageNo},#{pageSize}")
    List<UserGrslEntity> selectAllSlNOUser(@Param("pid") int pid,@Param("pageNo") int pageNo,
                                           @Param("pageSize") int pageSize);
    @Select("SELECT title,content from user_grsl where id=#{pid}")
    UserGrslEntity selectonme(@Param("pid")int pid);
}
