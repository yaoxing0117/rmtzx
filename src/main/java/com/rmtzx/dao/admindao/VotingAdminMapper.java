package com.rmtzx.dao.admindao;

import com.rmtzx.entity.appentity.userEntity.UserVotingEntity;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface VotingAdminMapper {
    //分页展示所有投票
    @Select("SELECT title,picture,content,start_date as startDate,stop_date as stopDate," +
            "voting_category as votingCategory,tp_remarks as tpRemarks,code,votes, " +
            "code_picture as codePicture FROM `voting_detailss` where  option_id=0  ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> selectVotingAll(@Param("pageNo") int pageNo,
                                           @Param("pageSize") int pageSize);
    //查询
    @Select("SELECT COUNT(*) FROM voting_detailss ")
    int paperPageCount();
    //添加投票
    @Insert("INSERT INTO `voting_detailss` (title,picture,content,start_date,stop_date, " +
            "voting_category,tp_remarks,code_picture,code,option_id) VALUES " +
            "(#{title},#{picture},#{content},#{startDate},#{stopDate},#{votingCategory},#{tpRemarks}, #{codePicture},0,0)")
    int insertOne(@Param("title") String title,
                  @Param("picture") String picture,
                  @Param("content") String content,
                  @Param("startDate") String startDate,
                  @Param("stopDate") String stopDate,
                  @Param("votingCategory") int votingCategory,
                  @Param("tpRemarks") String tpRemarks,
                  @Param("codePicture") int codePicture);
    @Update("UPDATE `voting_detailss` SET title=#{title},picture=#{picture},content=#{content},stop_date=#{stopDate}," +
            "voting_category=#{votingCategory},tp_remarks=#{tpRemarks} WHERE (id=#{id})")
    int updateVoting(@Param("title") String title,
                     @Param("picture") String picture,
                     @Param("content") String content,
                     @Param("stopDate") String stopDate,
                     @Param("votingCategory") int votingCategory,
                     @Param("tpRemarks") String tpRemarks,
                     @Param("id")int id);
    @Delete("DELETE FROM `voting_detailss` WHERE (id=#{id})")
    int deleteId(@Param("id")int id);
    @Delete("DELETE FROM `voting_detailss` WHERE (option_id=#{id})")
    int deletePid(@Param("id")int id);
//全部投票模糊查询
    @Select("SELECT title,picture,content,start_date as startDate,stop_date as stopDate," +
            "voting_category as votingCategory,tp_remarks as tpRemarks,code,votes, " +
            "code_picture as codePicture FROM `voting_detailss` where title like CONCAT('%',#{title},'%') and option_id=0  " +
            "ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> selectAllMH(@Param("title") String title,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    //分类投票模糊查询
    @Select("SELECT title,picture,content,start_date as startDate,stop_date as stopDate," +
            "voting_category as votingCategory,tp_remarks as tpRemarks,code,votes, " +
            "code_picture as codePicture FROM `voting_detailss` where  where title like CONCAT('%',#{title},'%') and option_id=0" +
            " and voting_category=#{votingCategory} ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> selectMH(@Param("title") String title,
                                    @Param("votingCategory")int votingCategory,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    @Select("SELECT title,picture,content,start_date as startDate,stop_date as stopDate," +
            "voting_category as votingCategory,tp_remarks as tpRemarks,code,votes, " +
            "code_picture as codePicture FROM `voting_detailss` where option_id=0  " +
            "ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> selectAllMHw(@Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    //分类投票模糊查询
    @Select("SELECT title,picture,content,start_date as startDate,stop_date as stopDate," +
            "voting_category as votingCategory,tp_remarks as tpRemarks,code,votes, " +
            "code_picture as codePicture FROM `voting_detailss` where  where option_id=0" +
            " and voting_category=#{votingCategory} ORDER BY start_date desc LIMIT #{pageNo},#{pageSize}")
    List<UserVotingEntity> selectMHw(@Param("votingCategory")int votingCategory,
                                    @Param("pageNo") int pageNo,
                                    @Param("pageSize") int pageSize);
    @Update("UPDATE `voting_detailss` SET code=#{code} WHERE (id=#{id})")
    int updateCode(@Param("code")int code,
                   @Param("id")int id);
}
