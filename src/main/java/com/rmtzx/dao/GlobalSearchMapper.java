package com.rmtzx.dao;

import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface GlobalSearchMapper {
    @Select("select id,source,title,article_source,article,release_time as releaseTime," +
            "abs_url as absUrl,differentiate from zd_news_fb where code=1 and title LIKE CONCAT('%',#{article},'%')  " +
            " LIMIT #{pageNo},#{pageSize}")
    List<ZdxwEntity> allGlobalSearch(@Param("article")String article,
                                     @Param("pageNo")int pageNo,
                                     @Param("pageSize")int pageSize);
}
