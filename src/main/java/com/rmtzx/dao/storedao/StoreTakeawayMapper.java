package com.rmtzx.dao.storedao;


import com.rmtzx.entity.store.StoreTakeawayEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 外卖店家-UCRD
 */
@Mapper
public interface StoreTakeawayMapper {
    //所有店铺展示 做分页处理 （根据状态code查询）
    @Select("SELECT * FROM store_store where store_code=1 LIMIT #{pageNo},#{pageSize}")
    List<StoreTakeawayEntity> selectAll(@Param("pageNo") int pageNo,
                                        @Param("pageSize") int pageSize);

    //单个店面id查询
    @Select("SELECT * FROM store_store where id=#{id}")
    StoreTakeawayEntity selectOne(@Param("id") int id);

    //店铺新增 INSERT INTO `store_store` (`id`, `shop_owner`, `main_camp`, `store_introduce`, `store_tel`,
    // `store_hours`, `store_picture`, `store_increase_time`, `store_location`, `store_priority`,
    // `store_area`, `pid`, `reserve_one`, `reserve_two`, `reserve_three`, `reserve_four`, `store_star`,
    // `store_address`, `store_code`) VALUES (#{},#{},#{},#{},#{},#{},
    // #{}, #{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{})
    // @Insert("")
    // int insertOne();
//
//     @Select("select * from store_store where shop_owner=#{shopOwner}")
    @Select("select * from store_store where shop_owner like CONCAT('%',#{shopOwner},'%') ")
    List<StoreTakeawayEntity> selectAllStore(@Param("shopOwner") String shopOwner);
    //店铺

    //店铺信息修改

    //店铺删除 修改状态 修改

    //店铺彻底删除 （ 包含菜品删除）

    //店铺搜索


}