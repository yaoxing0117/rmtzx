package com.rmtzx.dao.storedao;

import com.rmtzx.entity.store.StoreDishEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
/**
 * 外卖店家-菜品数据实体
 */
public interface StoreDishMapper {
    //菜品查询 商家id查询
    @Select("SELECT * FROM store_dish where pid=#{pid}")
    List<StoreDishEntity> selectAll(@Param("pid")int pid);
    //新增菜品 INSERT INTO `store_dish` (`id`, `dish_name`, `dish_price`, `dish_picture`,
    // `dish_grade`, `dish_comment`, `dish_times`, `pid`) VALUES ('1', '菜名', '100', '图片', '介绍', '10', '5', '1')
    // @Insert("")
    // int insertDish();
    @Select("select * from store_dish where dish_name like CONCAT('%',#{dishName},'%') ")
    List<StoreDishEntity> selectAlldish(@Param("dishName")String dishName);
    //菜品删除 id
    @Select("select * from store_dish where dish_name like CONCAT('%',#{dishName},'%') and pid=#{pid}")
    List<StoreDishEntity> selectAllDishStore(@Param("dishName")String dishName,
                                             @Param("pid")int pid);
    //菜品修改

    //评论次数增加
    @Update("UPDATE store_dish SET dish_comment=#{dishComment} WHERE (id=#{id})")
    int updateComment(@Param("dishComment")int dishComment,
                      @Param("id")int id);
    //查询当前id下的菜品评论次数
    @Select("select dish_comment from store_dish where id=#{id}")
    int selectComment(@Param("id")int id);

}
