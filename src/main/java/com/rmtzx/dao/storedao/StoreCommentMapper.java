package com.rmtzx.dao.storedao;

import com.rmtzx.entity.store.StoreCommentEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 店家-菜品 评论UCRD
 */
public interface StoreCommentMapper {
    //增加评论（菜品or店面评论） id+ 类别 INSERT INTO `store_comment` (`comment_category`, `comment_content`,
    // `comment_picture`, `comment_time`, `comment_star`, `user_id`) VALUES ('类别', '内容', '图片', '时间', '5', '1')
    @Insert("INSERT INTO store_comment (comment_category,comment_content," +
            "comment_picture,comment_time,comment_star,user_id,pid,store_id) VALUES " +
            "(#{commentCategory},#{commentContent},#{commentPicture},#{commentTime},#{commentStar},#{userId},#{pid},#{storeId})")
    int insertComment(@Param("commentCategory") String commentCategory,
                      @Param("commentContent") String commentContent,
                      @Param("commentPicture") String commentPicture,
                      @Param("commentTime") String commentTime,
                      @Param("commentStar") int commentStar,
                      @Param("userId") int userId,
                      @Param("pid") int pid,
                      @Param("storeId") int storeId
                      );

    //商家回复评论（ ）
//UPDATE `store_comment` SET `comment_reply`='回复内容', `comment_time_reply`='回复时间', `comment_store`='回复店家' WHERE (`id`='1')
    @Update("UPDATE store_comment SET comment_reply=#{commentReply},comment_time_reply=#{commentTimeReply}," +
            "comment_store=#{commentStore} WHERE (id=#{id})")
    int updateStore(@Param("commentReply") String commentReply,
                    @Param("commentTimeReply") String commentTimeReply,
                    @Param("commentStore") String commentStore,
                    @Param("id") int id);
    //删除评论 需要后台权限


    //评论查询 类别+id(店家 菜品 评论通用 )
    @Select("SELECT * FROM store_comment where comment_category=#{commentCategory} and pid=#{pid}")
    List<StoreCommentEntity> selectComment(@Param("commentCategory") String commentCategory,
                                           @Param("pid") int pid);
    //店家评论查询
    @Select("SELECT * FROM store_comment where store_id=#{storeId}  ")
    List<StoreCommentEntity> selectCommentstore(@Param("storeId") int storeId);

    //查询订单表中是否存在该用户订单（验证是否有评论）

}
