package com.rmtzx.dao.storedao;

import com.rmtzx.entity.store.StoreOrderEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * 订单数据表UCRD
 */
public interface StoreOrderMapper {
    //下单INSERT INTO store_order (user_id,user_address,user_name,user_tel,spot_name,
    // dish_id,dish_name,dish_number,dish_money,order_number,order_state,user_remarks,
    // pid,order_total) VALUES (#{user_id},#{user_address},#{user_name},#{user_tel},#{spot_name},
    // #{dish_id},#{dish_name},#{dish_number},#{dish_money},#{order_number},#{order_state},#{user_remarks},#{pid},#{order_total})
    @Insert("INSERT INTO store_order (user_id,user_address,user_name,user_tel,spot_name," +
            "dish_id,dish_name,dish_number,dish_money,order_number,order_state,user_remarks," +
            "pid,order_total) VALUES (#{userId},#{userAddress},#{userName},#{userTel},#{spotName}," +
            "#{dishId},#{dishName},#{dishNumber},#{dishMoney},#{orderNumber},#{orderState},#{userRemarks},#{pid},#{orderTotal})")
    int insertOrder(@Param("userId")int userId,//用户id
                    @Param("userAddress")String userAddress,//用户地址haoleshabi
                    @Param("userName")String userName,//用户名称
                    @Param("userTel")String userTel,//用户电话
                    @Param("spotName")String spotName,//店家名
                    @Param("dishName")String dishName,//菜名
                    @Param("dishNumber")int dishNumber,//菜品数量
                    @Param("dishMoney")int dishMoney,//价格
                    @Param("orderNumber")String orderNumber,//订单号
                    @Param("orderState")String orderState,//订单状态
                    @Param("dishId")int dishId,//菜品id
                    @Param("userRemarks")String userRemarks,//用户备注
                    @Param("pid")int pid,//
                    @Param("orderTotal")int orderTotal);//合计
    //最新id查询
    @Select("select id from store_order ORDER BY id desc LIMIT 0,1")
    int selectId();

    //订单评论
    // @Update("UPDATE store_order SET order_picture=#{orderPicture},order_comment=#{orderComment}," +
    //         "order_comment_time=#{orderCommentTime} WHERE (pid=#{pid})")
    // int updateSelect(@Param("orderPicture")String orderPicture,
    //                  @Param("orderComment")String orderComment,
    //                  @Param("orderCommentTime")String orderCommentTime,
    //                  @Param("pid")int pid);
    //回复

    //取消订单
    @Update("UPDATE store_order SET order_state=#{orderState} WHERE (pid=#{pid})")
    int updateOrder(@Param("orderState")String orderState,
                    @Param("pid")int pid);
    //用户查询订单（当前or历史）

    //修改订单状态

    //用户订单查询
    @Select("select * from store_order WHERE user_id=#{userId} ORDER BY order_time desc  ")
    List<StoreOrderEntity> selectOrder(@Param("userId")int userId);

}
