package com.rmtzx.dao.yqdao;

import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import com.rmtzx.entity.pojo.CommentEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface YQZdxwService {
    //点击量写入
    int  clicks(@Param("id")int id);
    ////转载量写入
    int  reload(@Param("id")int id);

    //查询点击量最高新闻
    List<CommentEntity> zuigaodianji();
    //查询转载量最高新闻
    List<CommentEntity> zuigaozhuanzai();
    //查询评论最高新闻
    List<CommentEntity> zuigaopinglun();

    List<ZdxwEntity> zdAll();
    ZdxwEntity commentAll();
}
