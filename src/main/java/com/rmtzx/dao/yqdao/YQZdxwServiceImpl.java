package com.rmtzx.dao.yqdao;

import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import com.rmtzx.entity.pojo.CommentEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YQZdxwServiceImpl implements YQZdxwService {
@Autowired
private YQZdxwMapper zdxwMapper;
    //点击量写入
    @Override
    public int clicks(int id) {
        int i = zdxwMapper.selectClicks(id);
        int clicks = i + 1;
        return zdxwMapper.clicks(id, clicks);
    }
    //转载量
    @Override
    public int reload(int id) {
        int i = zdxwMapper.selectReload(id);
        int reload =i+1;
        return zdxwMapper.reload(id, reload);
    }
//最高点击率评论
    @Override
    public List<CommentEntity> zuigaodianji() {
        int[] ints = zdxwMapper.zuigaodianji();
        for (int i = 0; i < ints.length; i++) {
            int anInt = ints[i];
            List<CommentEntity> commentEntities = zdxwMapper.zdComment(anInt);
            if (commentEntities!=null){
                return commentEntities;
            }
        }
        return zdxwMapper.zdComment(ints[1]);
    }
    @Override
    public List<CommentEntity> zuigaopinglun() {
        int commentID = zdxwMapper.zuigaopinglun();
        return zdxwMapper.zdComment(commentID);
    }
//最高转载评论
    @Override
    public List<CommentEntity> zuigaozhuanzai() {
        int commentID = zdxwMapper.zuigaozhuanzai();
        return zdxwMapper.zdComment(commentID);
    }



    @Override
    public List<ZdxwEntity> zdAll() {
        return zdxwMapper.zdAll();
    }

    @Override
    public ZdxwEntity commentAll() {
        List<ZdxwEntity> zdxwEntities = zdxwMapper.commentAll();
        ZdxwEntity zdxwEntity=new ZdxwEntity();
        int i1=0;
        int i2=0;
        int i3=0;
        for (ZdxwEntity z :zdxwEntities) {
            i1=z.getClicks()+i1;
            i2=z.getCommentQuantity()+i2;
            i3=z.getReload()+i3;
        }
        zdxwEntity.setCommentQuantity(i2);
        zdxwEntity.setClicks(i1);
        zdxwEntity.setReload(i3);
        return zdxwEntity;
    }

}
