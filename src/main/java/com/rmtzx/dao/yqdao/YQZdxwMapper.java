package com.rmtzx.dao.yqdao;

import com.rmtzx.entity.appentity.ywEntity.ZdxwEntity;
import com.rmtzx.entity.pojo.CommentEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Mapper
public interface YQZdxwMapper {

    //id查询数据
    @Select("select id,source,title,article_source,article,release_time as releaseTime,abs_url as absUrl,differentiate,local_url as localUrl from zd_news_fb where code=1 and id=#{id}")
    ZdxwEntity singleDate(@Param("id") int id);

    //查询点击率
    @Select("SELECT clicks FROM zd_news_fb where id=#{id}")
    int selectClicks(@Param("id") int id);
    //点击量写入
    @Update("update zd_news_fb set clicks=#{clicks} where id=#{id}")
    int  clicks(@Param("id")int id,@Param("clicks")int clicks);
    ////转载量写入
    @Update("update zd_news_fb set reload=#{reload} where id=#{id}")
    int  reload(@Param("id")int id,@Param("reload")int reload);
    //转载次数
    @Select("SELECT reload FROM zd_news_fb where id=#{id}")
    int selectReload(@Param("id") int id);
    //查询点击量最高新闻
    @Select("select id from zd_news_fb  ORDER BY  clicks desc LIMIT 0,10")
    int[] zuigaodianji();
    //查询转载量最高新闻
    @Select("select id from zd_news_fb  ORDER BY  reload desc LIMIT 0,1")
    int zuigaozhuanzai();
    @Select("select id from zd_news_fb  ORDER BY  comment_quantity desc LIMIT 0,1")
    int zuigaopinglun();
    //查询点击量最高新闻评论
    @Select("select id,commentator,comment_content as commentContent,comment_title as commentTitle,commentary_time as commentaryTime" +
            ",user_tx as userTx,userId,comment_category as commentCategory from zzq_comment where comment_ID=#{commentID} ORDER BY  commentary_time desc LIMIT 0,10")
    List<CommentEntity> zdComment(@Param("commentID")int commentID);

    //
    @Select("select title,clicks,reload,comment_quantity as commentQuantity " +
            "from zd_news_fb ORDER BY  clicks desc LIMIT 0,10")
    List<ZdxwEntity> zdAll();
    //总量查询 点击 转载 评论
    @Select("select clicks,reload,comment_quantity as commentQuantity " +
            "from zd_news_fb ORDER BY  clicks desc LIMIT 0,10")
    List<ZdxwEntity> commentAll();



}
