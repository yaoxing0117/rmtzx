package com.rmtzx.dao.yqdao;

import com.rmtzx.entity.Tweets;
import com.rmtzx.entity.appentity.NYEntity;
import com.rmtzx.entity.yqfxentity.EnergyEntity;
import com.rmtzx.entity.yqfxentity.StatisticsEntity;
import com.rmtzx.entity.yqfxentity.ThesaurusEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface YqfxMapper {
    //zzq_comment评论表
    //准东
    //thesaurus词库
    //微博查询
    //插入
//    @Insert("INSERT INTO tweets_fb ( created_at, like_num, repost_num," +
//            "comment_num,content,user_id,tool, image_url,video_url," +
//            "location, origin_weibo,crawled_at,weibo_url) VALUES " +
//            "('${createdAt}',${likeNum},${repostNum},${commentNum},'${content}'," +
//            "'${userId}','${tool}','${imageUrl}','${videoUrl}','${location}','${originWeibo}'" +
//            ",'${crawledAt}','${weiboUrl}')")
//    int tweetsInsert(@Param("createdAt")String createdAt,
//                     @Param("likeNum")Integer likeNum,
//                     @Param("repostNum")Integer repostNum,
//                     @Param("commentNum")Integer commentNum,
//                     @Param("content")String content,
//                     @Param("userId")String userId,
//                     @Param("tool")String tool,
//                     @Param("imageUrl")String imageUrl,
//                     @Param("videoUrl")String videoUrl,
//                     @Param("location")String location,
//                     @Param("originWeibo")String originWeibo,
//                     @Param("crawledAt")String crawledAt,
//                     @Param("weiboUrl")String weiboUrl);
//    @Select("select * from tweets_fb where")
//    List<Tweets> allTweets();
    //微信总条数
    @Select("select COUNT(id) FROM weixin_gzh")
    int wxPaperPageCount();

    @Select("SELECT count(article) FROM weixin_gzh WHERE article LIKE '%${thesauruOne}%'")
    int wxallSelect(@Param("thesauruOne") String thesauruOne);

    //自治区总条数
    @Select("select COUNT(id) FROM zzq_news")
    int zzqPaperPageCount();

    @Select("SELECT count(article) FROM zzq_news WHERE article LIKE '%${thesauruOne}%'")
    int zzqallSelect(@Param("thesauruOne") String thesauruOne);

    //中央总条数
    @Select("select COUNT(id) FROM zy_news")
    int zyPaperPageCount();

    @Select("SELECT count(article) FROM zy_news WHERE article LIKE '%${thesauruOne}%'")
    int zyallSelect(@Param("thesauruOne") String thesauruOne);

    //州总条数
    @Select("select COUNT(id) FROM z_news")
    int zPaperPageCount();

    @Select("SELECT count(article) FROM z_news WHERE article LIKE '%${thesauruOne}%'")
    int zallSelect(@Param("thesauruOne") String thesauruOne);

    //报纸总条数
    @Select("select COUNT(id) FROM newspaper")
    int bzPaperPageCount();

    @Select("SELECT count(news_article) FROM newspaper WHERE news_article LIKE '%${thesauruOne}%'")
    int bzallSelect(@Param("thesauruOne") String thesauruOne);

    //微博总数
    @Select("select wb_number as wbNumber FROM statistics ORDER BY id desc LIMIT 0,1")
    int wbAll();

    //其他总条数
    @Select("select COUNT(id) FROM others_news")
    int qtPaperPageCount();

    @Select("SELECT count(article) FROM others_news WHERE article LIKE '%${thesauruOne}%'")
    int qtallSelect(@Param("thesauruOne") String thesauruOne);

    //准东总条数
    @Select("select COUNT(id) FROM weixin_gzh where source='准东开发区零距离' ")
    int zdPaperPageCount();

    @Select("SELECT count(article) FROM weixin_gzh WHERE article LIKE '%${thesauruOne}%' where source='准东开发区零距离' ")
    int zdallSelect(@Param("thesauruOne") String thesauruOne);

    //能源总条数
    @Select("select COUNT(id) FROM nengyuan")
    int nyPaperPageCount();

    @Select("SELECT count(article) FROM nengyuan WHERE article LIKE '%${thesauruOne}%'")
    int nyallSelect(@Param("thesauruOne") String thesauruOne);

    //统计表
    @Insert("INSERT INTO statistics (wx_number, ny_number,qt_number, bz_number, z_number, zy_number, zzq_number," +
            " zong_number,order_number,dang_number,zd_number,wz_number,time) VALUES (${wxNumber},${nyNumber}, ${qtNumber}, " +
            "${bzNumber},${zzqNumber},${zyNumber},${zNumber},${zongNumber},${orderNumber},${dangNumber},${zdNumber},${wzNumber},'${time}')")
    int allPaperPageCount(@Param("wxNumber") int wxNumber,
                          @Param("nyNumber") int nyNumber,
                          @Param("qtNumber") int qtNumber,
                          @Param("bzNumber") int bzNumber,
                          @Param("zzqNumber") int zzqNumber,
                          @Param("zyNumber") int zyNumber,
                          @Param("zNumber") int zNumber,
                          @Param("zongNumber") int zongNumber,
                          @Param("orderNumber") int orderNumber,
                          @Param("dangNumber") int dangNumber,
                          @Param("zdNumber") int zdNumber,
                          @Param("wzNumber") int wzNumber,
                          @Param("time") String time
    );

    //查询所有热词
    @Select("SELECT thesauru_one AS thesauruOne FROM thesaurus")
    List<ThesaurusEntity> allThe();

    //热词排行
    @Select("SELECT thesauru_one as thesauruOne,adding_time as addingTime,heat " +
            ",adding_name as addingName FROM thesaurus ORDER BY heat desc LIMIT 0,20 ")
    List<ThesaurusEntity> allSelects();

    //写入热度次数
    @Update("UPDATE thesaurus SET heat=#{heat} where thesauru_one=#{thesauruOne}")
    int addSelect(@Param("heat") int heat, @Param("thesauruOne") String thesauruOne);

    //查询当前排序最高
    @Select("SELECT order_number as orderNumber FROM statistics ORDER BY order_number desc LIMIT 0,1")
    int allSelect();

    //查询历史数据与今日
    @Select("SELECT zong_number as zongNumber FROM statistics ORDER BY order_number desc LIMIT 0,2")
    List<StatisticsEntity> allls();

    @Select("SELECT z_number as zNumber,zy_number as zyNumber,zzq_number as zzqNumber,wz_number as wzNumber," +
            "qt_number as qtNumber,wx_number as wxNumber,ny_number as nyNumber,wb_number as wbNumber," +
            "zong_number as zongNumber,dang_number as dangNumber,time,qt_number as qtNumber,bz_number as bzNumber" +
            ",zd_number as zdNumber FROM statistics ORDER BY order_number desc LIMIT 0,1")
    List<StatisticsEntity> allStatistics();

    //昨日数据
    @Select("SELECT z_number as zNumber,zy_number as zyNumber,zzq_number as zzqNumber,wz_number as wzNumber," +
            "qt_number as qtNumber,wx_number as wxNumber,ny_number as nyNumber,wb_number as wbNumber," +
            "zong_number as zongNumber,dang_number as dangNumber,time,qt_number as qtNumber,bz_number as bzNumber" +
            ",zd_number as zdNumber FROM statistics  WHERE time LIKE '%${time}%' ORDER BY order_number desc LIMIT 0,1")
    StatisticsEntity allStatisticszuo(@Param("time") String time);

    //-------------------------------------------------------------------



    @Select("select category, mt_energy as mtEnergy,ny_energy as nyEnergy,sy_energy as syEnergy," +
            "dl_energy as dlEnergy,trq_energy as trqEnergy, zong_energy as zongEnergy from energy_tj where id=${id}")
    EnergyEntity nyAll(@Param("id") int id);

    //写入当天能源
    @Update("UPDATE energy_tj SET mt_energy=${mtEnergy}, ny_energy=${nyEnergy}, sy_energy=${syEnergy}," +
            "dl_energy=${dlEnergy}, trq_energy=${trqEnergy}, zong_energy=${zongEnergy} WHERE (id=${id})")
    int tjAll(@Param("id") int id,
              @Param("mtEnergy") int mtEnergy,
              @Param("nyEnergy") int nyEnergy,
              @Param("syEnergy") int syEnergy,
              @Param("dlEnergy") int dlEnergy,
              @Param("trqEnergy") int trqEnergy,
              @Param("zongEnergy") int zongEnergy);

    //清除本周统计
    @Update("UPDATE energy_tj SET mt_energy=${mtEnergy}, ny_energy=${nyEnergy}, sy_energy=${syEnergy}, dl_energy=${dlEnergy}," +
            "trq_energy=${trqEnergy},zong_energy=${zongEnergy} WHERE (id=${id})")
    int zongUpdate(@Param("id") int id,
                   @Param("mtEnergy") int mtEnergy,
                   @Param("nyEnergy") int nyEnergy,
                   @Param("syEnergy") int syEnergy,
                   @Param("dlEnergy") int dlEnergy,
                   @Param("trqEnergy") int trqEnergy,
                   @Param("zongEnergy") int zongEnergy);

    //查询当前周id
    @Select("select id from energy_tj LIMIT 0,1")
    int bzAll();

    //根据类别查询能源表
    @Select("select COUNT(news_column) FROM nengyuan where news_column='${newsColumn}'")
    int nybOne(@Param("newsColumn") String newsColumn);
    //返回数据
}
