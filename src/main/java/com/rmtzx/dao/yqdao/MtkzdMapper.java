package com.rmtzx.dao.yqdao;

import com.rmtzx.entity.appentity.ywEntity.NewsPaperEntity;
import com.rmtzx.entity.appentity.ywEntity.ZxwEntity;
import com.rmtzx.entity.appentity.ywEntity.ZyxwEntity;
import com.rmtzx.entity.appentity.ywEntity.ZzqxwEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 媒体看准东数据统计
 */
public interface MtkzdMapper {
    //自治区表模糊查询
    @Select("select * from zzq_news where article like CONCAT('%',${article},'%')")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "article", column = "article"),
            @Result(property = "articleSource", column = "article_source"),
            @Result(property = "author", column = "author"),
            @Result(property = "code", column = "code"),
            @Result(property = "editor", column = "editor"),
            @Result(property = "newsColumn", column = "news_column"),
            @Result(property = "pictureUrl", column = "picture_url"),
            @Result(property = "releaseTime", column = "release_Time"),
            @Result(property = "title", column = "title"),
            @Result(property = "localUrl", column = "local_url"),
            @Result(property = "category", column = "category"),
            @Result(property = "absUrl", column = "abs_url"),
            @Result(property = "differentiate", column = "differentiate")
    })
    List<ZzqxwEntity> selectAllZzq(@Param("article") String article);

    //中央表模糊查询
    @Select("select * from zy_news where article like CONCAT('%',${article},'%')")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "article", column = "article"),
            @Result(property = "article_source", column = "article_source"),
            @Result(property = "author", column = "author"),
            @Result(property = "code", column = "code"),
            @Result(property = "editor", column = "editor"),
            @Result(property = "news_column", column = "news_column"),
            @Result(property = "pictureUrl", column = "picture_url"),
            @Result(property = "releaseTime", column = "release_Time"),
            @Result(property = "title", column = "title"),
            @Result(property = "localUrl", column = "local_url"),
            @Result(property = "category", column = "category"),
            @Result(property = "absUrl", column = "abs_url"),
            @Result(property = "differentiate", column = "differentiate")
    })
    List<ZyxwEntity> selectAllZy(@Param("article") String article);

    //州表模糊查询
    @Select("select * from z_news where article like CONCAT('%',${article},'%')")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "article", column = "article"),
            @Result(property = "article_source", column = "article_source"),
            @Result(property = "author", column = "author"),
            @Result(property = "code", column = "code"),
            @Result(property = "editor", column = "editor"),
            @Result(property = "news_column", column = "news_column"),
            @Result(property = "pictureUrl", column = "picture_url"),
            @Result(property = "releaseTime", column = "release_Time"),
            @Result(property = "title", column = "title"),
            @Result(property = "localUrl", column = "local_url"),
            @Result(property = "category", column = "category"),
            @Result(property = "absUrl", column = "abs_url"),
            @Result(property = "differentiate", column = "differentiate")
    })
    List<ZxwEntity> selectAllZ(@Param("article") String article);
    //报纸表模糊查询
    @Select("select * from newspaper where news_article like CONCAT('%',${article},'%') ")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "titleH1", column = "title_h1"),
            @Result(property = "titleH2", column = "title_h2"),
            @Result(property = "titleH3", column = "title_h3"),
            @Result(property = "titleH4", column = "title_h4"),
            @Result(property = "editor", column = "editor"),
            @Result(property = "newsArticle", column = "news_article"),
            @Result(property = "pictureUrl", column = "picture_url"),
            @Result(property = "releaseTime", column = "release_Time"),
            @Result(property = "source", column = "source"),
            @Result(property = "localUrl", column = "local_url"),
            @Result(property = "category", column = "category"),
            @Result(property = "absUrl", column = "abs_url"),
            @Result(property = "d_z", column = "d_z"),
            @Result(property = "differentiate", column = "differentiate")
    })
    List<NewsPaperEntity> selectAllNews(@Param("article") String article);
    // INSERT INTO yq_news_fb(id,article,article_source,author,code,editor,
    // news_column,picture_url,release_time,title,news_click,local_url,abs_url,
    // category,differentiate,news_comment,news_reprinted) VALUES
    //(#{id},#{article},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{},#{})
    // @Insert("")
    // int insertYQ(@Param(""));
}
