package com.rmtzx.dao.webdao;


import com.rmtzx.entity.webentity.HeadLineEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface HeadLineMapper {
    //头条返回
    @Select("select title,article,category,release_time as releaseTime,source,differentiate" +
            " from web_announcement where code=1 ORDER BY id desc LIMIT 0,3 ")
    List<HeadLineEntity> oneHeadLine();
}
