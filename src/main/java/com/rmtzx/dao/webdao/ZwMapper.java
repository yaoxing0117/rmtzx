package com.rmtzx.dao.webdao;



import com.rmtzx.entity.pojo.ClassTableEntity;
import com.rmtzx.entity.webentity.Announcement;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;


/**
 * web政务
 */
@Mapper
public interface ZwMapper {
    //类别查询
    @Select("select id,large_category as largeCategory,small_category as smallCategory ,pid from class_table")
    List<ClassTableEntity> allClass();
    //大分类
    @Select("select id,large_category as largeCategory,pid from class_table where pid=0")
    List<ClassTableEntity> daAllClass();
    //小分类
    @Select("select id,large_category as largeCategory,pid from class_table where pid>0")
    List<ClassTableEntity> xiaoAllClass();
    //政务列表展示
    @Select("select id,article,editor,picture,release_time as releaseTime,title,category,differentiate " +
            "from web_announcement where code=1 and cid=#{cid}" +
            " ORDER BY release_time DESC LIMIT #{pageNo},#{pageSize} ")
    List<Announcement> allList(@Param("cid")int cid,
                               @Param("pageNo")int pageNo,
                               @Param("pageSize")int pageSize );
    //政务详情展示
    @Select("select id,article,editor,picture,release_time as releaseTime,title,category, " +
            "pre_url as preUrl,differentiate,local_url as localUrl from web_announcement where code=1 and id=#{id}")
    Announcement details( @Param("id")int id);
    //表总条数查询
    @Select("SELECT COUNT(*) FROM web_announcement where cid=#{cid}")
    int zwPaperPageCount(@Param("cid")int cid);
}
