package com.rmtzx.dao.webdao;

import com.rmtzx.entity.webentity.UserInteractionEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface UserInteractionMapper {
    //提交信件
    @Insert("INSERT INTO web_user ( user_name, user_Dq, user_tel," +
            " user_tjtime, user_nr, user_zt, user_xjlb)" +
            " VALUES ( #{userName}, #{userDq}, #{userTel}, #{userTjtime}, " +
            "#{userNr},#{userZt},#{userXjlb})")
    int insertInteraction(@Param("userName")String userName,
                          @Param("userDq")String userDq,
                          @Param("userTel")String userTel,
                          @Param("userTjtime")String userTjtime,
                          @Param("userNr")String userNr,
                          @Param("userZt")String userZt,
                          @Param("userXjlb")String userXjlb
                          );
    //展示所有信件
    @Select("SELECT id,xj_id as XJId,user_zt as userZt,code,user_lb as userLb," +
            "xj_rd as XJRd,admin_hfbm as adminHfbm,admin_hftime as adminHftime," +
            "admin_hfnr as adminHfnr,state,user_xjlb as userXjlb" +
            " FROM `web_user` where user_xjlb=#{userXjlb}  LIMIT #{pageNo},#{pageSize}")
    List<UserInteractionEntity> allUser1( @Param("userXjlb")String userXjlb,
                                          @Param("pageNo")int pageNo,
                                          @Param("pageSize")int pageSize);
    @Select("Select id,xj_id as XJId,user_zt as userZt,code,user_lb as userLb," +
            "            xj_rd as XJRd,admin_hfbm as adminHfbm,admin_hftime as adminHftime,admin_hfnr as adminHfnr,state" +
            "           ,user_xjlb as userXjlb FROM `web_user`  LIMIT #{pageNo},#{pageSize}")
    List<UserInteractionEntity> allUserI(@Param("pageNo")int pageNo,
                                         @Param("pageSize")int pageSize);
    //查询总数据量
    @Select("SELECT COUNT(*) FROM web_user ")
    int paperPageCount();
    //查询信件(三选项搜索)
    @Select("Select id,xj_id as XJId,user_zt as userZt,code,user_lb as userLb," +
            "xj_rd as XJRd,admin_hfbm as adminHfbm,admin_hftime as adminHftime,admin_hfnr as adminHfnr,state" +
            ",user_xjlb as userXjlb FROM web_user Where  user_name LIKE  CONCAT('%',#{userName},'%') or user_xjlb LIKE " +
            " CONCAT('%',#{userXjlb},'%') " +
            " or user_zt LIKE CONCAT('%',#{userZt},'%') ORDER BY user_tjtime desc  LIMIT #{pageNo},#{pageSize}")
    List<UserInteractionEntity> allSelect3( @Param("userName")String userName,
                                           @Param("userXjlb")String userXjlb,
                                           @Param("userZt")String userZt,
                                           @Param("pageNo")int pageNo,
                                           @Param("pageSize")int pageSize);
    @Select("Select id,xj_id as XJId,user_zt as userZt,code,user_lb as userLb," +
            "xj_rd as XJRd,admin_hfbm as adminHfbm,admin_hftime as adminHftime,admin_hfnr as adminHfnr,state" +
            ",user_xjlb as userXjlb FROM web_user Where user_name LIKE  CONCAT('%',#{userName},'%') " +
            " or user_zt LIKE CONCAT('%',#{userZt},'%') ORDER BY user_tjtime desc  LIMIT #{pageNo},#{pageSize}")
    List<UserInteractionEntity> allSelect( @Param("userName")String userName,
                                           @Param("userZt")String userZt,
                                           @Param("pageNo")int pageNo,
                                           @Param("pageSize")int pageSize);
    //查看详情信件
    @Select("SELECT user_name as userName,user_tjtime as userTjtime,admin_hfbm as adminHfbm ," +
            "admin_hftime as adminHftime,admin_hfnr as adminHfnr,user_nr as userNr,user_zt as userZt,user_xjlb as userXjlb FROM web_user where id=#{id}")
    UserInteractionEntity oneUser(@Param("id") int id );
    //查询浏览次数
    @Select("Select xj_rd FROM web_user where id=#{id}")
    int rdUser(@Param("id")int id);
    //增加浏览次数
    @Update("UPDATE web_user SET  xj_rd=#{XJRd} WHERE (id=#{id})")
    int zjUser(@Param("id")int id,
               @Param("XJRd")int XJRd);
}
