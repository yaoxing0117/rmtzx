package com.rmtzx.dao.webdao;

import com.rmtzx.entity.webentity.ZdtsEntity;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;



import java.util.List;

@Mapper
public interface ZdtsMapper {
    //准东图说+微视频列表展示
    @Select("SELECT id,source,title,article_source as articleSource,release_time as releaseTime, " +
            "picture_url as pictureUrl,category,abs_url as absUrl FROM viewzd_fb where code=1 and category=#{category} " +
            "ORDER BY release_time desc " +
            " LIMIT #{pageNo},#{pageSize}")
    List<ZdtsEntity> allZdts(@Param("category")String category,
                             @Param("pageNo")int pageNo,
                             @Param("pageSize")int pageSize);
    //准东图说+微视频列表展示
    @Select("SELECT id,source,title,article_source as articleSource,release_time as releaseTime, " +
            "picture_url as pictureUrl,category,abs_url as absUrl,article FROM viewzd_fb where code=1  " +
            "and category='图片' or category='视频' ORDER BY release_time desc " +
            " LIMIT #{pageNo},#{pageSize}")
    List<ZdtsEntity> allZdtss(@Param("pageNo")int pageNo,
                              @Param("pageSize")int pageSize);
    //准东图说+微视频展示详情
    @Select("select id,source,title,article_source,article,abs_url as absUrl," +
            "release_time as releaseTime,picture_url as pictureUrl,abs_url as absUrl,category,local_url as localUrl " +
            "from viewzd_fb where code=1 and id=#{id}")
    ZdtsEntity oneZdts(@Param("id")int id);
    //图说准东+微视频总条数
    @Select("SELECT COUNT(*) FROM viewzd_fb where category=#{category} ")
    int paperPageCount(@Param("category")String category);
    //图说准东+微视频总条数
    @Select("SELECT COUNT(*) FROM viewzd_fb  ")
    int paperPageCounts();
}
