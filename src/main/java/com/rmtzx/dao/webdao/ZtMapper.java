package com.rmtzx.dao.webdao;

import com.rmtzx.entity.webentity.ZtEntity;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ZtMapper {
    //列表(id 预览图 详情接口)
    @Select("SELECT id,picture,category,differentiate FROM `web_announcement` where category=#{category} and code=1 LIMIT #{pageNo},#{pageSize}")
    List<ZtEntity> allZt(@Param("category")String category,
                         @Param("pageNo")int pageNo,
                         @Param("pageSize")int pageSize);
    //详情()
    @Select("SELECT * FROM `web_announcement` where id=#{id} ")
    ZtEntity oneZt(@Param("id")int id);
    //查询当前专题
    @Select("SELECT COUNT(*) FROM web_announcement where category=#{category} ")
    int paperPageCounts(@Param("category")String category);
}
