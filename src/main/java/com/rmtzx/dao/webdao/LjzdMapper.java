package com.rmtzx.dao.webdao;

import com.rmtzx.entity.webentity.LjzdEntity;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface LjzdMapper {
    //了解列表返回
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture,category,differentiate " +
            "FROM ljzd_detail_fb  LIMIT 0,7 ")
    List<LjzdEntity> allLjzd();
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture,category,differentiate,local_url as localUrl " +
            "FROM ljzd_detail_fb where id=#{id}")
    LjzdEntity oneLjzd3(@Param("id")int id);
    //产业列表返回
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture,category,differentiate FROM `ljzd_detail_fb` " +
            " LIMIT 7,17 ")
    List<LjzdEntity> allCycj();
    //据类别返回详情
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture,differentiate,local_url as localUrl FROM `ljzd_detail_fb` " +
            " where category=#{category}")
    LjzdEntity oneLjzd(@Param("category")String category);
    //投资列表返回("产业促进")
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture,category FROM `cycj_detail_fb` " +
            " where category=#{category}  ORDER BY cj_time desc LIMIT #{pageNo},#{pageSize} ")
    List<LjzdEntity> allTZ(@Param("category")String category,
                           @Param("pageNo")int pageNo,
                           @Param("pageSize")int pageSize);
    //据类别返回详情
    @Select("SELECT id,title,article,cj_time as cjTime,editor,author,picture FROM `cycj_detail_fb` " +
            " where id=#{id}")
    LjzdEntity oneTZ(@Param("id")int id);
    //投资人信息
    @Insert("INSERT INTO investment_information(investment_user,investment_tel," +
            "investment_mail,investment_wx,investment_bz)" +
            "VALUES(#{investmentUser},#{investmentTel}," +
            "#{investmentMail},#{investmentWx},#{investmentBz})")
    int investment(@Param("investmentUser")String investmentUser,
                   @Param("investmentTel")String investmentTel,
                   @Param("investmentMail")String investmentMail,
                   @Param("investmentWx")String investmentWx,
                   @Param("investmentBz")String investmentBz);

    //表总条数查询
    @Select("SELECT COUNT(*) FROM cycj_detail_fb where category=#{category} ")
    int paperPageCount(@Param("category")String category);
}
