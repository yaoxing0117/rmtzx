package com.rmtzx.yw.utilTest;


import com.rmtzx.dao.appdao.WXPayMapper;
import com.rmtzx.entity.appentity.WXGZHEntity;
import com.rmtzx.entity.bo.WXPayEntity;
import com.rmtzx.entity.store.StoreDishEntity;
import com.rmtzx.service.storeservice.StoreDishService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RandomTest {
    @Autowired
    private StoreDishService storeDishService;
    @Autowired
    private WXPayMapper wxPayMapper;

//
//    @Autowired
//    private WXGZHMapper yqfxQuartz;
    @Test
    public void contextLoads() {
     Date today = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 24);
     Date today1 = new Date(System.currentTimeMillis() - 1000 * 60 * 60 * 48);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleDateFormat.format(today);//获取昨天日期
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
        String time1 = simpleDateFormat.format(today1);//获取昨天日期
        System.out.println(time+"-------------"+time1);
    } @Test
    public void dishTest() {
        WXPayEntity wx0000021 = wxPayMapper.selectWX("WX0000021");
        System.out.println(wx0000021);

    }

}
